//
//  main.swift
//  Déplacement Péninsule
//
//  Created by RK on 25/06/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation
import UIKit

UIApplicationMain(Process.argc, Process.unsafeArgv,NSStringFromClass(Application), NSStringFromClass(AppDelegate))