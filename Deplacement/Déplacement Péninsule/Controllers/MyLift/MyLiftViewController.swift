//
//  MyLiftViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class MyLiftViewController: UIViewController {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    // activity indicator
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            if (self.revealViewController().rightViewController != nil) {
                self.revealViewController().rightViewController = nil
            }
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        //        navigationItem.title = defLang == "fr" ? R.fr.row3 : R.en.row3
        navigationItem.title = "My Lift"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // change bar button item tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    func initView() {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
