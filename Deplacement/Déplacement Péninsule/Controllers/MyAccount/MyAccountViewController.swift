//
//  MyAccountViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/26/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class MyAccountViewController: UIViewController {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var public_profile = 0
    
    var _user: UserInfo!
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var lblOfflineID: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    @IBOutlet weak var lblTitleOccupation: UILabel!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblTitleLanguage: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblTitleSmoker: UILabel!
    @IBOutlet weak var lblSmoker: UILabel!
    @IBOutlet weak var lblTitleShare: UILabel!
    var facebook_url = ""
    var twitter_url = ""
    
    // Activity 
    @IBOutlet weak var lblTitleActivity: UILabel!
    @IBOutlet weak var lblLiftOffered: UILabel!
    @IBOutlet weak var lblTotalReviews: UILabel!
    
    @IBOutlet weak var lblTitleSuspendAccount: UILabel!
    @IBOutlet weak var txvSuspendAccount: UITextView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {

            let accountStoryboard: UIStoryboard = UIStoryboard(name: "Account_RightSide", bundle:  nil)
            let rightMenuViewController = accountStoryboard.instantiateViewControllerWithIdentifier("AccountRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.revealViewController().panGestureRecognizer()
            self.revealViewController().tapGestureRecognizer()
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.row3 : R.en.row3
        
        imvProfile.layer.borderColor = UIColor(red: 226/255.0, green: 226/255, blue: 226/255.0, alpha: 1.0).CGColor
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {    
        
        lblOfflineID.text = defLang == "fr" ? R.fr.offline_id : R.en.offline_id
        lblTitleOccupation.text = "Occupation"
        lblTitleLanguage.text = defLang == "fr" ? R.fr.languages : R.en.languages
        lblTitleSmoker.text = defLang == "fr" ? R.fr.smoker : R.en.smoker
        lblTitleShare.text = defLang == "fr" ? R.fr.share : R.en.share
        
        lblTitleActivity.text = defLang == "fr" ? R.fr.activity : R.en.activity
        lblLiftOffered.text = defLang == "fr" ? R.fr.lift_offered : R.en.lift_offered
        lblTotalReviews.text = defLang == "fr" ? R.fr.total_review : R.en.total_review
        
        lblTitleSuspendAccount.text = defLang == "fr" ? R.fr.suspend_account : R.en.suspend_account
        txvSuspendAccount.text = defLang == "fr" ? R.fr.suspend_account_description : R.en.suspend_account_description
        
        imvProfile.setImageWithUrl(NSURL(string: _user.user_pic)!, placeHolderImage: UIImage(named: _user.gender == 1 ? "male_profile" : "female_profile"))
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        getProfile()
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    
    @IBAction func gotoSetting(sender: AnyObject) {
        
        let deststoryboard = UIStoryboard(name: "Settings", bundle:  nil)
        let destController = deststoryboard.instantiateViewControllerWithIdentifier("PersonalInfo") as! UINavigationController
        
        if self.revealViewController() != nil {
         
            self.revealViewController().pushFrontViewController(destController, animated: true)
        }
    
    }
    
    // --------------------------------------------------
    // MARK: get profile
    // --------------------------------------------------
    
    func getProfile() {

        public_profile = prefs.integerForKey("PUBLIC_PROFILE")
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let params = [
            "public_profile": "\(public_profile)",
            "user_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        
        debugPrint(params)
        
        Alamofire.request(.GET, kBaseURL+"users/profile", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
//                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/profile")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
     
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        // profile image
                        
                        if let _ = jsonResponse["user_data"]["user_data"]["profile_pic"].string {
                            self._user.user_pic = jsonResponse["user_data"]["user_data"]["profile_pic"].string!
                        }
                        
                        // address
                        self._user.firstName = jsonResponse["user_data"]["user_data"]["first_name"].string!
                        self._user.lastName = jsonResponse["user_data"]["user_data"]["last_name"].string!
                        
                        self._user.country = self.defLang == "fr" ? R.fr.country_name : R.en.country_name
                        self._user.province = self.defLang == "fr" ? R.fr.province_name : R.en.province_name
                        
//                        self._user.country = jsonResponse["user_data"]["user_data"]["country"].string! == "0" ? "" : ", " + jsonResponse["user_data"]["user_data"]["country"].string!
//                        self._user.province = jsonResponse["user_data"]["user_data"]["province"].string! == "0" ? "" : ", " + jsonResponse["user_data"]["user_data"]["province"].string!
//                        
                        if let _city = jsonResponse["user_data"]["user_data"]["city"].string {
                            self._user.city = _city
                        }
                        
                        // rating, offlineID
                        
                        if let _ = jsonResponse["user_data"]["user_data"]["avg_vote"].string {
                            
                            self._user.rating = Float(jsonResponse["user_data"]["user_data"]["avg_vote"].string!)!
                        }
                        
                        if let  _ = jsonResponse["user_data"]["user_data"]["phone"].string {
                            
                            self._user.phoneNumber = jsonResponse["user_data"]["user_data"]["phone"].string!
                        }
                        
                        if let _occupation = jsonResponse["user_data"]["user_data"]["occupation"].string {
                            
                            self._user.occupation = _occupation
                        }
                        
//                        // language
//                        if let language = jsonResponse["user_data"]["user_data"]["language"].array {
//
//                            if(language.count > 0) {
//                                
//                                self._user.language.removeAll()
//                                for var index = 0; index < language.count; index++ {
//                                    self._user.language.append(jsonResponse["user_data"]["user_data"]["language"][index].string!)
//                                }
//                            }
//                        }
                        
                        // language
                        if let language = jsonResponse["user_data"]["user_data"]["language"].array {
                            if(language.count > 0) {
                                for var index = 0; index < language.count; index++ {
                                    
                                    let user_language = jsonResponse["user_data"]["user_data"]["language"][index].string!
                                    
                                    if(user_language == "english" || user_language == "English") {
                                        self._user.language[0] = self.defLang == "fr" ? R.fr.english : R.en.english
                                    } else if(user_language == "French" || user_language == "french") {
                                        self._user.language[1] = self.defLang == "fr" ? R.fr.french : R.en.french
                                    }
                                }
                            }
                        }
                        
                        if let _ = jsonResponse["user_data"]["user_data"]["smoker"].string {
                            
                            self._user.smoker = Int(jsonResponse["user_data"]["user_data"]["smoker"].string!)!
                        }
                        
                        if let _ = jsonResponse["user_data"]["user_data"]["facebook_url"].string {
                            
                            self._user.facebook_url = jsonResponse["user_data"]["user_data"]["facebook_url"].string!
                        
//                            self._user.facebook_url = jsonResponse["user_data"]["user_data"]["facebook_url"] == nil ? "" : jsonResponse["user_data"]["user_data"]["facebook_url"].string!
                        }
                        
                        if let _ = jsonResponse["user_data"]["user_data"]["twitter_url"].string {
                        
//                            self._user.twitter_url = jsonResponse["user_data"]["user_data"]["twitter_url"] == nil ? "" : jsonResponse["user_data"]["user_data"]["twitter_url"].string!
                         
                            self._user.twitter_url = jsonResponse["user_data"]["user_data"]["twitter_url"].string!
                        }
                        
                        if let _ = jsonResponse["user_data"]["user_data"]["vote_count"].string {
                        
                            self._user.totalReviews = Int(jsonResponse["user_data"]["user_data"]["vote_count"].string!)!
                        }
                        
                        if(jsonResponse["user_data"]["ride_offered"] != nil) {
                        
                            self._user.liftOffered = jsonResponse["user_data"]["ride_offered"].count
                        }
                        
                        self.checkLicenseVerified()
                        
                    } else {
                        print("Error parsing /users/profile")
                        self.removeActivityView()
                    }
                } else {
                    
                    self.removeActivityView()
                }
                
        })
    }
    
    func checkLicenseVerified() {
        
        let params = [
            "hash_key": prefs.stringForKey("USER_HASH")!
        ]
        
        debugPrint(params)
        
        Alamofire.request(.GET, kBaseURL+"users/lic_verify", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.loadDetails()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/lic_verify")
                    print(response.result.error)
                    JLToast.makeText("Please send valid data", duration: R.tDuration).show()
                    self.loadDetails()
                    return
                }
                
                let jsonResponse = JSON(value)
                
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].int {
                    
                    if(status == 0) {
                        self._user.offlineID = self.defLang == "fr" ? R.fr.id_none_verified : R.en.id_none_verified
                    } else {
                        self._user.offlineID = self.defLang == "fr" ? R.fr.id_verified : R.en.id_verified
                    }
                    
                    self.loadDetails()
                    
                } else {
                    
                    self._user.offlineID = self.defLang == "fr" ? R.fr.id_none_verified : R.en.id_none_verified
                    self.loadDetails()
                }
            })
    }
    
    
    func loadDetails() {
        
        lblName.text = _user.name
        
        if(_user.city == "") {
            
            lblAddress.text = (_user.province != "" ? _user.province : "") + (_user.country != "" ? ". " + _user.country : "")
        } else {
            lblAddress.text = _user.city + (_user.province != "" ? ", " + _user.province : "") + (_user.country != "" ? ". " + _user.country : "")
        }
        
        ratingView.rating = _user.rating
        lblOfflineID.text = ((defLang == "fr" ? R.fr.offline_id : R.en.offline_id) + _user.offlineID)
        lblEmail.text = _user.email
        lblPhoneNumber.text = _user.phoneNumber
        lblOccupation.text = _user.occupation
        
        var language_setting = _user.language[0]
        if(language_setting == "") {
            language_setting = _user.language[1]
        } else if(_user.language[1] != ""){
            language_setting = _user.language[0] + "," + _user.language[1]
        }
        
        lblLanguage.text = language_setting
        
        if(_user.smoker == 0) {
            self.lblSmoker.text = defLang == "fr" ? R.fr.no : R.en.no
        } else {
            self.lblSmoker.text = defLang == "fr" ? R.fr.yes : R.en.yes
        }
        
        facebook_url = _user.facebook_url
        twitter_url = _user.twitter_url
        
        lblLiftOffered.text = ((defLang == "fr" ? R.fr.lifts_offered : R.en.lifts_offered) + "\(_user.liftOffered)")
        lblTotalReviews.text = ((defLang == "fr" ? R.fr.total_reviews : R.en.total_reviews) + "\(_user.totalReviews)")
        
        imvProfile.setImageWithUrl(NSURL(string: _user.user_pic)!, placeHolderImage: UIImage(named: _user.gender == 1 ? "male_profile" : "female_profile"))
    }
}



