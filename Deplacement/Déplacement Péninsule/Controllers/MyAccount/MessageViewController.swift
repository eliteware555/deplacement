//
//  MessageDetail.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class MessageViewController: UIViewController, InputbarDelegate {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    var messageListModel: MessageListModel!
    
    // user info view
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFromTo: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    
    // message
    @IBOutlet weak var _inputbar: Inputbar!
    @IBOutlet weak var tblMessages: UITableView!
    
    @IBOutlet weak var layout_bottom_inputbar: NSLayoutConstraint!
    @IBOutlet weak var layout_height_inputbar: NSLayoutConstraint!
    
    var lastReceivedDate = ""
    
    
    var messageList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        tblMessages.estimatedRowHeight = 65
        tblMessages.rowHeight = UITableViewAutomaticDimension
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        navigationItem.title = defLang == "fr" ? R.fr.profile_message : R.en.profile_message
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initView() {
        
        imvProfile.layer.borderColor = UIColor(red: 226/255.0, green: 226/255, blue: 226/255.0, alpha: 1.0).CGColor
        
        _inputbar.placeholder = "Enter Message"
        _inputbar.rightButtonImage = UIImage(named: "send.png")
        _inputbar.rightButtonText = ""
        _inputbar.leftButtonImage = UIImage(named: "emoj.png")
    
        loadUserDetails()
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    func loadUserDetails() {
        
        imvProfile.setImageWithUrl(NSURL(string: messageListModel.pic_url)!, placeHolderImage: UIImage(named: messageListModel.gender == 1 ? "male_profile" : "female_profile"))
        
        let boldAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 15.0)!]
        let attributed_name_role = NSMutableAttributedString(string: messageListModel.name + " " + messageListModel.role)
        
        print(messageListModel.name.characters.count)
        
        attributed_name_role.addAttributes(boldAttributes, range: NSMakeRange(0, messageListModel.name.characters.count))
        
        lblName.attributedText = attributed_name_role
        lblFromTo.text = messageListModel.from_to
        lblCreatedDate.text = messageListModel.created_date
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        
        getMessageList()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillShowNotification)
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillHideNotification)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
        
        self.layout_bottom_inputbar.constant = keyboardSize!.size.height
        
        UIView.animateWithDuration(((userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue)!, delay: 0, options: .CurveEaseInOut, animations: { () -> Void in
            
            self.view.layoutIfNeeded()
            
            self.tableViewScrollToBottomAnimated(true)
            
            }, completion: nil)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
        
        self.layout_bottom_inputbar.constant -= keyboardSize!.size.height;
        self.layout_height_inputbar.constant = self.view.keyboardTriggerOffset;
        
        UIView.animateWithDuration(((userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue)!, delay: 0, options: .CurveEaseInOut, animations: { () -> Void in
            
            self.view.layoutIfNeeded()
            
            self.tableViewScrollToBottomAnimated(true)
            
            }, completion: nil)
    }
    
    func tableViewScrollToBottomAnimated(animated: Bool) {
        
        if(messageList.count == 0) {
            return
        }
        
        let delay = 0.1*Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, llrint(delay))
        dispatch_after(time, dispatch_get_main_queue(), { () -> Void in
            
            self.tblMessages.scrollToRowAtIndexPath(NSIndexPath(forRow: self.messageList.count-1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Middle, animated: animated)
        })
    }
    
    // -----------------------------------------------------------------------------
    //   Mark: - InputBar Delegate
    // -----------------------------------------------------------------------------
    func inputbarDidPressRightButton(inputbar: Inputbar!) {
        
        let _message = MessageModel()
        
        _message.message = inputbar.text()
        
        inputbar.resignFirstResponder()
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        _message.time = dateFormatter.stringFromDate(date)
        
        _message.setFromMe()
        
        sendMessage(_message)
        
//        addMessage(_message)
    }
    
    func inputbarDidPressLeftButton(inputbar: Inputbar!) {
        
    }
    
    func inputbarDidChangeHeight(new_height: CGFloat) {
        //Update DAKeyboardControl
        self.view.keyboardTriggerOffset = new_height;
        
        self.layout_height_inputbar.constant = new_height;
        
        UIView.animateWithDuration(0.5) { () -> Void in
            self.view.layoutIfNeeded()
            
            self.tableViewScrollToBottomAnimated(true)
        }
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
    }
    
    // -----------------------------------------------------------------------------
    //   Mark: - UITableView Datasource
    // -----------------------------------------------------------------------------
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if(!messageList[indexPath.row].isKindOfClass(MessageModel)) {
            return 30
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(!messageList[indexPath.row].isKindOfClass(MessageModel)) {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("MessageHeader") as! MessageHeader
            cell.setDate(messageList[indexPath.row] as! String)
            
            return cell
            
        } else {
           
            let message = messageList[indexPath.row] as! MessageModel
            
            if(message.fromMe) {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("SendCell") as! SendCell
                cell.setMessage(message)
                
                return cell
                
            } else {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("ReceiveCell") as! ReceiveCell
                cell.setMessage(message)
                
                return cell
            }
        }
    }
    
    func getMessageList() {
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "thread_hash": messageListModel.thread_hash
        ]
        
        print(params)
        
        Alamofire.request(.GET, kBaseURL+"message/detail", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /message/detail")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
//                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let message_cnt = jsonResponse["message_data"]["msg_threads"]["participent_msgs"].array?.count {
                            
                            if(message_cnt > 0) {
                                
                                self.messageList.removeAllObjects()
                                
                                for var index = 0; index < message_cnt; index++ {
                                    
                                    let _message = MessageModel()
                                    
                                    _message.message = jsonResponse["message_data"]["msg_threads"]["participent_msgs"][index]["message"].string!
                                    
                                    _message.time = jsonResponse["message_data"]["msg_threads"]["participent_msgs"][index]["created_date"].string!
                                    
                                    print(_message.time)
                                    
                                    let sender_hash = jsonResponse["message_data"]["msg_threads"]["participent_msgs"][index]["hash_key"].string!
                                    
                                    if(sender_hash == self.prefs.stringForKey("USER_HASH")!) {
                                        _message.setFromMe()
                                    }

                                    self.addMessage(_message)
                                }
                                
                                self.tblMessages.reloadData()
                            }
                        }
                        
                    } else {
                        print("Error parsing /message/detail")
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    }
                }                 
            })
    }
    
    func addMessage(message: MessageModel) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
//        let created_date = NSDate(timeIntervalSince1970: NSTimeInterval(Double(Int(message.time)!)))
        let created_date = dateFormatter.dateFromString(message.time)
        
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        if((messageList.count == 0) || (lastReceivedDate != dateFormatter.stringFromDate(created_date!))) {
            
            lastReceivedDate = dateFormatter.stringFromDate(created_date!)
            messageList.addObject(dateFormatter.stringFromDate(created_date!))
        }
        
        // time change
        dateFormatter.dateFormat = "hh:mm a"
        message.time = dateFormatter.stringFromDate(created_date!)
        messageList.addObject(message)
    }
    
    func sendMessage(message: MessageModel) {
//        addMessage(_message)
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "message": message.message,
            "reply_message": "\(1)",
            "thread_id": "\(messageListModel.thread_id)",
            "subject": "",
            "msg_id": "\(messageListModel.id)",
            "recipients": "\([messageListModel.sender_id])"
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)

//                Alamofire.request(.GET, kBaseURL+"message/inbox", parameters: params)
        
        Alamofire.request(.GET, kBaseURL+"message/inbox", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /message/inbox")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        self.addMessage(message)
                        
                        if(self.defLang == "fr") {
                            JLToast.makeText("Votre message a été envoyé.", duration: R.tDuration).show()
                        } else {
                            JLToast.makeText("Your message has been sent.", duration: R.tDuration).show()
                        }
//                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        
                        self.tblMessages.reloadData()
                        
                    } else {
                        print("Error parsing /message/inbox")
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    }
                }
                
            })
    }
}


