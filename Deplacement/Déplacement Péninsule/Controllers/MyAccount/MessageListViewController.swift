//
//  MessageViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class MessageListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    // activity indicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    var messageList: [MessageListModel] = []
    
    var _user: UserInfo!
    
    var selectedIndexPath: NSIndexPath!
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var tblMessageList: UITableView!
    
    @IBOutlet weak var lblNotice: UILabel!
    @IBOutlet weak var imvIntro: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let accountStoryboard: UIStoryboard = UIStoryboard(name: "Account_RightSide", bundle:  nil)
            let rightMenuViewController = accountStoryboard.instantiateViewControllerWithIdentifier("AccountRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.revealViewController().panGestureRecognizer()
            self.revealViewController().tapGestureRecognizer()
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        //        navigationItem.title = defLang == "fr" ? R.fr.row3 : R.en.row3
        navigationItem.title = "Messages"
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // change bar button item tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        getMessageList()
    }
    
    func initView() {
        
        lblNotice.text = defLang == "fr" ? R.fr.no_message_data : R.en.no_message_data
        
    }
    
    // -----------------------------------------------------------------------------
    //   Mark: - UITableView Datasource
    // -----------------------------------------------------------------------------
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 165
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MessageListCell") as! MessageListCell
        
        let message =  messageList[indexPath.row] as MessageListModel
        
        cell.setMessage(message)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedIndexPath = indexPath
        self.performSegueWithIdentifier("Segue2Message", sender: self)
    }
    
    func getMessageList() {
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        
        print(params)
        
        Alamofire.request(.GET, kBaseURL+"message/inbox", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /message/inbox")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
//                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let message_cnt = jsonResponse["message_data"].array?.count {
                            
                            if(message_cnt > 0) {
                                
                                self.messageList.removeAll()
                                
                                for var index = 0; index < message_cnt; index++ {
                                    
                                    let _message = MessageListModel()
                                    _message.id = Int(jsonResponse["message_data"][index]["id"].string!)!
                                    _message.name = jsonResponse["message_data"][index]["first_name"].string! + " " + jsonResponse["message_data"][index]["last_name"].string!
                                    _message.hash_key = jsonResponse["message_data"][index]["hash_key"].string!
                                    _message.thread_hash = jsonResponse["message_data"][index]["thread_hash"].string!
                                    
                                    _message.pic_url = jsonResponse["message_data"][index]["profile_pic"].string == nil ? "" : jsonResponse["message_data"][index]["profile_pic"].string!
                                    _message.sender_id = Int(jsonResponse["message_data"][index]["sender_id"].string!)!
                                    _message.thread_id = Int(jsonResponse["message_data"][index]["thread_id"].string!)!
                                    
                                    let dateFormatter = NSDateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let created_date = dateFormatter.dateFromString(jsonResponse["message_data"][index]["created_date"].string!)

                                    dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                                    
                                    if (self.defLang == "fr" ) {
                                        dateFormatter.locale = NSLocale(localeIdentifier: "fr_FR")
                                    }
                                    _message.created_date = dateFormatter.stringFromDate(created_date!)
                                    
                                    let last_date = NSDate(timeIntervalSince1970: NSTimeInterval(Double(Int(jsonResponse["message_data"][index]["last_msg_date"].string!)!)))
                                    dateFormatter.dateFormat = "hh:mm a"
                                    _message.last_message_date = dateFormatter.stringFromDate(last_date)
                                    
                                    _message.message = jsonResponse["message_data"][index]["message"].string!
                                    _message.unread_cnt = Int(jsonResponse["message_data"][index]["trashed"].string!)!
                                    
                                    
                                    if let user_role = jsonResponse["message_data"][index]["user_role"].string {
                                    
                                        _message.role = user_role
                                    }
                                    
                                    if let start_point = jsonResponse["message_data"][index]["start_point"].string {
                                        _message.start_point = start_point
                                    }
                                    
                                    if let end_point = jsonResponse["message_data"][index]["end_point"].string {
                                        _message.end_point = end_point
                                    }
                                    
                                    if(_message.start_point != "" && _message.end_point != "") {
                                        _message.from_to = _message.start_point + "->\n" + _message.end_point
                                    }
                                    
                                    if let _ = jsonResponse["message_data"][index]["gender"].string {
                                        _message.gender = Int(jsonResponse["message_data"][index]["gender"].string!)!
                                    }
                                    
                                    self.messageList.append(_message)                                    
                                }
                                
                                self.imvIntro.hidden = true

                                self.tblMessageList.reloadData()
                                
                            }
                        } else {
                            
                            self.lblNotice.hidden = false
                        }
                        
                    } else {
                        print("Error parsing /message/inbox")
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    }
                }
                
            })
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "Segue2Message") {
            
            let messageVC = segue.destinationViewController as! MessageViewController
            messageVC.messageListModel = self.messageList[selectedIndexPath.row]
        }
    }
}
