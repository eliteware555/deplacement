//
//  RecentLiftsViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class RecentLiftsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var recentLift = true
    
    var public_profile = 0
    
    var _user: UserInfo!
    
    var recentTrips: [Trip] = []
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var tblLiftsList: UITableView!
    
    @IBOutlet weak var lblNotice: UILabel!
    @IBOutlet weak var imvIntro: UIImageView!
    
    var recent_trip_count = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let accountStoryboard: UIStoryboard = UIStoryboard(name: "Account_RightSide", bundle:  nil)
            let rightMenuViewController = accountStoryboard.instantiateViewControllerWithIdentifier("AccountRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.revealViewController().panGestureRecognizer()
            self.revealViewController().tapGestureRecognizer()
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        if(recentLift) {
            navigationItem.title = defLang == "fr" ? R.fr.profile_recent_lift : R.en.profile_recent_lift
        } else {
            navigationItem.title = defLang == "fr" ? R.fr.row4 : R.en.row4
        }
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // change bar button item tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    func initView() {
        
        // initialize code here according to language setting
        lblNotice.text = defLang == "fr" ? R.fr.no_lift : R.en.no_lift
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        getRecentTrip()
    }
    
    // -----------------------------------------------------------------------------
    //   Mark: - UITableView Datasource
    // -----------------------------------------------------------------------------
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentTrips.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("RecentTripCell") as! RecentTripCell
        
        let start = defLang == "fr" ? recentTrips[indexPath.row].start_point_fr : recentTrips[indexPath.row].start_point_en
        let end = defLang == "fr" ? recentTrips[indexPath.row].end_point_fr : recentTrips[indexPath.row].end_point_en
        
        let fromToString = start + " , \n" + end
        let blueBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 143/255.0, green: 187/255.0, blue: 216/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 13.0)!]
        let attributedFromTo = NSMutableAttributedString(string: fromToString as String)
        
        attributedFromTo.addAttributes(blueBoldAttributes, range: NSMakeRange(0, start.characters.count))
        attributedFromTo.addAttributes(blueBoldAttributes, range: NSMakeRange(start.characters.count + 4, end.characters.count))
        
        cell.lblFromTo.attributedText = attributedFromTo
        
        let blackBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 85/255.0, green: 85/255.0,
            blue: 85/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 12.0)!]
        
        let dateString = recentTrips[indexPath.row].departureDate + " -\n" + recentTrips[indexPath.row].returnDate
        let attributedDate = NSMutableAttributedString(string: dateString)
        
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(0, 2))
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(12, 8))
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(23, 2))
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(35, 8))
        
        cell.lblDateFromTo.attributedText = attributedDate
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.selected = false
        
        let offerstoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle:  nil)
        let destController = offerstoryboard.instantiateViewControllerWithIdentifier("RideDetailVC") as! RideDetailTVC
        
        destController.ride_hash = recentTrips[indexPath.row].ride_hash
        
        self.navigationController?.pushViewController(destController, animated: true)
    }
    
    func getRecentTrip() {
        
        public_profile = prefs.integerForKey("PUBLIC_PROFILE")
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let params = [
            "public_profile": "\(public_profile)",
            "user_hash": prefs.stringForKey("USER_HASH")!
        ]
        
        debugPrint(params)
        
        Alamofire.request(.GET, kBaseURL+"users/profile", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/profile")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {

                        if let tripcount = jsonResponse["user_data"]["ride_offered"].array?.count {

                            if(tripcount > 0) {

                                self._user.recentTrips.removeAll()

                                for var index = 0; index < tripcount; index++ {

                                    let trip = Trip()
                                    trip.start_point_en = jsonResponse["user_data"]["ride_offered"][index]["start_point_en"].string!
                                    trip.start_point_fr = jsonResponse["user_data"]["ride_offered"][index]["start_point_fr"].string!
                                    trip.end_point_en = jsonResponse["user_data"]["ride_offered"][index]["end_point_en"].string!
                                    trip.end_point_fr = jsonResponse["user_data"]["ride_offered"][index]["end_point_fr"].string!

                                    trip.start_lat = jsonResponse["user_data"]["ride_offered"][index]["start_lat"].string!
                                    trip.start_lng = jsonResponse["user_data"]["ride_offered"][index]["start_lon"].string!
                                    trip.end_lat = jsonResponse["user_data"]["ride_offered"][index]["end_lat"].string!
                                    trip.end_lng = jsonResponse["user_data"]["ride_offered"][index]["end_lon"].string!
                                    
                                    trip.ride_hash = jsonResponse["user_data"]["ride_offered"][index]["hash_key"].string!

                                    let dateFormatter = NSDateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let departureDate = dateFormatter.dateFromString(jsonResponse["user_data"]["ride_offered"][index]["start_date"].string!)
                                    let returnDate = dateFormatter.dateFromString(jsonResponse["user_data"]["ride_offered"][index]["end_date"].string!)
                                    
                                    dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                                    
                                    if(departureDate != nil) {
                                    
                                        trip.departureDate = dateFormatter.stringFromDate(departureDate!)
                                    }
                                    
                                    if(returnDate != nil) {
                                        
                                        trip.returnDate = dateFormatter.stringFromDate(returnDate!)
                                    }
                                    
                                    self._user.recentTrips.append(trip)
                                }
                                
                                self.imvIntro.hidden = true
                                
                                self.loadDetails()
                                
                            } else {
                                
                                self.lblNotice.hidden = false
                            }
                            
                        } else {
                            
                            self.lblNotice.hidden = false
                        }
                        
                    } else {
                        
                        self.lblNotice.hidden = false
                        
                        print("Error parsing /users/profile")
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    }
                    
                } else {
                    
                    self.lblNotice.hidden = false
                }
                
            })
    }
    
    func loadDetails() {
        
        recentTrips = _user.recentTrips
        tblLiftsList.reloadData()
    }
}


