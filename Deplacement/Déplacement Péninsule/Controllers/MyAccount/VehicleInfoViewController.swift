//
//  VehicleInfoViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class VehicleInfoViewController: UIViewController {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var public_profile = 0
    
    var _user: UserInfo!
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    // 
    @IBOutlet weak var lblTitleVehicleInfo: UILabel!
    @IBOutlet weak var lblTitleVehicleName: UILabel!
    @IBOutlet weak var lblTitleVehicleColor: UILabel!
    @IBOutlet weak var lblVehicleColor: UILabel!
    @IBOutlet weak var lblTitleAirConditions: UILabel!
    @IBOutlet weak var lblAirConditions: UILabel!
    @IBOutlet weak var lblTitleBikeRack: UILabel!
    @IBOutlet weak var lblBikeRack: UILabel!
    @IBOutlet weak var lblTitleSkiRack: UILabel!
    @IBOutlet weak var lblSkiRack: UILabel!
    @IBOutlet weak var lblTitleComfort: UILabel!
    @IBOutlet weak var viewComfortLevel: FloatRatingView!
    @IBOutlet weak var lblTitlePreferences: UILabel!
    @IBOutlet weak var imvEat: UIImageView!
    @IBOutlet weak var imvMusic: UIImageView!
    @IBOutlet weak var imvPets: UIImageView!
    @IBOutlet weak var imvHandicapped: UIImageView!
    @IBOutlet weak var imvWomen: UIImageView!
    @IBOutlet weak var imvChildren: UIImageView!
    @IBOutlet weak var imvSmoke: UIImageView!
    @IBOutlet weak var imvChat: UIImageView!
    
    @IBOutlet weak var btnSelectCar: UIButton!
    
    let carDropDown = DropDown()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let accountStoryboard: UIStoryboard = UIStoryboard(name: "Account_RightSide", bundle:  nil)
            let rightMenuViewController = accountStoryboard.instantiateViewControllerWithIdentifier("AccountRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.revealViewController().panGestureRecognizer()
            self.revealViewController().tapGestureRecognizer()
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.vehicle_info : R.en.vehicle_info
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        carDropDown.dataSource = []
        carDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.btnSelectCar.setTitle(item, forState: .Normal)
            
            self.loadDetails(index)
        }
        carDropDown.anchorView = btnSelectCar
        carDropDown.bottomOffset = CGPoint(x: 0, y: btnSelectCar.bounds.height + 3)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // change bar button item tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    func initView() {
        
        lblTitleVehicleInfo.text = defLang == "fr" ? R.fr.vehicle_info : R.en.vehicle_info
        lblTitleVehicleName.text = (defLang == "fr" ? (R.fr.year + "," + R.fr.make + "," + R.fr.model) : (R.en.year + "," + R.en.make + "," + R.en.model))
        lblTitleAirConditions.text = defLang == "fr" ? R.fr.air_conditioning : R.en.air_conditioning
        lblTitleBikeRack.text = defLang == "fr" ? R.fr.bike_rack : R.en.bike_rack
        lblTitleSkiRack.text = defLang == "fr" ? R.fr.ski_rack : R.en.ski_rack
        lblTitleComfort.text = defLang == "fr" ? R.fr.comfort_level : R.en.comfort_level
        lblTitlePreferences.text = defLang == "fr" ? R.fr.preferences : R.fr.preferences
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        getVehicleInfo()
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    @IBAction func selectCar(sender: UIButton) {
        
        if(carDropDown.hidden) {
            carDropDown.show()
        } else {
            carDropDown.hide()
        }
    }
    
    // --------------------------------------------------
    // MARK: load vehicle info
    // --------------------------------------------------
    func getVehicleInfo() {
        
        public_profile = prefs.integerForKey("PUBLIC_PROFILE")
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let params = [
            "public_profile": "\(public_profile)",
            "user_hash": prefs.stringForKey("USER_HASH")!
        ]
        
        debugPrint(params)
        
        Alamofire.request(.GET, kBaseURL+"users/profile", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/profile")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
//                        if let _ = jsonResponse["user_data"]["rating_datas"].array {
                        
                            self._user.totalReviews = jsonResponse["user_data"]["rating_datas"].count
//                        }
                        
//                        if let _  =  jsonResponse["user_data"]["veh_datas"].array {

                            if let _vehiclecount = jsonResponse["user_data"]["veh_datas"].array?.count {

                                if(_vehiclecount > 0) {

                                    self._user.vehicle.removeAll()
                                    
                                    self.carDropDown.dataSource.removeAll()

                                    for var index = 0; index < _vehiclecount; index++ {

                                        let _vehicle = Vehicle()
                                        
                                        _vehicle.year = jsonResponse["user_data"]["veh_datas"][index]["year_built"].string!
                                        _vehicle.make_name = jsonResponse["user_data"]["veh_datas"][index]["make_name"].string!
                                        _vehicle.model_name = jsonResponse["user_data"]["veh_datas"][index]["model_name"].string!
                                        
                                        _vehicle.makeSimpleName()
                                        
                                        _vehicle.color = jsonResponse["user_data"]["veh_datas"][index]["color"].string!
                                        
                                        if(self.defLang == "fr") {
                                            _vehicle.airCondition = Int(jsonResponse["user_data"]["veh_datas"][index]["ac"].string!)! == 0 ? R.fr.no : R.fr.yes
                                            _vehicle.bikeRack = Int(jsonResponse["user_data"]["veh_datas"][index]["bike_rack"].string!) == 0 ? R.fr.no : R.fr.yes
                                            _vehicle.skiRack = Int(jsonResponse["user_data"]["veh_datas"][index]["ski_rack"].string!) == 0 ? R.fr.no : R.fr.yes
                                        } else {
                                            _vehicle.airCondition = Int(jsonResponse["user_data"]["veh_datas"][index]["ac"].string!)! == 0 ? R.en.no : R.en.yes
                                            _vehicle.bikeRack = Int(jsonResponse["user_data"]["veh_datas"][index]["bike_rack"].string!) == 0 ? R.en.no : R.en.yes
                                            _vehicle.skiRack = Int(jsonResponse["user_data"]["veh_datas"][index]["ski_rack"].string!) == 0 ? R.en.no : R.en.yes
                                        }
                                        _vehicle.comfortLevel = Float(Int(jsonResponse["user_data"]["veh_datas"][index]["comfort_level"].string!)!)

                                        self.carDropDown.dataSource.append(_vehicle.simple_name)
                                        
                                        self._user.vehicle.append(_vehicle)
                                    }
                                }
                            }
//                        }
                        
                        self._user.prefs_food = Int(jsonResponse["user_data"]["preference_data"]["food"].string!)!
                        self._user.prefs_kid = Int(jsonResponse["user_data"]["preference_data"]["children"].string!)!
                        self._user.prefs_handicapped = Int(jsonResponse["user_data"]["preference_data"]["handicapped"].string!)!
                        self._user.prefs_music = Int(jsonResponse["user_data"]["preference_data"]["music"].string!)!
                        self._user.prefs_pets = Int(jsonResponse["user_data"]["preference_data"]["pets"].string!)!
                        self._user.prefs_smoking = Int(jsonResponse["user_data"]["preference_data"]["smoking"].string!)!
                        self._user.prefs_ladies = Int(jsonResponse["user_data"]["preference_data"]["women_only"].string!)!
                        self._user.prefs_chat = Int(jsonResponse["user_data"]["preference_data"]["chatting"].string!)!

                    
                        if(self._user.vehicle.count > 0) {                    
                            self.loadDetails(0)
                        }
                        
                    } else {
                        print("Error parsing /users/profile")
                    }
                }
                
            })
    }
    
    func loadDetails(index: Int) {
        
        btnSelectCar .setTitle(_user.vehicle[index].simple_name, forState: .Normal)
        lblVehicleColor.text = _user.vehicle[index].color
        lblBikeRack.text = _user.vehicle[index].bikeRack
        lblSkiRack.text = _user.vehicle[index].skiRack
        viewComfortLevel.rating = _user.vehicle[index].comfortLevel
        lblAirConditions.text = _user.vehicle[index].airCondition
        
        imvEat.image = UIImage(named: "eat"+"\(_user.prefs_food)")
        imvChildren.image = UIImage(named: "children"+"\(_user.prefs_kid)")
        imvHandicapped.image = UIImage(named: "handicapped"+"\(_user.prefs_handicapped)")
        imvMusic.image = UIImage(named: "music"+"\(_user.prefs_music)")
        imvPets.image = UIImage(named: "pets"+"\(_user.prefs_pets)")
        imvSmoke.image = UIImage(named: "smoke"+"\(_user.prefs_smoking)")
        imvWomen.image = UIImage(named: "women"+"\(_user.prefs_ladies)")
        imvChat.image = UIImage(named: "chat"+"\(_user.prefs_chat)")
    }
}
