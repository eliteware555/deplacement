//
//  AuthentificationVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class AuthentificationVC: UIViewController {
    
    var firstFocus = false

    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var _user: UserInfo!
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // ui outlet
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var lblTitleAuthentication: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitleEmailVerification: UILabel!
    @IBOutlet weak var lblTitleMobileVerification: UILabel!
    @IBOutlet weak var lblTitleIDDetails: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let settingstoryboard: UIStoryboard = UIStoryboard(name: "Setting_RightSlide", bundle:  nil)
            let rightMenuViewController = settingstoryboard.instantiateViewControllerWithIdentifier("SettingRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.authentifications : R.en.authentifications
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        firstFocus = true
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // set back button tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillShowNotification)
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillHideNotification)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        getUserIDDetails()
    }
    
    func initView() {
        
        // email textfield bottom line and placeholder
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.0).CGColor
        border.frame = CGRect(x: 0, y: txtEmail.frame.size.height - width, width: txtEmail.frame.size.width, height: txtEmail.frame.height)
        border.borderWidth = width
        txtEmail.layer.addSublayer(border)
        txtEmail.layer.masksToBounds = true
        
        // phone number textfield bottom line and place holder
        let border2 = CALayer()
        border2.borderColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.0).CGColor
        border2.frame = CGRect(x: 0, y: txtEmail.frame.size.height - width, width: txtEmail.frame.size.width, height: txtEmail.frame.height)
        border2.borderWidth = width
        txtPhoneNumber.layer.addSublayer(border2)
        txtPhoneNumber.layer.masksToBounds = true
        
        // initialize ui here ( according to the current language)
        
        lblTitleAuthentication.text = defLang == "fr" ? R.fr.authentifications : R.en.authentifications
        lblDescription.text = defLang == "fr" ? R.fr.authentification_info : R.en.authentification_info
        lblTitleEmailVerification.text = defLang == "fr" ? R.fr.email_verification : R.en.email_verification
        lblTitleMobileVerification.text = defLang == "fr" ? R.fr.mobile_verification : R.en.mobile_verification
        lblTitleIDDetails.text = defLang == "fr" ? R.fr.id_details : R.en.id_details
        lblFrom.text = (defLang == "fr" ? R.fr.from : R.en.from) + ": "
        lblExpiryDate.text = (defLang == "fr" ? R.fr.to : R.en.to) + ": "
        
        btnSave .setTitle((defLang == "fr" ? R.fr.btn_save : R.en.btn_save), forState: .Normal)
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if(firstFocus) {
            
            firstFocus = false
            
            UIView.animateWithDuration(0.5) { () -> Void in
                var f = self.view.frame
                f.origin.y -= 30
                self.view.frame = f
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        firstFocus = true
        
        UIView.animateWithDuration(0.5) { () -> Void in
            var f = self.view.frame
            f.origin.y = 0.0
            self.view.frame = f
        }
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {

        self.view.endEditing(true)
    }
    
    @IBAction func saveAction(sender: UIButton) {
        
        self.view.endEditing(true)
        
        // check the validation of input value
        if(txtEmail.text?.characters.count == 0) {
            
            JLToast.makeText("Please input your email for authetication")
            return
        } else if (txtPhoneNumber.text?.characters.count == 0) {
        
            JLToast.makeText("Please input your phone number for authetication")
            return
        }
        
        authenticate()
        
        print("saved")
    }
    
    func getUserIDDetails() {
        
        let params = [
            "hash_key": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        
        debugPrint(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "users/user", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/user")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    if(status) {
                        
                        if let _ = jsonResponse["user_data"]["id_proof_submited_method"].string {
                        
                            self._user.id_proof_method = Int(jsonResponse["user_data"]["id_proof_submited_method"].string!)!
                        }
                        
                        
                        if let _ = jsonResponse["user_data"]["id_proof_expiry_date"].string {
                            
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            let id_expiry_date = dateFormatter.dateFromString(jsonResponse["user_data"]["id_proof_expiry_date"].string!)
                            
                            if(id_expiry_date != nil) {
                            
                                dateFormatter.dateFormat = "dd MMM yyyy"
                                self._user.id_expiry_date = dateFormatter.stringFromDate(id_expiry_date!)
                            }
                            
                        }
                        
                        self.loadDetails()
                        
                    } else {
                        print("Error parsing /users/user")
                    }
                }
        }
    }
    
    func loadDetails() {
        
        txtEmail.text = _user.email
        txtPhoneNumber.text = _user.phoneNumber
        
        if(_user.id_proof_method == 1) {
            lblFrom.text =  (defLang == "fr" ? R.fr.expiry_date : R.en.expiry_date) + ": office"
        } else if (_user.id_proof_method == 2) {
            lblFrom.text =  (defLang == "fr" ? R.fr.expiry_date + ": Télécopier au bureau" : R.en.expiry_date  + ": Faxed Directly")
        } else if (_user.id_proof_method == 3) {
            lblFrom.text =  (defLang == "fr" ? R.fr.expiry_date : R.en.expiry_date) +  ": Fax from Public safety NB"
        }

        lblExpiryDate.text = (defLang == "fr" ? R.fr.expiry_date : R.en.expiry_date) + _user.id_expiry_date
    }
    
    // authentificate user email or phone number
    func authenticate() {
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "primary_email": txtEmail.text!,
            "primary_phone": txtPhoneNumber.text!
        ]
        
        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/authentication", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/authentication")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    if(status) {
                        
                        
                    } else {
                        print("Error parsing /settings/authentication")
                    }
                }
        }
    }
}


//let dateFormatter = NSDateFormatter()
//dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//let membersinceDate = dateFormatter.dateFromString(jsonResponse["user_data"]["id_proof_upload_date"].string!)
//
//let calendar = NSCalendar.currentCalendar()
//let components = calendar.components([.Year, .Month, .Day, .Hour, .Minute], fromDate: membersinceDate!)
//components.year += 1
//
//let expireDate = calendar.dateFromComponents(components)
//
//dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
//self._user.memberSince = dateFormatter.stringFromDate(membersinceDate!)
//self._user.expireDate = dateFormatter.stringFromDate(expireDate!)
//
//let currentDate = NSDate()
//
//let diff = calendar.components([.Day], fromDate: currentDate, toDate: expireDate!, options: [])
//self._user.expireRemaining = "\(diff.day)"








