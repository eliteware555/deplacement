//
//  MembershipVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON
import StoreKit

class MembershipVC: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var _user: UserInfo!
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    // membership view
     @IBOutlet weak var viewMemberShip: UIView! // membership superview
    
    @IBOutlet weak var lblTitleLineWithUs: UILabel! //Lift with Us!
    @IBOutlet weak var lblDescriptionLine: UILabel! //Book your seats and publish your lift easy
    @IBOutlet weak var lblTitleCurrentMemberShip: UILabel! //Current Membership Status
    @IBOutlet weak var lblMemberShipSince: UILabel! //Member since 17 Feb 2016
    @IBOutlet weak var lblPaymentMothod: UILabel! //Payment through stripe
    @IBOutlet weak var lblExpiersDate: UILabel! //Expires on 17 Feb 2017 [360 days remaining]
    @IBOutlet weak var btnRenew: UIButton!
    
    // not membership view
    @IBOutlet weak var viewNotMemberShip: UIScrollView!
    
    //check box
    @IBOutlet weak var imvOnline: UIImageView!
    @IBOutlet weak var imvOffice: UIImageView!
    
    // description
    // membership
    @IBOutlet weak var lblOnlinePayment: UILabel!
    
    // not membership
    @IBOutlet weak var lblDeplacementOffice: UILabel!
    
    // Pay or Notify Admin button
    @IBOutlet weak var btnPayOrNotify: UIButton!
    
    @IBOutlet weak var scrollViewHConstraint: NSLayoutConstraint!
    
    var onlinePayment: Bool = true
    
    // In App Purchase
    var productIDs: Array<String!> = []
    var productsArray: Array<SKProduct!> = []
    
    var transactionInProgress = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let settingstoryboard: UIStoryboard = UIStoryboard(name: "Setting_RightSlide", bundle:  nil)
            let rightMenuViewController = settingstoryboard.instantiateViewControllerWithIdentifier("SettingRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
//            leftMenuButton.action = "revealToggle:"
            leftMenuButton.action = #selector(self.revealViewController().revealToggle(_:))
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = #selector(self.revealViewController().revealToggle(_:))
//            rightMenuButton.action = "rightRevealToggle:"
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.membership : R.en.membership
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        initVars()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // set back button tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // check whether an user had taken membership for using Deplacement Peninsule
        verifyMembership()
    }
    
    func initView() {
        
        defLang == "fr" ? R.fr.lift_with_us : R.en.lift_with_us
        
        lblTitleLineWithUs.text = defLang == "fr" ? R.fr.lift_with_us : R.en.lift_with_us
        lblDescriptionLine.text = defLang == "fr" ? R.fr.book_your_seat : R.en.book_your_seat
        lblTitleCurrentMemberShip.text = defLang == "fr" ? R.fr.current_membership_status : R.en.current_membership_status
        lblMemberShipSince.text = defLang == "fr" ? R.fr.member_since : R.en.member_since
        lblPaymentMothod.text = defLang == "fr" ? R.fr.payment_through : R.en.payment_through
        lblExpiersDate.text = defLang == "fr" ? R.fr.expires_on : R.en.expires_on
        
        btnRenew.setTitle(defLang == "fr" ? R.fr.btn_renew : R.en.btn_renew, forState: .Normal)
        
        // hide membership view & not membership view
        self.viewMemberShip.hidden = true
        self.viewNotMemberShip.hidden = true
        
        lblDeplacementOffice.text = "\n"
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    func initVars() {
        
        productIDs.append("com.maaspros.deplacement.membershipfee")
        
        requestProductInfo()
        
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
    }
    
    //
    func requestProductInfo() {
        
        if SKPaymentQueue.canMakePayments() {
            
            let productIdentifiers = NSSet(array: productIDs)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
            
            productRequest.delegate = self
            productRequest.start()
            
        } else {
            
            print("Cannot perform In App Purchases.")
        }
    }
    
    // MARK: - SKProductRequestDelegate protocol
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        
        if response.products.count != 0 {
            
            for product in response.products {
                
                productsArray.append(product as SKProduct)
            }
            
        } else {
            
            print("There are no proudcts.")
        }
        
        if (response.invalidProductIdentifiers.count != 0) {
            
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    // MARK: - SKPaymentTransactionObserver
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions as [SKPaymentTransaction] {
            
            switch transaction.transactionState {
            case SKPaymentTransactionState.Purchased:
                
                print("Transaction completed succesfully.")
                
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                transactionInProgress = false
                
                // delegate here.
                // should call api for confirming that user paid registration membership
                
                submitPayment()
                
                break
                
            case SKPaymentTransactionState.Failed:
                
                print("Transaction Failed")
                
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                transactionInProgress = false
                
                break
                
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    // check wheteher an user had taken membership for Deplacement Peninsule
    func verifyMembership() {
        
        let params = [
            "hash_key": prefs.stringForKey("USER_HASH")!,
            "login_hash": prefs.stringForKey("USER_HASH")!
        ]
        
        //        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "users/membership_verify", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/membership_verify")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("membership_verify ------------\n", jsonResponse)
                
                if let status = jsonResponse["status"].string {
                    
                    if (status == "1") {
                        
                        self.viewMemberShip.hidden = false
                        self.viewNotMemberShip.hidden = true
                        
                        self.getMembershipData()
                        
                    } else if (status == "0" || status == "") {
                        
                        self.viewMemberShip.hidden = true
                        self.viewNotMemberShip.hidden = false
                        
                    } else {
                        
                        JLToast.makeText(status, duration: 1.0).show()
                    }
                }
        }
    }
    
    func getMembershipData() {
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        
        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/membership", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/authentication")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("membership -------------\n", jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        let dateFormatter = NSDateFormatter()
                        
                        if let _ = jsonResponse["membership_data"]["reg_data"]["paid_date"].string {
                            
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            
                            let membersince_date = dateFormatter.dateFromString(jsonResponse["membership_data"]["reg_data"]["paid_date"].string!)
                            let expiry_date = dateFormatter.dateFromString(jsonResponse["membership_data"]["reg_data"]["expiry_date"].string!)
                            
                            dateFormatter.dateFormat = "dd MMM yyyy"
                            
                            if(membersince_date != nil) {
                                
                                self._user.memberSince = dateFormatter.stringFromDate(membersince_date!)
                            }
                            
                            if(expiry_date != nil) {
                                
                                self._user.expireDate = dateFormatter.stringFromDate(expiry_date!)
                            }
                        }
                        
                        if let _ = jsonResponse["membership_data"]["reg_data"]["num_days"].int {
                            
                            self._user.expireRemaining = String(jsonResponse["membership_data"]["reg_data"]["num_days"].int!)
                        }
                        
                        if let _ = jsonResponse["membership_data"]["reg_data"]["payment_method"].string {
                            
                            self._user.paymentMethod = Int(jsonResponse["membership_data"]["reg_data"]["payment_method"].string!)!
                        }
                        
                        self.loadDetails()
                        
                    } else {
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                    }
                }
        }
    }
    
    func loadDetails() {
        
        lblMemberShipSince.text = ((defLang == "fr" ? R.fr.member_since : R.en.member_since) + _user.memberSince)
        
        if(_user.paymentMethod == 1) {
            lblPaymentMothod.text = ((defLang == "fr" ? R.fr.member_since : R.en.payment_through) + "stripe")
        } else if (_user.paymentMethod == 3) {
            
            lblPaymentMothod.text = ((defLang == "fr" ? R.fr.member_since : R.en.payment_through) + "office")
        }
        
        lblExpiersDate.text = ((defLang == "fr" ? R.fr.expires_on : R.en.expires_on) +  _user.expireDate + "\n" + "[" + _user.expireRemaining + (defLang == "fr" ? R.fr.days_remaining : R.en.days_remaining) + " ]")
        
    }
    
    func submitPayment() {
        
        //get 1 year later date
        let today = NSDate()
        let tomorrow = NSCalendar.currentCalendar().dateByAddingUnit(
            .Year,
            value: 1,
            toDate: today,
            options: NSCalendarOptions(rawValue: 0))
        print(tomorrow)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strExpirydate:String = dateFormatter.stringFromDate(tomorrow!)
        
        //        let plainPassword = (password as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        //        let base64Password = plainPassword!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
//        let strtoken = (token.tokenId as String).dataUsingEncoding(NSUTF8StringEncoding)
//        let base64Token =  strtoken!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
//        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "amount": "$10.00",
            "expiry_date": strExpirydate,
            "stripe_token": "",
            "stripe_data[status]":"success"
        ]
        
//        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/stripe", parameters: params)
            .validate()
            .responseJSON
            {
                response in
                self.removeActivityView()
                
                guard let value = response.result.value else
                {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                    
                let jsonResponse = JSON(value)
//                    print(jsonResponse["status"])
                print(jsonResponse)
                
                
                let strMsg:String = String(jsonResponse["message"])
                
                JLToast.makeText(strMsg).show()
                
                if (jsonResponse["status"].bool == true) {
                    
                    self.verifyMembership()
                }
        }
    }
    
    @IBAction func paymentMethodChanged(sender: UIButton) {
        
        if (sender.tag == 400) {
            
            imvOnline.image = UIImage(named: "checked1.png")
            imvOffice.image = UIImage(named: "unchecked1.png")
            
            // Online
            lblOnlinePayment.hidden = false
            lblDeplacementOffice.text = "\n"
            btnPayOrNotify.backgroundColor = UIColor(red: 250/225.0, green: 164/255.0, blue: 42/255.0, alpha: 1.0)
            btnPayOrNotify.setTitle("Pay", forState: .Normal)
            
            // Online Payment via In App Purchase
            onlinePayment = true
            
            self.scrollViewHConstraint.constant = 504
            self.view.layoutIfNeeded()
            
        } else {
            
            imvOffice.image = UIImage(named: "checked1.png")
            imvOnline.image = UIImage(named: "unchecked1.png")
            
            // Deplacement Peninsule Office
            lblOnlinePayment.hidden = true
            lblDeplacementOffice.text = "You can send you payment by main or in person at the following address:\n\n22 boul.Is St-Pierre\nCaraquet NB E1W 1B6\n\nPlease make your check payable to the Community inclusion network in the Acadian Peninsula(RIC_PA)."
            
            btnPayOrNotify.backgroundColor = UIColor(red: 48/2555.0, green: 174/255.0, blue: 176/255.0, alpha: 1.0)
            btnPayOrNotify.setTitle("Notify Admin", forState: .Normal)
            
            // Deplacement Office Direct Payment
            onlinePayment = false
            
            self.scrollViewHConstraint.constant = 600
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func PayOrNotifyAdminTapped(sender: UIButton) {
        
        if (onlinePayment) {
            
            if (self.productsArray.count != 0) {
            
                let payment = SKPayment(product: self.productsArray[0] as SKProduct)
                SKPaymentQueue.defaultQueue().addPayment(payment)
                self.transactionInProgress = true
                
            } else {
                
//                submitPayment()
                
                print("No Valid Product")
            }
            
        } else {
            
            let parmas = [
                "login_hash": prefs.stringForKey("USER_HASH")!,
                "site_lang":defLang,
                "office":"1",
                "payment_method":"3",
                "process":"1"
            ]
            
//            print(parmas)
            
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
            
            Alamofire.request(.GET, kBaseURL + "settings/membership", parameters: parmas)
                .validate()
                .responseJSON
                {
                    response in
                    self.removeActivityView()
                    guard let value = response.result.value else
                    {
                        print("Error: did not receive data")
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                        return
                    }
                    
                    guard response.result.error == nil else {
                        print(response.result.error)
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                        return
                    }
                    
                    let jsonResponse = JSON(value)
                    print("notify membership -------- \n \(jsonResponse)")
                    
                    let strMsg:String = String(jsonResponse["msg"])
                    JLToast.makeText(strMsg).show()
                    
            }
        }
    }
}



