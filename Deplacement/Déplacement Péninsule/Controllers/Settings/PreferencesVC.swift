//
//  PreferencesVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

import Alamofire
import JLToast
import SwiftyJSON

class PreferencesVC: UIViewController {

    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var _user: UserInfo!
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var lblTitlePreferences: UILabel!
    @IBOutlet weak var lblChat: UILabel!
    @IBOutlet weak var lblMusic: UILabel!
    @IBOutlet weak var lblPets: UILabel!
    @IBOutlet weak var lblSmoking: UILabel!
    @IBOutlet weak var lblFood: UILabel!
    @IBOutlet weak var lblLadies: UILabel!
    @IBOutlet weak var lblKids: UILabel!
    @IBOutlet weak var lblHandcapped: UILabel!
    
    @IBOutlet weak var imvChat: UIImageView!
    @IBOutlet weak var imvMusic: UIImageView!
    @IBOutlet weak var imvPets: UIImageView!
    @IBOutlet weak var imvSmoking: UIImageView!
    @IBOutlet weak var imvFood: UIImageView!
    @IBOutlet weak var imvLadies: UIImageView!
    @IBOutlet weak var imvKid: UIImageView!
    @IBOutlet weak var imvHandicapped: UIImageView!
    
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnMusic: UIButton!
    @IBOutlet weak var btnPets: UIButton!
    @IBOutlet weak var btnSmoking: UIButton!
    @IBOutlet weak var btnFood: UIButton!

    @IBOutlet weak var swLadies: UISwitch!
    @IBOutlet weak var swKid: UISwitch!
    @IBOutlet weak var swHandicapped: UISwitch!
    
    @IBOutlet weak var btnSave: UIButton!
    
    let chatDropDown = DropDown()
    let musicDropDown = DropDown()
    let petsDropDown = DropDown()
    let smokeDropDown = DropDown()
    let foodDropDown = DropDown()

    
    var prefs_chat: Int = 0
    var prefs_music: Int = 0
    var prefs_pets: Int = 0
    var prefs_smoking: Int = 0
    var prefs_food: Int = 0
    
    var prefs_ladies: Int = 0
    var prefs_kid: Int = 0
    var prefs_handicapped: Int = 0
    
    var arrPrefs: [String] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let settingstoryboard: UIStoryboard = UIStoryboard(name: "Setting_RightSlide", bundle:  nil)
            let rightMenuViewController = settingstoryboard.instantiateViewControllerWithIdentifier("SettingRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.preferences : R.en.preferences
        
        // initialize acitivity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 200
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        _user.saveDefaultPrefs()
        
        if(defLang == "fr") {
            arrPrefs = [R.fr.no, R.fr.despends_on_conditions, R.fr.yes]
        } else {
            arrPrefs = [R.en.no, R.en.despends_on_conditions, R.en.yes]
        }
        
        chatDropDown.dataSource = arrPrefs
        chatDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.btnChat.setTitle(item, forState: .Normal)
            self.prefs_chat = index
        
            self.changePreferenceState(0)
        }
        chatDropDown.anchorView = btnChat
        chatDropDown.bottomOffset = CGPoint(x: 0, y: btnChat.bounds.height + 3)
        
        musicDropDown.dataSource = arrPrefs
        musicDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.btnMusic.setTitle(item, forState: .Normal)
            self.prefs_music = index
            
            self.changePreferenceState(1)
        }
        musicDropDown.anchorView = btnMusic
        musicDropDown.bottomOffset = CGPoint(x: 0, y: btnMusic.bounds.height + 3)
        
        petsDropDown.dataSource = arrPrefs
        petsDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.btnPets.setTitle(item, forState: .Normal)
            self.prefs_pets = index
            
            self.changePreferenceState(2)
        }
        petsDropDown.anchorView = btnPets
        petsDropDown.bottomOffset = CGPoint(x: 0, y: btnPets.bounds.height + 3)
        
        smokeDropDown.dataSource = arrPrefs
        smokeDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.btnSmoking.setTitle(item, forState: .Normal)
            self.prefs_smoking = index
            
            self.changePreferenceState(3)
        }
        smokeDropDown.anchorView = btnSmoking
        smokeDropDown.bottomOffset = CGPoint(x: 0, y: btnSmoking.bounds.height + 3)
        
        foodDropDown.dataSource = arrPrefs
        foodDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.btnFood.setTitle(item, forState: .Normal)
            self.prefs_food = index
            
            self.changePreferenceState(4)
        }
        foodDropDown.anchorView = btnFood
        foodDropDown.bottomOffset = CGPoint(x: 0, y: btnFood.bounds.height + 3)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // set back button tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        _user.loadPreferences()
        
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func initView() {
        
        lblTitlePreferences.text = defLang == "fr" ? R.fr.set_your_preferences : R.en.set_your_preferences
        lblChat.text = defLang == "fr" ? R.fr.prefs_chat : R.en.prefs_chat
        lblMusic.text = defLang == "fr" ? R.fr.prefs_music : R.en.prefs_music
        lblPets.text = defLang == "fr" ? R.fr.prefs_pets : R.en.prefs_pets
        lblSmoking.text = defLang == "fr" ? R.fr.prefs_smoking : R.en.prefs_smoking
        lblFood.text = defLang == "fr" ? R.fr.prefs_food : R.en.prefs_food
        lblLadies.text = defLang == "fr" ? R.fr.prefer_ladies_only : R.en.prefer_ladies_only
        lblKids.text = defLang == "fr" ? R.fr.allow_lift_with_kid : R.en.allow_lift_with_kid
        lblHandcapped.text = defLang == "fr" ? R.fr.ready_handicapped_person : R.en.ready_handicapped_person
        
        btnSave.setTitle(defLang == "fr" ? R.fr.btn_save : R.en.btn_save, forState: .Normal)
        
        loadPreferences()
    }
    
    // remove activity indicator from view
    func removeActivityView() {
        
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(200)?.removeFromSuperview()
    }
    
    @IBAction func changePreferences(sender: UIButton) {
        
        switch sender {
        
        case btnChat:
            
            if(chatDropDown.hidden) {
                chatDropDown.show()
            } else {
                chatDropDown.hide()
            }
            
            break
            
        case btnMusic:
            
            if(musicDropDown.hidden) {
                musicDropDown.show()
            } else {
                musicDropDown.hide()
            }

            break
            
        case btnPets:
            
            if(petsDropDown.hidden) {
                petsDropDown.show()
            } else {
                petsDropDown.hide()
            }

            break
            
        case btnSmoking:
            
            if(smokeDropDown.hidden) {
                smokeDropDown.show()
            } else {
                smokeDropDown.hide()
            }

            break
            
        case btnFood:
            
            if(foodDropDown.hidden) {
                foodDropDown.show()
            } else {
                foodDropDown.hide()
            }

            break
            
        default: ()
        }
    }
    
    func changePreferenceState(which: Int) {
        
        switch which {
            
        case 0:
            imvChat.image = UIImage(named: "chat" + "\(prefs_chat)")
            break
            
        case 1:
            imvMusic.image = UIImage(named: "music" + "\(prefs_music)")
            break
            
        case 2:
            imvPets.image = UIImage(named: "pets" + "\(prefs_pets)")
            break
        
        case 3:
            imvSmoking.image = UIImage(named: "smoke" + "\(prefs_smoking)")
            break
            
        case 4:
            imvFood.image = UIImage(named: "eat" + "\(prefs_food)")
            break
            
        default: ()
        }
    }
    
    func changeOtherPreferenceState(which: Int) {
        
        switch which {
        case 0:
            imvLadies.image = UIImage(named: "women" + "\(prefs_ladies)")
            break
            
        case 1:
            imvKid.image = UIImage(named: "children" + "\(prefs_kid)")
            
            break
            
        case 2:
            imvHandicapped.image = UIImage(named: "handicapped" + "\(prefs_handicapped)")
            break
            
        default: ()
        }
    }
    
    @IBAction func switchPreferences(sender: UISwitch) {
        
        switch sender {
            
        case swLadies:
            
            if(sender.on) {
                prefs_ladies = 1
            } else {
                prefs_ladies = 0
            }
            
            changeOtherPreferenceState(0)
            
            break
            
        case swKid:
            
            if(sender.on) {
                prefs_kid = 1
            } else {
                prefs_kid = 0
            }
            
            changeOtherPreferenceState(1)
            break
            
        case swHandicapped:
            
            if(sender.on) {
                prefs_handicapped = 1
            } else {
                prefs_handicapped = 0
            }
            
            changeOtherPreferenceState(2)
            break
            
        default: ()
        }
    }
    
    @IBAction func saveAction() {
        
        setPreferenceData(prefs.stringForKey("USER_HASH"))
    }
    
    func setPreferenceData(hash: String!) {
        
        let user_id: String = (_user.user_id == -1) ? hash : "\(_user.user_id)"
        
        let params = [
            "login_hash": hash,
            "user_id": user_id,
            "pref_data": [
                "chatting": "\(prefs_chat)",
                "food": "\(prefs_food)",
                "music": "\(prefs_music)",
                "pets": "\(prefs_pets)",
                "smoking": "\(prefs_smoking)",
                "women_only": "\(prefs_ladies)",
                "handicapped": "\(prefs_handicapped)",
                "children": "\(prefs_kid)"
            ]
        ]
        
        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/preference", parameters: params as? [String : AnyObject])
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/preference")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    if(status) {
                        
                        self.savePreference()
                        
                    } else {
                        print("Error parsing /settings/preference")
                    }
                }
        }
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    }
    
    func loadPreferences() {
        
        prefs_chat = _user.prefs_chat
        prefs_music = _user.prefs_music
        prefs_pets = _user.prefs_pets
        prefs_smoking = _user.prefs_smoking
        prefs_food = _user.prefs_food
        prefs_ladies = _user.prefs_ladies
        prefs_kid = _user.prefs_kid
        prefs_handicapped = _user.prefs_handicapped
        
        imvChat.image = UIImage(named: "chat" + "\(prefs_chat)")
        imvMusic.image = UIImage(named: "music" + "\(prefs_music)")
        imvPets.image = UIImage(named: "pets" + "\(prefs_pets)")
        imvSmoking.image = UIImage(named: "smoke" + "\(prefs_smoking)")
        imvFood.image = UIImage(named: "eat" + "\(prefs_food)")
        
        imvLadies.image = UIImage(named: "women" + "\(prefs_ladies)")
        imvKid.image = UIImage(named: "children" + "\(prefs_kid)")
        imvHandicapped.image = UIImage(named: "handicapped" + "\(prefs_handicapped)")
        
        btnChat.setTitle(chatDropDown.dataSource[prefs_chat], forState: .Normal)
        btnMusic.setTitle(chatDropDown.dataSource[prefs_music], forState: .Normal)
        btnPets.setTitle(chatDropDown.dataSource[prefs_pets], forState: .Normal)
        btnSmoking.setTitle(chatDropDown.dataSource[prefs_smoking], forState: .Normal)
        btnFood.setTitle(chatDropDown.dataSource[prefs_food], forState: .Normal)
        
        swLadies.on = prefs_ladies == 0 ? false : true
        swKid.on = prefs_kid == 0 ? false : true
        swHandicapped.on = prefs_handicapped == 0 ? false : true
    }    
    
    func savePreference() {
        
        _user.prefs_chat = prefs_chat
        _user.prefs_music = prefs_music
        _user.prefs_pets = prefs_pets
        _user.prefs_smoking = prefs_smoking
        _user.prefs_food = prefs_food
        _user.prefs_ladies = prefs_ladies
        _user.prefs_kid = prefs_kid
        _user.prefs_handicapped = prefs_handicapped
        
        _user.savePreferences()
    }
}




