//
//  ChangePasswordVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class ChangePasswordVC: UIViewController, UITextFieldDelegate {

    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var _user: UserInfo!
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    // ui outlet
    @IBOutlet weak var txtCurrentPwd: UITextField!
    @IBOutlet weak var txtNewPwd: UITextField!
    @IBOutlet weak var txtConfirmPwd: UITextField!
    
    @IBOutlet weak var lblChangePassword: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let settingstoryboard: UIStoryboard = UIStoryboard(name: "Setting_RightSlide", bundle:  nil)
            let rightMenuViewController = settingstoryboard.instantiateViewControllerWithIdentifier("SettingRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.change_password : R.en.change_password
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // set back button tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    func initView() {
        
        // current password textfield bottom line and placeholder
        let border1 = CALayer()
        let width = CGFloat(1.0)
        border1.borderColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.0).CGColor
        border1.frame = CGRect(x: 0, y: txtCurrentPwd.frame.size.height - width, width: txtCurrentPwd.frame.size.width, height: txtCurrentPwd.frame.height)
        border1.borderWidth = width
        txtCurrentPwd.layer.addSublayer(border1)
        txtCurrentPwd.layer.masksToBounds = true
        
        // current password textfield bottom line and placeholder
        let border2 = CALayer()
        border2.borderColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.0).CGColor
        border2.frame = CGRect(x: 0, y: txtCurrentPwd.frame.size.height - width, width: txtNewPwd.frame.size.width, height: txtNewPwd.frame.height)
        border2.borderWidth = width
        txtNewPwd.layer.addSublayer(border2)
        txtNewPwd.layer.masksToBounds = true
        
        // current password textfield bottom line and placeholder
        let border3 = CALayer()
        border3.borderColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.0).CGColor
        border3.frame = CGRect(x: 0, y: txtConfirmPwd.frame.size.height - width, width: txtConfirmPwd.frame.size.width, height: txtConfirmPwd.frame.height)
        border3.borderWidth = width
        txtConfirmPwd.layer.addSublayer(border3)
        txtConfirmPwd.layer.masksToBounds = true
        
        lblChangePassword.text = defLang == "fr" ? R.fr.change_password : R.en.change_password
        txtCurrentPwd.placeholder = defLang == "fr" ? R.fr.current_password : R.en.current_password
        txtNewPwd.placeholder = defLang == "fr" ? R.fr.new_password : R.en.new_password
        txtConfirmPwd.placeholder = defLang == "fr" ? R.fr.confirm_password : R.en.confirm_password
        
        btnSave.setTitle(defLang == "fr" ? R.fr.btn_save : R.en.btn_save, forState: .Normal)
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func saveAction(sender: UIButton) {
        
        self.view.endEditing(true)
        
        changePassword()
        
        print("saved")
    }
    
    func changePassword() {
        
        let currentPassword = txtCurrentPwd.text!
        let newPassword = txtNewPwd.text!
        let confirmPassword = txtConfirmPwd.text!
        
        if(currentPassword.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.input_current_password : R.en.input_current_password, duration: R.tDuration).show()
            return
        } else if(newPassword.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.input_new_password : R.en.input_new_password, duration: R.tDuration).show()
            return
        } else if(confirmPassword.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.input_confirm_password : R.en.input_confirm_password, duration: R.tDuration).show()
            return
        }
        
        let currentPlainPassword = (currentPassword as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let currentBase64Password = currentPlainPassword!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        let newPlainPassword = (newPassword as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let newBase64Password = newPlainPassword!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        // sending params
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "old_password": currentBase64Password,
            "new_password": newBase64Password
        ]        
        
        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/change_password", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil  else {
                    print("Error calling GET on /settings/change_password")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        JLToast.makeText("Your password has been succesfully changed.", duration: R.tDuration).show()
                        
                    } else {
                        print("Error parsing /settings/change_password")
                        JLToast.makeText("Your current password is incorrect.", duration: R.tDuration).show()
                    }
                } else {
                    print("Error parsing /settings/change_password")
                }
            })
    }
}
