//
//  VehicleSettingVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class VehicleSettingVC: UIViewController {

    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var _user: UserInfo!
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var lblTitleVehicleDetails: UILabel!
    @IBOutlet weak var lblTitleModel: UILabel!
    @IBOutlet weak var lblTitleLicensePlate: UILabel!
    @IBOutlet weak var lblTitleComfortLevel: UILabel!
    @IBOutlet weak var lblTitleAirConditions: UILabel!
    @IBOutlet weak var lblTitleCo2Out: UILabel!
    @IBOutlet weak var lblTitleFuelEfficiency: UILabel!
    @IBOutlet weak var lblTitleDistancePerYear: UILabel!
    @IBOutlet weak var lblTitleNumberOfPassengers: UILabel!
    @IBOutlet weak var lblTitleBikeRack: UILabel!
    @IBOutlet weak var lblTitleSkiRack: UILabel!
    @IBOutlet weak var lblTitleTypeOfTires: UILabel!
    @IBOutlet weak var lblTitlePicture: UILabel!
    
    @IBOutlet weak var lblLicensePlate: UILabel!
    @IBOutlet weak var viewComfortLevel: FloatRatingView!
    @IBOutlet weak var lblAirConditions: UILabel!
    @IBOutlet weak var lblCo2Output: UILabel!
    @IBOutlet weak var lblFuelEfficiency: UILabel!
    @IBOutlet weak var lblDistancePerYear: UILabel!
    
    @IBOutlet weak var lblPassengerSeat: UILabel!
    @IBOutlet weak var lblBikeRack: UILabel!
    @IBOutlet weak var lbbSkiRack: UILabel!
    @IBOutlet weak var lblTypeOfTires: UILabel!
    
    @IBOutlet weak var imvVehicle: UIImageView!
    
    @IBOutlet weak var btnCarSelection: UIButton!
    
    var carDropDown = DropDown()
    
    var selectedIdx = -1
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let settingstoryboard: UIStoryboard = UIStoryboard(name: "Setting_RightSlide", bundle:  nil)
            let rightMenuViewController = settingstoryboard.instantiateViewControllerWithIdentifier("SettingRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
//        navigationItem.title = defLang == "fr" ? R.fr.vehicle_settings : R.en.vehicle_settings
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        carDropDown.dataSource = []
        carDropDown.selectionAction = { [unowned self] (index, item) in
            
            self.loadDetails(index)
            
            self.selectedIdx = index
        }
        carDropDown.anchorView = btnCarSelection
        carDropDown.bottomOffset = CGPoint(x: 0, y: btnCarSelection.bounds.height + 3)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // set back button tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        getVehicleInfo()
    }
    
    func initView() {
        
        lblTitleVehicleDetails.text = defLang == "fr" ? R.fr.vehicle_details : R.en.vehicle_details
        lblTitleLicensePlate.text = defLang == "fr" ? R.fr.license_plate : R.en.license_plate
        lblTitleComfortLevel.text = defLang == "fr" ? R.fr.comfort_level : R.en.comfort_level
        lblTitleAirConditions.text = defLang == "fr" ? R.fr.air_conditions : R.en.air_conditions
        lblTitleCo2Out.text = defLang == "fr" ? R.fr.setting_co2_output : R.en.setting_co2_output
        lblTitleFuelEfficiency.text = defLang == "fr" ? R.fr.setting_fuel_efficiency : R.en.setting_fuel_efficiency
        lblTitleDistancePerYear.text = defLang == "fr" ? R.fr.setting_distance_driven : R.en.setting_distance_driven
        
        lblTitleNumberOfPassengers.text = defLang == "fr" ? R.fr.setting_seats_passenger : R.en.setting_seats_passenger
        lblTitleBikeRack.text = defLang == "fr" ? R.fr.bike_rack : R.en.bike_rack
        lblTitleSkiRack.text = defLang == "fr" ? R.fr.ski_rack : R.en.ski_rack
        lblTitleTypeOfTires.text = defLang == "fr" ? R.fr.type_tires : R.en.type_tires
        
        lblTitlePicture.text = defLang == "fr" ? R.fr.setting_picture : R.en.setting_picture
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    @IBAction func addCarAction(sender: AnyObject ) {
        
        let deststoryboard = UIStoryboard(name: "OfferLift", bundle:  nil)
        let destController = deststoryboard.instantiateViewControllerWithIdentifier("AddCarVC") as! AddCarVC
        destController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        destController.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        destController.bComesFromPriceVehicle = false
        destController.vehicleSettingVC = self
        
        destController.addorEditVehicle = 1
        
        self.presentViewController(destController, animated: true, completion: nil)
    }
    
    @IBAction func editCarAction() {
        
        if(selectedIdx < 0) {
            return
        }
        
        let deststoryboard = UIStoryboard(name: "OfferLift", bundle:  nil)
        let destController = deststoryboard.instantiateViewControllerWithIdentifier("AddCarVC") as! AddCarVC
        destController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        destController.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        destController.bComesFromPriceVehicle = false
        destController.vehicleSettingVC = self
        
        destController.addorEditVehicle = 0
        destController.edit_vehicle = _user.vehicle[selectedIdx]
        
        self.presentViewController(destController, animated: true, completion: nil)
    }
    
    @IBAction func deleteCarAction() {
        
        if(selectedIdx < 0) {
            return
        }
        
        let alertController = UIAlertController(title: defLang == "fr" ? R.fr.vehicle_delete_title : R.en.vehicle_delete_title, message: defLang == "fr" ? R.fr.vehicle_delete_alert : R.en.vehicle_delete_alert, preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: defLang == "fr" ? R.fr.alert_cancel : R.en.alert_cancel, style: .Cancel) { (action:UIAlertAction!) in

        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: defLang == "fr" ? R.fr.alert_ok : R.en.alert_ok, style: .Default) { (action:UIAlertAction!) in
            
            self .deleteCar()
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true, completion:nil)
        
    }
    
    func deleteCar() {
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let veh_id = _user.vehicle[selectedIdx].veh_id
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": "en",
            "veh_id": "\(veh_id)",
            "file_name": ""
        ]
        
        print(JSON(params))
        
        Alamofire.request(.GET, kBaseURL+"settings/veh_delete", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/veh_delete")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        
                        self.getVehicleInfo()
//                        self._user.vehicle.removeAtIndex(self.selectedIdx)
//                        self.carDropDown.dataSource.removeAtIndex(self.selectedIdx)
                        
                        self.loadDetails(0)
                        
                    } else {
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        print("Error parsing /settings/veh_delete")
                    }
                }
            })
    }
    
    @IBAction func carSelection(sender: UIButton) {
        
        if(carDropDown.hidden) {
            carDropDown.show()
        } else {
            carDropDown.hide()
        }
    }
    
    func getVehicleInfo() {
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": "en"
        ]
 
        
        Alamofire.request(.GET, kBaseURL+"settings/vehicle", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/vehicle")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {

                    if(status) {
                        
                        if let _vehiclecount = jsonResponse["veh_data"].array?.count {

                            if(_vehiclecount > 0) {

                                self._user.vehicle.removeAll()
                                self.carDropDown.dataSource.removeAll()

                                for var index = 0; index < _vehiclecount; index++ {

                                    let _vehicle = Vehicle()

                                    _vehicle.veh_id = Int(jsonResponse["veh_data"][index]["id"].string!)!
                                    _vehicle.year = jsonResponse["veh_data"][index]["year_built"].string!
                                    _vehicle.make_name = jsonResponse["veh_data"][index]["make_name"].string!
                                    _vehicle.model_name = jsonResponse["veh_data"][index]["model_name"].string!

                                    let body_type: Int = Int(jsonResponse["veh_data"][index]["veh_body_type"].string!)!

                                    switch body_type {
                                    case 1:
                                        _vehicle.body_type = "Coupe"
                                        break

                                    case 2:
                                        _vehicle.body_type = "Sedan"
                                        break

                                    case 3:
                                        _vehicle.body_type = "SUV"
                                        break

                                    case 4:
                                        _vehicle.body_type = "Van"
                                        break

                                    case 5:
                                        _vehicle.body_type = "Wagon"
                                        break

                                    case 6:
                                        _vehicle.body_type = "Convertible"
                                        break

                                    case 7:
                                        _vehicle.body_type = "Hybrid Car"
                                        break

                                    case 8:
                                        _vehicle.body_type = "Luxury Car"
                                        break

                                    case 9:
                                        _vehicle.body_type = "Electric Car"
                                        break

                                    default: ()
                                    }

                                    _vehicle.color = jsonResponse["veh_data"][index]["color"].string!

                                    _vehicle.makeDetailName()

                                    let vehiclename = _vehicle.year + " " + _vehicle.make_name + "," + _vehicle.model_name + ", " + _vehicle.body_type + ", " + _vehicle.color

                                    _vehicle.license_plate = jsonResponse["veh_data"][index]["number_plate"].string!
                                    _vehicle.comfortLevel = Float(Int(jsonResponse["veh_data"][index]["comfort_level"].string!)!)
                                    
                                    if(Int(jsonResponse["veh_data"][0]["ac"].string!)! == 0) {
                                        
                                        _vehicle.airCondition = self.defLang == "fr" ? R.fr.no : R.en.no
                                    } else {
                                        _vehicle.airCondition = self.defLang == "fr" ? R.fr.yes : R.fr.yes
                                    }
                                    
                                    _vehicle.co2_output = jsonResponse["veh_data"][index]["co2_output"].string! + " Kg"
                                    _vehicle.fuel_efficiency = jsonResponse["veh_data"][index]["fuel_efficiency"].string! + " L/100KM"
                                    _vehicle.distance_per_year = jsonResponse["veh_data"][index]["distance_driven_per_year"].string! + " KM"
                                    
                                    _vehicle.seats = Int(jsonResponse["veh_data"][index]["seats"].string!)!
                                    _vehicle.transmission = Int(jsonResponse["veh_data"][index]["transmission"].string!)!
                                    _vehicle.mileage = Int(jsonResponse["veh_data"][index]["mileage"].string!)!
                                    _vehicle.luggageSize = Int(jsonResponse["veh_data"][index]["luggage_size"].string!)!
                                    _vehicle.license_number = jsonResponse["veh_data"][index]["license_number"].string!
                                    
                                    _vehicle.ratedValue = Int(jsonResponse["veh_data"][index]["comfort_level"].string!)!
                                    
                                    if(Int(jsonResponse["veh_data"][index]["tyre_type"].string!) == 1) {
                                        
                                        _vehicle.type_tires = "Normal"
                                    } else {
                                        
                                        _vehicle.type_tires = self.defLang == "fr" ? "Hiver" : "Winter"
                                    }
                                    
                                    if(Int(jsonResponse["veh_data"][index]["ski_rack"].string!) == 0) {
                                        _vehicle.skiRack = self.defLang == "fr" ? R.fr.no : R.en.no
                                    } else {
                                        _vehicle.skiRack = self.defLang == "fr" ? R.fr.yes : R.en.yes
                                    }
                                    
                                    if(Int(jsonResponse["veh_data"][index]["bike_rack"].string!) == 0) {
                                        _vehicle.bikeRack = self.defLang == "fr" ? R.fr.no : R.en.no
                                    } else {
                                        _vehicle.bikeRack = self.defLang == "fr" ? R.fr.yes : R.en.yes
                                    }
                                    
                                    _vehicle.veh_pic = jsonResponse["veh_data"][index]["vehicle_image"].string!
                                    
                                    self.carDropDown.dataSource.append(vehiclename)
                                    self._user.vehicle.append(_vehicle)
                                }
                             
                                self.loadDetails(0)
                            }
                        }
                        
                    } else {
                        print("Error parsing /settings/vehicle")
                    }
                }

                
            })
    }

    func loadDetails(index: Int) {
        
        carDropDown.reloadAllComponents()
        carDropDown.selectRowAtIndex(index)
        
        selectedIdx = index
        
        lblTitleModel.text = _user.vehicle[index].detail_name
        
        lblLicensePlate.text = _user.vehicle[index].license_plate
        viewComfortLevel.rating = _user.vehicle[index].comfortLevel
        lblAirConditions.text = _user.vehicle[index].airCondition
        lblCo2Output.text = _user.vehicle[index].co2_output
        lblFuelEfficiency.text = _user.vehicle[index].fuel_efficiency
        lblDistancePerYear.text = _user.vehicle[index].distance_per_year
        lblPassengerSeat.text = String(_user.vehicle[index].seats)
        lblBikeRack.text = _user.vehicle[index].bikeRack
        lbbSkiRack.text = _user.vehicle[index].skiRack
        lblTypeOfTires.text = _user.vehicle[index].type_tires
        
        imvVehicle.setImageWithUrl(NSURL(string: _user.vehicle[index].veh_pic)!, placeHolderImage: UIImage(named: "car_place.png"))
        
    }
}

