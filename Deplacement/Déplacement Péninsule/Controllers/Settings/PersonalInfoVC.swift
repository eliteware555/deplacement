//
//  SettingViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

import Alamofire
import JLToast
import SwiftyJSON

class PersonalInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate {

    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var _user: UserInfo!
    
    let extra_cell_height = 45
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    // ui outlet (personal info)
    @IBOutlet weak var imvProfile: UIImageView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var tblPhoneNumber: UITableView!
    @IBOutlet weak var tblEmail: UITableView!
    
    @IBOutlet weak var txtAddressLine1: UITextField!
    @IBOutlet weak var txtAddressLine2: UITextField!
    
    @IBOutlet weak var txtCity: AutoCompleteTextField!
    @IBOutlet weak var txtProvince: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    

    @IBOutlet weak var txvShortBio: UITextView!
    
    // constraint outlet
    @IBOutlet weak var layout_height_tblPhoneNumber: NSLayoutConstraint!    // original 70
    @IBOutlet weak var layout_height_tblEmail: NSLayoutConstraint!          // original 70
    @IBOutlet weak var layout_height_personalInfo: NSLayoutConstraint!      // original 640
    @IBOutlet weak var layout_height_superview: NSLayoutConstraint!         // original 1260
    
    var phoneFieldCnt = 1
    var emailFieldCnt  = 1
    
    // additional info
    @IBOutlet weak var imvSmoker: UIImageView!
    @IBOutlet weak var imvNoSmoker: UIImageView!
    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var txtOccupation: UITextField!
    @IBOutlet weak var imvEnglish: UIImageView!
    @IBOutlet weak var imvFrench: UIImageView!
    @IBOutlet weak var imvSingle: UIImageView!
    @IBOutlet weak var imvMarried: UIImageView!
    @IBOutlet weak var imvMale: UIImageView!
    @IBOutlet weak var imvFemale: UIImageView!
    
    @IBOutlet weak var swShareMobile: UISwitch!
    @IBOutlet weak var swShareEmail: UISwitch!
    
    @IBOutlet weak var tapGesture: UITapGestureRecognizer!
    
    var picker: UIImagePickerController? = UIImagePickerController()
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // sending params
    var _smoker: Int = 0 // no smoker (1 - smoker)
    var _gender: Int = 1 // Male ( 0 - female)
    var _marital: Int = 0 // Signle  ( 1 - Married)
    var _languages: Array<String> = ["", ""]
    var _sharePhoneNumber: Int = 1
    var _shareEmailAddress: Int = 1
    var _phoneNumber: String = ""
    var _email: String = ""
    var _extra_email: [String] = ["", ""]
    var _extra_phone: [String] = ["", ""]
    
    @IBOutlet weak var lblPersonalInfo : UILabel!
    @IBOutlet weak var lblCity:  UILabel!
    @IBOutlet weak var lblProvince: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    
    
    // ----------------------------
    @IBOutlet weak var lblAdditionalInformation: UILabel!
    @IBOutlet weak var lblSmoker: UILabel!
    @IBOutlet weak var lblSmokerYes: UILabel!
    @IBOutlet weak var lblSmokerN0: UILabel!
    
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var lblFrench: UILabel!
    @IBOutlet weak var lblMaritalStatus: UILabel!
    @IBOutlet weak var lblSingle: UILabel!
    @IBOutlet weak var lblMarried: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblShareDescription: UILabel!
    @IBOutlet weak var lblShareMobilePhone: UILabel!
    @IBOutlet weak var lblShareEmail: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!

    let dropdownProvince = DropDown()
    let dropdownCountry = DropDown()

    @IBOutlet weak var btnProvince: UIButton!
    @IBOutlet weak var btnCountry: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let settingstoryboard: UIStoryboard = UIStoryboard(name: "Setting_RightSlide", bundle:  nil)
            let rightMenuViewController = settingstoryboard.instantiateViewControllerWithIdentifier("SettingRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"

            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.personal_info : R.en.personal_info
        
        imvProfile.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        // initialize acitivity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 200
        
        configureAutoCompleteTexTField()
        handleAutoCompletedTextField()
        
        dropdownProvince.dataSource = [defLang == "fr" ? R.fr.province_name : R.en.province_name]
        dropdownProvince.selectionAction = { [unowned self] (index, item) in
            
            self.txtProvince.text = item
        }
        dropdownProvince.anchorView = btnProvince
        dropdownProvince.bottomOffset = CGPoint(x:0, y: btnProvince.bounds.height + 3)
        
        dropdownCountry.dataSource = [defLang == "fr" ? R.fr.country_name : R.en.country_name]
        dropdownCountry.selectionAction = { [unowned self] (index, item) in
            
            self.txtCountry.text = item
        }
        dropdownCountry.anchorView = btnCountry
        dropdownCountry.bottomOffset = CGPoint(x:0, y: btnCountry.bounds.height + 3)
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        let paddingView: UIView = UIView(frame: CGRectMake(0, 0, 50, 30))
        txtCity.leftView = paddingView
        txtCity.leftViewMode = .Always
        
        getUserDetails(prefs.stringForKey("USER_HASH")!)
        loadUserDetails()
        
        tapGesture.cancelsTouchesInView = false

        imvProfile.setImageWithUrl(NSURL(string: _user.user_pic)!, placeHolderImage: UIImage(named: _user.gender == 1 ? "male_profile" : "female_profile"))
    }
    
    private func configureAutoCompleteTexTField() {
        
        // get city strings from plist
   
        txtCity.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        txtCity.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        txtCity.autoCompleteCellHeight = 35.0
        txtCity.maximumAutoCompleteCount = 20
        txtCity.hidesWhenSelected = true
        txtCity.hidesWhenEmpty = true
        txtCity.clearButtonMode = .Never
        txtCity.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        txtCity.autoCompleteAttributes = attributes
    }
    
    private func handleAutoCompletedTextField() {
        
        txtCity.onTextChange = { text in
            if !text.isEmpty {
                
                var cityData = [String]()
                
                let path = NSBundle.mainBundle().pathForResource("city", ofType: "plist")
                let dict = NSDictionary(contentsOfFile: path!)
                
                cityData = dict?.objectForKey("CityName") as! [String]
                
                let filteredStrings = cityData.filter({(item: String) -> Bool in
                    
                    let stringMatch = item.lowercaseString.rangeOfString(text.lowercaseString)
                    return stringMatch != nil ? true : false
                })
                
                self.txtCity.autoCompleteStrings = filteredStrings
            }
        }
        
        txtCity.onSelect = { text, indexpath in

//            self.txtCity.resignFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        initView()
    }
    
    // for multi-language
    func initView() {
        
        lblPersonalInfo.text = defLang == "fr" ? R.fr.personal_info : R.en.personal_info
        txtFirstName.placeholder = defLang == "fr" ? R.fr.first_name : R.en.first_name
        txtLastName.placeholder = defLang == "fr" ? R.fr.last_name : R.en.last_name
        
        txtAddressLine1.placeholder = defLang == "fr" ? R.fr.addrees_line + "\(1)" : R.en.addrees_line + "\(1)"
        txtAddressLine2.placeholder = defLang == "fr" ? R.fr.addrees_line + "\(2)" : R.en.addrees_line + "\(2)"
        
        lblCity.text = defLang == "fr" ? R.fr.city : R.en.city
        lblProvince.text = defLang == "fr" ? R.fr.province : R.en.province
        lblCountry.text = defLang == "fr" ? R.fr.country : R.en.country
        
        txvShortBio.placeholder = defLang == "fr" ? R.fr.short_bio : R.en.short_bio
        
        lblAdditionalInformation.text = defLang == "fr" ? R.fr.additional_information : R.en.additional_information
        
        lblSmoker.text = defLang == "fr" ? R.fr.smoker : R.en.smoker
        lblSmokerYes.text = defLang == "fr" ? R.fr.yes : R.en.yes
        lblSmokerN0.text = defLang == "fr" ? R.fr.no : R.en.no
        
        lblDOB.text = defLang == "fr" ? R.fr.dob : R.en.dob
//        lblOccupation.text = defLang == "fr" ? R.fr.occupation : R.en.occupation
        txtOccupation.placeholder = defLang == "fr" ? R.fr.occupation : R.en.occupation
        
        lblLanguage.text = defLang == "fr" ? R.fr.language : R.en.language
        lblEnglish.text = defLang == "fr" ? R.fr.english : R.en.english
        lblFrench.text = defLang == "fr" ? R.fr.french : R.en.french
        
        lblMaritalStatus.text = defLang == "fr" ? R.fr.marital_status : R.en.marital_status
        lblSingle.text = defLang == "fr" ? R.fr.single : R.en.single
        lblMarried.text = defLang == "fr" ? R.fr.married : R.en.married
        
        lblGender.text = defLang == "fr" ? R.fr.gender : R.en.gender
        lblMale.text = defLang == "fr" ? R.fr.male : R.en.male
        lblFemale.text = defLang == "fr" ? R.fr.female : R.en.female
        
        lblShareDescription.text = defLang == "fr" ? R.fr.share_information : R.en.share_information
        lblShareMobilePhone.text = defLang == "fr" ? R.fr.share_mobile_phone : R.en.share_mobile_phone
        lblShareEmail.text = defLang == "fr" ? R.fr.share_email_address : R.en.share_email_address
        
        btnSave.setTitle(defLang == "fr" ? R.fr.btn_save : R.en.btn_save, forState: .Normal)
    }
    
    // remove activity indicator from view
    func removeActivityView() {
        
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(200)?.removeFromSuperview()
    }
    
    @IBAction func showProvinceMenu(sender: UIButton) {
        
        if(dropdownProvince.hidden) {
            dropdownProvince.show()
        } else {
            dropdownProvince.hide()
        }
    }
    
    @IBAction func showCountryMenu(sender: UIButton) {
        
        if(dropdownCountry.hidden) {
            dropdownCountry.show()
        } else {
            dropdownCountry.hide()
        }
    }
    
    // tap action for upload user's profile image
    @IBAction func profileImageSetAction(sender: AnyObject) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .Default) { (alert: UIAlertAction) -> Void in
            self.openCamera()
        }
        let albumAction = UIAlertAction(title: "Gallery", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
        }
        
        picker?.delegate = self
        
        alert.addAction(cameraAction)
        alert.addAction(albumAction)
        alert.addAction(cancelAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    //
    func openGallery() {
        picker?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker?.allowsEditing = true
        self.presentViewController(picker!, animated: true, completion: nil)
    }
    
    func openCamera() {
    
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            
            picker?.sourceType = UIImagePickerControllerSourceType.Camera
            picker?.allowsEditing = true
            picker?.modalPresentationStyle = .FullScreen
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    // smoker
    @IBAction func smokerSelected(sender: UIButton) {
        
        if(_smoker == 1) {
            return
        }
        
        _smoker = 1
        setSmoker(_smoker)
    }
    
    // no smoker
    @IBAction func  noSmokerSelected(sneder: UIButton) {
        
        if(_smoker == 0) {
            return
        }
        
        _smoker = 0
        setSmoker(_smoker)
    }
    
    func setSmoker(isSmoker: Int) {
        
        if(isSmoker == 1) {
            imvSmoker.image = UIImage(named: "checked1.png")
            imvNoSmoker.image = UIImage(named: "unchecked1.png")
        } else {
            imvSmoker.image = UIImage(named: "unchecked1.png")
            imvNoSmoker.image = UIImage(named: "checked1.png")
        }
    }
    
    @IBAction func englishSelected(sender: UIButton) {
        
        if(_languages[0] == "") {
            _languages[0] = "english"
        } else {
            _languages[0] = ""
        }
        
        selectEnglish()

//        print(_languages)
    }
    
    func selectEnglish() {
        
        if(_languages[0] == "") {
            imvEnglish.image = UIImage(named: "unchecked2.png")
        } else {
            imvEnglish.image = UIImage(named: "checked2.png")
        }
    }
    
    func selectFrench() {
        
        if(_languages[1] == "") {
            imvFrench.image = UIImage(named: "unchecked2.png")
        } else {
            imvFrench.image = UIImage(named: "checked2.png")
        }
    }
    
    @IBAction func frenchSelected(sender: UIButton) {
        
        if(_languages[1] == "") {
            _languages[1] = "French"
        } else {
            _languages[1] = ""
        }
        
        selectFrench()
        
//        print(_languages)
    }
    
    // single selected
    @IBAction func bachelorSelected(sender: UIButton) {
        
        if(_marital == 0) {
            return
        }
        
        _marital = 0
        setMaritalStatus(_marital)
    }
    
    // married selected
    @IBAction func marriedSelected(sender: UIButton) {
        
        if(_marital == 1) {
            return
        }
        
        _marital = 1
        setMaritalStatus(_marital)
    }
    
    func setMaritalStatus(isMarrided: Int) {
        
        _marital = isMarrided;
        
        if(isMarrided == 1) {
            
            imvMarried.image = UIImage(named: "checked1.png")
            imvSingle.image = UIImage(named: "unchecked1.png")
            
        } else {
            imvMarried.image = UIImage(named: "unchecked1.png")
            imvSingle.image = UIImage(named: "checked1.png")
        }
    }
    
    // Male selected
    @IBAction func maleSelected(sender: UIButton) {
        
        if(_gender == 1) {
            return
        }
        
        _gender = 1
        setGender(_gender)
    }
    
    // Female selected
    @IBAction func femaleSelected(sender: UIButton) {
        
        if(_gender == 0) {
            return
        }
        
        _gender = 0
        setGender(_gender)
    }
    
    func setGender(isMale: Int) {
        
        if(isMale == 1) {
            
            imvMale.image = UIImage(named: "checked1.png")
            imvFemale.image = UIImage(named: "unchecked1.png")
        } else {
            imvMale.image = UIImage(named: "unchecked1.png")
            imvFemale.image = UIImage(named: "checked1.png")
        }
    }
    
    // toggle action for sharing mobile phone number
    @IBAction func shareMobilePhone(sender: AnyObject) {
        
        if(swShareMobile.on) {
            _sharePhoneNumber = 1
        } else {
            _sharePhoneNumber = 0
        }
    }
    
    func setShareMobile(isShare: Int) {
        
        if(isShare == 1 && !swShareMobile.on) {
            swShareMobile.setOn(true, animated: true)
        } else if(isShare == 0 && swShareMobile.on){
            swShareMobile.setOn(false, animated: true)
        }
    }
    
    // toggle action for sharing email
    @IBAction func shareEmailAction(sender: AnyObject) {
        
        if(swShareEmail.on) {
            _shareEmailAddress = 1
        } else {
            _shareEmailAddress = 0
        }
    }
    
    func setShareEmail(isShare: Int) {
        
        if(isShare == 1 && !swShareEmail.on) {
            swShareEmail.setOn(true, animated: true)
        } else if(isShare == 0 && swShareEmail.on) {
            swShareEmail.setOn(false, animated: true)
        }
    }
    
    // add a phone number input field
    @IBAction func addroDeletePhoneField(sender: UIButton) {
        
        let index: Int = sender.tag - 100
        
        if(index == 0) {
            
            // add a new field for inputing a phone number
            if(phoneFieldCnt == 3) {
                return
            }
            
            phoneFieldCnt++
            
            // increase the height of phone number input field tableview, information view, and super view
            layout_height_tblPhoneNumber.constant += CGFloat(extra_cell_height)
            layout_height_personalInfo.constant += CGFloat(extra_cell_height)
            layout_height_superview.constant += CGFloat(extra_cell_height)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
            tblPhoneNumber.reloadData()
            
        } else {
            
            phoneFieldCnt--
            
            tblPhoneNumber.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
            
            // decrease the height of phone number input field tableview, information view, and super view
            layout_height_tblPhoneNumber.constant -= CGFloat(extra_cell_height)
            layout_height_personalInfo.constant -= CGFloat(extra_cell_height)
            layout_height_superview.constant -= CGFloat(extra_cell_height)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
            tblPhoneNumber.reloadData()
        }
        
    }
    
    // add a email input field
    @IBAction func addorDeleteEmailField(sender: UIButton) {
        
        let index: Int = sender.tag - 110
        
        if(index == 0) {
            
            // add a new field for inputing a phone number
            if(emailFieldCnt == 3) {
                return
            }
            
            emailFieldCnt++
            
            // increase the height of phone number input field tableview, information view, and super view
            layout_height_tblEmail.constant += CGFloat(extra_cell_height)   // 70
            layout_height_personalInfo.constant += CGFloat(extra_cell_height)
            layout_height_superview.constant += CGFloat(extra_cell_height)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
            tblEmail.reloadData()
            
        } else {
            
            emailFieldCnt--
            
            tblEmail.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
            
            // decrease the height of phone number input field tableview, information view, and super view
            layout_height_tblEmail.constant -= CGFloat(extra_cell_height)
            layout_height_personalInfo.constant -= CGFloat(extra_cell_height)
            layout_height_superview.constant -= CGFloat(extra_cell_height)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
            tblEmail.reloadData()
        }
    }
    
    // ---------------------------------------------------------------------------------------
    // MARK: - UITableView Datasource
    // ---------------------------------------------------------------------------------------
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if(indexPath.row == 0) {
            return 85 // original 70
        } else {
            return CGFloat(extra_cell_height)
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tblPhoneNumber) {
            return phoneFieldCnt
        } else {
            return emailFieldCnt
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: AddPhoneEmailCell!
        
        if(tableView == self.tblPhoneNumber) {
            
            cell = tableView .dequeueReusableCellWithIdentifier("AddPhoneCell", forIndexPath: indexPath) as! AddPhoneEmailCell
            
            cell.txtPhoneorEmail.placeholder = defLang == "fr" ? R.fr.phone_number : R.en.phone_number
            
            cell.lblDescription.text = defLang == "fr" ? R.fr.phone_description : R.en.phone_description
            
            if(indexPath.row == 0) {
                cell.txtPhoneorEmail.text = self._phoneNumber
            } else {
                cell.txtPhoneorEmail.text = self._extra_phone[indexPath.row - 1]
            }
            
            cell.btnAdd.tag = 100 + indexPath.row
            cell.txtPhoneorEmail.tag = 105 + indexPath.row
            
        } else {
            
            cell = tableView .dequeueReusableCellWithIdentifier("AddEmailCell", forIndexPath: indexPath) as! AddPhoneEmailCell
            
            cell.txtPhoneorEmail.placeholder = defLang == "fr" ? R.fr.email_address : R.en.email_address
            
            cell.lblDescription.text = defLang == "fr" ? R.fr.email_description : R.en.email_description
            
            if(indexPath.row == 0) {
                cell.txtPhoneorEmail.text = self._email
            } else {
                cell.txtPhoneorEmail.text = self._extra_email[indexPath.row - 1]
            }
            
            cell.btnAdd.tag = 110 + indexPath.row
            cell.txtPhoneorEmail.tag = 115 + indexPath.row
        }
        
        if(indexPath.row != 0) {
            cell.lblDescription.hidden = true
            cell.imvAdd.image = UIImage(named: "close.png")
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        if(indexPath.row == 0) {
            return false
        } else {
            return true
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.Delete) {            

        }
    }
    
    // ---------------------------------------------------------------------------------------
    // MARK: - UIImagePickerController Delegate
    // ---------------------------------------------------------------------------------------
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let newImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            self.imvProfile.contentMode = .ScaleAspectFit
            
//            print(newImage.size)
            
            self.imvProfile.image = newImage.resizedImageByMagick("128x128#")
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // ---------------------------------------------------------------------------------------
    // MARK: - UITextField Delegate
    // ---------------------------------------------------------------------------------------
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if(textField == txtBirthday) {
//            let date: NSDate = NSDate()
//            let dateFormatter:NSDateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat = "MM-dd-yyyy"
//            let dateString: String = dateFormatter.stringFromDate(date)
            
//            textField.text = dateString
            
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Date
            
            if(defLang == "fr") {
                datePickerView.locale = NSLocale(localeIdentifier: "fr_FR")
            }
            
            textField.inputView = datePickerView
            
            if(textField.text?.characters.count != 0) {
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                if let _birthday = dateFormatter.dateFromString(textField.text!) {
                    
                    datePickerView.setDate(_birthday, animated: true)
                }
            }
            
            datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        } 
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        if(txtBirthday.isFirstResponder()) {
            txtBirthday.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if(textField.tag == 105) {
            _phoneNumber = textField.text!
        } else if(textField.tag == 106 || textField.tag == 107) {
            _extra_phone[textField.tag - 106] = textField.text!
        } else if (textField.tag == 115) {
            _email = textField.text!
        } else if (textField.tag == 116 || textField.tag == 117) {
            _extra_email[textField.tag - 116] = textField.text!
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if(textField == txtFirstName) {
            txtLastName.becomeFirstResponder()
        }
        
        if(textField == txtAddressLine1) {
            txtAddressLine2.becomeFirstResponder()
        }
        
        textField .resignFirstResponder()
        
        return true
    }
    
    // ---------------------------------------------------------------------------------------
    // MARK: - save action
    // ---------------------------------------------------------------------------------------
    @IBAction func saveAction(sender: UIButton) {
        
        if(txtFirstName.text?.characters.count == 0 && txtFirstName.text?.characters.count
             == 0) {
            JLToast.makeText(self.defLang == "fr" ? R.fr.require_name : R.en.require_name, duration: R.tDuration).show()
            return
        } else if(_phoneNumber.characters.count == 0) {
            JLToast.makeText(self.defLang == "fr" ? R.fr.require_phone_number : R.en.require_phone_number, duration: R.tDuration).show()
            return
        } else if(_email.characters.count == 0) {
            JLToast.makeText(self.defLang == "fr" ? R.fr.require_email : R.en.require_email, duration: R.tDuration).show()
            return
        } else if(txtCity.text?.characters.count == 0) {
            JLToast.makeText(self.defLang == "fr" ? R.fr.require_city : R.en.require_city, duration: R.tDuration).show()
            return
        }  else if(_languages[0] == "" && _languages[1] == "") {
            JLToast.makeText(self.defLang == "fr" ? R.fr.require_language : R.en.require_language, duration: R.tDuration).show()
            return
        }
        
//        else if(txtBirthday.text?.characters.count == 0) {
//            JLToast.makeText(self.defLang == "fr" ? R.fr.require_birthday : R.en.require_birthday, duration: 2).show()
//            return
//        }
        
        var language_setting = _languages[0]
        if(language_setting == "") {
            language_setting = _languages[1]
        } else {
            language_setting = _languages[0] + "," + _languages[1]
        }
        
        let params: [String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "city": txtCity.text!,
            "dob": txtBirthday.text!,
            "email": _email,
            "first_name": txtFirstName.text!,
            "last_name": txtLastName.text!,
            "maritial_status": "\(_marital)",
            "gender": "\(_gender)" ,
            "phone": "\(_phoneNumber)" ,
            "smoker": "\(_smoker)" ,
            "email_public": "\(_shareEmailAddress)",
            "phone_public": "\(_sharePhoneNumber)",
            "language": language_setting,
            "user_description": txvShortBio.text!,
            "occupation": txtOccupation.text!,
            "extra_email1": _extra_email[0],
            "extra_email2": _extra_email[1],
            "extra_phone1": _extra_phone[0],
            "extra_phone2": _extra_phone[1],
            "address_line": [
                "address1": txtAddressLine1.text!,
                "address2": txtAddressLine2.text!
            ],
            "site_lang": defLang
        ]
        
//        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/personal_info", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /search/list")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
//                print("The JSON is: \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let message = jsonResponse["message"].string {
                            
//                            JLToast.makeText(message, duration: R.tDuration).show()
                            
                            self.showAlert(message)
                            
                            self.saveSetting()
                        }
                        
                    } else {
                        
                        if let message = jsonResponse["message"].string {
                            JLToast.makeText(message, duration: R.tDuration).show()
                        } else {
                            print("Error parsing /settings/personal_info")
                        }
                    }
                }
        }
    }
    
    func showAlert(message: String!) {
        
        // Initialize Alert Controller
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        
        alertController.view.tintColor = UIColor.darkGrayColor()
        
        // Initialize Actions
        let yesString = defLang == "fr" ? R.fr.yes : R.en.yes
        let yesAction = UIAlertAction(title: yesString, style: .Default) { (action) -> Void in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // Add Actions
        alertController.addAction(yesAction)
        
        // Present Alert Controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func saveSetting() {
        
        _user.firstName = txtFirstName.text!
        _user.lastName = txtLastName.text!
        _user.email = _email
        _user.phoneNumber = _phoneNumber
        
        _user.city = txtCity.text!
        _user.province = txtProvince.text!
        _user.country = txtCountry.text!
        
        _user.occupation = txtOccupation.text!
        _user.language = _languages
        _user.smoker = _smoker
        _user.extra_email = _extra_email
        _user.extra_phone = _extra_phone
    
        _user.addressLine.removeAll()
        _user.addressLine.append(txtAddressLine1.text!)
        _user.addressLine.append(txtAddressLine2.text!)
        
        _user.shortBio = txvShortBio.text!
        _user.birthday = txtBirthday.text!
        _user.gender = _gender
        _user.shareEmail = _shareEmailAddress
        _user.shareMobile = _sharePhoneNumber
        
        // save _user with setting value
        _user.save()
    }
   
    func getUserDetails(hash: String!) {
    
        let params = [
            "hash_key": hash,
            "site_lang": defLang
        ]
        
//        debugPrint(params)
    
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
    
        Alamofire.request(.GET, kBaseURL + "users/user", parameters: params)
            .validate()
            .responseJSON { response in
    
                self.removeActivityView()
    
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
    
                guard response.result.error == nil else {
                    print("Error calling GET on /users/user")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
    
                let jsonResponse = JSON(value)
                print(jsonResponse)
    
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
    
                        // set the userinfo with received values
                        // add extra email
                        if let extra_email = jsonResponse["user_data"]["extra_email"].array {

                            if(extra_email.count > 0) {

                                self.emailFieldCnt = extra_email.count + 1
                                self._user.extra_email.removeAll()
                                for var index = 0; index < extra_email.count; index++ {
                                    self._user.extra_email.append(jsonResponse["user_data"]["extra_email"][index].string!)
                                }
                            }
                        }

                        //  add extra phone
                        if let extra_phone = jsonResponse["user_data"]["extra_phone"].array {

                            if(extra_phone.count > 0) {

                                self.phoneFieldCnt = extra_phone.count + 1
                                self._user.extra_phone.removeAll()
                                for var index = 0; index < extra_phone.count; index++ {
                                    self._user.extra_email.append(jsonResponse["user_data"]["extra_phone"][index].string!)
                                }
                            }
                        }

                        // add addressLine
                        if let addressLine = jsonResponse["user_data"]["address_line"].array {

                            if(addressLine.count > 0) {

                                self._user.addressLine.removeAll()
                                for var index = 0; index < addressLine.count; index++ {
                                    self._user.addressLine.append(jsonResponse["user_data"]["address_line"][index].string!)
                                }
                            }
                        }
                        
                        // language
                        if let language = jsonResponse["user_data"]["language"].array {
                            if(language.count > 0) {
                                for var index = 0; index < language.count; index++ {
                                    
                                    let user_language = jsonResponse["user_data"]["language"][index].string!
                                    
                                    if(user_language == "english" || user_language == "English") {
                                        self._user.language[0] = user_language
                                    } else if(user_language == "french" || user_language == "French") {
                                        self._user.language[1] = user_language
                                    }
                                }
                            }
                        }
                        
                        if let _ = jsonResponse["user_data"]["first_name"].string {
                            self._user.firstName = jsonResponse["user_data"]["first_name"].string!
                        }
                        
                        if let _ = jsonResponse["user_data"]["last_name"].string {
                            self._user.lastName = jsonResponse["user_data"]["last_name"].string!
                        }
                        
                        if let _ = jsonResponse["user_data"]["phone"].string {
                            self._user.phoneNumber = jsonResponse["user_data"]["phone"].string!
                        }
                        
                        if let _ = jsonResponse["user_data"]["username"].string {
                            self._user.email = jsonResponse["user_data"]["username"].string!
                        }
                        
                        if let _ = jsonResponse["user_data"]["city"].string {
                            self._user.city = jsonResponse["user_data"]["city"].string!
                        }
                        
                        self._user.country = self.defLang == "fr" ? R.fr.country_name : R.en.country_name
                        self._user.province = self.defLang == "fr" ? R.fr.province_name : R.en.province_name
                        
//                        self._user.province = jsonResponse["user_data"]["province"].string! == "0" ? "" : jsonResponse["user_data"]["province"].string!
//                        self._user.country = jsonResponse["user_data"]["country"].string! == "0" ? "" : jsonResponse["user_data"]["country"].string!
//                        self._user.shortBio = jsonResponse["user_data"]["user_description"].string!
                        
                        if let _ = jsonResponse["user_data"]["user_description"].string {
                            
                            self._user.shortBio = jsonResponse["user_data"]["user_description"].string!
                        }
                        
                        
                        if let _ = jsonResponse["user_data"]["occupation"].string {
                            self._user.occupation = jsonResponse["user_data"]["occupation"].string!
                        }
                        
                        if let _ = jsonResponse["user_data"]["dob"].string {
                            
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            
                            if let _birthday = dateFormatter.dateFromString(jsonResponse["user_data"]["dob"].string!) {
                            
                                dateFormatter.dateFormat = "MM-dd-yyyy"
                                self._user.birthday = dateFormatter.stringFromDate(_birthday)
                            }
                            
                        }
                        
                        if let _ = jsonResponse["user_data"]["maritial_status"].string {
                        
                            self._user.marital_status = Int(jsonResponse["user_data"]["maritial_status"].string!)!
                        }
                        
                        if let _ = jsonResponse["user_data"]["gender"].string {
                            
                            self._user.gender = Int(jsonResponse["user_data"]["gender"].string!)!
                        }

                        if let _ = jsonResponse["user_data"]["phone_public"].string {
                            
                            self._user.shareMobile = Int(jsonResponse["user_data"]["phone_public"].string!)!
                        }
                        
                        if let _ = jsonResponse["user_data"]["email_public"].string {
                            
                            self._user.shareEmail = Int(jsonResponse["user_data"]["email_public"].string!)!
                        }
    
                        if let profile_pic = jsonResponse["user_data"]["profile_pic"].string {
                            self._user.user_pic = profile_pic
                        }
    
                        self.loadUserDetails()
    
                    } else {
                        if let message = jsonResponse["message"].string {
                            JLToast.makeText(message, duration: R.tDuration).show()
                        } else {
                            print("Error parsing /users/user")
                            return
                        }
                    }
                }
        }
    }
    
    
    // load user details 
    func loadUserDetails() {
        
        self.imvProfile.setImageWithUrl(NSURL(string: _user.user_pic)!)
        
        txtFirstName.text = _user.firstName
        txtLastName.text = _user.lastName
        _phoneNumber = _user.phoneNumber
        _email = _user.email
        txtAddressLine1.text = _user.addressLine[0]
        txtAddressLine2.text = _user.addressLine[1]
        txtCity.text = _user.city
        txtProvince.text = _user.province
        txtCountry.text = _user.country
        txvShortBio.text = _user.shortBio
        setSmoker(_user.smoker)
        txtBirthday.text = _user.birthday
        txtOccupation.text = _user.occupation
        setMaritalStatus(_user.marital_status)
        setGender(_user.gender)
        
        _languages = _user.language
        
        selectEnglish()
        selectFrench()
        
//        for var index = 0; index < _languages.count; index++ {
//            
//            if(_languages[index] == "english") {
//                selectEnglish()
//            } else {
//                selectFrench()
//            }
//        }
        
        setShareEmail(_user.shareEmail)
        setShareMobile(_user.shareMobile)
        
        tblPhoneNumber.reloadData()
        tblEmail.reloadData()
    }

}
