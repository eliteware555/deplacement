
//
//  AppLanguageVC.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair.
//  Copyright © 2015 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class AppLanguageVC: UIViewController {

    let prefs = NSUserDefaults.standardUserDefaults()
    @IBOutlet var languageLabel: UILabel!
    @IBOutlet var languageSegmentedControl: UISegmentedControl!
    
    var defLang = "fr"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        let isIntroShown:Int = prefs.integerForKey("INTROSHOWN") as Int //app first time app launch intro shown flag
        
        //if intro flag set, then launch login screen, else show intro screen.
        if (isIntroShown == 1) {
            let isLoggedIn:Bool = prefs.boolForKey("ISLOGGEDIN") as Bool //app logged in flag
            //if logged in flag set, then launch main screen, else show login screen.
            if (isLoggedIn) {
                
                getUserDetails()
                
            } else {
                
                showLoginScreen()
            }
        } else {
            
            languageLabel.hidden = false
            languageSegmentedControl.hidden = false
        }
    }
    
    @IBAction func languageChanged(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            prefs.setObject("en", forKey: "LANGUAGE")
        case 1:
            prefs.setObject("fr", forKey: "LANGUAGE")
        default:
            break
        }
        
        prefs.synchronize()
        
        self.performSegueWithIdentifier("appIntroSegue", sender: self)
    }
    
    func showLoginScreen() {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("loginController") as! LoginVC
        self.presentViewController(controller, animated: false, completion: nil)
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    func getUserDetails() {
        
        if let _ = prefs.stringForKey("LANGUAGE") {
            
            defLang = prefs.stringForKey("LANGUAGE")!
        }
        
        let params = [
            "public_profile": "\(1)",
            "user_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        
        debugPrint(params)
        
        Alamofire.request(.GET, kBaseURL+"users/profile", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                guard let value = response.result.value else {
                    
                    print("Error: did not receive data")
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    
                    print("Error calling GET on /users/profile")
                    
                    print(response.result.error)
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        // first name
                        if let first_name = jsonResponse["user_data"]["user_data"]["first_name"].string {
                            
                            self.prefs.setObject(first_name, forKey: "USER_FIRST_NAME")
                        }
                        
                        // last name
                        if let last_name = jsonResponse["user_data"]["user_data"]["last_name"].string {
                            
                            self.prefs.setObject(last_name, forKey: "USER_LAST_NAME")
                        }
                        
                        // email
                        if let username = jsonResponse["user_data"]["user_data"]["username"].string {
                            
                            self.prefs.setObject(username, forKey: "USER_EMAIL")
                        }
                        
                        // profile image
                        if let profile_pic = jsonResponse["user_data"]["user_data"]["profile_pic"].string {
                            
                            self.prefs.setObject(profile_pic, forKey: "USER_PIC")
                        }
                        
                        // gender
                        if let gender = jsonResponse["user_data"]["user_data"]["gender"].string {
                            
                            self.prefs.setInteger(Int(gender)!, forKey: "USER_GENDER")
                        }
                        
                        // email verified
                        if let email_verified = jsonResponse["user_data"]["user_data"]["email_verified"].string {
                            
                            self.prefs.setBool(email_verified == "1" ? true : false, forKey: "EMAIL_VERIFIED")
                        }
                        
                        // phone verified
                        if let phone_verified = jsonResponse["user_data"]["user_data"]["phone_verified"].string {
                            
                            self.prefs.setBool(phone_verified == "1" ? true : false, forKey: "PHONE_VERIFIED")
                        }
                        
                        // admin approved
                        if let admin_approved = jsonResponse["user_data"]["user_data"]["admin_approved"].int {
                            
                            self.prefs.setBool(admin_approved == 1 ? true : false, forKey: "ADMIN_APPROVED")
                        }

                        self.prefs.synchronize()
                        
                        if(!self.prefs.boolForKey("EMAIL_VERIFIED")) {
                            JLToast.makeText(self.defLang == "fr" ? R.fr.verify_your_email : R.en.verify_your_email, duration: R.tDuration).show()
                        }
                        
                        // redirect to main screen
                        self.dismissViewControllerAnimated(true, completion: nil)
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewControllerWithIdentifier("RevealViewController") as! SWRevealViewController
                        
                        self.presentViewController(controller, animated: false, completion: nil)
                        
                        
                    } else {
                        
                        print("Error parsing /users/profile")
                        
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    }
                    
                } else {
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                }
                
            })
    }
}
