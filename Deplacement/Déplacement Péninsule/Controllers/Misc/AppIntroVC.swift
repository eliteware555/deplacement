//
//  AppIntroVC.swift
//  Déplacement Péninsule
//
//  Copyright (c) 2015 MaaS Pros. All rights reserved.
//

import UIKit

class AppIntroVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var startButton: UIButton!
    
    let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var defLang:String = "fr"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if prefs.objectForKey("LANGUAGE") != nil {
            defLang = prefs.objectForKey("LANGUAGE") as! String
        } else {
            prefs.setObject("fr", forKey: "LANGUAGE")
            defLang =  "fr"
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        initViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initViews() {
        
        //initialize scrollview
        initScrollView()
        
        //pagecontrol initial page on load
        pageControl.currentPage = 0
        pageControl.hidden = false
        
        //text view initialization
        textView.textAlignment = .Center
        textView.text = defLang == "fr" ? R.fr.intro1 : R.en.intro1
        textView.hidden =  false
        
        //start button rounded edges
        startButton.layer.cornerRadius = 4.0
        startButton.setTitle(defLang == "fr" ? R.fr.start : R.en.start, forState: .Normal)
    }
    
    func initScrollView() {
        //initialize scrollview
        self.scrollView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.width * 4, self.scrollView.frame.height)
        self.scrollView.delegate = self
        
        //add images to scrollview
        let imgOne = UIImageView(frame: CGRectMake(0, 0,scrollViewWidth, scrollViewHeight))
        imgOne.backgroundColor = UIColor.whiteColor()
        let imgTwo = UIImageView(frame: CGRectMake(scrollViewWidth, 0,scrollViewWidth, scrollViewHeight))
        imgOne.backgroundColor = UIColor.whiteColor()
        let imgThree = UIImageView(frame: CGRectMake(scrollViewWidth*2, 0,scrollViewWidth, scrollViewHeight))
        imgOne.backgroundColor = UIColor.whiteColor()
        let imgFour = UIImageView(frame: CGRectMake(scrollViewWidth*3, 0,scrollViewWidth, scrollViewHeight))
        imgOne.backgroundColor = UIColor.whiteColor()
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
        self.scrollView.addSubview(imgFour)
    }
    
    //UIScrollViewDelegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = CGRectGetWidth(scrollView.frame)
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text according to page
        if Int(currentPage) == 0 {
            textView.text = defLang == "fr" ? R.fr.intro1 : R.en.intro1
        } else if Int(currentPage) == 1 {
            textView.text = defLang == "fr" ? R.fr.intro2 : R.en.intro2
        } else if Int(currentPage) == 2 {
            textView.text = defLang == "fr" ? R.fr.intro3 : R.en.intro3
        } else {
            textView.text = defLang == "fr" ? R.fr.intro4 : R.en.intro4
            // Show the "Let's Start" button in the last slide (with a fade in animation)
            UIView.animateWithDuration(1.0, animations: { () -> Void in
                self.startButton.alpha = 1.0
            })
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    //start button click action
    @IBAction func startClicked(sender: UIButton) {
        //save first time app launch intro shown flag
        prefs.setInteger(1, forKey: "INTROSHOWN")
        prefs.synchronize()
        
        showLoginScreen()
    }
    
    func showLoginScreen() {
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("loginController") as! LoginVC
        self.presentViewController(controller, animated: false, completion: nil)
    }
}

