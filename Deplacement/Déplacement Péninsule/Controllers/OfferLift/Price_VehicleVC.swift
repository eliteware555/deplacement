//
//  Price_VehicleVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/21/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class Price_VehicleVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    var ride_key = "" // it comes from step1 response, key for getting form data
    
    var pricing_ride_key = ""
    
    var id = 0  //
    var ride_id = -1 // ride id -  for submitting a ride
    var hash_key = "" // // hash_key for a ride from form data, for submitting a rdie
    
    var waypoint_id = 0 // when to use without stopover
    
    // waypoints array and stopover count
    var stopOverCnt = 0
    var waypoints: [WayPoint] = []
    
    // vehicles array
    var vehicles:[Vehicle] = []
    
    var price_per_seat: [Int] = []
    
    // total suggested price
    var suggested_price = 0
    
    // start and end point address for a ride
    var from_point_en = ""  // start -> end
    var from_point_fr = ""
    var to_point_en = ""
    var to_point_fr = ""
    
    var selected_veh_id = -1    // selected vehicle id for submitting a ride, step22 - pricing
    var selected_veh_index = 0  // selected vehicle index in vehicle array
    var seat_available = 1       //
    
    var luggae_space = 1        // 
    
    var auto_accept = 1         // manually [in less than 12 hours]
    
    var arrPreferenceStates = [Int](count:7, repeatedValue: 0) // preference status value.
    
    var detour = 1 // none, 15mins, 30mins, any detour
    
    var isVisiblePricePerRoute = false
    
    var defLang = "fr"
    var prefs = NSUserDefaults.standardUserDefaults()
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
  
    
    // constraint outlet
    // stopovers tableview height
    @IBOutlet weak var layout_height_tbl_stopovers: NSLayoutConstraint! // original height = 50, each cell height = 50
    @IBOutlet weak var layout_height_view_stopovers: NSLayoutConstraint! // height = original height (420) + stopover tableview dynamic height ( according to stopovers' count)
    @IBOutlet weak var layout_height_superview_price: NSLayoutConstraint! // price & vehicle super view (270) + height_view_stopovers + (stopovercnt - 1) * 50
    
    @IBOutlet weak var layout_height_superview: NSLayoutConstraint! // root view of offer left 2 screen
    
    @IBOutlet weak var tblStopovers: UITableView!
    // stopover view (price per seat)
    @IBOutlet weak var stopoverView: UIView!
    @IBOutlet weak var _scrollView: UIScrollView!
    
    // without stopover
    @IBOutlet weak var pricePerSeatViewNoStopover: UIView!
    @IBOutlet weak var txtPricePerSeatNoStopover: UITextField!
    @IBOutlet weak var btnPricePlusNoStopover: UIButton!
    @IBOutlet weak var btnPriceMinusNoStopover: UIButton!
    
    // ui configuration
    @IBOutlet weak var lblPriceandVehicle: UILabel!
    @IBOutlet weak var lblPricePerSeat: UILabel!
    @IBOutlet weak var lblFromTo: UILabel!
    @IBOutlet weak var lblSuggestionPrice: UILabel! // - title
    @IBOutlet weak var lblPriceBaseOn: UILabel!
    @IBOutlet weak var btnPricePerRoute: UIButton!
    @IBOutlet weak var lblSuggestedPrice: UILabel! // value from a server
    @IBOutlet weak var lblStopovers: UILabel!
    @IBOutlet weak var lblEachPriceSuggestion: UILabel!
    
    // button to show total suggested price
    @IBOutlet weak var btnSuggestedPrice: UIButton!
    // departure & destination
    @IBOutlet weak var lblDeparture: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    
    @IBOutlet weak var btnSaveStopover: UIButton!
    @IBOutlet weak var btnCancelStopover: UIButton!
    @IBOutlet weak var lblStopOverPricePerSeat: UILabel!
    
    @IBOutlet weak var lblFurtherLiftDetails: UILabel!
    
    // Your car
    // vehicle delete button
    @IBOutlet weak var lblYourCar: UILabel!
    @IBOutlet weak var btnDelVehicle: UIButton!
    @IBOutlet weak var txtVehicle: UITextField!   // no editable
    @IBOutlet weak var btnSelectVehicle: UIButton!
    let dropDownVehicle = DropDown()
    
    // Seat Available
    @IBOutlet weak var lblSeatAvailable: UILabel!
    @IBOutlet weak var txtAvailableSeat: UITextField! // no editable
    
    // Leggage
    @IBOutlet weak var lblLuggageSpace: UILabel!
    @IBOutlet weak var btnDelLuggage: UIButton!
    @IBOutlet weak var txtLuggage: UITextField!
    @IBOutlet weak var btnSelectLuggage: UIButton!
    let dropDownLuggage = DropDown()
    
    @IBOutlet weak var btnDelDetour: UIButton!
    @IBOutlet weak var txtDetour: UITextField!
    @IBOutlet weak var btnSelectDetour: UIButton!
    let dropDownDetour = DropDown()
    
    @IBOutlet weak var lblBookingMethod: UILabel!
    @IBOutlet weak var imvManualBooking: UIImageView!
    @IBOutlet weak var lblManualBooking: UILabel!
    @IBOutlet weak var imvAutoBooking: UIImageView!
    @IBOutlet weak var lblAutoBooking: UILabel!
    
    // preference image view
    @IBOutlet weak var lblPreferences: UILabel!
    @IBOutlet weak var imvSmoke:UIImageView!
    @IBOutlet weak var imvEat: UIImageView!
    @IBOutlet weak var imvMusic: UIImageView!
    @IBOutlet weak var imvPet: UIImageView!
    @IBOutlet weak var imvChat: UIImageView!
    @IBOutlet weak var imvHandiCapped: UIImageView!
    @IBOutlet weak var imvChildren: UIImageView!
    
    @IBOutlet weak var lblMakeDetour: UILabel!
    
    // addition information
    @IBOutlet weak var lblAdditionalInfor: UILabel!
    @IBOutlet weak var txvAdditionInfo: UITextView!
    
    @IBOutlet weak var btnSaveContinue: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // set navigation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.row2 : R.en.row2
        
        // hide stopover view
        stopoverView.hidden = true
        
        txtDetour.enabled = false
        
//        // initialize right bar button item
//        let rightButton = UIButton()
//        rightButton.setImage(UIImage(named: "bell.png"), forState: .Normal)
//        rightButton.frame = CGRectMake(0, 0, 30, 30)
//        rightButton.addTarget(self, action: Selector("rightMenuClicked:"), forControlEvents: .TouchUpInside)
//        
//        let rightMenu = UIBarButtonItem()
//        rightMenu.customView = rightButton
//        self.navigationItem.rightBarButtonItem = rightMenu
        
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        // Vehicle Dropdown
        dropDownVehicle.dataSource = []
        dropDownVehicle.selectionAction = { [unowned self] (index, item) in
            
            print(index)
            
            self.txtVehicle.text = item
            
            // selected vehicle
            self.selected_veh_index = index
            
            let _vehicle = self.vehicles[index] as Vehicle
            
            self.selected_veh_id = _vehicle.veh_id
            
            self.seat_available = _vehicle.seats
            self.txtAvailableSeat.text = String(self.seat_available)
            
            self.dropDownLuggage.selectRowAtIndex(_vehicle.luggageSize-1)
            self.txtLuggage.text = self.dropDownLuggage.dataSource[_vehicle.luggageSize-1]
            self.luggae_space = _vehicle.luggageSize
        }
        dropDownVehicle.anchorView = btnSelectVehicle
        dropDownVehicle.bottomOffset = CGPoint(x:0, y: btnSelectVehicle.bounds.height + 3)
        
        // Luggage Dropdown
        if(defLang == "fr") {
            dropDownLuggage.dataSource = [R.fr.luggage_small, R.fr.luggage_medium, R.fr.luggage_large]
        } else {
            dropDownLuggage.dataSource = [R.en.luggage_small, R.en.luggage_medium, R.en.luggage_large]
        }
        
        dropDownLuggage.selectionAction = { [unowned self] (index, item) in
            self.txtLuggage.text = item
            self.luggae_space = index + 1
        }
        dropDownLuggage.anchorView = btnSelectLuggage
        dropDownLuggage.bottomOffset = CGPoint(x:0, y: btnSelectLuggage.bounds.height + 3)
        
        // Detour Dropdown
        if(defLang == "fr") {
            dropDownDetour.dataSource = [R.fr.detour_none, R.fr.detour_15mins, R.fr.detour_30mins, R.fr.detour_any]
        } else {
            dropDownDetour.dataSource = [R.en.detour_none, R.en.detour_15mins, R.en.detour_30mins, R.en.detour_any]
        }
        dropDownDetour.selectionAction = { [unowned self] (index, item) in
            self.txtDetour.text = item
            self.detour = index + 1
        }
        dropDownDetour.anchorView = btnSelectDetour
        dropDownDetour.bottomOffset = CGPoint(x:0, y: btnSelectDetour.bounds.height + 3)
        
        txtLuggage.text = dropDownLuggage.dataSource[0]
        txtDetour.text = dropDownDetour.dataSource[0]
        
        initView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        
        loadFormData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(UITextViewTextDidBeginEditingNotification)
        NSNotificationCenter.defaultCenter().removeObserver(UITextViewTextDidEndEditingNotification)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func keyboardWillShow(notificatoin: NSNotification) {
        
        if(!txvAdditionInfo.isFirstResponder()) {
            return
        }
        
        _scrollView.contentSize.height += 150
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(!txvAdditionInfo.isFirstResponder()) {
            return
        }
        
        _scrollView.contentSize.height -= 150
    }
    
    // remove activity indicator from super view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func initView() {
        
        lblPriceandVehicle.text = defLang == "fr" ? R.fr.price_vehicle_info : R.en.price_vehicle_info
        lblPricePerSeat.text = defLang == "fr" ? R.fr.price_per_seat : R.en.price_per_seat
        lblPriceBaseOn.text = defLang == "fr" ? R.fr.price_description : R.en.price_description
        btnPricePerRoute.setTitle(defLang == "fr" ? R.fr.pricer_per_route : R.en.price_per_route, forState: .Normal)
        lblSuggestionPrice.text = defLang == "fr" ? R.fr.our_price_suggestions : R.en.our_price_suggestions
        lblEachPriceSuggestion.text = defLang == "fr" ? R.fr.our_price_description : R.en.our_price_description
        lblStopovers.text = defLang == "fr" ? R.fr.stopoevers : R.en.stopovers
        lblStopOverPricePerSeat.text = defLang == "fr" ? R.fr.price_per_seat : R.en.price_per_seat
        
        btnSaveStopover.setTitle(defLang == "fr" ? R.fr.btn_save : R.en.btn_save, forState: .Normal)
        btnCancelStopover.setTitle(defLang == "fr" ? R.fr.btn_cancel : R.en.btn_cancel, forState: .Normal)
        
        lblFurtherLiftDetails.text = defLang == "fr" ? R.fr.tile_lift_detail : R.en.tile_lift_detail
        
        lblYourCar.text = defLang == "fr" ? R.fr.your_car : R.en.your_car
        txtVehicle.placeholder = defLang == "fr" ? R.fr.select_vehicle : R.en.select_vehicle
        lblSeatAvailable.text = defLang == "fr" ? R.fr.seat_available : R.en.seat_available
        lblLuggageSpace.text = defLang == "fr" ? R.fr.luggage_space : R.en.luggage_space
        
        lblBookingMethod.text = defLang == "fr" ? R.fr.booking_method : R.en.booking_method
        lblManualBooking.text = defLang == "fr" ? R.fr.book_manually : R.en.book_manually
        lblAutoBooking.text = defLang == "fr" ? R.fr.book_automatically : R.en.book_automatically
        
        lblPreferences.text = defLang == "fr" ? R.fr.preferences : R.en.preferences
        lblMakeDetour.text = defLang == "fr" ? R.fr.title_detour : R.en.title_detour
        lblAdditionalInfor.text = defLang == "fr" ? R.fr.title_additional_info : R.en.title_additional_info
        
        txvAdditionInfo.placeholder = defLang == "fr" ? R.fr.additional_info_holder : R.en.additional_info_holder
        
        btnSaveContinue.setTitle(defLang == "fr" ? R.fr.save_continue : R.en.save_continue, forState: .Normal)
        
        btnPricePerRoute.hidden = true
        pricePerSeatViewNoStopover.hidden = true
        
//        let strSuggestion = NSMutableAttributedString(string: "Our price suggestions \n(Automatically calculated)")
//        strSuggestion.addAttribute(NSForegroundColorAttributeName, value: UIColor(colorLiteralRed: 223/255.0, green: 25/255.0, blue: 108/255.0, alpha: 1.0), range: NSMakeRange(0, 22))
//        strSuggestion.addAttribute(NSForegroundColorAttributeName, value: UIColor(colorLiteralRed: 48/255.0, green: 48/255.0, blue: 48/255.0, alpha: 1.0), range: NSMakeRange(22, 27))
//        lblSuggestionPrice.attributedText = strSuggestion
//        
//        lblDeparture.text = "Canada Square, \nUnited Kingdom"
//        lblDestination.text = "Liberpool, United Kingdom"
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        
        self.view .endEditing(true)
    }
    
    // -----------------------------------------------------------------------------
    //   Mark: - UITableView Datasource
    // -----------------------------------------------------------------------------
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stopOverCnt
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StopOverPriceCell", forIndexPath:  indexPath) as! StopOverPriceCell
            
        // stopovers
        cell.setPointMarker("blue_dot.png")
        cell.ctrlMinus.tag = 200 + indexPath.row * 2
        cell.ctrlPlus.tag = 200 + indexPath.row * 2 + 1
        
        let waypoint = waypoints[indexPath.row] as WayPoint
        cell.lblPlace.text = defLang == "fr" ? waypoint.end_loc_fr : waypoint.end_loc_en
        cell.lblPrice.text = "$" + String(waypoint.suggested_price)
        cell.txtPrice.text = String(waypoint.price_per_seat)
        
        return cell
    }
    
    // -----------------------------------------------------------------------------
    // MARK: IBAction
    // -----------------------------------------------------------------------------

    @IBAction func showPricePerRoute(sender: UIButton) {
        
        isVisiblePricePerRoute  = !isVisiblePricePerRoute
        
        if(isVisiblePricePerRoute) {
            showPricePerSeat()
        } else {
            hidePricePerSeat()
        }
    }
    
    func showPricePerSeat() {
    
        stopoverView.hidden = false
        
        layout_height_tbl_stopovers.constant += (CGFloat)(stopOverCnt-1) * 50
        layout_height_view_stopovers.constant += (CGFloat)(stopOverCnt-1) * 50
        layout_height_superview_price.constant += (CGFloat)(stopOverCnt-1) * 50 + 420
        layout_height_superview.constant += (CGFloat)(stopOverCnt-1) * 50 + 420
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func hidePricePerSeat() {
        
        stopoverView.hidden = true
        
        layout_height_tbl_stopovers.constant -= (CGFloat)(stopOverCnt - 1) * 50
        layout_height_view_stopovers.constant -= (CGFloat)(stopOverCnt - 1) * 50
        layout_height_superview_price.constant -= (CGFloat)(stopOverCnt-1) * 50 + 420
        layout_height_superview.constant -= (CGFloat)(stopOverCnt-1) * 50 + 420
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    // save price per seat
    @IBAction func savePricePerSeat(sender: AnyObject?) {
        
        updateRidePrice()
    }
    
    @IBAction func cancelPricePerSeat(sender: AnyObject?) {
        
        isVisiblePricePerRoute  = !isVisiblePricePerRoute
        
        for var index = 0; index < waypoints.count; index++ {
            waypoints[index].price_per_seat = price_per_seat[index]
        }
        
        loadPageDetails()
        
        hidePricePerSeat()
    }
    
    @IBAction func priceControlNoStopover(sender: UIButton!) {
        
        
        var currentPrice: Int = Int(txtPricePerSeatNoStopover.text!)!
        
        if(currentPrice <= 1 || currentPrice >= suggested_price * 2) {
            return
        }
        
        if(sender == btnPriceMinusNoStopover) {
            
            currentPrice--
            
        } else  {
            
            currentPrice++
        }
        
        if(currentPrice >= suggested_price*14/10) {
            txtPricePerSeatNoStopover.backgroundColor = UIColor(red: 250/255.0, green: 164/255.0, blue: 41/255.0, alpha: 1.0)
        } else {
            txtPricePerSeatNoStopover.backgroundColor = UIColor.whiteColor()
        }
        
        txtPricePerSeatNoStopover.text = "\(currentPrice)"
    }
    
    // control price per seat for each stopovers (plus)
    @IBAction func pricePlusControlAction(sender: UIButton!) {
        
        let cell =  tblStopovers .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag-200)/2, inSection: 0)) as! StopOverPriceCell
        
        let waypoint = waypoints[(sender.tag-200)/2] as WayPoint
        
        if(waypoint.price_per_seat >= waypoint.suggested_price * 2) {
            return
        }
        
        waypoint.price_per_seat++
        
        if(waypoint.price_per_seat >= (waypoint.suggested_price * 14 / 10)) {
            cell.txtPrice.backgroundColor = UIColor(red: 250/255.0, green: 164/255.0, blue: 41/255.0, alpha: 1.0)
        }
        
        cell.txtPrice.text = "\(waypoint.price_per_seat)"

        var totalPrice = 0
        for _waypoint in waypoints {
            totalPrice += _waypoint.price_per_seat
        }
        
        btnSuggestedPrice.setTitle("\(totalPrice)", forState: .Normal)
    
    }
    
    // control price per seat for each stopovers (minus)
    @IBAction func priceMinusControlAction(sender: UIButton!) {
        
        let cell =  tblStopovers .cellForRowAtIndexPath(NSIndexPath(forRow: (sender.tag-200)/2, inSection: 0)) as! StopOverPriceCell
        
        let waypoint = waypoints[(sender.tag-200)/2] as WayPoint
        
        if(waypoint.price_per_seat <= 1) {
            return
        }
        
        waypoint.price_per_seat--
        
        if(waypoint.price_per_seat <= (waypoint.suggested_price * 14 / 10)) {
            cell.txtPrice.backgroundColor = UIColor.whiteColor()
        }
        
        cell.txtPrice.text = "\(waypoint.price_per_seat)"
        
        var totalPrice = 0
        for _waypoint in waypoints {
            totalPrice += _waypoint.price_per_seat
        }
        
        btnSuggestedPrice.setTitle("\(totalPrice)", forState: .Normal)
    }
    
    @IBAction func deleteAction(sender: UIButton!) {
        
        switch sender {
        case btnDelVehicle:
            txtVehicle.text = ""
            txtLuggage.text = ""
            
            // cancel the current selected vehicle state
            selected_veh_id = -1
            
            dropDownVehicle.show()
            
            break
            
        case btnDelLuggage:
            txtLuggage.text = ""
            
            // cancel the current selected luggaage state
            luggae_space = 1; // default value
            
            dropDownLuggage.show()
            break
            
        case btnDelDetour:
            txtDetour.text = ""
            
            // 
            detour = 1
            
            dropDownLuggage.show()
            break
        default: ()
        }
    }
    
    @IBAction func minusSeatAction(sender: UIButton!) {
        
        if(selected_veh_id < 0) {
            return
        }
        
        if(seat_available == 1) {
            return
        }
        
        seat_available--
        
        txtAvailableSeat.text = "\(seat_available)"
    }
    
    @IBAction func plusSeatAction(sender: UIButton!) {
        
        if(selected_veh_id < 0) {
            return
        }
        
        let vehicle = vehicles[selected_veh_index] as Vehicle
        
        if(seat_available >=  vehicle.seats) {
            return
        }
        
        if(seat_available >= vehicle.seats) {
            
            return
        }
        
        seat_available++
        
        txtAvailableSeat.text = "\(seat_available)"
    }
    
    @IBAction func selectManualBooking(sender: UIButton!) {
        
        imvManualBooking.image = UIImage(named: "checked1.png")
        imvAutoBooking.image = UIImage(named: "unchecked1.png")
        
        auto_accept = 1
    }
    
    @IBAction func selectAutoBooking(sender: UIButton!) {
        imvManualBooking.image = UIImage(named: "unchecked1.png")
        imvAutoBooking.image = UIImage(named: "checked1.png")
        
        auto_accept = 2
    }
    
//    func rightMenuClicked(sender: AnyObject) {
//        
//        print("right menu bell clicked")
//    }
    
    @IBAction func selectVehicle(sender: UIButton) {
        
        if(dropDownVehicle.hidden) {
        
            dropDownVehicle.show()
        } else {
            dropDownVehicle.hide()
        }
    }
    
    @IBAction func selectLeggage(sender: UIButton!) {
        
        if(dropDownLuggage.hidden) {
            dropDownLuggage.show()
        } else {
            dropDownLuggage.hide()
        }
    }
    
    @IBAction func selectDetour(sender: UIButton!) {
        
        if(dropDownDetour.hidden) {
            dropDownDetour.show()
        } else {
            dropDownDetour.hide()
        }
    }    
    
    @IBAction func selectPreference(sender: UIButton!) {
        
        arrPreferenceStates[sender.tag - 140]++
        
        switch sender.tag {
        case 140:
            arrPreferenceStates[0] %= 3
            imvSmoke.image = UIImage(named: "smoke\(arrPreferenceStates[0]).png")
            break
        case 141:
            arrPreferenceStates[1] %= 3
            imvEat.image = UIImage(named: "eat\(arrPreferenceStates[1]).png")
            break
        case 142:
            arrPreferenceStates[2] %= 3
            imvMusic.image = UIImage(named: "music\(arrPreferenceStates[2]).png")
            break
        case 143:
            arrPreferenceStates[3] %= 3
            imvPet.image = UIImage(named: "pets\(arrPreferenceStates[3]).png")
            break
        case 144:
            arrPreferenceStates[4] %= 3
            imvChat.image = UIImage(named: "chat\(arrPreferenceStates[4]).png")
            break
        case 145:
            arrPreferenceStates[5] %= 2
            imvHandiCapped.image = UIImage(named: "handicapped\(arrPreferenceStates[5]).png")
            break
        case 146:
            arrPreferenceStates[6] %= 2
            imvChildren.image = UIImage(named: "children\(arrPreferenceStates[6]).png")
            break
            
        default: ()
        }
    }
    
    // add a new car
    @IBAction func addNewCarAction(sender: AnyObject?) {

        let destController = self.storyboard!.instantiateViewControllerWithIdentifier("AddCarVC") as! AddCarVC
        destController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        destController.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        destController.bComesFromPriceVehicle = true
        destController.priceVehicelVC = self
        
        destController.addorEditVehicle = 1
        
        self.presentViewController(destController, animated: true, completion: nil)
        
//        self.performSegueWithIdentifier("Segue2AddCar", sender: self)
    }
    
    @IBAction func continueAction(sender: AnyObject?) {
        
        // check the validation of setting and inputed value.
        if(selected_veh_id < 0) {
            JLToast.makeText("Please select a vehicle.", duration: 3.0).show()
            return
        }
        
        if(stopOverCnt > 0) {
            
            submit()
            
        } else {
            
            updatePriceNoStopOver()
        }
    }
    
    func loadFormData() {
        
        if(!(UIApplication.sharedApplication().delegate as! AppDelegate).bLoadFromData) {
            return
        }
        
        let params: [String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "step": "pricing",
            "key": ride_key,
            "site_lang": defLang
        ]
        
        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "/ride/offer_ride/pricing/" + ride_key, parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/offer_ride")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let _ = jsonResponse["form_data"]["ride"]["id"].string {
                            self.id = Int(jsonResponse["form_data"]["ride"]["id"].string!)!
                        }
                        
                        if let _ = jsonResponse["from_data"]["ride"]["waypoint_id"].string {
                            self.waypoint_id = Int(jsonResponse["form_data"]["ride"]["waypoint_id"].string!)!
                        }
                        
                        // ride
                        self.ride_id = Int(jsonResponse["form_data"]["ride"]["ride_id"].string!)!
                        self.hash_key = jsonResponse["form_data"]["ride"]["hash_key"].string!
                        
                        self.suggested_price = jsonResponse["form_data"]["ride"]["suggested_price"].int!
                        
                        self.from_point_en = jsonResponse["form_data"]["ride"]["start_point_en"].string!
                        self.from_point_fr = jsonResponse["form_data"]["ride"]["start_point_fr"].string!
                        self.to_point_en = jsonResponse["form_data"]["ride"]["end_point_en"].string!
                        self.to_point_fr = jsonResponse["form_data"]["ride"]["end_point_fr"].string!
                        
                        // vehicle
                        if let vehicle_count = jsonResponse["form_data"]["vehicles"].array?.count {
                            
                            if(vehicle_count > 0) {
                                
                                self.vehicles.removeAll()
                                self.dropDownVehicle.dataSource.removeAll()
                                
                                for var index = 0; index < vehicle_count; index++ {
                                
                                    let vehicle = Vehicle()
                                    
                                    vehicle.veh_id = Int(jsonResponse["form_data"]["vehicles"][index]["veh_id"].string!)!
                                    vehicle.seats = Int(jsonResponse["form_data"]["vehicles"][index]["seats"].string!)!
                                    vehicle.year = jsonResponse["form_data"]["vehicles"][index]["year"].string!
                                    vehicle.make_name = jsonResponse["form_data"]["vehicles"][index]["make"].string!
                                    vehicle.model_name = jsonResponse["form_data"]["vehicles"][index]["model"].string!
                                    
                                    vehicle.luggageSize = Int(jsonResponse["form_data"]["vehicles"][index]["luggage_size"].string!)!
                                    
                                    self.vehicles.append(vehicle)
                                    
                                    vehicle.makeSimpleName()
                                    
                                    self.dropDownVehicle.dataSource.append(vehicle.simple_name)
                                }
                            }
                        }
                        
                        if let waypoints_count = jsonResponse["form_data"]["waypoints"].array?.count {
                            
                            if(waypoints_count > 0) {
                                
                                self.stopOverCnt = waypoints_count
                                
                                self.waypoints.removeAll()
                                
                                for var index = 0; index < waypoints_count; index++ {
                                    
                                    let _waypoint = WayPoint()
                                    
                                    _waypoint.ride_key = jsonResponse["form_data"]["waypoints"][index]["ride_key"].string!
                                    _waypoint.id = Int(jsonResponse["form_data"]["waypoints"][index]["id"].string!)!
                                    _waypoint.ride_id = Int(jsonResponse["form_data"]["waypoints"][index]["ride_id"].string!)!
                                    
                                    _waypoint.start_point_en = jsonResponse["form_data"]["waypoints"][index]["start_point_en"].string!
                                    _waypoint.start_point_fr = jsonResponse["form_data"]["waypoints"][index]["start_point_fr"].string!
                                    _waypoint.start_loc_en = jsonResponse["form_data"]["waypoints"][index]["start_loc_en"].string!
                                    _waypoint.start_loc_fr = jsonResponse["form_data"]["waypoints"][index]["start_loc_fr"].string!
                                    _waypoint.start_lat = jsonResponse["form_data"]["waypoints"][index]["start_lat"].string!
                                    _waypoint.start_lon = jsonResponse["form_data"]["waypoints"][index]["start_lon"].string!
                                    
                                    _waypoint.end_point_en = jsonResponse["form_data"]["waypoints"][index]["end_point_en"].string!
                                    _waypoint.end_point_fr = jsonResponse["form_data"]["waypoints"][index]["end_point_fr"].string!
                                    _waypoint.end_loc_en = jsonResponse["form_data"]["waypoints"][index]["end_loc_en"].string!
                                    _waypoint.end_loc_fr = jsonResponse["form_data"]["waypoints"][index]["end_loc_fr"].string!
                                    _waypoint.end_lat = jsonResponse["form_data"]["waypoints"][index]["end_lat"].string!
                                    _waypoint.end_lon = jsonResponse["form_data"]["waypoints"][index]["end_lon"].string!
                                    
                                    _waypoint.waypoints = jsonResponse["form_data"]["waypoints"][index]["waypoints"].string!
                                    _waypoint.suggested_price = jsonResponse["form_data"]["waypoints"][index]["suggested_price"].int!
                                    
                                    let _price_per_seat = Int(Float(jsonResponse["form_data"]["waypoints"][index]["price_per_seat"].string!)!)
                                    self.price_per_seat.append(_price_per_seat)
                                    
                                    _waypoint.price_per_seat = _price_per_seat
                                    
                                    _waypoint.route_distance = jsonResponse["form_data"]["waypoints"][index]["route_distance"].string!
                                    _waypoint.position = Int(jsonResponse["form_data"]["waypoints"][index]["position"].string!)!
                                    
                                    self.waypoints.append(_waypoint)
                                    
                                }
                            }
                        }
                        
                        self.loadPageDetails()
                        
                    } else {
                        print("fail to offer a ride /ride/offer_ride")
                    }
                } else {
                    print("Error parsing /ride/offer_ride")
                }
                
        }
    }
    
    func loadPageDetails() {
        
        // suggested price
        btnSuggestedPrice.setTitle(String(suggested_price), forState: .Normal)
        
        let suggestedPrice = defLang == "fr" ? R.fr.suggested_price : R.en.suggested_price
        lblSuggestedPrice.text = suggestedPrice + ": $" + String(suggested_price)
        
        lblFromTo.text = ((defLang == "fr" ? from_point_fr : from_point_en) + "->\n" + (defLang == "fr" ? to_point_fr : to_point_en))
        
        lblDeparture.text = defLang == "fr" ? from_point_fr : from_point_en
        lblDestination.text = defLang == "fr" ? to_point_fr : to_point_en
        
        if(stopOverCnt > 0) {
            btnPricePerRoute.hidden = false
        } else {
            pricePerSeatViewNoStopover.hidden = false
            txtPricePerSeatNoStopover.text = "\(suggested_price)"
        }
        
        self.tblStopovers.reloadData()
    }
    
    func submit() {
        
        let params: [String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "step": "pricing",
            "key": ride_key,
            "ride_data": [
                "process": "\(1)",
                "ride_id":  "\(ride_id)",
                "veh_id": "\(selected_veh_id)",
                "auto_accept": "\(auto_accept)",
                "step_completed": "\(2)",
                "comment": txvAdditionInfo.text,
                "luggage_per_seat" : "\(luggae_space)",
                "seats_offered": "\(seat_available)",
                "detour": "\(detour)",
                "chatting": "\(arrPreferenceStates[4])",
                "music": "\(arrPreferenceStates[2])",
                "pets": "\(arrPreferenceStates[3])",
                "smoking": "\(arrPreferenceStates[0])",
                "food": "\(arrPreferenceStates[1])",
                "children": "\(arrPreferenceStates[6])",
                "handicapped": "\(arrPreferenceStates[5])",
                "women_only": "\(0)"
            ]
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ride/offer_ride", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON { response in
                
//                print("request : \(response.request)")
//                print("response : \(response.response)")
//                print("data : \(response.data)")
//                print("result : \(response.result)")
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/offer_ride")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        let _redirectURL = jsonResponse["redirectUrl"].string
                        
                        let _index = _redirectURL?.rangeOfString("/", options: .BackwardsSearch)?.startIndex.advancedBy(1)
                        self.pricing_ride_key = (_redirectURL?.substringFromIndex(_index!))!
                        
                        self.performSegueWithIdentifier("Segue2ConfirmTrip", sender: self)
                        
                        print("Ride Key: " + self.ride_key)
                        
                    } else {
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                        print("fail to offer a ride /ride/offer_ride")
                    }
                } else {
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    print("Error parsing /ride/offer_ride")
                }
        }
    }
    
    func updateRidePrice() {
        
        var _stopovers = Array<Dictionary<String, AnyObject>>()
        
        for var index = 0; index < waypoints.count; index++ {
            
            let _waypoint = waypoints[index] as WayPoint
            
                _stopovers.append(
                    [
                        "id": "\(_waypoint.id)",
                        "ride_id": "\(_waypoint.ride_id)",
                        "value": "\(_waypoint.price_per_seat)"
                    ]
                )
        }
        
        let params :[String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "stopover": _stopovers
        ]
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ride/update_price", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON { response in
                
                print("request : \(response.request)")
                print("response : \(response.response)")
                print("data : \(response.data)")
                print("result : \(response.result)")
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/update_price")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
//                        JLToast.makeText(jsonResponse["message"].string!, duration:R.tDuration).show()
                        
                        if(self.defLang == "fr") {
                            
                            JLToast.makeText("Prix mis à jour avec succès", duration: R.tDuration).show()
                            
                        } else {
                            
                            JLToast.makeText("Price updated successfully", duration: R.tDuration).show()
                        }

                        var _total_price = 0
                        for var index = 0; index < self.waypoints.count; index++ {
                            
                            self.price_per_seat[index] = self.waypoints[index].price_per_seat
                            _total_price += self.price_per_seat[index]
                        }
                        
                        self.suggested_price = _total_price
                        
                    } else {
                        JLToast.makeText(jsonResponse["message"].string!, duration:R.tDuration).show()
                        print("fail to offer a ride /ride/update_price")
                    }
                } else {
                    print("Error parsing /ride/update_price")
                }
        }
    }
    
    func updatePriceNoStopOver() {
        
        var _stopovers = Array<Dictionary<String, AnyObject>>()
        
        _stopovers.append(
            [
                "id": "\(id)",
                "ride_id": "\(ride_id)",
                "value": txtPricePerSeatNoStopover.text!
        ])
        
        let params :[String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "stopover": _stopovers
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ride/update_price", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON { response in
                
                print("request : \(response.request)")
                print("response : \(response.response)")
                print("data : \(response.data)")
                print("result : \(response.result)")
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/update_price")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        self.suggested_price = Int(self.txtPricePerSeatNoStopover.text!)!
                        
                        self.submit()
                        
                    } else {
                        JLToast.makeText(jsonResponse["message"].string!, duration:R.tDuration).show()
                        print("fail to offer a ride /ride/update_price")
                    }
                } else {
                    print("Error parsing /ride/update_price")
                }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "Segue2ConfirmTrip") {
            
            let destinationVC: ConfirmTripVC = segue.destinationViewController as! ConfirmTripVC
            destinationVC.ride_key = self.pricing_ride_key
        }
    }
}






