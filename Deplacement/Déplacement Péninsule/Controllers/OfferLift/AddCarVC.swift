//
//  AddCarVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/23/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class AddCarVC: UIViewController, UITextFieldDelegate, FloatRatingViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var ratingControl: FloatRatingView!
    
    var defLang = "fr"
    var prefs = NSUserDefaults.standardUserDefaults()
    
    var arrAdditionSelection = [Bool](count:3, repeatedValue: false)
    
    @IBOutlet weak var lblNavTitle: UILabel!
    
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var txtYear: UITextField!
    let dropdownYear = DropDown()
    
    @IBOutlet weak var btnMake: UIButton!
    @IBOutlet weak var txtMake: UITextField!
    let dropdownMake = DropDown()
    
    @IBOutlet weak var btnModel: UIButton!
    @IBOutlet weak var txtModel: UITextField!
    let dropdownModel = DropDown()
    
    @IBOutlet weak var txtVehicleColor: UITextField!
    
    @IBOutlet weak var txtDistancePerYear: UITextField!
    
    @IBOutlet weak var lblTransmission: UILabel!
    @IBOutlet weak var imvAutomatic: UIImageView!
    @IBOutlet weak var lblAutomatic: UILabel!
    @IBOutlet weak var imvManual: UIImageView!
    @IBOutlet weak var lblManual: UILabel!
    
    @IBOutlet weak var lblMileage: UILabel!
    @IBOutlet weak var imvCity: UIImageView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var imvHighway: UIImageView!
    @IBOutlet weak var lblHighway: UILabel!
    
    @IBOutlet weak var txtFuelEfficiency: UITextField!
    
    @IBOutlet weak var txtCo2Output: UITextField!

    @IBOutlet weak var txtVehicleType: UITextField!
    @IBOutlet weak var btnVehicleType: UIButton!
    let dropdownVehicleType = DropDown()
    
    @IBOutlet weak var txtLuggageSize: UITextField!
    @IBOutlet weak var btnLuggageSize: UIButton!
    let dropdownLuggageSize = DropDown()
    
    @IBOutlet weak var txtTireType: UITextField!
    @IBOutlet weak var btnTireType: UIButton!
    let dropdownTireType = DropDown()
    
    @IBOutlet weak var txtPassengerSeat: UITextField!
    @IBOutlet weak var btnPassengerSeat: UIButton!
    let dropdownPassengerSeats = DropDown()
    
    @IBOutlet weak var lblLicensePlate: UILabel!
    @IBOutlet weak var txtGNBB: UITextField!
    @IBOutlet weak var txt123: UITextField!
    
    @IBOutlet weak var lblAdditionalFeatures: UILabel!
    @IBOutlet weak var lblAirConditioning: UILabel!
    @IBOutlet weak var imvAirCondition: UIImageView!
    @IBOutlet weak var lblBikeRack: UILabel!
    @IBOutlet weak var imvBikeRack: UIImageView!
    @IBOutlet weak var lblSkirack: UILabel!
    @IBOutlet weak var imvSkiRack: UIImageView!
    
    @IBOutlet weak var lblComfortLevel: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var imvCar: UIImageView!
    
    @IBOutlet weak var comfortHelp: UIView!
    @IBOutlet weak var lblBasic: UILabel!
    @IBOutlet weak var lblNormal: UILabel!
    @IBOutlet weak var lblComfortable: UILabel!
    @IBOutlet weak var lblLuxury: UILabel!
    
    @IBOutlet weak var btnShowComfortHelpView: UIButton!
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    var carRating: Int = 0
    
    var arrMakes: [VehicleMake] = []
    var selectedMakeIdx =  -1
    
    var arrModel: [VehicleModel] = []
    var selectedModelIdx =  -1
    
    var transmission = 1
    var mileage = 1
    var emission = Emission()
    
    var body_type = 0
    
    var addorEditVehicle: Int = 1 // add = 1, edit = 0
    var edit_vehicle = Vehicle()
    
    var seats = 0
    var luggageSize = 0
    var type_tire = 0
    
    var bNewCarAdded: Bool = false
    var bComesFromPriceVehicle: Bool!
    
    var vehicleSettingVC: VehicleSettingVC!
    var priceVehicelVC: Price_VehicleVC!
    
    var picker: UIImagePickerController? = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // initialize rating bar
        // Required float rating view params
        self.ratingControl.emptyImage = UIImage(named: "star2.png")
        self.ratingControl.fullImage = UIImage(named: "star1.png")
        // Optional params
        self.ratingControl.delegate = self
        self.ratingControl.contentMode = UIViewContentMode.ScaleAspectFit
        self.ratingControl.maxRating = 4
        self.ratingControl.minRating = 1
        self.ratingControl.rating = 0
        self.ratingControl.editable = true
        self.ratingControl.halfRatings = false
        self.ratingControl.floatRatings = false
        
        // initialize year dropdown
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let year = calendar.component(.Year, fromDate: date)
        
        for var index = 1995; index <= year; index++ {
            dropdownYear.dataSource.append("\(index)")
        }
        dropdownYear.selectionAction = { [unowned self] (index, item) in
            self.txtYear.text = item
            
            self.txtMake.text = ""
            self.selectedMakeIdx = -1
            
            self.txtModel.text = ""
            self.selectedModelIdx = -1
            
            // load make & model, type
            self.loadMake()
        }
        dropdownYear.anchorView = btnYear
        dropdownYear.bottomOffset = CGPoint(x:0, y: btnYear.bounds.height + 3)
        
        // make dropdown
        dropdownMake.dataSource = []
        dropdownMake.selectionAction = { [unowned self] (index, item) in
            self.txtMake.text = item
            self.selectedMakeIdx = index
            
            self.txtModel.text = ""
            self.selectedModelIdx = -1
            
            self.loadModel()
        }
        dropdownMake.anchorView = btnMake
        dropdownMake.bottomOffset = CGPoint(x:0, y: btnMake.bounds.height + 3)

        // model dropdown
        dropdownModel.dataSource = []
        dropdownModel.selectionAction = { [unowned self] (index, item) in
            self.txtModel.text = item
            self.selectedModelIdx = index
            
            self.loadCO2OutputEmission()

        }
        dropdownModel.anchorView = btnModel
        dropdownModel.bottomOffset = CGPoint(x:0, y: btnModel.bounds.height + 3)
        
        dropdownVehicleType.dataSource = ["Coupe", "Sedan", "SUV", "Van", "Wagon", "Comfertable", "Hybrid Car", "Luxury Car", "Electric Car"]
        dropdownVehicleType.selectionAction = { [unowned self] (index, item) in
            self.txtVehicleType.text = item
            self.body_type = index + 1
        }
        dropdownVehicleType.anchorView = btnVehicleType
        dropdownVehicleType.bottomOffset = CGPoint(x:0, y: btnVehicleType.bounds.height + 3)
       
        // luggage size
        if(defLang == "fr") {
            dropdownLuggageSize.dataSource = [R.fr.luggage_small, R.fr.luggage_medium, R.fr.luggage_large]
        } else {
            dropdownLuggageSize.dataSource = [R.en.luggage_small, R.en.luggage_medium, R.en.luggage_large]
        }
        dropdownLuggageSize.selectionAction = { [unowned self] (index, item) in
            self.txtLuggageSize.text = item
            
            self.luggageSize = index + 1
        }
        dropdownLuggageSize.anchorView = btnLuggageSize
        dropdownLuggageSize.bottomOffset = CGPoint(x:0, y: btnLuggageSize.bounds.height + 3)
        
        // Type of tires dropdown
        if(defLang == "fr") {
            dropdownTireType.dataSource = ["Normal", "Hiver"]
        } else {
            dropdownTireType.dataSource = ["Normal", "Winter"]
        }
        dropdownTireType.selectionAction = { [unowned self] (index, item) in
            self.txtTireType.text = item
            
            self.type_tire = index + 1
        }
        dropdownTireType.anchorView = btnTireType
        dropdownTireType.bottomOffset = CGPoint(x:0, y: btnTireType.bounds.height + 3)
        
        // passenger seats dropdown
        dropdownPassengerSeats.dataSource = ["1", "2", "3", "4", "5", "6"]
        dropdownPassengerSeats.selectionAction = { [unowned self] (index, item) in
            self.txtPassengerSeat.text = item
            
            self.seats = Int(item)!
        }
        dropdownPassengerSeats.anchorView = btnPassengerSeat
        dropdownPassengerSeats.bottomOffset = CGPoint(x:0, y: btnPassengerSeat.bounds.height + 3)
        
        // initialize acitivity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        if(addorEditVehicle == 0) {
            imvCar.setImageWithUrl(NSURL(string: edit_vehicle.veh_pic)!, placeHolderImage:UIImage(named: "car_place"))
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {
        
        lblNavTitle.text = defLang == "fr" ? R.fr.add_new_vehicle : R.en.add_new_vehicle
        txtYear.placeholder = defLang == "fr" ? R.fr.year : R.en.year
        txtMake.placeholder = defLang == "fr" ? R.fr.make : R.en.make
        txtModel.placeholder = defLang == "fr" ? R.fr.model : R.en.model
        txtVehicleColor.placeholder = defLang == "fr" ? R.fr.colour : R.en.colour
        txtDistancePerYear.placeholder = defLang == "fr" ? R.fr.distance_per_year : R.en.distance_per_year
        lblTransmission.text = defLang == "fr" ? R.fr.transmission : R.en.transmission
        lblAutomatic.text = defLang == "fr" ? R.fr.automatic : R.en.automatic
        lblManual.text = defLang == "fr" ? R.fr.manual : R.en.manual
        lblMileage.text = defLang == "fr" ? R.fr.mileage : R.en.mileage
        lblCity.text = defLang == "fr" ? R.fr.city : R.en.city
        lblHighway.text = defLang == "fr" ? R.fr.highway : R.en.highway
        txtFuelEfficiency.placeholder = defLang == "fr" ? R.fr.fuel_efficiency : R.en.fuel_efficiency
        txtCo2Output.placeholder = defLang == "fr" ? R.fr.co2_output : R.en.co2_output
        txtVehicleType.placeholder = defLang == "fr" ? R.fr.type_vehicle : R.en.type_vehicle
        txtLuggageSize.placeholder = defLang == "fr" ? R.fr.luggage_small : R.en.luggage_small
        txtTireType.placeholder = defLang == "fr" ? R.fr.type_tires : R.en.type_tires
        txtPassengerSeat.placeholder = defLang == "fr" ? R.fr.passenger_seats : R.en.passenger_seats
        lblLicensePlate.text = defLang == "fr" ? R.fr.license_plate : R.en.license_plate
        txtGNBB.placeholder = defLang == "fr" ? R.fr.gnbb : R.en.gnbb
        
        lblAdditionalFeatures.text = defLang == "fr" ? R.fr.additional_features : R.en.additional_features
        lblAirConditioning.text = defLang == "fr" ? R.fr.air_conditioning : R.en.air_conditioning
        lblBikeRack.text = defLang == "fr" ? R.fr.bike_rack : R.en.bike_rack
        lblSkirack.text = defLang == "fr" ? R.fr.ski_rack : R.en.ski_rack
        
        lblComfortLevel.text = defLang == "fr" ? R.fr.comfort_level : R.en.comfort_level
        lblBasic.text = defLang == "fr" ? R.fr.comfort_basic : R.en.comfort_basic
        lblNormal.text = defLang == "fr" ? R.fr.comfort_normal : R.en.comfort_normal
        lblComfortable.text = defLang == "fr" ? R.fr.comfort_comfortable : R.en.comfort_comfortable
        lblLuxury.text = defLang == "fr" ? R.fr.comfort_luxury : R.en.comfort_luxury
        
        btnSave.setTitle(defLang == "fr" ? R.fr.btn_save : R.en.btn_save, forState: .Normal)
        btnCancel.setTitle(defLang == "fr" ? R.fr.btn_cancel : R.en.btn_cancel, forState: .Normal)
        
        if(addorEditVehicle == 0) {
            
            txtYear.text = edit_vehicle.year
            txtMake.text = edit_vehicle.make_name
            txtModel.text = edit_vehicle.model_name
            
            txtVehicleColor.text = edit_vehicle.color
            txtDistancePerYear.text = edit_vehicle.distance_per_year
            
            selectTransmission(edit_vehicle.transmission)
            selectMileage(edit_vehicle.mileage)
            
            txtFuelEfficiency.text = edit_vehicle.fuel_efficiency
            txtCo2Output.text = edit_vehicle.co2_output
            txtVehicleType.text = edit_vehicle.body_type
            txtLuggageSize.text = dropdownLuggageSize.dataSource[edit_vehicle.luggageSize-1]
            txtTireType.text = edit_vehicle.type_tires
            
            txtPassengerSeat.text = "\(edit_vehicle.seats)"
            
            txtGNBB.text = edit_vehicle.license_plate
            txt123.text = edit_vehicle.license_number
            
            arrAdditionSelection[0] = ((edit_vehicle.airCondition == "No" || edit_vehicle.airCondition == "Non") ? false : true)
            arrAdditionSelection[1] = ((edit_vehicle.bikeRack == "No" || edit_vehicle.bikeRack == "Non") ? false : true)
            arrAdditionSelection[2] = ((edit_vehicle.skiRack == "No" || edit_vehicle.skiRack == "Non") ? false : true)
            
            changeAirCondition()
            changeBikeRack()
            changeSkiRack()
            
            ratingControl.rating = Float(edit_vehicle.ratedValue)
            
            // set make and model
            
//            selectedMakeIdx = dropdownMake.dataSource.indexOf(txtMake.text)
//            selectedModelIdx = dropdownModel.dataSource.indexOf(<#T##element: String##String#>)
            
            loadMake()
            
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // remove activity indicator from super view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    @IBAction func yearSelection(sender: UIButton) {
        
        if(dropdownYear.hidden) {
            dropdownYear.show()
        } else {
            dropdownYear.hide()
        }
    }
    
    @IBAction func makeSelection(sender: UIButton) {
        
        if(dropdownMake.hidden) {
            dropdownMake.show()
        } else {
            dropdownMake.hide()
        }
    }
    
    @IBAction func modelSelection(sender: UIButton) {
        
        if(dropdownModel.hidden) {
            dropdownModel.show()
        } else {
            dropdownModel.hide()
        }
    }
    
    @IBAction func vehicleTypeSelection(sender: UIButton) {
        
        if(dropdownVehicleType.hidden) {
            dropdownVehicleType.show()
        } else {
            dropdownVehicleType.hide()
        }
    }
    
    @IBAction func luggaseSizeSelection(sender: UIButton) {
        
        if(dropdownLuggageSize.hidden) {
            dropdownLuggageSize.show()
        } else {
            dropdownLuggageSize.hide()
        }
    }
    
    @IBAction func tireTypeSelection(sender: UIButton) {
        
        if(dropdownTireType.hidden) {
            dropdownTireType.show()
        } else {
            dropdownTireType.hide()
        }
    }
    
    @IBAction func passengerSeatSelection(sender: UIButton) {
        
        if(dropdownPassengerSeats.hidden) {
            dropdownPassengerSeats.show()
        } else {
            dropdownPassengerSeats.hide()
        }
    }
    
    @IBAction func showComfortView(sender: UIButton) {
        
        sender.selected = !sender.selected
        showComfortHelpView(sender.selected)
    }
    
    func showComfortHelpView(willShow: Bool) {

        if(willShow) {
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.comfortHelp.alpha = 1
                }, completion: { (finished) -> Void in
                    self.comfortHelp.hidden = false
            })
            
        } else {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.comfortHelp.alpha = 0
                }, completion: { (finished) -> Void in
                    self.comfortHelp.hidden = finished
            })
        }
    }
    
    @IBAction func closeAction(sender: AnyObject) {
    
        self.dismissViewControllerAnimated(true) { () -> Void in
            
            if(self.bNewCarAdded) {
                
                if(self.bComesFromPriceVehicle! && (self.priceVehicelVC != nil)) {
                   self.priceVehicelVC.loadFormData()
                    
                } else if(!self.bComesFromPriceVehicle! && (self.vehicleSettingVC != nil)) {
                    self.vehicleSettingVC.getVehicleInfo()
                }
            }
        }
    }
    
    @IBAction func cancelACtion(sender: UIButton) {
        self.dismissViewControllerAnimated(true) { () -> Void in
        }
    }
    
    @IBAction func saveAction(sender: UIButton) {
        
        if(txtYear.text?.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.require_year : R.en.require_year, duration: R.tDuration).show()
            return
        } else if(txtMake.text?.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.require_make : R.en.require_make, duration: R.tDuration).show()
            return
        } else if(txtModel.text?.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.require_model : R.en.require_model, duration: R.tDuration).show()
            return
        } else if(txtDistancePerYear.text?.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.require_distance_driven : R.en.require_distance_driven, duration: R.tDuration).show()
            return
        } else if(txtVehicleType.text?.characters.count == 0) {
            
            JLToast.makeText(defLang == "fr" ? R.fr.require_type_vehicle : R.en.require_type_vehicle, duration: R.tDuration).show()
            return
            
        } else if(txtLuggageSize.text?.characters.count == 0) {
            
            JLToast.makeText(defLang == "fr" ? R.fr.require_luggage : R.en.require_luggage, duration: R.tDuration).show()
            return
            
        } else if(txtPassengerSeat.text?.characters.count == 0) {
            
            JLToast.makeText(defLang == "fr" ? R.fr.require_seats : R.en.require_seats, duration: R.tDuration).show()
            return
            
        } else if(txt123.text?.characters.count == 0) {
            
            JLToast.makeText(defLang == "fr" ? R.fr.require_plat_number : R.en.require_plat_number, duration: R.tDuration).show()
            return
            
        }

        addorEditVehicle(addorEditVehicle)
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        
        self.view .endEditing(true)
        
        if(btnShowComfortHelpView.selected) {
            
            btnShowComfortHelpView.selected = !btnShowComfortHelpView.selected
            showComfortHelpView(btnShowComfortHelpView.selected)
        }
    }
    
    @IBAction func automaticSelected(sender: UIButton) {
        
        if(transmission == 1) {
            return
        }
        
        selectTransmission(1)
    }
    
    @IBAction func manualSelected(sender: UIButton) {
        
        if(transmission == 2) {
            return
        }
        
        selectTransmission(2)
    }
    
    func selectTransmission(_transmission: Int) {
        
        transmission = _transmission
        
        // automatic
        if(_transmission == 1) {
            imvAutomatic.image = UIImage(named: "checked1")
            imvManual.image = UIImage(named: "unchecked1")
            
        } else {
            
            imvAutomatic.image = UIImage(named: "unchecked1")
            imvManual.image = UIImage(named: "checked1")
        }
        
        loadEmission()
    }
    
    @IBAction func citySelected(sender: UIButton) {
        
        if(mileage == 1) {
            return
        }
        
        selectMileage(1)
    }
    
    @IBAction func highwaySelected(sender: UIButton) {
        
        if(mileage == 2) {
            return
        }
        
        selectMileage(2)
    }
    
    func selectMileage(_mileage: Int) {
        
        mileage = _mileage
        
        if(_mileage == 1) {
            
            imvCity.image = UIImage(named: "checked1")
            imvHighway.image = UIImage(named: "unchecked1")
            
        } else {
            
            imvCity.image = UIImage(named: "unchecked1")
            imvHighway.image = UIImage(named: "checked1")
        }
        
        loadEmission()
    }
    
    @IBAction func airCondtionClicked(sender: AnyObject) {
        
        arrAdditionSelection[0] = !arrAdditionSelection[0]
        
        changeAirCondition()
    }
    
    func changeAirCondition() {
        
        if(arrAdditionSelection[0]) {
            
            imvAirCondition.image = UIImage(named: "checked2.png")
        } else {
            
            imvAirCondition.image = UIImage(named: "unchecked2.png")
        }
    }
    
    @IBAction func bikeRackClicked(sender: AnyObject) {
        
        arrAdditionSelection[1] = !arrAdditionSelection[1]
        
        changeBikeRack()
    }
    
    func changeBikeRack() {
        
        if(arrAdditionSelection[1]) {
            
            imvBikeRack.image = UIImage(named: "checked2.png")
        } else {
            
            imvBikeRack.image = UIImage(named: "unchecked2.png")
        }
    }
    
    @IBAction func skiRackClicked(sender: AnyObject) {
        
        arrAdditionSelection[2] = !arrAdditionSelection[2]
        
        changeSkiRack()
    }
    
    func changeSkiRack() {
        if(arrAdditionSelection[2]) {
            
            imvSkiRack.image = UIImage(named: "checked2.png")
        } else {
            
            imvSkiRack.image = UIImage(named: "unchecked2.png")
        }
    }
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(ratingView: FloatRatingView, isUpdating rating:Float) {
//        self.liveLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
    }
    
    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {
        
        print("current rating value : \(rating)")
//        self.updatedLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
        
        self.carRating = Int(rating)
    }
    
    @IBAction func carImageUploadAction(sender: AnyObject) {
        print("camera selected")
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .Default) { (alert: UIAlertAction) -> Void in
            self.openCamera()
        }
        let albumAction = UIAlertAction(title: "Gallery", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (alert: UIAlertAction!) -> Void in
            
        }
        
        picker?.delegate = self
        
        alert.addAction(cameraAction)
        alert.addAction(albumAction)
        alert.addAction(cancelAction)
        presentViewController(alert, animated: true, completion: nil)
        
    }
    
    //
    func openGallery() {
        picker?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker?.allowsEditing = true
        self.presentViewController(picker!, animated: true, completion: nil)
    }
    
    func openCamera() {
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            
            picker?.sourceType = UIImagePickerControllerSourceType.Camera
            picker?.allowsEditing = true
            picker?.modalPresentationStyle = .FullScreen
            self.presentViewController(picker!, animated: true, completion: nil)
        }
    }
    
    // ---------------------------------------------------------------------------------------
    // MARK: - UIImagePickerController Delegate
    // ---------------------------------------------------------------------------------------
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let newImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            imvCar.contentMode = .ScaleAspectFit
            imvCar.image = newImage.resizedImageByMagick("168x126#")
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // ------------------------------------------------------------------------
    // MARK: UITextFieldDelegate
    // ------------------------------------------------------------------------
    func textFieldDidEndEditing(textField: UITextField) {
       
        if(textField == txtDistancePerYear) {
            loadCO2OutputEmission()
        }
    }
    
    func loadMake() {
        
        let params :[String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "year": txtYear.text!
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/load_makes", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/load_makes")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let model_cnt = jsonResponse["makes"].array?.count {
                            
                            if(model_cnt > 0) {
                                
                                self.arrMakes.removeAll()
                                self.dropdownMake.dataSource.removeAll()
                                
                                for var index = 0; index < model_cnt; index++ {
                                    
                                    let make = VehicleMake()
                                    make.value = Int(jsonResponse["makes"][index]["value"].string!)!
                                    make.label = jsonResponse["makes"][index]["label"].string!
                                    
                                    self.arrMakes.append(make)
                                    self.dropdownMake.dataSource.append(make.label)
                                }
                                
                                if(self.addorEditVehicle == 0) {
                                    
                                    self.selectedMakeIdx = self.dropdownMake.dataSource.indexOf(self.txtMake.text!)!
                                    self.loadModel()
                                }
                            }
                        }
                        
                    } else {
//                        JLToast.makeText(jsonResponse["message"].string!, duration:R.tDuration).show()
                        print("fail to offer a ride /settings/load_makes")
                    }
                } else {
                    print("Error parsing /settings/load_makes")
                }
        }
    }
    
    func loadModel() {
        
        print(selectedMakeIdx)
        
        let make_id = arrMakes[selectedMakeIdx].value
        
        let params :[String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "year": txtYear.text!,
            "make_id": "\(make_id)"
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/load_models", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/load_models")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let model_cnt = jsonResponse["models"].array?.count {
                            
                            if(model_cnt > 0) {
                                
                                self.arrModel.removeAll()
                                self.dropdownModel.dataSource.removeAll()
                                
                                for var index = 0; index < model_cnt; index++ {
                                    
                                    let model = VehicleModel()
                                    model.value = Int(jsonResponse["models"][index]["value"].string!)!
                                    model.label = jsonResponse["models"][index]["label"].string!
                                    
                                    self.arrModel.append(model)
                                    self.dropdownModel.dataSource.append(model.label)
                                }
                                
                                if(self.addorEditVehicle == 0) {
                                    
                                    self.selectedModelIdx = self.dropdownModel.dataSource.indexOf(self.txtModel.text!)!
                                }
                            }
                        }
                        
                    } else {
                        JLToast.makeText(jsonResponse["message"].string!, duration:R.tDuration).show()
                        print("fail to offer a ride /settings/load_models")
                    }
                } else {
                    print("Error parsing /settings/load_models")
                }
        }
    }
    
    func loadCO2OutputEmission() {
        
        if(selectedMakeIdx < 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.require_make : R.en.require_make, duration: R.tDuration).show()
            return
        } else if(selectedModelIdx < 0) {
            
            JLToast.makeText(defLang == "fr" ? R.fr.require_model : R.en.require_model, duration: R.tDuration).show()
            return
            
        } else if(txtDistancePerYear.text?.characters.count == 0) {
            JLToast.makeText(defLang == "fr" ? R.fr.require_distance_driven : R.en.require_distance_driven, duration: R.tDuration).show()
            return
        }
        
        let make_id = (arrMakes[selectedMakeIdx] as VehicleMake).value
        let model_id = (arrModel[selectedModelIdx] as VehicleModel).value
        
        let params :[String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "year": txtYear.text!,
            "make_id": "\(make_id)",
            "model_id": "\(model_id)",
            "distance": txtDistancePerYear.text!,
            "transmission": "\(transmission)",
            "mileage": "\(mileage)"
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/co2_emission", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/co2_emission")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        self.emission.veh_id = Int(jsonResponse["emission"]["veh_id"].string!)!
                        self.emission.transmission = Int(jsonResponse["emission"]["transmission"].string!)!
                        self.emission.city_mileage = Float(jsonResponse["emission"]["city_mileage"].string!)!
                        self.emission.highway_mileage = Float(jsonResponse["emission"]["highway_mileage"].string!)!
                        self.emission.mileage_mpg = Int(jsonResponse["emission"]["mileage_mpg"].string!)!
                        self.emission.co2_emission = Int(jsonResponse["emission"]["co2_emission"].string!)!
                        self.emission.co2_output = jsonResponse["emission"]["co2_output"].float!
                        
                        self.loadEmission()
                        
                    } else {
                        JLToast.makeText(jsonResponse["message"].string!, duration:R.tDuration).show()
                        print("fail to offer a ride /settings/co2_emission")
                    }
                } else {
                    print("Error parsing /settings/co2_emission")
                }
        }
        
    }
    
    func loadEmission() {
        
        if(emission.veh_id <= 0) {
            return
        }
        
        if(mileage == 1) {
            txtFuelEfficiency.text = String(emission.city_mileage)
        } else {
            txtFuelEfficiency.text = String(emission.highway_mileage)
        }
        
        if(transmission == 1) {
            
            txtCo2Output.text = String(self.emission.co2_output)
            
        } else {
            
            txtCo2Output.text = String(self.emission.co2_output)
        }
    }
    
    func addorEditVehicle(_addorEditVehicle: Int) {
        
        var req_item = "add"
        // add
        if(_addorEditVehicle == 0) {
            req_item = "edit"
        }
        
        let make_id = (arrMakes[selectedMakeIdx] as VehicleMake).value
        let model_id = (arrModel[selectedModelIdx] as VehicleModel).value
        
        let params :[String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "req_item": "\(req_item)",
            "veh_id": _addorEditVehicle == 1 ? "" : "\(edit_vehicle.veh_id)",
            "veh_data": [
                "ac": arrAdditionSelection[0] ? "\(1)" : "\(0)",
                "bike_rack": arrAdditionSelection[1] ? "\(1)" : "\(0)",
                "body_type": "\(body_type)",
                "co2_out": txtCo2Output.text!,
                "color": txtVehicleColor.text!,
                "comfort_level": "\(carRating)",
                "distance_driven": txtDistancePerYear.text!,
                "fuel_efficiency": txtFuelEfficiency.text!,
                "license_code": txtGNBB.text!,
                "license_number": txt123.text!,
                "luggage": "\(luggageSize)",
                "make": "\(make_id)",
                "mileage": "\(mileage)",
                "mileage_checked": "\(1)",
                "model": "\(model_id)",
                "process": "\(1)",
                "seats": "\(seats)",
                "ski_rack": arrAdditionSelection[2] ? "\(1)" : "\(0)",
                "transmission": "\(transmission)",
                "transmission_checked": "\(1)",
                "tyre_type": "\(type_tire)",
                "year": txtYear.text!
            ]
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "settings/manage_veh", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /settings/co2_emission")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {

                        self.bNewCarAdded = true
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        
                        self.dismissViewControllerAnimated(true, completion: { () -> Void in
                            
                            if(self.bNewCarAdded) {
                                
                                if(self.bComesFromPriceVehicle! && (self.priceVehicelVC != nil)) {
                                    self.priceVehicelVC.loadFormData()
                                    
                                } else if(!self.bComesFromPriceVehicle! && (self.vehicleSettingVC != nil)) {
                                    self.vehicleSettingVC.getVehicleInfo()
                                }
                            }
                        })
                        
                    } else {
                        JLToast.makeText(jsonResponse["message"].string!, duration:R.tDuration).show()
                        print("fail to offer a ride /settings/co2_emission")
                    }
                } else {
                    print("Error parsing /settings/co2_emission")
                }
        }
    }
}



 