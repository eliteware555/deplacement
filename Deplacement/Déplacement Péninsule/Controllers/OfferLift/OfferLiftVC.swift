//
//  OfferListVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/20/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class OfferLiftVC: UIViewController, NSURLConnectionDataDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var ride_key = ""
   
    var stopOverCnt = 0
    var liftKind: Int = 0   // oneoff lift = 0, regular lift = 1
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
    /**
     **    root view
     **/
    // root view height
    @IBOutlet weak var layout_height_superView: NSLayoutConstraint!
    
    /**
     **    route view
     **/
    // route view height constraint
    @IBOutlet weak var layout_height_routeView: NSLayoutConstraint!
    // map container view height
    @IBOutlet weak var layout_height_mapView: NSLayoutConstraint!       // orginal = 210
    // google map kind tab indicator
    @IBOutlet weak var layout_leading_tabIndicator: NSLayoutConstraint!     // original = 0
    
    /**
     **    date and time view
     **/
    // one-off lift return date and time container view height
    @IBOutlet weak var layout_height_oneoff_returnView: NSLayoutConstraint!    // original = 95
    @IBOutlet weak var layout_height_regular_returnView: NSLayoutConstraint!    // original  = 150
    // date and time containver view height
    @IBOutlet weak var layout_height_dateTimeSuperView: NSLayoutConstraint! // (One-off lift view = 380, regular lift view =  540)
    // regular lift view height constraint
    @IBOutlet weak var layout_height_regularLiftView: NSLayoutConstraint!    // 490 - 150 = 340
    
    // date amd time containcer view
    @IBOutlet weak var dateTimeSuperView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    // map container view
    @IBOutlet weak var mapContainer: UIView!
    // hide or show google map
    @IBOutlet weak var hideOrShowMap: UIButton!
    // google map kind tab button & indicator
    @IBOutlet weak var tabRoute: UIButton!
    @IBOutlet weak var tabDeparture: UIButton!
    @IBOutlet weak var tabDestination: UIButton!
    @IBOutlet weak var tabIndicator: UILabel!
    // hide or show mapview flag
    var isVisibleMapView = true
    
    // tab button array
    var arrTabs = [UIButton]()
    var selectedTabIdx: Int! = nil
    
    /**
     **  return date and time view toogle, return view (One-off lift & Regular lift)
     **/
    // return toggle switch
    @IBOutlet weak var oneoff_returnToogle: UISwitch!
    @IBOutlet weak var regular_returnToggle: UISwitch!
    // return container view
    @IBOutlet weak var oneoff_returnView: UIView!           // height = 90
    @IBOutlet weak var regular_returnView: UIView!          // 150
    @IBOutlet weak var regularLiftView: UIView! // regular lift date & time view
    
    /**
     **  flexibility selection dropdown
     **/
    // one-off lift
    @IBOutlet weak var oneoff_flexibilityDropDown: UIButton!
    let oneoffFlexDropdown = DropDown()
    
    var oneoff_flexibility: String = "00:15:00"
    
    // regular lift
    @IBOutlet weak var regular_flexibilityDropDown: UIButton!
    let regularFlexDropdown = DropDown()

    var regular_flexibility: String = "00:15:00"
    
    // segment control (One-off lift or Regular lift)
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
    // route view outlet
    @IBOutlet weak var txtFrom: AutoCompleteTextField!
    @IBOutlet weak var txtTo: AutoCompleteTextField!
    @IBOutlet weak var tblStopover: UITableView!
    
    @IBOutlet weak var viewMap: GMSMapView!
    
    // my location
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    // AsyncTask for getting origin, destination, route
    var mapTasks = MapTasks()
    
    var originMarker: GMSMarker!             // departure marker (blue)
    var destinationMarker: GMSMarker!           // destination marker (green)
    var routePolyline: GMSPolyline!
    
    // add way points
    var markersArray: Array<GMSMarker> = []     // stopover markers array
    var waypointsArray: Array<String> = []      // waypoints array , (lat, lon)
    var waypointsNameArray: Array<String> = []  // waypoints name array
    
    // Traveling Mode
    var travelMode = TravelModes.driving
    
    // original coordinate & destination coordinate
    var originalCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    
    @IBOutlet weak var lblRouteInfo: UILabel!
    @IBOutlet weak var viewStopOver: UIView! // autocompletetextfield superview
    @IBOutlet weak var layout_height_viewStopover: NSLayoutConstraint!
    // stopover tableview height constraint
    @IBOutlet weak var layout_height_stopoverHeight: NSLayoutConstraint!
    
    var originString = ""
    var destinationString = ""
    
    var fromLat = ""
    var fromLon = ""
    var toLat = ""
    var toLon = ""
    
    private var responseData: NSMutableData?
    private var connection: NSURLConnection?
//        private let googleMapsKey = "AIzaSyDR1WywZ2zVya_3A92sHKQrDi-bJKAHydE"
    private let googleMapsKey = "AIzaSyDg2tlPcoqxx2Q2rfjhsAKS-9j0n3JA_a4"  //
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    
    // present activated AutoCompleteTextField
    var active = 0
    
    @IBOutlet weak var btnAddStopover: UIButton!
    
    // date & time view outlet
    // one-off lift
    @IBOutlet weak var oneoff_departureDate: UITextField!
    @IBOutlet weak var oneoff_departureTime: UITextField!
    @IBOutlet weak var oneoff_returnDate: UITextField!
    @IBOutlet weak var oneoff_returnTime: UITextField!
    
    // regular lift
    @IBOutlet weak var regular_departureTime: UITextField!
    @IBOutlet weak var regular_From: UITextField!
    @IBOutlet weak var regular_To: UITextField!
    @IBOutlet weak var regular_returnTime: UITextField!
    
    @IBOutlet weak var regular_depSun: UIButton!
    @IBOutlet weak var regular_depMon: UIButton!
    @IBOutlet weak var regular_depTue: UIButton!
    @IBOutlet weak var regular_depWed: UIButton!
    @IBOutlet weak var regular_depThu: UIButton!
    @IBOutlet weak var regular_depFri: UIButton!
    @IBOutlet weak var regular_depSat: UIButton!
    
    var arrDepartures = [UIButton]()
    
    @IBOutlet weak var regular_resSun: UIButton!
    @IBOutlet weak var regular_resMon: UIButton!
    @IBOutlet weak var regular_resTue: UIButton!
    @IBOutlet weak var regular_resWed: UIButton!
    @IBOutlet weak var regular_resThu: UIButton!
    @IBOutlet weak var regular_resFri: UIButton!
    @IBOutlet weak var regular_resSat: UIButton!
    var arrReturns = [UIButton]()
    
    // regular depature date & return return date array
    var arrRegularDepatureDays = [Int]()
    var arrRegularReturnDays = [Int]()
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    // - new
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblTitleSelectRoute: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblDepatureDate: UILabel!
    @IBOutlet weak var lblDepatureTime: UILabel!
    @IBOutlet weak var lblReturn: UILabel!
    @IBOutlet weak var lblReturnDate: UILabel!
    @IBOutlet weak var lblReturnTime: UILabel!
    @IBOutlet weak var lblStopoverDescription: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblFlexibility: UILabel!
    
    @IBOutlet weak var lblRegularDepatureDays: UILabel!
    @IBOutlet weak var lblRegularDepatureTime: UILabel!
    @IBOutlet weak var lblRegularBeginningFrom: UILabel!
    @IBOutlet weak var lblRegularEndingTo: UILabel!
    @IBOutlet weak var lblRegularReturn: UILabel!
    @IBOutlet weak var lblRegularFlexibility: UILabel!
    @IBOutlet weak var lblRegularReturnDays: UILabel!
    @IBOutlet weak var lblRegularReturnTime: UILabel!
    
    var bSubmittable = false
    
    var bFromAutoCompleted = false
    var bToAutoCompleted = false
    
    var _user: UserInfo!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        // one-off flexiblity dropdown
        oneoffFlexDropdown.dataSource = ["+/- 15 minutes", "+/- 30 minutes"]
        oneoffFlexDropdown.selectionAction = { [unowned self] (index, item) in
            self.oneoff_flexibilityDropDown.setTitle(item, forState: .Normal)
            
            if(index == 0) {
                self.oneoff_flexibility = "00:15:00"
            } else {
                self.oneoff_flexibility = "00:30:00"
            }
        }
        oneoffFlexDropdown.anchorView = oneoff_flexibilityDropDown
        oneoffFlexDropdown.bottomOffset = CGPoint(x: 0, y: oneoff_flexibilityDropDown.bounds.height + 3)
        
        // regular flexibility dropdown
        regularFlexDropdown.dataSource = ["+/- 15 minutes", "+/- 30 minutes"]
        regularFlexDropdown.selectionAction = { [unowned self] (index,item) in
            
            self.regular_flexibilityDropDown.setTitle(item, forState: .Normal)
            
            if(index == 0) {
                self.regular_flexibility = "00:15:00"
            } else {
                self.regular_flexibility = "00:30:00"
            }
        }
        regularFlexDropdown.anchorView = regular_flexibilityDropDown
        regularFlexDropdown.bottomOffset = CGPoint(x: 0, y: regular_flexibilityDropDown.bounds.height + 3)
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        initUI()
    }
    
    override func viewWillAppear(animated: Bool) {

        super.viewWillAppear(animated)
        
        // customize navigation bar and status bar color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // initialize the value
    func initUI() {
    
        if self.revealViewController() != nil {
            
            if (self.revealViewController().rightViewController != nil) {
                self.revealViewController().rightViewController = nil
            }
            
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
//        // initialize right bar button item
//        let rightButton = UIButton()
//        rightButton.setImage(UIImage(named: "bell.png"), forState: .Normal)
//        rightButton.frame = CGRectMake(0, 0, 30, 30)
//        rightButton.addTarget(self, action: Selector("rightMenuClicked:"), forControlEvents: .TouchUpInside)
//        
//        let rightMenu = UIBarButtonItem()
//        rightMenu.customView = rightButton
//        self.navigationItem.rightBarButtonItem = rightMenu
        
        // set navigation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.row2 : R.en.row2
        
        arrTabs.append(tabRoute)
        arrTabs.append(tabDeparture)
        arrTabs.append(tabDestination)
        
        for btnTab in arrTabs {
            btnTab.addTarget(self, action: "onTabClicked:", forControlEvents: .TouchUpInside)
        }
        
        // shows Route Map View
        selectedTabIdx = 0
        
        // --------------
        arrDepartures.append(regular_depSun)
        arrDepartures.append(regular_depMon)
        arrDepartures.append(regular_depTue)
        arrDepartures.append(regular_depWed)
        arrDepartures.append(regular_depThu)
        arrDepartures.append(regular_depFri)
        arrDepartures.append(regular_depSat)
        
        for btnDeparture in arrDepartures {
            btnDeparture.addTarget(self, action: "onDepartureDateSelected:", forControlEvents: .TouchUpInside)
        }
        
        arrReturns.append(regular_resSun)
        arrReturns.append(regular_resMon)
        arrReturns.append(regular_resTue)
        arrReturns.append(regular_resWed)
        arrReturns.append(regular_resThu)
        arrReturns.append(regular_resFri)
        arrReturns.append(regular_resSat)
        
        for btnReturn in arrReturns {
            btnReturn.addTarget(self, action: "onReturnDateSelected:", forControlEvents: .TouchUpInside)
        }
        
        // configure autocomplete text field
        configureTextField()
        handleTextFieldInterfaces()
        
        // setup mapview (set your country location - default)
        // will be updated with user's current location
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(45.3342122, longitude: -76.0338359, zoom: 6.0)
        viewMap.camera = camera
        
        // clear map
        if let _ = self.routePolyline {
            self.clearRoute()
            self.waypointsArray.removeAll(keepCapacity: false)
        }
        
        locationManager.delegate = self
        
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined) {
            
            locationManager.requestWhenInUseAuthorization()
        } else {
            
            locationManager.startUpdatingLocation()
        }
        
        // set initial value.
        
        let date: NSDate = NSDate()
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let timeString: String = dateFormatter.stringFromDate(date)
        oneoff_departureTime.text = timeString
        oneoff_returnTime.text = timeString
        
        regular_departureTime.text = timeString
        regular_returnTime.text = timeString
    }
    
    func initView() {

        lblTitleSelectRoute.text = defLang == "fr" ? R.fr.select_your_route : R.en.select_your_route
        
        txtFrom.placeholder = defLang == "fr" ? R.fr.from_loc : R.en.from_loc
        txtTo.placeholder = defLang == "fr" ? R.fr.to_loc : R.en.to_loc
        
        btnAddStopover.setTitle(defLang == "fr" ? R.fr.add_stopover : R.en.add_stopover, forState: .Normal)
        lblStopoverDescription.text = defLang == "fr" ? R.fr.add_stopover_description : R.en.add_stopover_description
        
        lblFrequency.text = defLang == "fr" ? R.fr.frequency : R.en.frequency
        
        segmentControl.setTitle(defLang == "fr" ? R.fr.one_off : R.en.one_off, forSegmentAtIndex: 0)
        segmentControl.setTitle(defLang == "fr" ? R.fr.regular : R.en.regular, forSegmentAtIndex: 1)
        
        lblDateTime.text = defLang == "fr" ? R.fr.date_time : R.en.date_time
        lblDepatureDate.text = defLang == "fr" ? R.fr.despature_date : R.en.despature_date
        lblDepatureTime.text = defLang == "fr" ? R.fr.despature_time : R.en.despature_time
        lblFlexibility.text = defLang == "fr" ? R.fr.flexibility : R.en.flexibility
        
        tabRoute.setTitle(defLang == "fr" ? R.fr.route : R.en.route, forState: .Normal)
        tabDeparture.setTitle(defLang == "fr" ? R.fr.Depature : R.en.Depature, forState: .Normal)
        tabDestination.setTitle(defLang == "fr" ? R.fr.destination : R.en.destination, forState: .Normal)
        
        lblReturn.text = defLang == "fr" ? R.fr.return_toggle : R.en.return_toggle
        lblReturnDate.text = defLang == "fr" ? R.fr.return_date : R.en.return_date
        lblReturnTime.text = defLang == "fr" ? R.fr.return_time : R.en.return_time
        
        // regular lift
        lblRegularDepatureDays.text = defLang == "fr" ? R.fr.regular_departure_days : R.en.regular_departure_days
        lblRegularDepatureTime.text = defLang == "fr" ? R.fr.despature_time : R.en.despature_time
        lblRegularBeginningFrom.text = defLang == "fr" ? R.fr.regular_beginning_from : R.en.regular_beginning_from
        lblRegularEndingTo.text = defLang == "fr" ? R.fr.regular_end_to : R.en.regular_end_to
        
        lblRegularFlexibility.text = defLang == "fr" ? R.fr.flexibility : R.en.flexibility
        
        lblRegularReturn.text = defLang == "fr" ? R.fr.return_toggle : R.en.return_toggle
        lblRegularReturnDays.text = defLang == "fr" ? R.fr.return_date : R.en.return_date
        lblRegularReturnTime.text = defLang == "fr" ? R.fr.return_time : R.en.return_time
        
        var index = 0
        for btnDepartureDay in arrDepartures {
            
            btnDepartureDay.setTitle(defLang == "fr" ? R.fr.days[index] : R.en.days[index], forState: .Normal)
            index++
        }
        
        index = 0
        for btnReturnDay  in arrReturns {
            btnReturnDay.setTitle(defLang == "fr" ? R.fr.days[index] : R.en.days[index], forState: .Normal)
            index++
        }
        
        btnSave.setTitle(defLang == "fr" ? R.fr.save_continue : R.en.save_continue, forState: .Normal)
        
        getMembershipData();
    }
    
    // check if user pay membership fee
    func getMembershipData() {
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator);
        
        let params = [
            "login_hash" : prefs.stringForKey("USER_HASH")!,
            "site_lang" : defLang
        ]
        
        debugPrint(params)
        
        Alamofire.request(.GET, kBaseURL+"settings/membership", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    
                    print("Error: did not receive data")
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    
                    print("Error calling GET on /settings/membership")
                    
                    print(response.result.error)
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if (status) {
                        
                        if let paid = jsonResponse["membership_data"]["reg_data"]["paid"].string {
                            
                            if(Int(paid) == 1) {
                            
                                self._user.membership_verified = true
                            } else {
                                
                                self._user.membership_verified = false
                            }
                        } else {
                            self._user.membership_verified = false
                        }
                        
                    } else {
                        
                        self._user.membership_verified = false
                        
                        if let message = jsonResponse["message"].string {
                            JLToast.makeText(message, duration: R.tDuration).show()
                        }
                    }                    
                }
            })
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    private func configureTextField() {
        
        txtFrom.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        txtFrom.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        txtFrom.autoCompleteCellHeight = 35.0
        txtFrom.maximumAutoCompleteCount = 20
        txtFrom.hidesWhenSelected = true
        txtFrom.hidesWhenEmpty = true
        txtFrom.clearButtonMode = .Never
        txtFrom.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        txtFrom.autoCompleteAttributes = attributes
        
        txtTo.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        txtTo.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        txtTo.autoCompleteCellHeight = 35.0
        txtTo.maximumAutoCompleteCount = 20
        txtTo.clearButtonMode = .Never
        txtTo.hidesWhenSelected = true
        txtTo.hidesWhenEmpty = true
        txtTo.enableAttributedText = true
        txtTo.autoCompleteAttributes = attributes
    }
    
    private func handleTextFieldInterfaces() {

        txtFrom.onTextChange = {text in
            
//            self.bSubmittable = false
            
            self.bFromAutoCompleted = false
            
            if !text.isEmpty {
                
                if self.connection != nil {
                    self.connection!.cancel()
                    self.connection = nil
                }
                let urlString = "\(self.baseURLString)?key=\(self.googleMapsKey)&input=\(text)"
                let url = NSURL(string: (urlString as NSString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
                if url != nil{
                    let urlRequest = NSURLRequest(URL: url!)
                    self.connection = NSURLConnection(request: urlRequest, delegate: self)
                    self.active = 0
                }
            }
        }
        
        txtFrom.onSelect = { text, indexpath in
            
            self.bFromAutoCompleted = true
            
            if self.txtTo.text?.characters.count != 0 {
                // draw a route
                self.createRoute()
            } else {
                
                // add destination marker
                
                self.mapTasks.geocodeAddress(self.txtFrom.text, withCompletionHandler: { (status, success) -> Void in
                    if !success {
                        print(status)
                    } else {
                        
                        let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)
                        
                        self.fromLat = String(format: "%f", self.mapTasks.fetchedAddressLatitude)
                        self.fromLon = String(format: "%f", self.mapTasks.fetchedAddressLongitude)

                        // move camera position and add a marker
                        self.viewMap.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 5.0)
                        self.setupLocationMarker(coordinate)
                    }
                })
            }
            
//            Location.geocodeAddressString(text, completion: { (placemark, error) -> Void in
//                if let coordinate = placemark?.location?.coordinate{
//                    self.fromLat = String(format:"%f", coordinate.latitude)
//                    self.fromLon = String(format:"%f", coordinate.longitude)
//                    
//                    // original coordinate
//                    self.originalCoordinate = coordinate
//                    
//                    if self.txtTo.text?.characters.count != 0 {
//                        // draw a route
//                        self.createRoute()
//                    } else {
//                        // add destination marker
//                    
//                        self.mapTasks.geocodeAddress(self.txtFrom.text, withCompletionHandler: { (status, success) -> Void in
//                            if !success {
//                                print(status)
//                            } else {
//                                let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)
//                                
//                                // move camera position and add a marker
//                                self.viewMap.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 5.0)
//                                self.setupLocationMarker(coordinate)
//                            }
//                        })
//                    }
//                }
//            })
        }
        
        txtTo.onTextChange = {text in
            
//            self.bSubmittable = false
            self.bToAutoCompleted = false
            
            if !text.isEmpty{
                
                if self.connection != nil{
                    self.connection!.cancel()
                    self.connection = nil
                }
                let urlString = "\(self.baseURLString)?key=\(self.googleMapsKey)&input=\(text)"
                let url = NSURL(string: (urlString as NSString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
                if url != nil{
                    let urlRequest = NSURLRequest(URL: url!)
                    self.connection = NSURLConnection(request: urlRequest, delegate: self)
                    self.active = 1
                }
            }
        }
        
        txtTo.onSelect = {text, indexpath in
            
            self.bToAutoCompleted = true
            
            if self.txtFrom.text?.characters.count != 0 {
                // draw a route
                self.createRoute()
            } else {
                
                // add destination marker
                
                self.mapTasks.geocodeAddress(self.txtTo.text, withCompletionHandler: { (status, success) -> Void in
                    if !success {
                        print(status)
                    } else {
                        
                        let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)
                        
                        self.toLat = String(format: "%f", self.mapTasks.fetchedAddressLatitude)
                        self.toLon = String(format: "%f", self.mapTasks.fetchedAddressLongitude)
                        
                        // move camera position and add a marker
                        self.viewMap.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 5.0)
                        self.setupLocationMarker(coordinate)
                    }
                })
            }
            
//            Location.geocodeAddressString(text, completion: { (placemark, error) -> Void in
//                
//                if let coordinate = placemark?.location?.coordinate{
//                    self.toLat = String(format:"%f", coordinate.latitude)
//                    self.toLon = String(format:"%f", coordinate.longitude)
//                    
//                    // destination coordinate
//                    self.destinationCoordinate = coordinate
//                    
//                    if self.txtFrom.text?.characters.count != 0 {
//                        // draw a route
//                        self.createRoute()
//                        
//                    } else {
//                        // add destination marker
//                        self.mapTasks.geocodeAddress(self.txtTo.text, withCompletionHandler: { (status, success) -> Void in
//                            if !success {
//                                print(status)
//                            } else {
//                                let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)                                
//                                
//                                // move camera position and add a marker
//                                self.viewMap.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 5.0)
//                                self.setupLocationMarker(coordinate)
//                            }
//                        })
//                    }
//                }
//            })
        }
    }
    
    // center the map to the beginning of the route
    // add a marker to the origin point
    // add a marker to the destination point
    private func setupLocationMarker(coordinate: CLLocationCoordinate2D) {
        
        if(self.active == 0) {
            
            if originMarker != nil {
                originMarker.map = nil
            }
            
            originMarker = GMSMarker(position: coordinate)
            originMarker.map = viewMap
            originMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 32/255.0, green: 200/255.0, blue: 196/255.0, alpha: 1.0))
            originMarker.title = self.txtFrom.text
            originMarker.appearAnimation = kGMSMarkerAnimationPop
            
        } else  {
            
            if destinationMarker != nil {
                destinationMarker.map = nil
            }
            
            destinationMarker = GMSMarker(position: coordinate)
            destinationMarker.map = viewMap
            destinationMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 76/255.0, green: 217/255.0, blue: 100/255.0, alpha: 1.0))
            destinationMarker.title = self.txtTo.text
            destinationMarker.appearAnimation = kGMSMarkerAnimationPop
        }
    }
    
    private func configureMapAndMarkersForRoute() {
        
        viewMap.camera = GMSCameraPosition.cameraWithTarget(mapTasks.originCoordinate, zoom: 3.5)
        
        // origin marker
        originMarker = GMSMarker(position: self.mapTasks.originCoordinate)
        originMarker.map = self.viewMap
        originMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 32/255.0, green: 200/255.0, blue: 196/255.0, alpha: 1.0))
        originMarker.title = self.mapTasks.originAddress
        
        originalCoordinate = self.mapTasks.originCoordinate
        
        // destination marker
        destinationMarker = GMSMarker(position: self.mapTasks.destinationCoordinate)
        destinationMarker.map = self.viewMap
        destinationMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 76/255.0, green: 217/255.0, blue: 100/255.0, alpha: 1.0))
        destinationMarker.title = self.mapTasks.destinationAddress
        
        destinationCoordinate = self.mapTasks.destinationCoordinate
        
        // way points markers
        if waypointsArray.count > 0 {
            
            for waypoint in waypointsArray {
                
                let lat: Double = (waypoint.componentsSeparatedByString(",")[0] as NSString).doubleValue
                let lng: Double = (waypoint.componentsSeparatedByString(",")[1] as NSString).doubleValue
                
                let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                marker.map = viewMap
                marker.icon = GMSMarker.markerImageWithColor(UIColor.purpleColor())
                
                markersArray.append(marker)
            }
        }
    }
    
    // draw the route lines on the map.
    // Just notice how easy it’s to create the route using the GMSPath and GMSPolyline classes, 
    // as well as the route points we acquired in the MapTasks class:
    func drawRoute() {
       
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)
        routePolyline = GMSPolyline(path: path)
        routePolyline.map = viewMap
    }
    
    // route info, duration time & total distance
    func displayRouteInfo() {
        
        lblRouteInfo.text = defLang == "fr" ? R.fr.duration: R.en.duration + mapTasks.totalDuration + " " + mapTasks.totalDistance
    }
    
    func clearRoute() {
        
        if let _ = originMarker {
            originMarker.map = nil
        }
        
        if let _ = destinationMarker {
            destinationMarker.map = nil
        }
        
        if let _ = routePolyline {
            routePolyline.map = nil
        }
        
        if markersArray.count > 0 {
            for marker in markersArray {
                marker.map = nil
            }
            
            markersArray.removeAll(keepCapacity: false)
        }
    }
    
    func createRoute() {
        
        if waypointsArray.count <= 0 {
            clearRoute()
            
            mapTasks.getDirections(self.txtFrom.text! as String, destination: self.txtTo.text! as String, waypoints: nil, travelMode: travelMode, completionHandler: { (status, success) -> Void in
                
                if success {
                    
//                    self.btnAddStopover.enabled = true
                    
                    self.bSubmittable = true
                    
                    self.configureMapAndMarkersForRoute()
                    self.drawRoute()
                    self.displayRouteInfo()
                    
                } else {
                    print(status)
                }
            })
        } else {
            recreateRoute()
        }
    }
    
    func recreateRoute() {
        
        if let _ = routePolyline {
            clearRoute()
            
            mapTasks.getDirections(self.txtFrom.text! as String, destination: self.txtTo.text! as String, waypoints: waypointsArray, travelMode: travelMode, completionHandler: { (status, success) -> Void in
                
                if success {
                    
                    self.bSubmittable = true
                    
//                    self.btnAddStopover.enabled = true
                    
                    self.configureMapAndMarkersForRoute()
                    self.drawRoute()
                    self.displayRouteInfo()
                    
                } else {
                    print(status)
                }
            })
        }
    }
    
    // add a way point in array (stopover)
    // redraw route
    func addWayPoint(coordinate: CLLocationCoordinate2D) {
        
        if let _ = routePolyline {
            
            let positionString = String(format: "%f", coordinate.latitude) + "," + String(format: "%f", coordinate.longitude)
            waypointsArray.append(positionString)
            
            // if we have already departure and destination
            if txtFrom.text?.characters.count != 0 && txtTo.text?.characters.count != 0 {
                recreateRoute()
            }
        }
    }
    
    // remove a way point in array (stopover)
    // redraw route
    func removeWayPoint(index: Int) {
        
        if let _ = routePolyline {
        
            waypointsArray.removeAtIndex(index)
            waypointsNameArray.removeAtIndex(index)
            
            // if we have already departure and destination
            if txtFrom.text?.characters.count != 0 && txtTo.text?.characters.count != 0 {
                createRoute()
            }
        }
    }
    
    // ------------------------------------------------------------------------
    //   Mark: - NSURLConnectionDataDelegate
    // ------------------------------------------------------------------------
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        responseData = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        responseData?.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        
        if let data = responseData {
            do {
                let result = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                
                if let status = result["status"] as? String{
                    if status == "OK"{
                        if let predictions = result["predictions"] as? NSArray{
                            var locations = [String]()
                            for dict in predictions as! [NSDictionary]{
                                locations.append(dict["description"] as! String)
                            }
                            
                            switch active {
                            case 0:
                                self.txtFrom.autoCompleteStrings = locations
                                break
                            case 1:
                                self.txtTo.autoCompleteStrings = locations
                                break
                            default:
                                break
                            }
                            
                            return
                        }
                    }
                }
                
                switch active {
                case 0:
                    self.txtFrom.autoCompleteStrings = nil
                    break
                case 1:
                    self.txtTo.autoCompleteStrings = nil
                    break
                default:
                    break
                }
                
            }
            catch let error as NSError {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        print("Error: \(error.localizedDescription)")
    }
    
    // ------------------------------------------------------------------------
    // Mark: - IBAction
    // ------------------------------------------------------------------------
    
    // swipe departure and destination place
    @IBAction func swipeClicked(sender: UIButton) {
        
        if(txtFrom.text?.characters.count == 0 || txtTo.text?.characters.count == 0) {
            return
        }
        
        let temp1 = txtFrom.text
        let temp2 = txtTo.text
        txtFrom.text = temp2
        txtTo.text = temp1
        
        let tLat1 = fromLat
        let tLat2 = toLat
        let tLon1 = fromLon
        let tLon2 = toLon
        fromLat = tLat2
        toLat = tLat1
        fromLon = tLon2
        toLon = tLon1
        
        createRoute()
    }
    
    // hide or show google map.
    @IBAction func hideOrShowGoogleMap(sender: AnyObject) {
        
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(13.0),
            NSForegroundColorAttributeName : UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        var attributedString: NSMutableAttributedString!
        
        if(isVisibleMapView) {
            
            isVisibleMapView = false
            self.mapContainer.hidden = true
            layout_height_mapView.constant = 0
            layout_height_routeView.constant -= 250
            layout_height_superView.constant -= 250
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                
                self.view.layoutIfNeeded()
            })
            
            attributedString = NSMutableAttributedString(string:"View Route in Google Map", attributes:attrs)
            
        } else {
            
            isVisibleMapView = true
            layout_height_mapView.constant = 250
            layout_height_routeView.constant += 250
            layout_height_superView.constant += 250
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
                
            })
            
            self.mapContainer.hidden = false
            attributedString = NSMutableAttributedString(string:"Hide Google Map", attributes:attrs)
        }
        
        sender.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    // tab button click action
    func onTabClicked(sender: UIButton!) {
        
        if sender == tabRoute && selectedTabIdx != 0 {
            
            // change tab button text color
            for btnTab in arrTabs {
                btnTab.setTitleColor(UIColor(colorLiteralRed: 110/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1.0), forState: .Normal)
            }
            
            tabRoute.setTitleColor(UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0), forState: .Normal)
            selectTab(0)
            
        } else if sender == tabDeparture && selectedTabIdx != 1 {
            
            // change tab button text color
            for btnTab in arrTabs {
                btnTab.setTitleColor(UIColor(colorLiteralRed: 110/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1.0), forState: .Normal)
            }
            
            tabDeparture.setTitleColor(UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0), forState: .Normal)
            selectTab(1)
            
        } else if sender == tabDestination && selectedTabIdx != 2 {
            
            // change tab button text color
            for btnTab in arrTabs {
                btnTab.setTitleColor(UIColor(colorLiteralRed: 110/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1.0), forState: .Normal)
            }
            
            tabDestination.setTitleColor(UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0), forState: .Normal)
            selectTab(2)
        }
    }
    
    func selectTab(newTabIndex: Int) {
        
        // present new tab selected state
        self.layout_leading_tabIndicator.constant = self.tabIndicator.frame.size.width * CGFloat(newTabIndex)
        UIView.animateWithDuration(0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
        
        // do process on mapview
        selectedTabIdx = newTabIndex
        
        switch selectedTabIdx {
        case 0:
            if let _ = routePolyline {
                viewMap.camera = GMSCameraPosition.cameraWithTarget(mapTasks.originCoordinate, zoom: 3.5)
            }
            break
            
        case 1:
            if let _ = originalCoordinate {
                viewMap.camera = GMSCameraPosition.cameraWithTarget(originalCoordinate, zoom: 10.0)
            }
            break
            
        case 2:
            if let _ = destinationCoordinate {
                viewMap.camera = GMSCameraPosition.cameraWithTarget(destinationCoordinate, zoom: 10.0)
            }            
            break
            
        default:
            break
        }
    }
    
    // add stopover in route ( wayPointsArray)
    @IBAction func addStopover(sender: AnyObject) {
        
        if txtFrom.text?.characters.count == 0 || txtTo.text?.characters.count == 0 {
            
            return
        }
        
        stopOverCnt++

        layout_height_routeView.constant += 50
        layout_height_stopoverHeight.constant += 50
        layout_height_superView.constant += 50
        
        UIView.animateWithDuration(0.5) { () -> Void in
            self.view .layoutIfNeeded()
            
            self.tblStopover.reloadData()
        }
    }
    
    // lift kind select action - (One-off lift, regular lift)
    @IBAction func segmentControlAction(sender: AnyObject) {
        
        var deltaHeight: CGFloat = 150.0
        
        if(segmentControl.selectedSegmentIndex == 0 && liftKind != 0) {
            
            print("One-off lift")
            
            if(!regular_returnToggle.on) {
                
                if(oneoff_returnToogle.on) {
                    deltaHeight = 0.0
                } else {
                    deltaHeight = 95.0
                }
                
            } else {
                
                if(!oneoff_returnToogle.on) {
                    deltaHeight = 245.0
                }
            }
            
            
            // hide regular lift view
            liftKind = 0
            self.regularLiftView.hidden = true
            
            // change the date & time super container view , rootview( decrease 150)
            layout_height_dateTimeSuperView.constant -= deltaHeight
            layout_height_superView.constant -= deltaHeight
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
        } else if(segmentControl.selectedSegmentIndex == 1 && liftKind != 1) {
            
            print("Regular Lift")
            
            if(!oneoff_returnToogle.on) {
                
                if(regular_returnToggle.on) {
                    deltaHeight = 245.0
                } else {
                    deltaHeight = 95.0
                }
                
            } else {
                
                if(!regular_returnToggle.on) {
                    deltaHeight = 0
                }
            }
            
            // show regular lift view
            liftKind = 1
            self.regularLiftView.hidden = false
            
            // change the date & time super container view , rootview ( increase 150)
            layout_height_dateTimeSuperView.constant += deltaHeight
            layout_height_superView.constant += deltaHeight
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    // regular lift
    // depature date select action -  onDepartureDateSelected:
    func onDepartureDateSelected(sender: UIButton!) {
        
        sender.selected = !sender.selected
        
        if(sender.selected) {
            sender.backgroundColor = UIColor(colorLiteralRed: 178/255.0, green: 209/255.0, blue: 49/255.0, alpha: 1.0)
            
            for var index = 0; index < arrDepartures.count; index++ {
                
                if(arrDepartures[index] == sender) {
                    arrRegularDepatureDays.append(index)
                    arrRegularDepatureDays = arrRegularDepatureDays.sort()
                    
                    print(arrRegularDepatureDays)
                    break
                }
            }
            
        } else {
            sender.backgroundColor = UIColor.whiteColor()
            
            for var index = 0; index < arrDepartures.count; index++ {
                
                if(arrDepartures[index] == sender) {
                    arrRegularDepatureDays.removeAtIndex(arrRegularDepatureDays.indexOf(index)!)
                    arrRegularDepatureDays = arrRegularDepatureDays.sort()
                    
                    print(arrRegularDepatureDays)
                    break
                }
            }
        }
    }
    
    func sortFunc(num1: Int, num2: Int) -> Bool {
        return num1 < num2
    }
    
    // return date select action - onReturnDateSelected:
    func onReturnDateSelected(sender: UIButton!) {
        
        sender.selected = !sender.selected
        
        if(sender.selected) {
            sender.backgroundColor = UIColor(colorLiteralRed: 178/255.0, green: 209/255.0, blue: 49/255.0, alpha: 1.0)
            
            for var index = 0; index < arrReturns.count; index++ {
                
                if(arrReturns[index] == sender) {
                    arrRegularReturnDays.append(index)
                    arrRegularReturnDays = arrRegularReturnDays.sort()
                    
                    print(arrRegularReturnDays)
                    break
                }
            }
            
        } else {
            sender.backgroundColor = UIColor.whiteColor()
            
            for var index = 0; index < arrReturns.count; index++ {
                
                if(arrReturns[index] == sender) {
                    arrRegularReturnDays.removeAtIndex(arrRegularReturnDays.indexOf(index)!)
                    arrRegularReturnDays = arrRegularReturnDays.sort()
                    
                    print(arrRegularReturnDays)
                    break
                }
            }
        }
    }
    
    // hide and show return date & time view
    @IBAction func returnSwitchToggle(sender: AnyObject) {
        
        // One-off lift
        if(liftKind == 0) {
        
            if(self.oneoff_returnToogle.on) {
            
                // show return date & time view
                oneoff_returnView.hidden = false
                layout_height_oneoff_returnView.constant = 95;
                layout_height_dateTimeSuperView.constant += 95
                layout_height_superView.constant += 95
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            } else {
                
                // hide return date & time view
                oneoff_returnView.hidden = true
                layout_height_oneoff_returnView.constant = 0
                layout_height_dateTimeSuperView.constant -= 95
                layout_height_superView.constant -= 95
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
        // regular lift
        else {
            if(self.regular_returnToggle.on) {
                
                // show return date & time view
                regular_returnView.hidden = false
                layout_height_regular_returnView.constant = 150;
                layout_height_regularLiftView.constant += 150
                layout_height_dateTimeSuperView.constant += 150
                layout_height_superView.constant += 150
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            } else {
                
                // hide return date & time view
                regular_returnView.hidden = true
                layout_height_regular_returnView.constant = 0
                layout_height_regularLiftView.constant -= 150
                layout_height_dateTimeSuperView.constant -= 150
                layout_height_superView.constant -= 150
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    // flexibility change action
    @IBAction func flexibilityChangeAction(sender: UIButton!) {
        
        // one_off lift
        if(liftKind == 0) {
            
            if(oneoffFlexDropdown.hidden) {
                oneoffFlexDropdown.show()
            } else {
                oneoffFlexDropdown.hide()
            }
        }
        // regular lift
        else {
            
            if(regularFlexDropdown.hidden) {
                regularFlexDropdown.show()
            } else {
                regularFlexDropdown.hide()
            }
        }
    }
    
    // ------------------------------------------------------------------------
    // MARK: UITextFieldDelegate
    // ------------------------------------------------------------------------
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if(textField == txtFrom && !bFromAutoCompleted) {
            textField.text = ""
        } else if (textField == txtTo && !bToAutoCompleted) {
            textField.text = ""
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {

        
        if(textField == oneoff_departureDate || textField == oneoff_returnDate || textField == regular_From || textField == regular_To) {
            
            let date: NSDate = NSDate()
            let dateFormatter:NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let dateString: String = dateFormatter.stringFromDate(date)
            
            textField.text = dateString
            
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Date
            if(defLang == "fr") {
                datePickerView.locale = NSLocale(localeIdentifier: "fr_FR")
            }
            textField.inputView = datePickerView
            
            datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
            
        } else if(textField == oneoff_departureTime || textField == oneoff_returnTime || textField == regular_departureTime || textField == regular_returnTime) {
            
            let date: NSDate = NSDate()
            let dateFormatter:NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let timeString: String = dateFormatter.stringFromDate(date)
            
            textField.text = timeString
            
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Time
            if(defLang == "fr") {
                datePickerView.locale = NSLocale(localeIdentifier: "fr_FR")
            }
            textField.inputView = datePickerView
            
            datePickerView.addTarget(self, action: Selector("timePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        }
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        if(oneoff_departureDate.isFirstResponder()) {
            oneoff_departureDate.text = dateFormatter.stringFromDate(sender.date)
        } else if(oneoff_returnDate.isFirstResponder()) {
            oneoff_returnDate.text = dateFormatter.stringFromDate(sender.date)
        } else if (regular_From.isFirstResponder()) {
            regular_From.text = dateFormatter.stringFromDate(sender.date)
        } else if(regular_To.isFirstResponder()) {
            regular_To.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    func timePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        
        if(oneoff_departureTime.isFirstResponder()) {
            oneoff_departureTime.text = dateFormatter.stringFromDate(sender.date)
        } else if(oneoff_returnTime.isFirstResponder()) {
            oneoff_returnTime.text = dateFormatter.stringFromDate(sender.date)
        } else if (regular_departureTime.isFirstResponder()) {
            regular_departureTime.text = dateFormatter.stringFromDate(sender.date)
        } else if(regular_returnTime.isFirstResponder()) {
            regular_returnTime.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // ------------------------------------------------------------------------
    // MARK: menu button click action
    // ------------------------------------------------------------------------
    
//    func rightMenuClicked(sender: AnyObject) {
//        
//        print("right menu bell clicked")
//    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
    }
    
    // ------------------------------------------------------------------------
    //   Mark: - UITableView Datasource
    // ------------------------------------------------------------------------
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stopOverCnt
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StopOverCell", forIndexPath:  indexPath) as! StopOverCell
        
        cell.txtStopLocation.willMoveToSuperview(self.viewStopOver)
        
        cell.txtStopLocation.onSelect = { text, indexpath in
            
            cell.bAutoCompleted = true
            
            self.waypointsNameArray.append(text)
            
            Location.geocodeAddressString(text, completion: { (placemark, error) -> Void in
                if let coordinate = placemark?.location?.coordinate{
                    
                    self.addWayPoint(coordinate)
                }
            })
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.Delete) {
            
            self.stopOverCnt--
            
            let cell: StopOverCell = tableView.cellForRowAtIndexPath(indexPath) as! StopOverCell
            
            if(cell.txtStopLocation.text?.characters.count != 0) {
                self.removeWayPoint(indexPath.row)
            }
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)

            layout_height_routeView.constant -= 50
            layout_height_stopoverHeight.constant -= 50
            layout_height_superView.constant -= 50
            UIView.animateWithDuration(0.5) { () -> Void in
                self.view .layoutIfNeeded()
            }
        }
    }
    
    @IBAction func saveAction(sender: AnyObject) {
        
//        if(!_user.admin_approved) {
//            
//            showAlert((defLang == "fr" ? R.fr.title_upload_license : R.en.title_upload_license), message_prefix: (defLang == "fr" ? R.fr.upload_license_prefix : R.en.upload_license_prefix), message_mid: (defLang == "fr" ? R.fr.upload_license_middle : R.en.upload_license_middle), message_subfix: (defLang == "fr" ? R.fr.upload_license_subfix : R.en.upload_license_subfix))
//            
//            return
//        }
        
        // check the validation of input value
        if(txtFrom.text?.characters.count == 0) {
            JLToast.makeText(self.defLang == "fr" ? R.fr.invalid_departure : R.en.invalid_departure, duration: R.tDuration).show()
            return
        } else if(txtTo.text?.characters.count == 0) {
            JLToast.makeText(self.defLang == "fr" ? R.fr.invalid_destination : R.en.invalid_destination, duration: R.tDuration).show()
            return
        } else if(liftKind == 0) {
            
            if(oneoff_departureDate.text?.characters.count == 0) {
                JLToast.makeText(self.defLang == "fr" ? R.fr.require_departure_date : R.en.require_departure_date, duration: R.tDuration).show()
                return
            } else if(oneoff_returnToogle.on && oneoff_returnDate.text?.characters.count == 0) {
                JLToast.makeText(self.defLang == "fr" ? R.fr.require_return_date : R.en.require_return_date, duration: R.tDuration).show()
                return
            }
            
        } else if(liftKind == 1) {
            
            if(arrRegularDepatureDays.count == 0) {
                
                JLToast.makeText(self.defLang == "fr" ? R.fr.require_departure_days:R.en.require_departure_days, duration:R.tDuration).show()
                return
                
            } else if(arrRegularReturnDays.count == 0 && regular_returnToggle.on) {
                JLToast.makeText(self.defLang == "fr" ? R.fr.require_return_days:R.en.require_return_days, duration:R.tDuration).show()
                return
            }
            
            if(regular_From.text?.characters.count == 0) {
                JLToast.makeText(self.defLang == "fr" ? R.fr.require_initial_date:R.en.require_initial_date, duration:R.tDuration).show()
                return
            } else if(regular_To.text?.characters.count == 0) {
                JLToast.makeText(self.defLang == "fr" ? R.fr.require_final_date:R.en.require_final_date, duration:R.tDuration).show()
                return
            }
        }
        
        offerLift()
    }
    
    func offerLift() {
        
        if(!_user.admin_approved) {
            
            showAlert((defLang == "fr" ? R.fr.title_upload_license : R.en.title_upload_license), message_prefix: (defLang == "fr" ? R.fr.upload_license_prefix : R.en.upload_license_prefix), message_mid: (defLang == "fr" ? R.fr.upload_license_middle : R.en.upload_license_middle), message_subfix: (defLang == "fr" ? R.fr.upload_license_subfix : R.en.upload_license_subfix))
            
            return
            
        } else if(!_user.membership_verified) {
            
            showAlert((defLang == "fr" ? R.fr.title_not_membership_paid : R.en.title_not_membership_paid), message_prefix: (defLang == "fr" ? R.fr.not_member_paid_prefix : R.en.not_member_paid_prefix), message_mid: (defLang == "fr" ? R.fr.not_member_paid_middle : R.en.not_member_paid_middle), message_subfix: (defLang == "fr" ? R.fr.not_member_paid_subfix : R.en.not_member_paid_subfix))
            
            return
        }
        
        var _routes = Array<Dictionary<String, AnyObject>>()
        
//        print(mapTasks.selectedRoute["legs"]!.count)

        var _stopovers = Array<Dictionary<String, AnyObject>>()
        var _waypoints = Array<Dictionary<String, AnyObject>>()
        
        if let route_cnt = mapTasks.selectedRoute["legs"]?.count {
            
            for var index = 0; index < route_cnt; index++ {   //mapTasks.selectedRoute["legs"]!.count
                
                let legs = mapTasks.selectedRoute["legs"] as! Array<Dictionary<NSObject, AnyObject>>
                let startLocationDictionary = legs[index]["start_location"] as! Dictionary<NSObject,AnyObject>
                let endLocationDictionary = legs[index]["end_location"] as! Dictionary<NSObject, AnyObject>
                
                let start_lat = startLocationDictionary["lat"] as! Double
                let start_lng = startLocationDictionary["lng"] as! Double
                
                let end_lat = endLocationDictionary["lat"] as! Double
                let end_lng = endLocationDictionary["lng"] as! Double
                
                let _distance = Int((legs[index]["distance"] as! Dictionary<NSObject,  AnyObject>)["value"] as! UInt)
                let _duration = Int((legs[index]["duration"] as! Dictionary<NSObject, AnyObject>)["value"] as! UInt)
                
                let start_address = legs[index]["start_address"] as! String
                let end_address = legs[index]["end_address"] as! String
                
                if(index == 0) {
                    
                    // set start location
                    fromLat = "\(start_lat)"
                    fromLon = "\(start_lng)"
                    
                    // set end location ( in case of route count = 1)
                    toLat = "\(end_lat)"
                    toLon = "\(end_lng)"
                    
                } else if (index == route_cnt - 1) {
                    
                    // set end location
                    toLat = "\(end_lat)"
                    toLon = "\(end_lng)"
                }
                
                _routes.append(
                    [
                        "start": [
                            "address": start_address,
                            "lat": "\(start_lat)",
                            "lng": "\(start_lng)"
                        ],
                        "end": [
                            "address": end_address,
                            "lat": "\(end_lat)",
                            "lng": "\(end_lng)"
                        ],
                        "distance": "\(_distance)",
                        "duration": "\(_duration)"
                    ]
                )
                
                if(index < mapTasks.selectedRoute["legs"]!.count - 1) {
                    _stopovers.append(
                        [
                            "address": end_address,
                            "lat": "\(end_lat)",
                            "lng": "\(end_lng)"
                        ]
                    )
                    
                    _waypoints.append(
                        [
                            "location": end_address,
                            "stopover": true
                        ]
                    )
                }
            }
            
        } else {
            return
        }
        
        let totalDistance = Int(mapTasks.totalDistanceInMeters)
        let totalDuration = Int(mapTasks.totalDurationInSeconds)
        
//        let route_cnt = mapTasks.selectedRoute["legs"]!.count
        
        var regular_return: String = ""
        
        if(liftKind == 1) {
            
            if(regular_returnToggle.on) {
                regular_return = "on"
            } else {
                regular_return = "off"
            }
        }
        
//        // calculate departure time and return time
//        let dateFormatter = NSDateFormatter()
//        
//        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
////        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")
//        
//        print(oneoff_departureDate.text! + " " + oneoff_departureTime.text!)
//        
//        let gmt_oneoff_departuretime = dateFormatter.dateFromString(oneoff_departureDate.text! + " " + oneoff_departureTime.text!)
//        
//        print(gmt_oneoff_departuretime)
//        
//        dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")
//        let str_gmt_oneoff_departuretime = dateFormatter.stringFromDate(gmt_oneoff_departuretime!)
//        
//        print(str_gmt_oneoff_departuretime)

        let params: [String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "step": "route",
            "ride_data": [
                "process": "\(1)",
                "frequency":liftKind == 0 ? "one-off" : "regular",
                
                "depature_date": liftKind == 0 ? oneoff_departureDate.text! : "",
                "depature_time": liftKind == 0 ? oneoff_departureTime.text! : "",
                
                "return_depature_date": liftKind == 0 && oneoff_returnToogle.on ? oneoff_returnDate.text! : "",
                "return_depature_time": liftKind == 0 && oneoff_returnToogle.on ? oneoff_returnTime.text! : "",
                
                "flexibility": liftKind == 0 ? oneoff_flexibility : "",
                "regular_flexibility": liftKind == 1 ? regular_flexibility : "",
                
                "depature_days": liftKind == 0 ? [] : arrRegularDepatureDays,
                "regular_depature_time": liftKind == 1 ? regular_departureTime.text! :"",
                
                "regular_begining_from": liftKind == 1 ? regular_From.text! : "",
                "regular_until": liftKind == 1 ? regular_To.text! : "",
                
                "regular_return": regular_return,
                "return_days": liftKind == 1 ? arrRegularReturnDays : [],
                "regular_return_depature_time": liftKind == 1 ? regular_departureTime.text! : "",
                
                "total_distance": "\(totalDistance)",
                "total_duration": "\(totalDuration)",
                
                "start": [
                    "address": mapTasks.originAddress!,
                    "lat": fromLat,
                    "lng": fromLon
                ],
                "end": [
                    "address": mapTasks.destinationAddress!,
                    "lat": toLat,
                    "lng": toLon
                ],
                "route": _routes,
                "waypoints": _waypoints,
                "stopover": _stopovers
            ]
        ]

        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ride/offer_ride", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON { response in
                
//                print("request : \(response.request)")
//                print("response : \(response.response)")
//                print("data : \(response.data)")
//                print("result : \(response.result)")
                
                self.removeActivityView()

                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/offer_ride")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        let _redirectURL = jsonResponse["redirectUrl"].string
                        
                        let _index = _redirectURL?.rangeOfString("/", options: .BackwardsSearch)?.startIndex.advancedBy(1)
                        self.ride_key = (_redirectURL?.substringFromIndex(_index!))!
                        
                        (UIApplication.sharedApplication().delegate as! AppDelegate).bLoadFromData = true
                        
                        self.performSegueWithIdentifier("SeguePriceVehicle", sender: self)
                        
                        print("Ride Key: " + self.ride_key)
                        
                    } else {
                        print("fail to offer a ride /ride/offer_ride")
                    }
                } else {
                    print("Error parsing /ride/offer_ride")
                }
            }
    }
    
    func showAlert(title: String!, message_prefix: String!, message_mid: String!, message_subfix: String!) {
        
        // Initialize Alert Controller
        let alertController = UIAlertController(title: title, message: message_prefix + message_mid + message_subfix, preferredStyle: .Alert)
        
        let pinkBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 223/255.0, green: 25/255.0, blue: 108/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 18.0)!]
        
        let blueBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 0/255.0, green: 172/255.0, blue: 145/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 17.0)!]
        
        let attributedTitle = NSMutableAttributedString(string: title)
        attributedTitle.addAttributes(pinkBoldAttributes, range: NSMakeRange(0, title.characters.count))
        alertController.setValue(attributedTitle, forKey: "attributedTitle")
        
        let attributedMessage = NSMutableAttributedString(string: message_prefix + message_mid + message_subfix)
        attributedMessage.addAttributes(blueBoldAttributes, range: NSMakeRange(message_prefix.characters.count, message_mid.characters.count))
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        
        alertController.view.tintColor = UIColor.darkGrayColor()
        
        // Initialize Actions
        let yesString = defLang == "fr" ? R.fr.yes : R.en.yes
        let yesAction = UIAlertAction(title: yesString, style: .Default) { (action) -> Void in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // Initialize Actions
        let noString = defLang == "fr" ? R.fr.no : R.en.no
        let noAction = UIAlertAction(title: noString, style: .Default) { (action) -> Void in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // Add Actions
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        
        // Present Alert Controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "SeguePriceVehicle" {
            
            let destinationVC: Price_VehicleVC = segue.destinationViewController as! Price_VehicleVC
            destinationVC.ride_key = self.ride_key
        }
    }
}

extension OfferLiftVC: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
            
            viewMap.myLocationEnabled = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            
            viewMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 6.0, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
    
}

