//
//  ConfirmTripVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/23/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class ConfirmTripVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var ride_key = ""
    var ride_id = -1 // ride id -  for publishing a ride

    var defLang = "fr"
    var prefs = NSUserDefaults.standardUserDefaults()
    
    var isVisibleGoogleMap = true
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    // Layout constraint
    @IBOutlet weak var layout_height_mapview: NSLayoutConstraint!
    @IBOutlet weak var layout_height_trip_view: NSLayoutConstraint!
    @IBOutlet weak var layout_height_superview: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleConfirmTrip: UILabel!
    @IBOutlet weak var lblDetailsCorrect: UILabel!
    @IBOutlet weak var lblSomethingWrong: UILabel!
    
    @IBOutlet weak var lblDateFrom: UILabel!
    @IBOutlet weak var lblDateTo: UILabel!
    
    var dateFrom = ""
    var dateTo = ""
    
    @IBOutlet weak var lblStartPoint: UILabel! // time
    @IBOutlet weak var lblEndPoint: UILabel!
    @IBOutlet weak var lblPricePerPerson: UILabel!
    
    var pricePerPerson = ""
    
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var btnHideOrShowMap: UIButton!
    
    @IBOutlet weak var btnPublish: UIButton!
    
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var viewMap: GMSMapView!
    
    @IBOutlet weak var tblRideInfo: UITableView!
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    // Google Map Route
    var mapTasks = MapTasks()
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    var routePolyline: GMSPolyline!
    var markersArray: Array<GMSMarker> = []
    var waypointsArray: Array<String> = []          // ("latitude, longitude") - text value
    var travelMode = TravelModes.driving
    
    
    // temp variable to set on ui, for drawing a route ( need to keep real address)
    var fromField = ""
    var toField = ""
    
    var start_loc = ""
    var end_loc = ""
    
    var departureDate = ""
    var returnDate = ""
    
    // regular lift
    var departureDays: [Int] = []
    var returnDays: [Int] = []
    
    var stopovers_info = ""
    var vehicle_info = ""
    var booking_confirmation = ""
    var detour = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        navigationItem.title = defLang == "fr" ? R.fr.row2 : R.en.row2
        
//        // initialize right bar button item
//        let rightButton = UIButton()
//        rightButton.setImage(UIImage(named: "bell.png"), forState: .Normal)
//        rightButton.frame = CGRectMake(0, 0, 30, 30)
//        rightButton.addTarget(self, action: Selector("rightMenuClicked:"), forControlEvents: .TouchUpInside)
//        
//        let rightMenu = UIBarButtonItem()
//        rightMenu.customView = rightButton
//        self.navigationItem.rightBarButtonItem = rightMenu
        
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
        
        // setup mapview ( set your country location -default)
        // will be updated with user's current location
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(45.3342122, longitude: -76.0338359, zoom: 6.0)
        viewMap.camera = camera
        viewMap.settings.zoomGestures = true
        
        // request a location when in use 
        // it will be run at once
        locationManager.delegate = self
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined) {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.startUpdatingLocation()
        }
        
        if let _ = self.routePolyline {
            self.clearRoute()
            self.waypointsArray.removeAll(keepCapacity: false)
        }
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
        
        loadFormData()
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).bLoadFromData = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initView() {
        
        lblTitleConfirmTrip.text = defLang == "fr" ? R.fr.confirm_trip : R.en.confirm_trip
        lblDetailsCorrect.text = defLang == "fr" ? R.fr.details_correct : R.en.details_correct
        lblSomethingWrong.text = defLang == "fr" ? R.fr.something_wrong : R.en.something_wrong
        
        lblStartPoint.text = defLang == "fr" ? R.fr.from + ":" : R.en.from + ":"
        lblEndPoint.text = defLang == "fr" ? R.fr.to + ":" : R.en.to + ":"
    }
    
    // remove activity indicator from super view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    @IBAction func publishLiftAction(sender: AnyObject?) {
        
        publishLift()
    }
    
    @IBAction func hideorShowGoogleMap(sender: AnyObject) {
        
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(13.0),
            NSForegroundColorAttributeName : UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        var attributedString: NSMutableAttributedString!
        
        if(isVisibleGoogleMap) {
            
            isVisibleGoogleMap = false
            
            self.mapContainer.hidden = true
            layout_height_mapview.constant = 0
            layout_height_trip_view.constant -= 210
            layout_height_superview.constant -= 210
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
            attributedString = NSMutableAttributedString(string:"View Route in Google Map", attributes:attrs)
            
        } else {
            
            isVisibleGoogleMap = true
            
            self.mapContainer.hidden = false
            layout_height_mapview.constant = 210
            layout_height_trip_view.constant += 210
            layout_height_superview.constant += 210
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
            attributedString = NSMutableAttributedString(string:"Hide Google Map", attributes:attrs)
        }
        
        sender.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    // -----------------------------------------------------------------------------
    // Mark: UITableView Datasource
    // -----------------------------------------------------------------------------
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("PublishStopOverCell", forIndexPath: indexPath) as! PublishStopOverCell
        switch indexPath.row {
        case 0:
            cell.lblSubject.text = defLang == "fr" ? R.fr.stopover : R.en.stopover
            cell.lblContent.text = stopovers_info
            
            break
        case 1:
            
            cell.lblSubject.text = defLang == "fr" ? R.fr.vehicle : R.en.vehicle
            cell.lblContent.text = vehicle_info
            break
        case 2:
            
            cell.lblSubject.text = defLang == "fr" ? R.fr.confirmation : R.en.confirmation
            cell.lblContent.text = booking_confirmation
            break
        case 3:
            
            cell.lblSubject.text = defLang == "fr" ? R.fr.detour : R.en.detour
            cell.lblContent.text = detour
            break
            
        default: ()
        }
        
        return cell
    }
    
    func clearRoute() {
        
        if let _ = originMarker {
            originMarker.map = nil
        }
        
        if let _ = destinationMarker {
            destinationMarker.map = nil
        }
        
        if let _ = routePolyline {
            routePolyline.map = nil
        }
        
        originMarker = nil
        destinationMarker = nil
        routePolyline = nil
        
        if markersArray.count > 0 {
            for marker in markersArray {
                marker.map = nil
            }
            
            markersArray.removeAll(keepCapacity: false)
        }
    }
    
    func createRoute() {
        if waypointsArray.count <= 0 {
            clearRoute()
            
            mapTasks.getDirections(self.lblStartPoint.text! as String, destination: self.lblEndPoint.text! as String, waypoints: nil, travelMode: travelMode, completionHandler: { (status, success) -> Void in
                
                if success {
                    
                    self.configureMapAndMarkersForRoute()
                    self.drawRoute()
                    self.displayRouteInfo()
                    
                } else {
                    print(status)
                }
            })
        } else {
            recreateRoute()
        }
    }
    
    func recreateRoute() {
        
        if let _ = routePolyline {
            clearRoute()
        }
        
        mapTasks.getDirections(self.fromField as String, destination: self.toField as String, waypoints: waypointsArray, travelMode: travelMode, completionHandler: { (status, success) -> Void in
            
            if success {
                
                self.configureMapAndMarkersForRoute()
                self.drawRoute()
                self.displayRouteInfo()
                
            } else {
                print(status)
            }
        })
    }
    
    func configureMapAndMarkersForRoute() {
        
        viewMap.camera = GMSCameraPosition.cameraWithTarget(mapTasks.originCoordinate, zoom: 3.5)
        
        // origin marker
        originMarker = GMSMarker(position: self.mapTasks.originCoordinate)
        originMarker.map = self.viewMap
        originMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 32/255.0, green: 200/255.0, blue: 196/255.0, alpha: 1.0))
        originMarker.title = self.mapTasks.originAddress
        
        // destination marker
        destinationMarker = GMSMarker(position: self.mapTasks.destinationCoordinate)
        destinationMarker.map = self.viewMap
        destinationMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 76/255.0, green: 217/255.0, blue: 100/255.0, alpha: 1.0))
        destinationMarker.title = self.mapTasks.destinationAddress
        
        if waypointsArray.count > 0 {
            for waypoint in waypointsArray {
                let lat: Double = (waypoint.componentsSeparatedByString(",")[0] as NSString).doubleValue
                let lng: Double = (waypoint.componentsSeparatedByString(",")[1] as NSString).doubleValue
                
                let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                marker.map = viewMap
                marker.icon = GMSMarker.markerImageWithColor(UIColor.purpleColor())
                
                markersArray.append(marker)
            }
        }
    }
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)
        routePolyline = GMSPolyline(path: path)
        routePolyline.map = viewMap
    }
    
    // route info, duration time & total distance
    func displayRouteInfo() {
        
        lblDuration.text = ((defLang == "fr" ? R.fr.duration : R.en.duration) + mapTasks.totalDuration + " " + mapTasks.totalDistance)
    }
    
    func loadFormData() {
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "step": "confirm",
            "key": ride_key,
            "site_lang": defLang
        ]
        
        print(params)
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ride/offer_ride/confirm/" + ride_key, parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/offer_ride")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if (jsonResponse["status"].bool != nil || jsonResponse["status"].string != nil) {
                    
                    // ride_id for publishing a ride              
                    self.ride_id = Int(jsonResponse["form_data"]["ride"]["ride_id"].string!)!
                    
                    self.fromField = jsonResponse["form_data"]["ride"]["map_data"]["start"]["address"].string!
                    self.toField = jsonResponse["form_data"]["ride"]["map_data"]["end"]["address"].string!
                    
                    self.start_loc = (self.defLang == "fr" ? jsonResponse["form_data"]["ride"]["start_point_fr"].string! : jsonResponse["form_data"]["ride"]["start_point_en"].string!)
                    self.end_loc = (self.defLang == "fr" ? jsonResponse["form_data"]["ride"]["end_point_fr"].string! : jsonResponse["form_data"]["ride"]["end_point_en"].string!)
                    
//                    self.departureDate = jsonResponse["form_data"]["ride"]["departure_time"].string!
//                    self.returnDate = jsonResponse["form_data"]["ride"]["return_time"].string!
                    
                    print(jsonResponse["form_data"]["ride"]["departure_time"].string!)
                    print(jsonResponse["form_data"]["ride"]["return_time"].string!)
                    
                    let _departure_date = NSDate(timeIntervalSince1970: Double(jsonResponse["form_data"]["ride"]["unix_start_date"].string!)!)
                    let _end_date = NSDate(timeIntervalSince1970: Double(jsonResponse["form_data"]["ride"]["unix_end_date"].string!)!)
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    self.departureDate = dateFormatter.stringFromDate(_departure_date)
                    self.returnDate = dateFormatter.stringFromDate(_end_date)
                    
                    print(self.departureDate)
                    print(self.returnDate)
                    
                    // departure days and return days
                    if let departureDaysCount = jsonResponse["form_data"]["ride"]["departure_days"].array?.count {
                        if (departureDaysCount > 0) {
                            self.departureDays.removeAll()
                            for var index = 0; index < departureDaysCount; index++ {
                                self.departureDays.append(Int(jsonResponse["form_data"]["ride"]["departure_days"][index].string!)!)
                            }
                        }
                    }
                    
                    if let returnDaysCount = jsonResponse["form_data"]["ride"]["return_days"].array?.count {
                        if (returnDaysCount > 0) {
                            self.returnDays.removeAll()
                            for var index = 0; index < returnDaysCount; index++ {
                                self.returnDays.append(Int(jsonResponse["form_data"]["ride"]["return_days"][index].string!)!)
                            }
                        }
                    }
                    
                    // price per person
                    self.pricePerPerson = String(Int(Float(jsonResponse["form_data"]["ride"]["price_per_seat"].string!)!))
                    
                    // stopover details
                    if let stopover_cnt = jsonResponse["form_data"]["ride"]["map_data"]["waypoints"].array?.count {
                        
                        if (stopover_cnt > 0) {
                            
                            self.stopovers_info = jsonResponse["form_data"]["ride"]["map_data"]["waypoints"][0]["location"].string!
                            for var index = 1; index < stopover_cnt; index++ {
                                self.stopovers_info += "\n" + jsonResponse["form_data"]["ride"]["map_data"]["waypoints"][index]["location"].string!
                            }
                        }
                    }
                    
                    // waypoints array for drawing a route
                    if let waypoint_cnt = jsonResponse["form_data"]["ride"]["map_data"]["stopovers"].array?.count {
                        
                        if (waypoint_cnt > 0) {
                            
                            self.waypointsArray.removeAll()
                            for var index = 0; index < waypoint_cnt; index++ {
                                
                                let positionString = jsonResponse["form_data"]["ride"]["map_data"]["stopovers"][index]["lat"].string! + "," + jsonResponse["form_data"]["ride"]["map_data"]["stopovers"][index]["lng"].string!
                                
                                self.waypointsArray.append(positionString)
                            }
                        }
                        
                    }
                    
                    // vehicle details
                    self.vehicle_info = jsonResponse["form_data"]["vehicle"]["year"].string! + jsonResponse["form_data"]["vehicle"]["make"].string! + "\n" + jsonResponse["form_data"]["vehicle"]["model"].string! + "\n" + "\(Int(jsonResponse["form_data"]["vehicle"]["seats"].string!)!)" + " seat(s)" + " "
                    
                    let luggageSize = Int(jsonResponse["form_data"]["ride"]["luggage_per_seat"].string!)!
                    
                    switch luggageSize {
                    case 1:
                        self.vehicle_info += (self.defLang == "fr" ? R.fr.luggage_small : R.en.luggage_small)
                        break
                        
                    case 2:
                        self.vehicle_info += (self.defLang == "fr" ? R.fr.luggage_medium : R.en.luggage_medium)
                        break
                        
                    case 3:
                        self.vehicle_info += (self.defLang == "fr" ? R.fr.luggage_large : R.en.luggage_large)
                        break
                        
                    default: ()
                    }
                    
                    self.vehicle_info += " Luggage"
                    
                    // booking confirmation
                    let _booking_confirmation = Int(jsonResponse["form_data"]["ride"]["auto_accept"].string!)!
                    
                    if(_booking_confirmation  == 1) {
                        self.booking_confirmation = self.defLang == "fr" ? R.fr.book_manually : R.en.book_manually
                    } else {
                        self.booking_confirmation = self.defLang == "fr" ? R.fr.book_automatically : R.en.book_automatically
                    }
                    
                    // detour
                    if let _detour = Int(jsonResponse["form_data"]["ride"]["detour"].string!) {
                        
                        switch _detour {
                            
                        case 1:
                            self.detour = self.defLang == "fr" ? R.fr.detour_none : R.en.detour_none
                            break
                            
                        case 2:
                            self.detour = self.defLang == "fr" ? R.fr.detour_15mins : R.en.detour_15mins
                            break
                            
                        case 3:
                            self.detour = self.defLang == "fr" ? R.fr.detour_30mins : R.en.detour_30mins
                            break
                            
                        case 4:
                            self.detour = self.defLang == "fr" ? R.fr.detour_any : R.en.detour_any
                            break
                            
                        default: ()
                        }
                    }
                    
                    self.loadPageDetails()
                    self.loadMap()
                    

                } else {
                    print("Error parsing /ride/offer_ride")
                }
        }
    }
    
    // update ui configuration with form date from a server
    // it is done before submitting a ride
    func loadPageDetails() {
        
        lblStartPoint.text = ((defLang == "fr" ? R.fr.from : R.en.from) + ": " + start_loc)
        lblEndPoint.text = ((defLang == "fr" ? R.fr.to : R.en.to) + ": " + end_loc)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let _departureDate = dateFormatter.dateFromString(departureDate)
        let _returnDate = dateFormatter.dateFromString(returnDate)
        
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        
        if (defLang == "fr" ) {
            dateFormatter.locale = NSLocale(localeIdentifier: "fr_FR")
        }
        
        // price per person
        let _pricePerPerson = "$ " + pricePerPerson + "/person"
        let greenBoldAttributes = [NSForegroundColorAttributeName: UIColor(colorLiteralRed: 32/255.0, green: 200/255.0, blue: 196/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 15.0)!]
        let attrubtedPriceString = NSMutableAttributedString(string: _pricePerPerson as String)
        attrubtedPriceString.addAttributes(greenBoldAttributes, range: NSMakeRange(0, pricePerPerson.characters.count + 2))
        lblPricePerPerson.attributedText = attrubtedPriceString
        
        lblDateFrom.text = dateFormatter.stringFromDate(_departureDate!)
        lblDateTo.text = dateFormatter.stringFromDate(_returnDate!)
        
        if(departureDays.count > 0) {
            
            lblDateFrom.text = lblDateFrom.text! +  " ["
            for var index = 0; index < departureDays.count; index++ {
                lblDateFrom.text = lblDateFrom.text! + (defLang == "fr" ? R.fr.days[departureDays[index]] : R.en.days[departureDays[index]])
            }
            
            lblDateFrom.text = lblDateFrom.text! +  "]"
        }
        
        if(returnDays.count > 0) {
            
            lblDateTo.text = lblDateTo.text! +  " [" + (defLang == "fr" ? R.fr.days[departureDays[0]] : R.en.days[departureDays[0]])
            
            for var index = 1; index < departureDays.count; index++ {
                lblDateTo.text = lblDateTo.text! + ", " + (defLang == "fr" ? R.fr.days[departureDays[index]] : R.en.days[departureDays[index]])
            }
            
            lblDateTo.text = lblDateTo.text! +  "]"
        }
        
        tblRideInfo.reloadData()
    }
    
    func loadMap() {
        
        if(fromField.characters.count != 0 && toField.characters.count != 0) {
            
            createRoute()
        }
    }
    
    func publishLift() {
        
        if(ride_id < 0) {
            return
        }
        
        let params: [String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "step": "confirm",
            "key": ride_key,
            "ride_data": [
                "process": "\(1)",
                "ride_id":  "\(ride_id)"
            ]
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ride/offer_ride", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/offer_ride")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        
                        self.gotoFindLift()
                        
                    } else {
                        print("fail to offer a ride /ride/offer_ride")
                    }
                } else {
                    print("Error parsing /ride/offer_ride")
                }
        }
    }
    
    func gotoFindLift() {
        
        let mainstoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle:  nil)
        let destController = mainstoryboard.instantiateViewControllerWithIdentifier("MyNavController") as! UINavigationController
        
        self.revealViewController().pushFrontViewController(destController, animated: true)
    }
    
}

extension ConfirmTripVC: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
            
            viewMap.myLocationEnabled = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            
            viewMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 6.0, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}

//        let greenBoldAttributes = [NSForegroundColorAttributeName: UIColor(colorLiteralRed: 32/255.0, green: 200/255.0, blue: 196/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 15.0)!]
//
//        let blackBoldAttributes = [NSForegroundColorAttributeName: UIColor(colorLiteralRed: 48/255.0, green: 48/255.0, blue: 48/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 15.0)!]
//
//        let attributedDateFrom = NSMutableAttributedString(string: dateFrom as String)
//        attributedDateFrom.addAttributes(blackBoldAttributes, range: NSMakeRange(0, 2))
//        attributedDateFrom.addAttributes(greenBoldAttributes, range: NSMakeRange(17, 5))
//        lblDeparture.attributedText = attributedDateFrom
//
//        let attributedDateTo = NSMutableAttributedString(string: dateTo as String)
//        attributedDateTo.addAttributes(blackBoldAttributes, range: NSMakeRange(0, 2))
//        attributedDateTo.addAttributes(greenBoldAttributes, range: NSMakeRange(17, 5))
//        lblDestination.attributedText = attributedDateFrom