//
//  GivenRateViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/6/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON


class GivenRateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    var bGiven: Bool = true
    var receivedRatingCount = 0
    var givenRatingCount = 0
    
    var arrRatings: [Rating] = []
    
    @IBOutlet weak var lblNoticeNoData: UILabel!
    @IBOutlet weak var imvIntro: UIImageView!
    
    @IBOutlet weak var tblGivenReviews: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if(bGiven) {
            navigationItem.title = defLang == "fr" ? R.fr.rating_given : R.en.rating_given
        } else {
            navigationItem.title = defLang == "fr" ? R.fr.rating_received : R.en.rating_received
        }
        
        tblGivenReviews.estimatedRowHeight = 150
        tblGivenReviews.rowHeight = UITableViewAutomaticDimension
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if(bGiven) {
            getGivenRatings()
            lblNoticeNoData.text = defLang == "fr" ? R.fr.no_given_ratings : R.en.no_given_ratings
        } else {
            getReceivedRatings()
            lblNoticeNoData.text = defLang == "fr" ? R.fr.no_received_ratings : R.en.no_received_ratings
        }
    }
    
    // ------------------------------------------------------------------------
    //   Mark: UITableView Datasource
    // ------------------------------------------------------------------------
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrRatings.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("GivenRatingCell", forIndexPath: indexPath) as! GivenRatingCell
        
//        let rating = arrRatings[indexPath.row] as Rating
        cell.setRatingModel(arrRatings[indexPath.row])
        
        return cell
    }
    
    func getGivenRatings() {
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        
        activityIndicator.startAnimating()
        self.view .addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ratings/rating_given", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ratings/rating_received")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let ratingCnt = jsonResponse["rating_data"]["rating_datas"].array?.count {
                            
                            if (ratingCnt > 0) {
                                
                                self.arrRatings.removeAll()
                                
                                for var index = 0; index < ratingCnt; index++ {
                                    
                                    let _rating = Rating()
                                    
                                    _rating.name = jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["first_name"].string! +  " " + jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["last_name"].string!
                                    _rating.vote = Float(jsonResponse["rating_data"]["rating_datas"][index]["vote"].string!)!
                                    
                                    let dateFormatter = NSDateFormatter()
                                    
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let voted_date = dateFormatter.dateFromString(jsonResponse["rating_data"]["rating_datas"][index]["voted_date"].string!)
                                    
                                    dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                                    if(self.defLang == "fr") {
                                        dateFormatter.locale = NSLocale(localeIdentifier: "fr_FR")
                                    }
                                    _rating.voted_date = dateFormatter.stringFromDate(voted_date!)
                                    
                                    _rating.comment = jsonResponse["rating_data"]["rating_datas"][index]["comment"].string!
                                    
                                    if let profile_pic = jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["profile_pic"].string {
                                        _rating.user_pic = profile_pic
                                    }
                                    
                                    if let _ = jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["gender"].string {
                                        _rating.gender = Int(jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["gender"].string!)!
                                    }
                                    
                                    self.arrRatings.append(_rating)
                                }
                                
//                                print(self.arrRatings.count)
                                
                                self.imvIntro.hidden = true
                                
                                self.tblGivenReviews.reloadData()
                                
                            } else {
                                
                                self.lblNoticeNoData.hidden = false
                            }
                            
                        } else {
                            
                            self.lblNoticeNoData.hidden = false
                        }
                        
                    } else {
                        
                        self.lblNoticeNoData.hidden = false
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        return
                    }
                    
                } else {
                    
                    self.lblNoticeNoData.hidden = false
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    print("Error parsing /ratings/rating_receivede")
                }
                
            })
    }
    
    func getReceivedRatings() {
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        //        print(params)
        
        activityIndicator.startAnimating()
        self.view .addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ratings/rating_received", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ratings/rating_received")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        if let ratingCnt = jsonResponse["rating_data"]["rating_datas"].array?.count {
                            
                            if (ratingCnt > 0) {
                                
                                self.arrRatings.removeAll()   //keepCapacity: false
                                
                                for var index = 0; index < ratingCnt; index++ {
                                    
                                    let _rating = Rating()
                                    
                                    _rating.name = jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["first_name"].string! +  " " + jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["last_name"].string!
                                    _rating.vote = Float(jsonResponse["rating_data"]["rating_datas"][index]["vote"].string!)!
                                    
                                    let dateFormatter = NSDateFormatter()
                                    
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let voted_date = dateFormatter.dateFromString(jsonResponse["rating_data"]["rating_datas"][index]["voted_date"].string!)
                                    
                                    dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                                    if(self.defLang == "fr") {
                                        dateFormatter.locale = NSLocale(localeIdentifier: "fr_FR")
                                    }
                                    _rating.voted_date = dateFormatter.stringFromDate(voted_date!)
                                    
                                    if let _ = jsonResponse["rating_data"]["rating_datas"][index]["comment"].string {
                                    
                                        _rating.comment = jsonResponse["rating_data"]["rating_datas"][index]["comment"].string!
                                    }
                                    
                                    if let _ = jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["profile_pic"].string {
                                    
                                        _rating.user_pic = jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["profile_pic"].string!
                                    }
                                    
                                    if let _ = jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["gender"].string {
                                        
                                        _rating.gender = Int(jsonResponse["rating_data"]["rating_datas"][index]["user_data"]["gender"].string!)!
                                    }
                                    
                                    self.arrRatings.append(_rating)
                                }
                                
//                                print(self.arrRatings.count)
                                
                                self.imvIntro.hidden = true
                                
                                self.tblGivenReviews.reloadData()
                                
                            } else {
                                
                                self.lblNoticeNoData.hidden = false
                            }
                            
                        } else {
                            
                            self.lblNoticeNoData.hidden = false
                        }
                        
                    } else {
                        
                        self.lblNoticeNoData.hidden = false
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        return
                    }
                    
                } else {
                    
                    self.lblNoticeNoData.hidden = false
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    print("Error parsing /ratings/rating_receivede")
                }
                
            })
    }}