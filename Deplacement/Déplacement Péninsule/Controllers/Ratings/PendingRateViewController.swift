//
//  PendingRateViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/6/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class PendingRateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    @IBOutlet weak var tblPendingRatingList: UITableView!
    
    var pendingRatings: [PendingRating] = []
    
    var selectedIdx = -1
    
    @IBOutlet weak var lblNoticeNoData: UILabel!
    @IBOutlet weak var imvIntro: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // Do any additional setup after loading the view.
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.rating_pending : R.en.rating_pending
        
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        lblNoticeNoData.hidden = true
        
        lblNoticeNoData.text = defLang == "fr" ? R.fr.no_pending_ratings : R.en.no_pending_ratings
        
        getPendingRatings()
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }

    // ------------------------------------------------------------------------
    //   Mark: UITableView Datasource
    // ------------------------------------------------------------------------
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 135
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pendingRatings.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("PendingRatingBeforeCell", forIndexPath: indexPath) as! PendingRatingBeforeCell
        
        cell.setPendingRating(pendingRatings[indexPath.row])
        
        cell.btnRate.tag = 200 + indexPath.row
        
        return cell
    }
    
    @IBAction func RateNowClicked(sender: UIButton) {
        
        selectedIdx = sender.tag - 200
        self.performSegueWithIdentifier("Segue2RatingNow", sender: self)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        
        let destinationVC = segue.destinationViewController as! RateNowViewController
        destinationVC.pendingRating = self.pendingRatings[selectedIdx]
    }
    
    func getPendingRatings() {

        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]

//        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view .addSubview(activityIndicator)

        Alamofire.request(.POST, kBaseURL + "ratings/rating_pending", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON(completionHandler: { response in

                self.removeActivityView()

                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }

                guard response.result.error == nil else {
                    print("Error calling GET on /ratings/rating_pending")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }

                let jsonResponse = JSON(value)
                print(jsonResponse)

                if let status = jsonResponse["status"].bool {

                    if(status) {

                        if let pendingCnt = jsonResponse["rating_data"]["trips"].array?.count {

                            if (pendingCnt > 0) {

                                self.pendingRatings.removeAll()

                                for var index = 0; index < pendingCnt; index++ {

                                    let _pending = PendingRating()

                                    if let ride_hash = jsonResponse["rating_data"]["trips"][index]["ride_hash"].string {
                                        _pending.ride_hash = ride_hash
                                    }

                                    if let ride_trip = jsonResponse["rating_data"]["trips"][index]["ride_trip"].int {
                                        _pending.ride_trip = ride_trip
                                    }

                                    if let start_point = jsonResponse["rating_data"]["trips"][index]["start_point"].string {
                                        _pending.start_point = start_point
                                    }

                                    if let end_point = jsonResponse["rating_data"]["trips"][index]["end_point"].string {
                                        _pending.end_point = end_point
                                    }

                                    if let date_time = jsonResponse["rating_data"]["trips"][index]["date_time"].string {

                                        if (date_time != "") {
                                            
                                            let dateFormatter = NSDateFormatter()

                                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                            let voted_date = dateFormatter.dateFromString(date_time)

                                            dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                                            if(self.defLang == "fr") {
                                                dateFormatter.locale = NSLocale(localeIdentifier: "fr_FR")
                                            }
                                            _pending.date_time = dateFormatter.stringFromDate(voted_date!)
                                        }
                                    }

                                    if let passengerCnt = jsonResponse["rating_data"]["trips"][index]["passengers"].array?.count {

                                        if(passengerCnt > 0) {

                                            _pending.passengers.removeAll()

                                            for var idx = 0; idx < passengerCnt; idx++ {

                                                let _passenger = Passenger()

                                                if let pas_hash_key = jsonResponse["rating_data"]["trips"][index]["passengers"][idx]["pas_hash_key"].string {
                                                    _passenger.pas_hash_key = pas_hash_key
                                                }

                                                if let first_name = jsonResponse["rating_data"]["trips"][index]["passengers"][idx]["first_name"].string {
                                                    _passenger.first_name = first_name
                                                }

                                                if let last_name = jsonResponse["rating_data"]["trips"][index]["passengers"][idx]["last_name"].string {
                                                    _passenger.last_name = last_name
                                                }

                                                if let profile_pic = jsonResponse["rating_data"]["trips"][index]["passengers"][idx]["profile_pic"].string {
                                                    _passenger.profile_pic = profile_pic
                                                }
                                                
                                                if let _ = jsonResponse["rating_data"]["trips"][index]["passengers"][idx]["gender"].string {
                                                    _passenger.gender = Int(jsonResponse["rating_data"]["trips"][index]["passengers"][idx]["gender"].string!)!
                                                }

                                                _pending.passengers.append(_passenger)
                                            }
                                        }
                                    }

                                    self.pendingRatings.append(_pending)
                                }
                                
                                self.tblPendingRatingList.reloadData()
                                
                                self.imvIntro.hidden = true
                                
                            } else {
                                
                                self.lblNoticeNoData.hidden = false
                            }
                        } else {
                            
                            self.lblNoticeNoData.hidden = false
                        }
                        
                    } else {
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        self.lblNoticeNoData.hidden = false
                        return
                    }
                    
                } else {
                    
                    print("Error parsing /ratings/rating_pending")
                    self.lblNoticeNoData.hidden = false
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                }
                
            })
    }
}


