//
//  RateNowViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/7/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class RateNowViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"

    @IBOutlet weak var tblPendingRating: UITableView!
    @IBOutlet weak var lblFromTo: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var pendingRating: PendingRating!
    
    var passengers: [Passenger] = []
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    var already_exist = false
    
    var ride_created_time = ""
    
    // for keyboardControl
    var selectedIndexPath: NSIndexPath!
    var bWillMoveView: Bool = false
    
    var user_role = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // Do any additional setup after loading the view.
        navigationItem.title = defLang == "fr" ? R.fr.rating_pending : R.en.rating_pending
        
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
        
        getRatingCompanign()
    }
    
    func initView() {
        
        lblFromTo.text = pendingRating.start_point + "->\n" + pendingRating.end_point
        lblDateTime.text = pendingRating.date_time
        
        btnSave.setTitle(defLang == "fr" ? R.fr.btn_save : R.en.btn_save, forState: .Normal)
        btnCancel.setTitle(defLang == "fr" ? R.fr.btn_cancel : R.en.btn_cancel, forState: .Normal)
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ------------------------------------------------------------------------
    //   Mark: UITableView Datasource
    // ------------------------------------------------------------------------
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 145
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passengers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("PendingRatingCell", forIndexPath: indexPath) as! PendingRatingCell
//        cell.btnSend.tag = 200 + indexPath.row*2
//        cell.btnCancel.tag = 200 + indexPath.row*2 + 1
        
        cell.txvRating.tag = 500 + indexPath.row
        
        cell.setPassenger(passengers[indexPath.row])
        
        cell.selectionStyle = .None  
        
        return cell
    }
    
    
    // ---------------------------------------------------------------
    // MARK: UITextField Delegate
    // ---------------------------------------------------------------
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        selectedIndexPath = NSIndexPath(forRow: textView.tag - 500, inSection: 0)
        
        if(selectedIndexPath.row == 0) {
            return
        }
        
        let offset = 150

        UIView.animateWithDuration(0.5) { () -> Void in
            var f = self.view.frame
            f.origin.y -= CGFloat(offset)
            self.view.frame = f
        }
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        
        selectedIndexPath = NSIndexPath(forRow: textView.tag - 500, inSection: 0)
        
        if(selectedIndexPath.row == 0) {
            return true
        }
        
        UIView.animateWithDuration(0.3) { () -> Void in
            var f = self.view.frame
            f.origin.y = 0
            self.view.frame = f
        }
        
        return true
    }
    
    @IBAction func sendAction(sender: UIButton) {
        
        // validation check 
        // can not submit without stars and comments
        
        let data_cnt = passengers.count
        
        for var index = 0; index < data_cnt; index++ {
            
            let cell = tblPendingRating.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as! PendingRatingCell
            let comments = cell.txvRating.text
            let ratedValue = cell.viewRating.rating
            
            let passenger_name = passengers[index].first_name + " " + passengers[index].last_name
            
            if(ratedValue == 0.0) {
                
                JLToast.makeText(defLang == "fr" ? R.fr.require_rating_stars + passenger_name : R.en.require_rating_stars + passenger_name, duration: R.tDuration).show()
                return
                
            } else  if (comments == "") {
                
                JLToast.makeText(defLang == "fr" ? R.fr.require_rating_comments + passenger_name : R.en.require_rating_comments + passenger_name, duration: R.tDuration).show()
                return
            }
        }
        
        var params: [String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "ride_hash": pendingRating.ride_hash,
            "ride_trip": pendingRating.ride_trip,
            "process": "\(1)"
        ]
        
        params["data_count"] = "\(data_cnt)"
        
        var arr_passenger_id: [String] = []
        
        for var index = 0; index < data_cnt; index++ {
            
            arr_passenger_id.append("\(passengers[index].passenger_id)")
            
            let passenger_id = passengers[index].passenger_id
            
            let cell = tblPendingRating.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as! PendingRatingCell
            let comments = cell.txvRating.text
            let ratedValue = cell.viewRating.rating
            
            params["passenger_stars_\(passenger_id)"] = "\(ratedValue)"
            params["comments_\(passenger_id)"] = "\(comments)"
            
        }
        
        if(user_role == "passenger") {
            
            params["passenger_id"] = arr_passenger_id[0]
   
        } else {
            params["passenger_id"] = arr_passenger_id
        }
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view .addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ratings/rating_campanign", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ratings/rating_campanign")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        
                        self.navigationController?.popViewControllerAnimated(true)
                        
                    } else {
                        
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                    }
                } else {
                    print("Error parsing /ratings/rating_campanign")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                }
            })
    }
    
    @IBAction func cancelAction(sender: UIButton) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
    }
    
    func getRatingCompanign() {
        
        let params: [String: AnyObject] = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "ride_hash": pendingRating.ride_hash,
            "ride_trip": "\(pendingRating.ride_trip)"
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view .addSubview(activityIndicator)
        
        Alamofire.request(.POST, kBaseURL + "ratings/rating_campanign", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ratings/rating_received")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        self.already_exist = jsonResponse["rating_data"]["already_exist"].string == nil ? false : true
                        
                        if let _ = jsonResponse["rating_data"]["ride_datas"]["created_date"].string {
                        
                            let ride_created_time = jsonResponse["rating_data"]["ride_datas"]["created_date"].string!
                            
                            self.user_role = jsonResponse["rating_data"]["user_role"].string!
                            
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                            let ride_created_date = dateFormatter.dateFromString(ride_created_time)
                            
                            if ride_created_date != nil {
                                
                                dateFormatter.dateFormat = "dd MMM yyyy hh:mm a";
                                self.ride_created_time = dateFormatter.stringFromDate(ride_created_date!)
                             
                                self.loadDetails()
                            }
                        }
                        
                        if let passenger_num = jsonResponse["rating_data"]["passenger_datas"].array?.count {
                        
                            print(passenger_num)
                            
                            if (passenger_num > 0) {
                                    
                                self.passengers.removeAll()
                                
                                for index in 0 ..< passenger_num {
                                    
                                    let _passenger = Passenger()
                                    _passenger.passenger_id = Int(jsonResponse["rating_data"]["passenger_datas"][index]["id"].string!)!
                                    _passenger.first_name = jsonResponse["rating_data"]["passenger_datas"][index]["first_name"].string!
                                    _passenger.last_name = jsonResponse["rating_data"]["passenger_datas"][index]["last_name"].string!
                                    
                                    _passenger.gender = Int(jsonResponse["rating_data"]["passenger_datas"][index]["gender"].string!)!
                                    
                                    if let profile_pic = jsonResponse["rating_data"]["passenger_datas"][index]["profile_pic"].string {
                                        _passenger.profile_pic = profile_pic
                                    }
                                    
                                    _passenger.pas_hash_key = jsonResponse["rating_data"]["passenger_datas"][index]["hash_key"].string!

                                    self.passengers.append(_passenger)

                                }
                                
                                self.tblPendingRating.reloadData()
                            }
                        }                        
                        
                    } else {
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                        return
                    }
                } else {
                    print("Error parsing /ratings/rating_receivede")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                }
            })
    }
    
    func loadDetails() {
        
        let blackBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 85/255.0, green: 85/255.0,
            blue: 85/255.0, alpha: 1.0), NSFontAttributeName: UIFont.init(name: "Helvetica-Bold", size: 12.0)!]
        
        let attributedDate = NSMutableAttributedString(string: ride_created_time)
        
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(0, 2))
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(12, 8))
        
        self.lblDateTime.attributedText = attributedDate
    }
}



