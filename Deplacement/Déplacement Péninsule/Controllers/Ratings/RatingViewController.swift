//
//  RatingViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class RatingViewController: UIViewController {

    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    var _user: UserInfo!
        
    @IBOutlet weak var btnPendingRate: UIButton!
    @IBOutlet weak var btnReceivedRate: UIButton!
    @IBOutlet weak var btnGivenRating: UIButton!
    
    @IBOutlet weak var lblPendingCnt: UILabel!
    @IBOutlet weak var lblReceivedCnt: UILabel!
    @IBOutlet weak var lblGivenRatingCnt: UILabel!
    
    @IBOutlet weak var lblRatingPending: UILabel!
    @IBOutlet weak var lblRatingGiven: UILabel!
    @IBOutlet weak var lblRatingReceived: UILabel!
    
    // menu button
    @IBOutlet weak var leftMenuButton: UIBarButtonItem!
    @IBOutlet weak var rightMenuButton: UIBarButtonItem!
    
//    var ratingGiven: [Rating] = []
//    var ratingReceived: [Rating] = []
//    var ratingPending: [PendingRating] = []
    
//    var bLoadFinished: Bool = false
//    
//    let dateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        defLang = prefs.stringForKey("LANGUAGE")!
        
        if self.revealViewController() != nil {
            
            let accountStoryboard: UIStoryboard = UIStoryboard(name: "Account_RightSide", bundle:  nil)
            let rightMenuViewController = accountStoryboard.instantiateViewControllerWithIdentifier("AccountRightSlideMenu")
            
            self.revealViewController().rightViewController = rightMenuViewController;
            
            leftMenuButton.target = self.revealViewController()
            leftMenuButton.action = "revealToggle:"
            
            rightMenuButton.target = self.revealViewController()
            rightMenuButton.action = "rightRevealToggle:"
            
            self.revealViewController().panGestureRecognizer()
            self.revealViewController().tapGestureRecognizer()
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // set naviation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.row5 : R.en.row5
        
        // change bar button item tint color
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.whiteColor()
        
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
        
//        getGivenRatings()
    }
    
    func initView() {
        
        btnPendingRate.setTitle(defLang == "fr" ? R.fr.rating_pending : R.en.rating_pending, forState: .Normal)
        btnReceivedRate.setTitle(defLang == "fr" ? R.fr.rating_received : R.en.rating_received, forState: .Normal)
        btnGivenRating.setTitle(defLang == "fr" ? R.fr.rating_given : R.en.rating_given, forState: .Normal)
    }
    
//    // remove activity indicator view
//    func removeActivityView() {
//        self.activityIndicator.stopAnimating()
//        self.view.viewWithTag(300)?.removeFromSuperview()
//    }
    
    @IBAction func pendingClicked(sender: UIButton) {
        
//        if(!bLoadFinished)  {
//            return
//        }
        
        self.performSegueWithIdentifier("Segue2Pending", sender: self)
    }
    
    @IBAction func receivedClicked(sender: UIButton) {
        
//        if(!bLoadFinished)  {
//            return
//        }
        
        self.performSegueWithIdentifier("Segue2Received", sender: self)
    }
    
    @IBAction func givenClicked(sender: UIButton) {
        
//        if(!bLoadFinished)  {
//            return
//        }
        
        self.performSegueWithIdentifier("Segue2Given", sender: self)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "Segue2Pending") {
            
            
        } else  if (segue.identifier == "Segue2Received") {
            
            let destinationVC = segue.destinationViewController as! GivenRateViewController
            destinationVC.bGiven = false
 
            
        } else {
            
            let destinationVC = segue.destinationViewController as! GivenRateViewController
            destinationVC.bGiven = true
        }
    }
}
