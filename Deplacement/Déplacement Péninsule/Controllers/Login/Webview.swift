//
//  Webview.swift
//  Déplacement Péninsule
//
//  Created by RK on 24/06/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class Webview: UIViewController,UIWebViewDelegate
{
    var strUrl: NSURL!
    var className:NSString!
    @IBOutlet var WebView:UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

      WebView.loadRequest(NSURLRequest(URL:strUrl));
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        let isPrifix:Bool = (request.URL?.absoluteString.hasPrefix("com.maaspros.deplacement:/oauth2callback"))!
        if (isPrifix)
        {
            GPPURLHandler.handleURL(request.URL, sourceApplication:"com.google.chrome.ios", annotation: nil) //, com.apple.mobilesafari
            
            self.dismissViewControllerAnimated(true, completion: nil)
//            if className == "login"
//            {
//                let viewController:LoginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("loginController") as! LoginVC
//                self.presentViewController(viewController, animated: true, completion: nil)
//            }
//            else
//            {
//                let viewController:RegisterVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("registerController") as! RegisterVC
//                self.presentViewController(viewController, animated: true, completion: nil)
//            }
           return false
        }
        
        return true
    }
    @IBAction func OnClikBack(sender:AnyObject)
    {
        if self.WebView.canGoBack {
            
            self.WebView.goBack()
            
        } else {
            
            self.dismissViewControllerAnimated(true, completion: nil)
//            if className == "login"
//            {
//                let viewController:LoginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("loginController") as! LoginVC
//                self.presentViewController(viewController, animated: true, completion: nil)
//            }
//            else
//            {
//                let viewController:RegisterVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("registerController") as! RegisterVC
//                self.presentViewController(viewController, animated: true, completion: nil)
//            }

        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   

}
