//
//  ForgotPasswordVC.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair.
//  Copyright © 2015 MaaS Pros. All rights reserved.
//

import UIKit

import Alamofire
import JLToast
import SwiftyJSON

class ForgotPasswordVC: UIViewController {

    @IBOutlet var resetButton: UIButton!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var pageTitleLabel: UILabel!
    
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defLang = prefs.stringForKey("LANGUAGE")!
    }

    override func viewDidAppear(animated: Bool) {
        initViews()
    }
    
    func initViews() {
        //set text to email field
        pageTitleLabel.text = defLang == "fr" ? R.fr.forgot_password_title : R.en.forgot_password_title
        emailField.placeholder = defLang == "fr" ? R.fr.email_address : R.en.email_address
        resetButton.setTitle(defLang == "fr" ? R.fr.reset_password : R.en.reset_password, forState: .Normal)
        
        //activity indicator view
        activityView.center = self.view.center
        activityView.color = UIColor.greenColor()
        activityView.tag = 111
        
        //emailfield bottom line and placeholder
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1).CGColor
        border.frame = CGRect(x: 0, y: emailField.frame.size.height - width, width:  emailField.frame.size.width, height: emailField.frame.size.height)
        border.borderWidth = width
        emailField.layer.addSublayer(border)
        emailField.layer.masksToBounds = true
    }
    
    //reset password button clicked
    @IBAction func forgotButtonClicked(sender: UIButton) {
        activityView.startAnimating()
        self.view.addSubview(activityView)
        
        
        if(emailField.hasText()) {
            forgotPassword(emailField.text!)
        } else {
            if(!emailField.hasText()) {
                JLToast.makeText(defLang == "fr" ? R.fr.email_required : R.en.email_required, duration: R.tDuration).show()
            }
            self.removeActivityView()
        }
    }
    
    //forgot password function
    func forgotPassword(email: String) {
        
        let params = [
            "email": email,
            "site_lang": defLang
        ]
        
        Alamofire.request(.POST, kBaseURL + "/users/forgot_password", parameters: params, encoding: .JSON)
            .validate()
            .responseJSON { response in
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/forgot_password")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("The JSON is: \(jsonResponse)")
                
                
                if let _ = jsonResponse["status"].bool {
                    if let message = jsonResponse["message"].string {
                        JLToast.makeText(message, duration: R.tDuration).show()
                    } else {
                        print("Error parsing /users/forgot_password")
                        return
                    }
                    self.removeActivityView()
                } else {
                    print("Error parsing /users/forgot_password")
                    return
                }
                
        }
    }
    
    func removeActivityView() {
        self.activityView.stopAnimating()
        self.view.viewWithTag(111)?.removeFromSuperview()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }

}
