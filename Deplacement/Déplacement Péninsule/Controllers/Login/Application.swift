//
//  Application.swift
//  Déplacement Péninsule
//
//  Created by RK on 25/06/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class Application: UIApplication
{
    let ApplicationOpenGoogleAuthNotification = "ApplicationOpenGoogleAuthNotification"
        
    override func openURL(url: NSURL) -> Bool
    {
        if url.absoluteString.hasPrefix("googlechrome-x-callback:")
        {
            return false
        }
        else if (url.absoluteString.hasPrefix("https://accounts.google.com/o/oauth2/auth"))
        {
            NSNotificationCenter.defaultCenter().postNotificationName(ApplicationOpenGoogleAuthNotification, object: url)
            return false;
        }
        
        return super.openURL(url)
    }
}
