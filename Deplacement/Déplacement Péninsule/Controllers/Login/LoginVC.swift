//
//  LoginVC.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair on 16/10/15.
//  Copyright © 2015 MaaS Pros. All rights reserved.
//

import UIKit

//FB signin
import FBSDKCoreKit
import FBSDKLoginKit

//G+ signin
import AddressBook
import MediaPlayer
import AssetsLibrary
import CoreLocation
import CoreMotion

import Alamofire
import JLToast
import SwiftyJSON

//Google+ SignIn Delegate
extension LoginVC: GPPSignInDelegate {
    
    func finishedWithAuth(auth: GTMOAuth2Authentication, error: NSError?){
        
        if error == nil {
            
            let person:GTLPlusPerson? = googleUser!.googlePlusUser
            
            if  person == nil {
                print("Failed G+. Person information not available")
                JLToast.makeText(self.defLang == "fr" ? R.fr.g_failed_person : R.en.g_failed_person, duration: R.tDuration).show()
            } else {
                
                let name:GTLPlusPersonName = (person?.name)!
                let email = self.googleUser!.userEmail
                if(email.isEmpty)
                {
                    JLToast.makeText(self.defLang == "fr" ? R.fr.g_failed_person : R.en.g_failed_person, duration: R.tDuration).show()
                } else {
                    
                    print("G+ info: ", person)
                    
                    let firstname = name.familyName
                    let lastname = name.givenName
                    let id = person?.identifier!
                    
                    // jis
                    if let gender = person?.gender {
                        
                        let photo = person?.image.url!
                        self.socialLogin(firstname, last_name: lastname, gender: gender, email: email, profile_img: photo, identifier: id, provider: "google")
                        
                    } else
                    {
                        let photo = person?.image.url!
                        self.socialLogin(firstname, last_name: lastname, gender: "1", email: email, profile_img: photo, identifier: id, provider: "google")
                        
                        //  JLToast.makeText(self.defLang == "fr" ? R.fr.g_failed_person : R.en.g_failed_person, duration: R.tDuration).show()
                    }
                    
                }
                
                googleUser!.signOut()
            }
        } else {
            print("Failed G+")
            JLToast.makeText(self.defLang == "fr" ? R.fr.g_failed : R.en.g_failed, duration: R.tDuration).show()
        }
    }
    
    func didDisconnectWithError(error: NSError!) {
        
        
    }
}

class LoginVC: UIViewController {
    
    @IBOutlet var signUp: UIButton!
    @IBOutlet var signIn: UIButton!
    @IBOutlet var forgotPassword: UIButton!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var orLabel: UILabel!
    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var rootView: UIView!
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    var googleUser: GPPSignIn?
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defLang = prefs.stringForKey("LANGUAGE")!
        
        googleUser = GPPSignIn.sharedInstance()
        
        googleUser?.shouldFetchGooglePlusUser = true
        googleUser?.shouldFetchGoogleUserEmail = true
        
        googleUser?.clientID = "922581048994-vpq8jv5p9ggksu14phc5urublcq8ppj7.apps.googleusercontent.com"
        
        googleUser?.scopes.append(kGTLAuthScopePlusLogin)   // https://www.googleapis.com/auth/plus.login
        googleUser?.scopes.append(kGTLAuthScopePlusMe)      // https://www.googleapis.com/auth/plus.me
        googleUser?.delegate = self
        
        googleUser?.trySilentAuthentication()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(openGooglePlusInWebview),
            name:"ApplicationOpenGoogleAuthNotification",
            object: nil)
    }
    
    func openGooglePlusInWebview(url:NSNotification) -> Void
    {
        let viewController:Webview = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("WebviewVC") as! Webview
        viewController.strUrl = url.object as! NSURL;
        viewController.className = "login"
        self.presentViewController(viewController, animated: true, completion: nil)
       
//        self.navigationController?.pushViewController(viewController, animated: true)
      
      //  self.shouldPerformSegueWithIdentifier("webview", sender: self);
    }
    override func viewDidAppear(animated: Bool) {        
        //hack to disable scrolling if device is iPhone4 or above.
        if(DeviceSize.isIphone4OrLess()) {
            self.scrollView.scrollEnabled = true
        } else {
            self.scrollView.scrollEnabled = false
        }
    
        //initialize views
        initViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    func initViews() {
        
        //set text to views
        accountLabel.text = defLang == "fr" ? R.fr.account : R.en.account
        orLabel.text = defLang == "fr" ? R.fr.or : R.en.or
        signIn.setTitle(defLang == "fr" ? R.fr.login : R.en.login, forState: .Normal)
        signUp.setTitle(defLang == "fr" ? R.fr.signup : R.en.signup, forState: .Normal)
        emailField.placeholder = defLang == "fr" ? R.fr.email_address : R.en.email_address
        passwordField.placeholder = defLang == "fr" ? R.fr.password : R.en.password
        forgotPassword.setTitle(defLang == "fr" ? R.fr.forgot_password : R.en.forgot_password, forState: .Normal)
        
        //activity indicator view
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 111
        
        //signin and signup button rounded corner
        signIn.layer.cornerRadius = 4.0
        signUp.layer.cornerRadius = 4.0
        
        //emailfield bottom line and placeholder
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1).CGColor
        border.frame = CGRect(x: 0, y: emailField.frame.size.height - width, width:  emailField.frame.size.width, height: emailField.frame.size.height)
        border.borderWidth = width
        emailField.layer.addSublayer(border)
        emailField.layer.masksToBounds = true
        
        //passwordfield bottom line and placeholder
        let border2 = CALayer()
        let width2 = CGFloat(1.0)
        border2.borderColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1).CGColor
        border2.frame = CGRect(x: 0, y: passwordField.frame.size.height - width, width:  passwordField.frame.size.width, height: passwordField.frame.size.height)
        border2.borderWidth = width2
        passwordField.layer.addSublayer(border2)
        passwordField.layer.masksToBounds = true
        
    }
    
    //tapgesturerecognizer to hide keyboard
    @IBAction func viewTapped(sender: UITapGestureRecognizer) {
        passwordField.resignFirstResponder()
        emailField.resignFirstResponder()
    }
    
    //Google+ SignIn function
    @IBAction func googleSignIn(sender: UIButton) {
        
        prefs.setObject("google", forKey: "SocialLogin") //need this for AppDelegate.openURL method
        
        googleUser?.authenticate()
    }
    
    //Facebook SignIn function
    @IBAction func fbSignIn(sender: UIButton) {
        
        prefs.setObject("facebook", forKey: "SocialLogin") //need this for AppDelegate.openURL method
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logInWithReadPermissions(["email", "public_profile"],
            fromViewController: self,
            handler: { (result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
                if error != nil {
                    print("Error")
                } else if(result.isCancelled){
                    print("Result cancelled")
                }else {
                    print("Success get user information.")
                    
                    let fbloginresult : FBSDKLoginManagerLoginResult = result
                    if(fbloginresult.grantedPermissions.contains("email") && fbloginresult.grantedPermissions.contains("public_profile")) {
                        self.getFBUserData() //fetch user details
                        fbLoginManager.logOut() //logout user after fetching details
                    } else {
                        JLToast.makeText(self.defLang == "fr" ? R.fr.permission_grant : R.en.permission_grant, duration: R.tDuration).show()
                    }
                }
        })
    }
    
    //fetch facebook profile details
    func getFBUserData(){
        if((FBSDKAccessToken.currentAccessToken()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, gender, picture.type(large)"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    let id = result.valueForKey("id")!
                    let first_name = result.valueForKey("first_name")!
                    let last_name = result.valueForKey("last_name")!
                    let gender = result.valueForKey("gender")!
                    let email = result.valueForKey("email")!
                    let image_url = "http://graph.facebook.com/\(id)/picture?type=large"
                    
                    self.socialLogin(first_name as! String, last_name: last_name as! String, gender: gender as! String, email: email as! String, profile_img: image_url, identifier: id as! String, provider: "facebook")
                } else {
                    JLToast.makeText(self.defLang == "fr" ? R.fr.f_failed : R.en.f_failed, duration: R.tDuration).show()
                }
            })
        }
    }
    
    //signin button clicked
    @IBAction func signInClicked(sender: UIButton) {
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        
        if(emailField.hasText() && passwordField.hasText()) {
            
                login(emailField.text, password: passwordField.text)
            
        } else {
            
            if(!emailField.hasText() && !passwordField.hasText()) {
                
                JLToast.makeText(defLang == "fr" ? R.fr.email_required + "\n" + R.fr.pass_required : R.en.email_required + "\n" + R.fr.pass_required, duration: R.tDuration).show()
                
            } else if(!emailField.hasText()) {
                
                JLToast.makeText(defLang == "fr" ? R.fr.email_required : R.en.email_required, duration: R.tDuration).show()
                
            } else if(!passwordField.hasText()) {
                
                JLToast.makeText(defLang == "fr" ? R.fr.pass_required : R.en.pass_required, duration: R.tDuration).show()
                
            }
            
            self.removeActivityView()
        }
    }
    
    //login function
    func login(username: String!, password: String!) {
        
        let plainPassword = (password as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64Password = plainPassword!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        let params = [
            "username": username,
            "password": base64Password,
            "site_lang": defLang
        ]
        
//        print(params)
        
        Alamofire.request(.GET, kBaseURL + "/users/login", parameters: params)
            .validate()
            .responseJSON { response in
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/login")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
//                print("The JSON is: \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    if(status) {
                        if let hash = jsonResponse["user_hash"].string {

                            self.prefs.setObject(hash, forKey: "USER_HASH")
                            self.prefs.synchronize()
                            
                            self.getUserDetails(hash)

                        } else {
                            
                            self.removeActivityView()
                            
                            if let message = jsonResponse["message"].string {
                                
                                JLToast.makeText(message, duration: R.tDuration).show()
                                
                            } else {
                                
                                print("Error parsing /users/login")
                                JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                            }
                        }
                        
                    } else {
                       
                        
                        self.removeActivityView()
                        if let message = jsonResponse["message"].string {
                            
                            JLToast.makeText(message, duration: R.tDuration).show()

                        } else {
                            
                            print("Error parsing /users/login")
                            JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                        }
                    }
                    
                } else {
                    
                    print("Error parsing /users/login")
                    
                    self.removeActivityView()
                    
                    if let message = jsonResponse["message"].string {
                        
                        JLToast.makeText(message, duration: R.tDuration).show()
                        
                    } else {
                        
                        print("Error parsing /users/user")
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    }
                }
                
        }
    }
    
    //social login function
    func socialLogin(first_name: String!, last_name: String!, gender: String!, email: String!, profile_img: String!, identifier: String!, provider: String!) {
        
        let params = [
            "first_name": first_name,
            "last_name": last_name,
            "gender": gender,
            "email": email,
            "profile_img": profile_img,
            "identifier": identifier,
            "provider": provider,
            "site_lang" : defLang
        ]
        
//        print(params)
        
        Alamofire.request(.GET, kBaseURL + "/users/social_login", parameters: params)
            .validate()
            .responseJSON { response in
                
//                print(response.result.value)
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /users/social_login")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
//                print("The JSON is(users/user): \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    if(status) {
                        if let hash = jsonResponse["user_hash"].string {
                            
                            self.prefs.setObject(hash, forKey: "USER_HASH")
                            self.prefs.synchronize()
                            
                            self.getUserDetails(hash)
                            
                        } else {
                            
                            print("Error parsing /users/social_login")
                            
                            JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                            self.removeActivityView()
                        }
                        
                    } else {
                        
                        self.removeActivityView()
                        if provider == "facebook" {
                            
                            JLToast.makeText(self.defLang == "fr" ? R.fr.f_failed : R.en.f_failed, duration: R.tDuration).show()
                            
                        } else {
                            
                            JLToast.makeText(self.defLang == "fr" ? R.fr.g_failed : R.en.g_failed, duration: R.tDuration).show()
                        }
                    }
                } else {
                    
                    print("Error parsing /users/social_login")
                    
                    self.removeActivityView()
                    
                    if provider == "facebook" {
                        JLToast.makeText(self.defLang == "fr" ? R.fr.f_failed : R.en.f_failed, duration: R.tDuration).show()
                    } else {
                        JLToast.makeText(self.defLang == "fr" ? R.fr.g_failed : R.en.g_failed, duration: R.tDuration).show()
                    }
                }
                
        }
    }
    
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(111)?.removeFromSuperview()
    }
    
    func getUserDetails(hash: String!) {
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        let params = [
            "public_profile": "\(1)",
            "user_hash": hash,
            "site_lang": defLang
        ]
        
//        debugPrint(params)
        
        Alamofire.request(.GET, kBaseURL+"users/profile", parameters: params)
            .validate()
            .responseJSON(completionHandler: { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    
                    print("Error: did not receive data")
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    
                    print("Error calling GET on /users/profile")
                    
                    print(response.result.error)
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
//                debugPrint(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        // first name
                        if let first_name = jsonResponse["user_data"]["user_data"]["first_name"].string {
                            
                            self.prefs.setObject(first_name, forKey: "USER_FIRST_NAME")
                        }
                        
                        // last name
                        if let last_name = jsonResponse["user_data"]["user_data"]["last_name"].string {
                            
                            self.prefs.setObject(last_name, forKey: "USER_LAST_NAME")
                        }
                        
                        // email
                        if let username = jsonResponse["user_data"]["user_data"]["username"].string {
                            
                            self.prefs.setObject(username, forKey: "USER_EMAIL")
                        }
                        
                        // profile image
                        if let profile_pic = jsonResponse["user_data"]["user_data"]["profile_pic"].string {
                            
                            self.prefs.setObject(profile_pic, forKey: "USER_PIC")
                        }
                        
                        // gender
                        if let gender = jsonResponse["user_data"]["user_data"]["gender"].string {
                            
                            self.prefs.setInteger(Int(gender)!, forKey: "USER_GENDER")
                        }
                        
                        // email verified
                        if let email_verified = jsonResponse["user_data"]["user_data"]["email_verified"].string {
                            
                            self.prefs.setBool(email_verified == "1" ? true : false, forKey: "EMAIL_VERIFIED")
                        }
                        
                        // phone verified
                        if let phone_verified = jsonResponse["user_data"]["user_data"]["phone_verified"].string {
                            
                            self.prefs.setBool(phone_verified == "1" ? true : false, forKey: "PHONE_VERIFIED")
                        }
                        
                        // admin approved
                        if let admin_approved = jsonResponse["user_data"]["user_data"]["admin_approved"].int {
                            
                            self.prefs.setBool(admin_approved == 1 ? true : false, forKey: "ADMIN_APPROVED")
                        }
                        
                        // logged in
                        self.prefs.setBool(true, forKey: "ISLOGGEDIN")
                        self.prefs.synchronize()
                        
                        if(!self.prefs.boolForKey("EMAIL_VERIFIED")) {
                            JLToast.makeText(self.defLang == "fr" ? R.fr.verify_your_email : R.en.verify_your_email, duration: R.tDuration).show()
                        }
                        
                        //redirect to main screen
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewControllerWithIdentifier("RevealViewController") as! SWRevealViewController
                        self.presentViewController(controller, animated: false, completion: nil)
                        
                        
                    } else {
                        
                        print("Error parsing /users/profile")
                        
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                        self.removeActivityView()
                    }
                    
                } else {
                    
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                }
                
            })
    }
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
//    {
//        if (segue.identifier == "webview")
//        {
//            let viewController:Webview = segue.destinationViewController as Webview
//            viewController.strUrl =
//        }
//    }
}



