//
//  ProductsListVC.swift
//  TieiT App
//
//  Created by Alok Nair.
//  Copyright (c) 2015 MaaS Pros Inc. All rights reserved.
//

import UIKit

class RidesListVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var rides: [Ride] = []
    var ride: Ride = Ride(hash_key: "", start_point_en: "", start_point_fr: "", start_lat: "", start_lon: "", end_point_en: "", end_point_fr: "", end_lat: "", end_lon: "", price_per_seat: "", driver_first_name: "", driver_last_name: "", auto_accept: "", unix_depature_time: "", seats_available: "")
    
    @IBOutlet var tableView: UITableView!
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defLang = prefs.stringForKey("LANGUAGE")!
        
        //set tableview delegate and datasource
        tableView.delegate = self
        tableView.dataSource = self
        
        //hide empty rows in uitableview
        let backgroundView = UIView(frame: CGRectZero)
        self.tableView.tableFooterView = backgroundView
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style: .Plain, target: nil, action: nil)
        navigationItem.title =  defLang == "fr" ? R.fr.carpooling + " (\(rides.count))" : R.en.carpooling + " (\(rides.count))"
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rides.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RideListCell", forIndexPath: indexPath) as! RideListCell
        cell.availableLabel.text = defLang == "fr" ? R.fr.available_seats : R.en.available_seats
        cell.startLabel.text =  defLang == "fr" ? (!rides[indexPath.row].start_point_fr.isEmpty ? rides[indexPath.row].start_point_fr : rides[indexPath.row].start_point_en) : rides[indexPath.row].start_point_en
        cell.endLabel.text =  defLang == "fr" ? (!rides[indexPath.row].end_point_fr.isEmpty ? rides[indexPath.row].end_point_fr : rides[indexPath.row].end_point_en) : rides[indexPath.row].end_point_en
        cell.contactLabel.text = rides[indexPath.row].driver_first_name + " " + rides[indexPath.row].driver_last_name
        cell.dateTimeLabel.text = DateUtils.convertUnix(Double(rides[indexPath.row].unix_depature_time)!)
        cell.rateLabel.text = "$" + rides[indexPath.row].price_per_seat
        cell.seatsLabel.text = rides[indexPath.row].seats_available

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "RideDetailSegue" {
            let destinationViewController: RideDetailTVC = segue.destinationViewController as! RideDetailTVC
            
            //fetch row value for selected row
            let row = tableView?.indexPathForSelectedRow?.row            
            destinationViewController.ride_hash = rides[row!].hash_key
            
//            if(Int(rides[row!].seats_available) == 0) {
//                destinationViewController.bCanBookRide = false
//            }            
        }
    }
}
