//
//  RideDetailTVC.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair on 11/01/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

enum TravelModes: Int {
    case driving
    case walking
    case bicycling
}

class RideDetailTVC: UITableViewController {
    
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var ride_hash = ""
    var availableSeats = 0
    var pricePerSeats: Float = 0.0
    
    var bCanBookRide: Bool = true
    
    var _user: UserInfo!
    
    var isBooked = true
    
    @IBOutlet weak var navBookRide: UIBarButtonItem!
    @IBOutlet weak var navContactDriver: UIBarButtonItem!
    
    @IBOutlet weak var viewMap: GMSMapView!
    
    @IBOutlet var fromLabel: UILabel!
    @IBOutlet var toLabel: UILabel!
    @IBOutlet var fromField: UILabel!
    @IBOutlet var toField: UILabel!
    @IBOutlet var departureLabel: UILabel!
    @IBOutlet var departureField: UILabel!
    @IBOutlet var etaLabel: UILabel!
    @IBOutlet var etaField: UILabel!
    @IBOutlet var seatsLabel: UILabel!
    @IBOutlet var seatsField: UILabel!
    @IBOutlet var luggageLabel: UILabel!
    @IBOutlet var luggageField: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var timeField: UILabel!
    @IBOutlet var detourLabel: UILabel!
    @IBOutlet var detourField: UILabel!
    @IBOutlet var stopoverLabel: UILabel!
    @IBOutlet var stopoverField: UILabel!
    @IBOutlet var smokeImage: UIButton!
    @IBOutlet var eatImage: UIButton!
    @IBOutlet var musicImage: UIButton!
    @IBOutlet var petsImage: UIButton!
    @IBOutlet var chatImage: UIButton!
    @IBOutlet var handicappedImage: UIButton!
    @IBOutlet var childrenImage: UIButton!
    
    @IBOutlet var driverImage: UIImageView!
    @IBOutlet var driverName: UILabel!
    @IBOutlet var driverAge: UILabel!
    @IBOutlet var driverRating: FloatRatingView!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var offlineLabel: UILabel!
    @IBOutlet var emailField: UILabel!
    @IBOutlet var phoneField: UILabel!
    @IBOutlet var offlineField: UILabel!
    
    @IBOutlet var vehicleName: UILabel!
    @IBOutlet var vehicleYear: UILabel!
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var acLabel: UILabel!
    @IBOutlet var acField: UILabel!
    @IBOutlet var regNumberLabel: UILabel!
    @IBOutlet var regNumberField: UILabel!
    
    @IBOutlet var liftsLabel: UILabel!
    @IBOutlet var liftsField: UILabel!
    @IBOutlet var reviewsLabel: UILabel!
    @IBOutlet var reviewsField: UILabel!
    
    @IBOutlet weak var btnBookText: UIBarButtonItem!
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    var mapTasks = MapTasks()
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    var routePolyline: GMSPolyline!
    var markersArray: Array<GMSMarker> = []
    var waypointsArray: Array<String> = []          // ("latitude, longitude") - text value
    var travelMode = TravelModes.driving
    
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        //hide empty uitableview rows
        let backgroundView = UIView(frame: CGRectZero)
        self.tableView.tableFooterView = backgroundView
        self.tableView.alwaysBounceVertical = false
        
        //dummy header to disable header sticking to uitableview
        let dummyViewHeight: CGFloat = 40
        let dummyView = UIView(frame: CGRectMake(0, 0, self.tableView.bounds.size.width, dummyViewHeight))
        self.tableView.tableHeaderView = dummyView
        self.tableView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        
        initViews()
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(45.3342122, longitude: -76.0338359, zoom: 6.0)
        viewMap.camera = camera
        viewMap.settings.zoomGestures = true
        
        locationManager.delegate = self
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined) {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.startUpdatingLocation()
        }
        
        if let _ = self.routePolyline {
            self.clearRoute()
            self.waypointsArray.removeAll(keepCapacity: false)
        }
        
//        if(!bCanBookRide) {
//            navBookRide.enabled = false
//            navBookRide.tintColor = nil
//        }
    }
    
    func initViews() {
        
        //set text to views
        fromLabel.text = defLang == "fr" ? R.fr.from : R.en.from
        toLabel.text = defLang == "fr" ? R.fr.to : R.en.to
        emailLabel.text = defLang == "fr" ? R.fr.email : R.en.email
        departureLabel.text = defLang == "fr" ? R.fr.depature : R.en.depature
        etaLabel.text = defLang == "fr" ? R.fr.eta : R.en.eta
        seatsLabel.text = defLang == "fr" ? R.fr.seats : R.en.seats
        luggageLabel.text = defLang == "fr" ? R.fr.luggage_size : R.en.luggage_size
        timeLabel.text = defLang == "fr" ? R.fr.time_flexibility : R.en.time_flexibility
        detourLabel.text = defLang == "fr" ? R.fr.detour : R.en.detour
        stopoverLabel.text = defLang == "fr" ? R.fr.stopovers : R.en.stopovers
        phoneNumberLabel.text = defLang == "fr" ? R.fr.phone_number : R.en.phone_number
        offlineLabel.text = defLang == "fr" ? R.fr.offline_id : R.en.offline_id
        acLabel.text = defLang == "fr" ? R.fr.ac : R.en.ac
        regNumberLabel.text = defLang == "fr" ? R.fr.reg_number : R.en.reg_number
        liftsLabel.text = defLang == "fr" ? R.fr.lifts_offered : R.en.lifts_offered
        reviewsLabel.text = defLang == "fr" ? R.fr.total_reviews : R.en.total_reviews
        
        //activity indicator view
        activityView.center = self.view.center
        activityView.color = UIColor.greenColor()
        activityView.tag = 111
        
        driverRating.editable = true
        
        //rounded driver image
        driverImage.layer.borderWidth=0.0
        driverImage.layer.masksToBounds = false
        driverImage.layer.borderColor = UIColor.whiteColor().CGColor
        driverImage.layer.cornerRadius = 13
        driverImage.layer.cornerRadius = driverImage.frame.size.height/2
        driverImage.clipsToBounds = true
        
        driverImage.image = UIImage(named: _user.gender == 0 ? "male_profile" : "female_profile")
        
        //rounded vehicle image
        vehicleImage.layer.borderWidth=0.0
        vehicleImage.layer.masksToBounds = false
        vehicleImage.layer.borderColor = UIColor.whiteColor().CGColor
        vehicleImage.layer.cornerRadius = 13
        vehicleImage.layer.cornerRadius = vehicleImage.frame.size.height/2
        vehicleImage.clipsToBounds = true
        
        self.btnBookText.title = defLang == "fr" ? R.fr.btn_book_now : R.en.btn_book_now
        
        fetchRide()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 10
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        //stopover field
        if(indexPath.section == 0 && indexPath.row == 7) {
            return 100
        }
        return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1)
        header.textLabel!.textColor = UIColor(red:0.57, green:0.57, blue:0.57, alpha:1)
        header.textLabel!.font = UIFont.boldSystemFontOfSize(14)
        header.alpha = 1
        
        switch section {
        case 0:
            header.textLabel!.text = defLang == "fr" ? R.fr.trip : R.en.trip
        case 1:
            header.textLabel!.text = defLang == "fr" ? R.fr.vehicle_info : R.en.vehicle_info
        case 2:
            header.textLabel!.text = defLang == "fr" ? R.fr.activity : R.en.activity
        default:
            header.textLabel!.text = defLang == "fr" ? "" : ""
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func fetchRide() {
        activityView.startAnimating()
        self.view.addSubview(activityView)
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "ride_hash": ride_hash,
            "site_lang": defLang
        ]
        print(params)
        
        Alamofire.request(.GET, kBaseURL + "/ride/ride_detail", parameters: params)
            .validate()
            .responseJSON { response in
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/ride_detail")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("The JSON is: \(jsonResponse)")
                
                
                if let status = jsonResponse["status"].bool  {
                    if(status) {
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["seats_available"].string {
                            self.availableSeats = Int(jsonResponse["ride_data"]["ride"]["seats_available"].string!)!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["price_per_seat"].string {
                            self.pricePerSeats = Float(jsonResponse["ride_data"]["ride"]["price_per_seat"].string!)!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["map_data"]["start"]["address"].string {
                            self.fromField.text = jsonResponse["ride_data"]["ride"]["map_data"]["start"]["address"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["map_data"]["end"]["address"].string {
                            self.toField.text = jsonResponse["ride_data"]["ride"]["map_data"]["end"]["address"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["driver_details"]["email"].string {
                            self.emailField.text = jsonResponse["ride_data"]["driver_details"]["email"].string!
                        }
                        
                        if  let _ = jsonResponse["ride_data"]["ride"]["start_date"].string {
                            self.departureField.text = jsonResponse["ride_data"]["ride"]["start_date"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["end_date"].string {
                            self.etaField.text = jsonResponse["ride_data"]["ride"]["end_date"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["seats_available"].string {
                            self.seatsField.text = jsonResponse["ride_data"]["ride"]["seats_available"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["luggage_per_seat"].string {
                            self.luggageField.text = jsonResponse["ride_data"]["ride"]["luggage_per_seat"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["flexibility"].string {
                            self.timeField.text = jsonResponse["ride_data"]["ride"]["flexibility"].string! == "00:15:00" ? "Around 15 minutes" : "Around 30 minutes"
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["detour"].string {
                            let detour = jsonResponse["ride_data"]["ride"]["detour"].string!
                            switch detour {
                            case "0":
                                self.detourField.text = "None"
                            case "1":
                                self.detourField.text = "Maximum 15 minutes"
                            case "2":
                                self.detourField.text = "Maximum 30 minutes"
                            case "3":
                                self.detourField.text = "Any detour"
                            default:
                                self.detourField.text = "None"
                            }
                        }
                        
                        
                        if (jsonResponse["ride_data"]["driver_details"]["first_name"].string != nil && jsonResponse["ride_data"]["driver_details"]["last_name"].string != nil) {
                            self.driverName.text = jsonResponse["ride_data"]["driver_details"]["first_name"].string! + " " + jsonResponse["ride_data"]["driver_details"]["last_name"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["driver_details"]["age"].string {
                            self.driverAge.text = jsonResponse["ride_data"]["driver_details"]["age"].string! + " years old"
                        }
                        
                        if let _ = jsonResponse["ride_data"]["driver_details"]["user_rating"].int {
                            self.driverRating.rating = Float(jsonResponse["ride_data"]["driver_details"]["user_rating"].int!)
                        }
                        
                        if let _ = jsonResponse["ride_data"]["driver_details"]["phone"].string {
                            self.phoneField.text = jsonResponse["ride_data"]["driver_details"]["phone"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["driver_details"]["id_proof_expiry_date"].string {
                            self.offlineField.text = !jsonResponse["ride_data"]["driver_details"]["id_proof_expiry_date"].string!.isEmpty ? self.defLang == "fr" ? "Vérifié" : "Verified" : self.defLang == "fr" ? "Non vérifié" : "Not Verified"
                        }
                        
                        if let _ = jsonResponse["ride_data"]["vehicle"]["ac"].string {
                            
                            self.acField.text = jsonResponse["ride_data"]["vehicle"]["ac"].string! != "0" ? "Yes" : "No"
                            
                        }
                        
                        if let _ = jsonResponse["ride_data"]["vehicle"]["number_plate"].string {
                            self.regNumberField.text = jsonResponse["ride_data"]["vehicle"]["number_plate"].string!
                        }
                        
                        if (jsonResponse["ride_data"]["vehicle"]["make"].string != nil &&  jsonResponse["ride_data"]["vehicle"]["model"].string != nil) {
                            self.vehicleName.text = jsonResponse["ride_data"]["vehicle"]["make"].string! + " " + jsonResponse["ride_data"]["vehicle"]["model"].string!
                        }
                        
                        if let _ = jsonResponse["ride_data"]["vehicle"]["year"].string {
                            self.vehicleYear.text = jsonResponse["ride_data"]["vehicle"]["year"].string!
                        }
                        
                        
                        if let _ = jsonResponse["ride_data"]["count_active_rides"].int {
                            self.liftsField.text = String(jsonResponse["ride_data"]["count_active_rides"].int!)
                        }
                        
                        if let _ = jsonResponse["ride_data"]["rating_datas"].array {
                            self.reviewsField.text = String(jsonResponse["ride_data"]["rating_datas"].count)
                        }
                        
                        if let profile_pic = jsonResponse["ride_data"]["driver_details"]["profile_pic"].string {
                            if(!profile_pic.isEmpty) {
                                self.load_driverimage(profile_pic)
                            }
                            
                        } else {
                            print("Error parsing profile_pic")
                        }
                        
                        if let vehicle_pic = jsonResponse["ride_data"]["vehicle"]["vehicle_image"].string {
                            if(!vehicle_pic.isEmpty) {
                                self.load_vehiclerimage(vehicle_pic)
                            }
                            
                        } else {
                            print("Error parsing vehicle_image")
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["smoking"].string {
                            self.smokeImage.setBackgroundImage(UIImage(named:  "smoke" +  jsonResponse["ride_data"]["ride"]["smoking"].string!), forState: .Normal)
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["food"].string {
                            self.eatImage.setBackgroundImage(UIImage(named:  "eat" +  jsonResponse["ride_data"]["ride"]["food"].string!), forState: .Normal)
                        }
                        
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["music"].string {
                            self.musicImage.setBackgroundImage(UIImage(named:  "music" +  jsonResponse["ride_data"]["ride"]["music"].string!), forState: .Normal)
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["pets"].string {
                            self.petsImage.setBackgroundImage(UIImage(named:  "pets" +  jsonResponse["ride_data"]["ride"]["pets"].string!), forState: .Normal)
                        }
                        
                        if let _ =  jsonResponse["ride_data"]["ride"]["chatting"].string {
                            self.chatImage.setBackgroundImage(UIImage(named:  "chat" +  jsonResponse["ride_data"]["ride"]["chatting"].string!), forState: .Normal)
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["children"].string {
                            self.childrenImage.setBackgroundImage(UIImage(named:  "children" +  jsonResponse["ride_data"]["ride"]["children"].string!), forState: .Normal)
                        }
                        
                        if let _ = jsonResponse["ride_data"]["ride"]["handicapped"].string {
                            self.handicappedImage.setBackgroundImage(UIImage(named:  "handicapped" +  jsonResponse["ride_data"]["ride"]["handicapped"].string!), forState: .Normal)
                        }
                        
                        
                        var txtStopOver = ""
                        
                        let stopover_cnt = jsonResponse["ride_data"]["ride"]["map_data"]["waypoints"].count
                        if  (stopover_cnt > 0 ) {
                            
                            txtStopOver = "\(jsonResponse["ride_data"]["ride"]["map_data"]["waypoints"][0]["location"].string!)"
                            
                            for var index = 1; index < stopover_cnt; index++ {
                                
                                txtStopOver += ", \n" + "\(jsonResponse["ride_data"]["ride"]["map_data"]["waypoints"][index]["location"].string!)"
                            }
                        }
                        
                        
                        if(txtStopOver != "") {
                            self.stopoverField.text = txtStopOver
                        }
                        
                        // waypoints
                        let waypoints_cnt = jsonResponse["ride_data"]["ride"]["map_data"]["stopovers"].count
                        
                        if(waypoints_cnt > 0) {
                            
                            self.waypointsArray.removeAll()
                            
                            for var index = 0; index < waypoints_cnt; index++ {
                                
                                let positionString = jsonResponse["ride_data"]["ride"]["map_data"]["stopovers"][index]["lat"].string! + "," + jsonResponse["ride_data"]["ride"]["map_data"]["stopovers"][index]["lng"].string!
                                
                                self.waypointsArray.append(positionString)
                                
                            }
                        }
                        
                        self.removeActivityView()
                        
                        self.loadMap()
                        
                    } else {
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                        self.removeActivityView()
                    }
                    
                } else {
                    print("Error parsing /ride/ride_detail")
                    return
                }
        }
    }
    
    func loadMap() {
        
        if(fromField.text?.characters.count != 0 && toField.text?.characters.count != 0) {
            
            createRoute()
        }
    }
    
    func load_driverimage(urlString:String) {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    self.driverImage.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
        }
        
        task.resume()
    }
    
    func load_vehiclerimage(urlString:String) {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    self.vehicleImage.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
        }
        
        task.resume()
    }
    
    func removeActivityView() {
        self.activityView.stopAnimating()
        self.view.viewWithTag(111)?.removeFromSuperview()
    }
    
    func clearRoute() {
        
        if let _ = originMarker {
            originMarker.map = nil
        }
        
        if let _ = destinationMarker {
            destinationMarker.map = nil
        }
        
        if let _ = routePolyline {
            routePolyline.map = nil
        }
        
        originMarker = nil
        destinationMarker = nil
        routePolyline = nil
        
        if markersArray.count > 0 {
            for marker in markersArray {
                marker.map = nil
            }
            
            markersArray.removeAll(keepCapacity: false)
        }
    }
    
    func createRoute() {
        if waypointsArray.count <= 0 {
            clearRoute()
            
            mapTasks.getDirections(self.fromField.text! as String, destination: self.toField.text! as String, waypoints: nil, travelMode: travelMode, completionHandler: { (status, success) -> Void in
                
                if success {
                    
                    self.configureMapAndMarkersForRoute()
                    self.drawRoute()
                    
                } else {
                    print(status)
                }
            })
        } else {
            recreateRoute()
        }
    }
    
    func recreateRoute() {
        
        if let _ = routePolyline {
            clearRoute()
        }
        
        mapTasks.getDirections(self.fromField.text! as String, destination: self.toField.text! as String, waypoints: waypointsArray, travelMode: travelMode, completionHandler: { (status, success) -> Void in
            
            if success {
                
                self.configureMapAndMarkersForRoute()
                self.drawRoute()
                
            } else {
                print(status)
            }
        })
    }
    
    func configureMapAndMarkersForRoute() {
        
        viewMap.camera = GMSCameraPosition.cameraWithTarget(mapTasks.originCoordinate, zoom: 3.5)
        
        // origin marker
        originMarker = GMSMarker(position: self.mapTasks.originCoordinate)
        originMarker.map = self.viewMap
        originMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 32/255.0, green: 200/255.0, blue: 196/255.0, alpha: 1.0))
        originMarker.title = self.mapTasks.originAddress
        
        // destination marker
        destinationMarker = GMSMarker(position: self.mapTasks.destinationCoordinate)
        destinationMarker.map = self.viewMap
        destinationMarker.icon = GMSMarker.markerImageWithColor(UIColor(red: 76/255.0, green: 217/255.0, blue: 100/255.0, alpha: 1.0))
        destinationMarker.title = self.mapTasks.destinationAddress
        
        if waypointsArray.count > 0 {
            for waypoint in waypointsArray {
                let lat: Double = (waypoint.componentsSeparatedByString(",")[0] as NSString).doubleValue
                let lng: Double = (waypoint.componentsSeparatedByString(",")[1] as NSString).doubleValue
                
                let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                marker.map = viewMap
                marker.icon = GMSMarker.markerImageWithColor(UIColor.purpleColor())
                
                markersArray.append(marker)
            }
        }
    }
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)
        routePolyline = GMSPolyline(path: path)
        routePolyline.map = viewMap
    }
    
    @IBAction func bookSeatBtnClicked(sender: AnyObject) {
        
        if(!_user.email_verified || !_user.phone_verified) {
            
            showAlert((defLang == "fr" ? R.fr.title_not_membership : R.en.title_not_membership) ,  message_prefix: (defLang == "fr" ? R.fr.not_member_prefix : R.en.not_member_prefix), message_mid: (defLang == "fr" ? R.fr.not_member_middle : R.en.not_member_middle), message_subfix: (defLang == "fr" ? R.fr.not_member_subfix : R.en.not_member_subfix))
            
            return
        }
        
        self.performSegueWithIdentifier("Segue2Book", sender: self)
    }
    
    @IBAction func bookSeatTextClicked(sender: AnyObject) {
        
        if(!_user.email_verified || !_user.phone_verified) {
            
            showAlert((defLang == "fr" ? R.fr.title_not_membership : R.en.title_not_membership) ,  message_prefix: (defLang == "fr" ? R.fr.not_member_prefix : R.en.not_member_prefix), message_mid: (defLang == "fr" ? R.fr.not_member_middle : R.en.not_member_middle), message_subfix: (defLang == "fr" ? R.fr.not_member_subfix : R.en.not_member_subfix))
            
            return
        }
        
        self.performSegueWithIdentifier("Segue2Book", sender: self)
    }
    
    func showAlert(title: String!, message_prefix: String!, message_mid: String!, message_subfix: String!) {
        
        // Initialize Alert Controller
        let alertController = UIAlertController(title: title, message: message_prefix + message_mid + message_subfix, preferredStyle: .Alert)
        
        let pinkBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 223/255.0, green: 25/255.0, blue: 108/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 16.0)!]
        
        let blueBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 0/255.0, green: 172/255.0, blue: 145/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 15.0)!]
        
        let attributedTitle = NSMutableAttributedString(string: title)
        attributedTitle.addAttributes(pinkBoldAttributes, range: NSMakeRange(0, title.characters.count))
        alertController.setValue(attributedTitle, forKey: "attributedTitle")
        
        let attributedMessage = NSMutableAttributedString(string: message_prefix + message_mid + message_subfix)
        attributedMessage.addAttributes(blueBoldAttributes, range: NSMakeRange(message_prefix.characters.count, message_mid.characters.count))
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        
        alertController.view.tintColor = UIColor.darkGrayColor()
        
        // Initialize Actions
        let yesString = defLang == "fr" ? R.fr.yes : R.en.yes
        let yesAction = UIAlertAction(title: yesString, style: .Default) { (action) -> Void in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // Initialize Actions
        let noString = defLang == "fr" ? R.fr.no : R.en.no
        let noAction = UIAlertAction(title: noString, style: .Default) { (action) -> Void in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        
        // Add Actions
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        
        // Present Alert Controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "Segue2Contact") {
            
        } else if(segue.identifier == "Segue2Book" || segue.identifier == "Segue2BookText") {
            
            let bookVC: BookViewController = segue.destinationViewController as! BookViewController
            bookVC.ride_hash = self.ride_hash
            bookVC.availableSeats = self.availableSeats
            bookVC.pricePerPerson = self.pricePerSeats
            
            bookVC.start_point = fromField.text!
            bookVC.end_point = toField.text!
            
            bookVC.rideDetailVC = self
        }
    }
    
    func updateData() {
        
        if(!isBooked) {
            
        }
    }
}

extension RideDetailTVC: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
            
            viewMap.myLocationEnabled = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            
            viewMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 6.0, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}

