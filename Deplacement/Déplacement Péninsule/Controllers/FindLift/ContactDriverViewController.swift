//
//  ContactDriverViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/21/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class ContactDriverViewController: UIViewController, UITextFieldDelegate {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblOfflinID: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var txtMessage: UITextField!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imvProfile.layer.borderColor = UIColor(red: 226/255.0, green: 226/255, blue: 226/255.0, alpha: 1.0).CGColor
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    ["seats": "1", "ride_hash": "caf698353dfde3ad4272c63aed231e04", "login_hash": "9c62f33ad26eec8cb3b04f3a92bac7bd", "site_lang": "en"]
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {
        
        lblNavTitle.text = defLang == "fr" ? R.fr.contact_driver : R.en.contact_driver
        txtMessage.placeholder = defLang == "fr" ? R.fr.profile_message : R.en.profile_message
        
        btnSend.setTitle(defLang == "fr" ? R.fr.btn_send : R.en.btn_send, forState: .Normal)
        btnCancel.setTitle(defLang == "fr" ? R.fr.btn_cancel : R.en.btn_cancel, forState: .Normal)
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    @IBAction func sendAction(sender: UIButton) {
        
    }
    
    @IBAction func cancelACtion(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func viewDidTapped(sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    // --------------------------------------------------------------------------
    // MARK: UITextFieldDelegate
    // --------------------------------------------------------------------------
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
    
    func sendMessage() {
        //        addMessage(_message)
        
//        let params = [
//            "login_hash": prefs.stringForKey("USER_HASH")!,
//            "site_lang": defLang,
//            "message": message.message,
//            "reply_message": "\(0)",
//            "thread_id": "\(messageListModel.thread_id)",
//            "subject": "",
//            "msg_id": "\(messageListModel.id)",
//            "recipients": "\([messageListModel.sender_id])"
//        ]
//        
//        print(JSON(params))
//        
//        activityIndicator.startAnimating()
//        self.view.addSubview(activityIndicator)
        
//        Alamofire.request(.GET, kBaseURL+"message/inbox", parameters: params)
//            .validate()
//            .responseJSON(completionHandler: { response in
//                
//                self.removeActivityView()
//                
//                guard let value = response.result.value else {
//                    print("Error: did not receive data")
//                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
//                    return
//                }
//                
//                guard response.result.error == nil else {
//                    print("Error calling GET on /message/inbox")
//                    print(response.result.error)
//                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
//                    return
//                }
//                
//                let jsonResponse = JSON(value)
//                print(jsonResponse)
//                
//                if let status = jsonResponse["status"].bool {
//                    
//                    if(status) {
//                        
//                        self.addMessage(message)
//                        
//                        if(self.defLang == "fr") {
//                            JLToast.makeText("Votre message a été envoyé.", duration: R.tDuration).show()
//                        } else {
//                            JLToast.makeText("Your message has been sent.", duration: R.tDuration).show()
//                        }
//                        //                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
//                        
//                        self.tblMessages.reloadData()
//                        
//                    } else {
//                        print("Error parsing /message/inbox")
//                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
//                    }
//                }
//                
//            })
    }
}
