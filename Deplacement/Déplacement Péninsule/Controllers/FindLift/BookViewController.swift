//
//  BookViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/21/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class BookViewController: UIViewController {
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var ride_hash = ""
    
    // activityindicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblStartPoint: UILabel!
    @IBOutlet weak var lblEndPoint: UILabel!
    @IBOutlet weak var lblTitleSeatsAvailable: UILabel!
    @IBOutlet weak var lblSeatsAvailable: UILabel!
    @IBOutlet weak var lblTitlePricePerPerson: UILabel!
    @IBOutlet weak var lblPricePerPerson: UILabel!
    @IBOutlet weak var lblCtrlTitleSeatsAvailable: UILabel!
    @IBOutlet weak var txtCtrlSeatsAvailable: UITextField!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var rideDetailVC: RideDetailTVC!
    
    var pricePerPerson: Float = 0.0
    var availableSeats = 5
    var currentSeats = 1
    
    var totalPrice = 0
    
    var start_point = ""
    var end_point = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // initialize activity indicator
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.greenColor()
        activityIndicator.tag = 300
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {
        
        lblNavTitle.text = defLang == "fr" ? R.fr.book_my_seat : R.en.book_my_seat
        lblFrom.text = defLang == "fr" ? R.fr.from : R.en.from
        lblTo.text = defLang == "fr" ? R.fr.to : R.en.to
        lblTitleSeatsAvailable.text = defLang == "fr" ? R.fr.book_seats_available : R.en.book_seats_available
        lblTitlePricePerPerson.text = defLang == "fr" ? R.fr.book_price_per_person : R.en.book_price_per_person
        lblCtrlTitleSeatsAvailable.text = defLang == "fr" ? R.fr.book_ctrl_seats_available : R.en.book_seats_available
        lblTotalPrice.text = defLang == "fr" ? R.fr.book_total : R.en.book_total
        
        btnBookNow.setTitle(defLang == "fr" ? R.fr.btn_book_now : R.en.btn_book_now, forState: .Normal)
        btnCancel.setTitle(defLang == "fr" ? R.fr.btn_cancel : R.en.btn_cancel, forState: .Normal)
        
        lblStartPoint.text = start_point
        lblEndPoint.text = end_point
        
        lblSeatsAvailable.text = "\(availableSeats)"
        lblPricePerPerson.text = "$" + String(format: "%.2f", pricePerPerson)
        
        totalPrice = currentSeats * Int(pricePerPerson)
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        
        lblTotalPrice.text = " Total : $" + formatter.stringFromNumber(totalPrice)!
        
        // for a test
        txtCtrlSeatsAvailable.text = "\(currentSeats)"
        

    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityIndicator.stopAnimating()
        self.view.viewWithTag(300)?.removeFromSuperview()
    }
    
    @IBAction func plusSeatsAction(sender: UIButton) {
        
        if(currentSeats >= availableSeats) {
            return
        }
        
        currentSeats++
        txtCtrlSeatsAvailable.text = "\(currentSeats)"
        
        // calculate total prices
        totalPrice = currentSeats * Int(pricePerPerson)
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        
        lblTotalPrice.text = " Total : $" + formatter.stringFromNumber(totalPrice)!
    }
    
    @IBAction func minusSeatsAction(sender: UIButton) {
    
        if(currentSeats <= 1) {
            return
        }
        
        currentSeats--
        txtCtrlSeatsAvailable.text = "\(currentSeats)"
        
        // calculate total prices
        totalPrice = currentSeats * Int(pricePerPerson)
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        
        lblTotalPrice.text = " Total : $" + formatter.stringFromNumber(totalPrice)!
    }
    
    @IBAction func bookAction(sender: UIButton) {
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang,
            "seats": "\(currentSeats)",
            "ride_hash": ride_hash
        ]
        
        print(JSON(params))
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        
        Alamofire.request(.GET, kBaseURL + "ride/book_ride", parameters: params)
            .validate()
            .responseJSON { response in
                
                self.removeActivityView()
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/book_ride")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("THE JSON is :\n \(jsonResponse)")
                
                if let status = jsonResponse["status"].bool {
                    
                    if(status) {
                        
                        print("success")
                        
                        if(self.defLang == "fr")  {
                            
                            JLToast.makeText("Votre demande de trajet \(self.start_point) à \(self.end_point) a été soumise au conducteur pour approbation.", duration: R.tDuration).show()
                            
                        } else {
                            
                            JLToast.makeText("Your lift request \(self.start_point) to \(self.end_point) has been approved by driver.", duration: R.tDuration).show()
                        }
                        
                        self.dismissViewControllerAnimated(true, completion: nil)

                        
                    } else {
                        JLToast.makeText(jsonResponse["message"].string!, duration: R.tDuration).show()
                    }
                    
                } else {
                    print("Error parsing /ride/book_ride")
                }
        }
    }
    
    @IBAction func cancelAction(sender: UIButton) {
        
        rideDetailVC.isBooked = false
    
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func closeAction(sender: AnyObject) {
        
        rideDetailVC.isBooked = false
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
