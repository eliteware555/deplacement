//
//  MyNavigationController.swift
//  Side navigation drawer
//
//  Created by Alok Nair on 02/04/15.
//  Copyright (c) 2015 MaaS Pros Inc. All rights reserved.
//

import UIKit

class MyNavigationController: ENSideMenuNavigationController, ENSideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //side navigation drawer menu
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "SideMenu",bundle: nil)
        let viewController = mainStoryboard.instantiateViewControllerWithIdentifier("menuController") as! UITableViewController
        sideMenu = ENSideMenu(sourceView: self.view, menuTableViewController: viewController, menuPosition:.Left)
        //sideMenu?.delegate = self //optional
        
        sideMenu?.menuWidth = 260.0 // optional, default is 160
        sideMenu?.bouncingEnabled = false
        
        // make navigation bar showing over side menu
        //view.bringSubviewToFront(navigationBar)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        
    }
    
    func sideMenuWillClose() {
        
    }
}
