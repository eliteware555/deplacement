//
//  SettingRightMenuController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/27/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class SettingRightMenuController: UITableViewController {
    
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblViewPublicProfile: UILabel!
    @IBOutlet weak var lblPersonalInfo: UILabel!
    @IBOutlet weak var lblChangePassword: UILabel!
    @IBOutlet weak var lblMembership: UILabel!
    @IBOutlet weak var lblAuthentifications: UILabel!
    @IBOutlet weak var lblVehicleSettings: UILabel!
    @IBOutlet weak var lblPreferences: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        defLang = prefs.stringForKey("LANGUAGE")!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        initView()
    }
    
    func initView() {
        
        lblProfile.text = defLang == "fr" ? R.fr.profile : R.en.profile
        lblViewPublicProfile.text = defLang == "fr" ? R.fr.view_public_profile : R.en.view_public_profile
        lblPersonalInfo.text = defLang == "fr" ? R.fr.personal_info : R.en.personal_info
        lblChangePassword.text = defLang == "fr" ? R.fr.change_password : R.en.change_password
        lblMembership.text = defLang == "fr" ? R.fr.membership : R.en.membership
        lblAuthentifications.text = defLang == "fr" ? R.fr.authentifications : R.en.authentifications
        lblVehicleSettings.text = defLang == "fr" ? R.fr.vehicle_settings : R.en.vehicle_settings
        lblPreferences.text = defLang == "fr" ? R.fr.preferences : R.en.preferences
        
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }

    //////////////////////////////
    // MARK - UITableView Delegate
    //////////////////////////////
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let settingstoryboard: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
        
        var destController: UINavigationController
        
        switch indexPath.row {
            
        case 0:
            return
            
        case 1:
            
            prefs.setInteger(1, forKey: "PUBLIC_PROFILE")
            prefs.synchronize()
            
            let accountstoryboard: UIStoryboard = UIStoryboard(name: "MyAccount", bundle: nil)
            destController = accountstoryboard.instantiateViewControllerWithIdentifier("MyAccount") as! UINavigationController
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.comefromSetting = true
            
            break
            
        case 2:
            
            destController = settingstoryboard.instantiateViewControllerWithIdentifier("PersonalInfo") as! UINavigationController
            
            break
        case 3:
            destController = settingstoryboard.instantiateViewControllerWithIdentifier("ChangePassword") as! UINavigationController
            break
            
        case 4:
            destController = settingstoryboard.instantiateViewControllerWithIdentifier("Membership") as! UINavigationController
            break
            
        case 5:
            destController = settingstoryboard.instantiateViewControllerWithIdentifier("Authentification") as! UINavigationController
            break
            
        case 6:
            destController = settingstoryboard.instantiateViewControllerWithIdentifier("VehicleSetting") as! UINavigationController
            break
            
        case 7:
            destController = settingstoryboard.instantiateViewControllerWithIdentifier("Preferences") as! UINavigationController
            break
            
        default:
            return
        }
        
        self.revealViewController().pushFrontViewController(destController, animated: true)
    }
}
