//
//  MenuController.swift
//  SidebarMenu
//
//  Created by Alok Nair on 02/04/15.
//  Copyright (c) 2015 MaaS Pros Inc. All rights reserved.
//

import UIKit

import Alamofire
import JLToast
import SwiftyJSON

class MenuController: UITableViewController {
    
    var badgeView: M13BadgeView!
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet weak var btnSearchLift: UIButton!
    @IBOutlet weak var btnOfferLift: UIButton!

    @IBOutlet var row3Label: UILabel!
    @IBOutlet var row4Label: UILabel!
    @IBOutlet var row5Label: UILabel!
    @IBOutlet var row6Label: UILabel!
    @IBOutlet var row7Label: UILabel!
    @IBOutlet var languageSegmentedControl: UISegmentedControl!
    
    var selectedIdx: NSIndexPath!
    
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    var _user: UserInfo!
    
    @IBOutlet weak var imvUnreadMessage: UIImageView!
    
    @IBOutlet weak var imvBell: UIImageView!
    
    var unreadMessageCnt: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!        
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        profileImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        languageSegmentedControl.layer.borderColor = UIColor.whiteColor().CGColor
        languageSegmentedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: UIControlState.Selected)
        languageSegmentedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        
//        getUserDetails(prefs.stringForKey("USER_HASH")!)
        
        _user = (UIApplication.sharedApplication().delegate as! AppDelegate).me
        
        badgeView = M13BadgeView.init(frame: CGRectMake(0, 0, 18.0, 18.0))
        badgeView.text = "0"
        badgeView.hidesWhenZero = true
        badgeView.font = UIFont.systemFontOfSize(13.0)
        badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentBottom
        badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight
        imvUnreadMessage.addSubview(badgeView)
        
        load_details()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // change selection state when to come from setting/view my profile
        if(defLang == "fr") {
            languageSegmentedControl.selectedSegmentIndex = 1
        } else {
            languageSegmentedControl.selectedSegmentIndex = 0
        }
        
        initViews()
        
        getUnreadMessage()
    }
    
    func initViews() {
        //set text to views
       
        btnSearchLift.setTitle(defLang == "fr" ? R.fr.row1 : R.en.row1, forState: .Normal)
        btnOfferLift.setTitle(defLang == "fr" ? R.fr.row2 : R.en.row2, forState: .Normal)
        
        row3Label.text = defLang == "fr" ? R.fr.row3 : R.en.row3
        row4Label.text = defLang == "fr" ? R.fr.row4 : R.en.row4
        row5Label.text = defLang == "fr" ? R.fr.row5 : R.en.row5
        row6Label.text = defLang == "fr" ? R.fr.row6 : R.en.row6
        row7Label.text = defLang == "fr" ? R.fr.row7 : R.en.row7
        
        imvUnreadMessage.image = UIImage(named: "message_new")
        
        load_image(_user.user_pic)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {        

        let revealViewController = self.revealViewController()
        var destController : UINavigationController
        
        var deststoryboard: UIStoryboard!
     
        switch indexPath.row {
            
        case 0:
            return
        case 1:
            
            prefs.setInteger(0, forKey: "PUBLIC_PROFILE")
            prefs.synchronize()
            
            deststoryboard = UIStoryboard(name: "MyAccount", bundle:  nil)
            destController = deststoryboard.instantiateViewControllerWithIdentifier("MyAccount") as! UINavigationController            
            
            selectedIdx = indexPath
            
            break
            
        case 2:
            deststoryboard = UIStoryboard(name: "MyAccount", bundle:  nil)
//            destController = deststoryboard.instantiateViewControllerWithIdentifier("MyLift") as! UINavigationController
            destController = deststoryboard.instantiateViewControllerWithIdentifier("RecentLifts") as! UINavigationController
            
            let recentLiftVC = destController.topViewController as! RecentLiftsViewController
            recentLiftVC.recentLift = false
            
            selectedIdx = indexPath
            
            break
            
        case 3:
            deststoryboard = UIStoryboard(name: "Ratings", bundle:  nil)
            destController = deststoryboard.instantiateViewControllerWithIdentifier("Ratings") as! UINavigationController
            
            selectedIdx = indexPath
            
            break
            
        case 4:
            deststoryboard = UIStoryboard(name: "Settings", bundle:  nil)
            destController = deststoryboard.instantiateViewControllerWithIdentifier("PersonalInfo") as! UINavigationController
            
            selectedIdx = indexPath
            
            break
            
        case 5:
            
            let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            prefs.setObject("", forKey: "USER_HASH")
            prefs.setObject("", forKey: "USER_EMAIL")
            prefs.setObject("", forKey: "USER_FIRST_NAME")
            prefs.setObject("", forKey: "USER_LAST_NAME")
            prefs.setObject("", forKey: "USER_PIC")
            prefs.setBool(false, forKey: "ISLOGGEDIN")
            prefs.synchronize()
            
            self.dismissViewControllerAnimated(true, completion: nil)
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "AppIntro", bundle: nil)
            let initViewController: UIViewController = storyBoard.instantiateViewControllerWithIdentifier("appLanguageVC") as! AppLanguageVC
            self.view.window!.rootViewController? = initViewController
            self.presentViewController(initViewController, animated: false, completion: nil)
            
            return
            
        default:
            return
        }
        
        revealViewController.pushFrontViewController(destController, animated: true)
    }
    
    
    func load_image(urlString:String) {
        
        print(_user.gender)
        
        self.profileImage.setImageWithUrl(NSURL(string: urlString)!, placeHolderImage: UIImage(named: _user.gender == 1 ? "male_profile" : "female_profile"))
        
//        let imgURL: NSURL = NSURL(string: urlString)!
//        let request: NSURLRequest = NSURLRequest(URL: imgURL)
//        
//        let session = NSURLSession.sharedSession()
//        let task = session.dataTaskWithRequest(request){
//            (data, response, error) -> Void in
//            
//            if (error == nil && data != nil) {
//                func display_image() {
//                    self.profileImage.image = UIImage(data: data!)
//                }
//                
//                dispatch_async(dispatch_get_main_queue(), display_image)
//            }
//        }
//        
//        task.resume()
    }
    
    func load_details() {
        self.nameLabel.text = _user.name
    }
    
    @IBAction func languageChanged(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            prefs.setObject("en", forKey: "LANGUAGE")
            defLang = "en"
        case 1:
            prefs.setObject("fr", forKey: "LANGUAGE")
            defLang = "fr"
        default:
            break
        }
        
        initViews()
    }
    
    @IBAction func searchLift() {
        
        if let _ = selectedIdx {
            let cell = tableView.cellForRowAtIndexPath(selectedIdx)
            cell?.selected = false
        }
        
        let mainstoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle:  nil)
        let destController = mainstoryboard.instantiateViewControllerWithIdentifier("MyNavController") as! UINavigationController
        
        self.revealViewController().pushFrontViewController(destController, animated: true)
    }
    
    @IBAction func offerLift() {  // OfferLift
        
        if let _ = selectedIdx {
            let cell = tableView.cellForRowAtIndexPath(selectedIdx)
            cell?.selected = false
        }
        
        let offerstoryboard: UIStoryboard = UIStoryboard(name: "OfferLift", bundle:  nil)
        let destController = offerstoryboard.instantiateViewControllerWithIdentifier("OfferLift") as! UINavigationController
        
        self.revealViewController().pushFrontViewController(destController, animated: true)
    }
    
    @IBAction func gotomessageList(sender: AnyObject) {
        
//        if (unreadMessageCnt > 0) {
        
            if let _ = selectedIdx {
                let cell = tableView.cellForRowAtIndexPath(selectedIdx)
                cell?.selected = false
            }
            
            let offerstoryboard: UIStoryboard = UIStoryboard(name: "MyAccount", bundle:  nil)
            let destController = offerstoryboard.instantiateViewControllerWithIdentifier("Message") as! UINavigationController
            
            self.revealViewController().pushFrontViewController(destController, animated: true)
//        }
        
    }
    
    func getUnreadMessage() {
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "site_lang": defLang
        ]
        
        Alamofire.request(.GET, kBaseURL + "/message/unread", parameters: params)
            .validate()
            .responseJSON { response in
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /message/unread")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
                    return
                }
                
                let jsonResponse = JSON(value)
                
                print(jsonResponse)
                
                if let status = jsonResponse["status"].bool {
                    if(status) {
                        

                        if let _ = jsonResponse["message_data"].int {
                            
                            self.unreadMessageCnt = jsonResponse["message_data"].int!
                            
                            self.badgeView.text = "\(jsonResponse["message_data"].int!)"
                        }
                       
                    } else {
                        print("Error parsing /message/unread")
                        return
                    }
                }
                
        }
    }
}


////user details function
//func getUserDetails(hash: String!) {
//    
//    let params = [
//        "hash_key": hash,
//        "site_lang": defLang
//    ]
//    
//    Alamofire.request(.GET, kBaseURL + "/users/user", parameters: params)
//        .validate()
//        .responseJSON { response in
//            
//            guard let value = response.result.value else {
//                print("Error: did not receive data")
//                JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
//                return
//            }
//            
//            guard response.result.error == nil else {
//                print("Error calling GET on /users/user")
//                print(response.result.error)
//                JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: R.tDuration).show()
//                return
//            }
//            
//            let jsonResponse = JSON(value)
//            
//            if let status = jsonResponse["status"].bool {
//                if(status) {
//                    if let first_name = jsonResponse["user_data"]["first_name"].string {
//                        self.prefs.setObject(first_name, forKey: "USER_FIRST_NAME")
//                        self.prefs.synchronize()
//                    } else {
//                        print("Error parsing first_name")
//                        return
//                    }
//                    
//                    if let last_name = jsonResponse["user_data"]["last_name"].string {
//                        self.prefs.setObject(last_name, forKey: "USER_LAST_NAME")
//                        self.prefs.synchronize()
//                    } else {
//                        print("Error parsing last_name")
//                        return
//                    }
//                    
//                    if let username = jsonResponse["user_data"]["username"].string {
//                        self.prefs.setObject(username, forKey: "USER_EMAIL")
//                        self.prefs.synchronize()
//                    } else {
//                        print("Error parsing username")
//                        return
//                    }
//                    
//                    if let profile_pic = jsonResponse["user_data"]["profile_pic"].string {
//                        self.prefs.setObject(profile_pic, forKey: "USER_PIC")
//                        self.prefs.synchronize()
//                        
//                        self.profileImage.setImageWithUrl(NSURL(string: profile_pic)!, placeHolderImage: UIImage(named: self._user.gender == 0 ? "male_profile" : "female_profile"))
//                        
//                        //                            self.load_image(profile_pic)
//                        
//                    } else {
//                        print("Error parsing profile_pic")
//                        return
//                    }
//                    
//                    self.load_details()
//                } else {
//                    if let message = jsonResponse["message"].string {
//                        JLToast.makeText(message, duration: R.tDuration).show()
//                    } else {
//                        print("Error parsing /users/user")
//                        return
//                    }
//                }
//            } else {
//                print("Error parsing /users/user")
//                return
//            }
//            
//    }
//}
//
