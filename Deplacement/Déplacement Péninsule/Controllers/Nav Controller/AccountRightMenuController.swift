//
//  RightSlideMenuControllerTableViewController.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/26/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class AccountRightMenuController: UITableViewController {
    
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"

    // menu item label
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblVehicleInfo: UILabel!
    @IBOutlet weak var lblRecentLifts: UILabel!
    @IBOutlet weak var lblReviewsandRatings: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        defLang = prefs.stringForKey("LANGUAGE")!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        initView()
    }
    
    func initView() {
        
        lblProfile.text = defLang == "fr" ? R.fr.profile : R.en.profile
        lblVehicleInfo.text = defLang == "fr" ? R.fr.profile_vehicle_info : R.en.profile_vehicle_info
        lblRecentLifts.text = defLang == "fr" ? R.fr.profile_recent_lift : R.en.profile_recent_lift
        lblReviewsandRatings.text = defLang == "fr" ? R.fr.profile_reviews_ratings : R.en.profile_reviews_ratings
        lblMessage.text = defLang == "fr" ? R.fr.profile_message : R.en.profile_message
        
    }

    // MARK: - Table view data source    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }


    //////////////////////////////
    // MARK - UITableView Delegate
    //////////////////////////////
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let accountstoryboard: UIStoryboard = UIStoryboard(name: "MyAccount", bundle: nil)
        
        var destController: UINavigationController
        
        switch indexPath.row {
            
        case 1:
            destController = accountstoryboard.instantiateViewControllerWithIdentifier("VehicleInfo") as! UINavigationController
            break
            
        case 2:
            destController = accountstoryboard.instantiateViewControllerWithIdentifier("RecentLifts") as! UINavigationController
            
            let recentLiftVC = destController.topViewController as! RecentLiftsViewController
            recentLiftVC.recentLift = true
            
            break
        case 3:
            
            let deststoryboard = UIStoryboard(name: "Ratings", bundle:  nil)
            destController = deststoryboard.instantiateViewControllerWithIdentifier("Ratings") as! UINavigationController
            
            break
            
        case 4:
            destController = accountstoryboard.instantiateViewControllerWithIdentifier("Message") as! UINavigationController
            break
            
        default:
            return
        }
        
        self.revealViewController().pushFrontViewController(destController, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
