//
//  OfferListVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/20/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class OfferLiftVC: UIViewController, ENSideMenuDelegate, UIGestureRecognizerDelegate, NSURLConnectionDataDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var stopOverCnt = 0
    var liftKind: Int = 0   // oneoff lift = 0, regular lift = 1
    
    /**
     **    root view
     **/
    // root view height
    @IBOutlet weak var layout_height_superView: NSLayoutConstraint!
    
    /**
     **    route view
     **/
    // route view height constraint
    @IBOutlet weak var layout_height_routeView: NSLayoutConstraint!
    // stopover tableview height constraint
    @IBOutlet weak var layout_height_stopoverHeight: NSLayoutConstraint!
    // map container view height
    @IBOutlet weak var layout_height_mapView: NSLayoutConstraint!       // orginal = 210
    // google map kind tab indicator
    @IBOutlet weak var layout_leading_tabIndicator: NSLayoutConstraint!     // original = 0
    
    /**
     **    date and time view
     **/
    // one-off lift return date and time container view height
    @IBOutlet weak var layout_height_oneoff_returnView: NSLayoutConstraint!    // original = 95
    @IBOutlet weak var layout_height_regular_returnView: NSLayoutConstraint!    // original  = 150
    // date and time containver view height
    @IBOutlet weak var layout_height_dateTimeSuperView: NSLayoutConstraint! // (One-off lift view = 380, regular lift view =  540)
    // regular lift view height constraint
    @IBOutlet weak var layout_height_regularLiftView: NSLayoutConstraint!    // 490 - 150 = 340
    
    // date amd time containcer view
    @IBOutlet weak var dateTimeSuperView: UIView!
    
    
    // map container view
    @IBOutlet weak var mapContainer: UIView!
    // hide or show google map
    @IBOutlet weak var hideOrShowMap: UIButton!
    // google map kind tab button & indicator
    @IBOutlet weak var tabRoute: UIButton!
    @IBOutlet weak var tabDeparture: UIButton!
    @IBOutlet weak var tabDestination: UIButton!
    @IBOutlet weak var tabIndicator: UILabel!
    // hide or show mapview flag
    var isVisibleMapView = true
    
    // tab button array
    var arrTabs = [UIButton]()
    var selectedTabIdx: Int! = nil
    
    /**
     **  return date and time view toogle, return view (One-off lift & Regular lift)
     **/
    // return toggle switch
    @IBOutlet weak var oneoff_returnToogle: UISwitch!
    @IBOutlet weak var regular_returnToggle: UISwitch!
    // return container view
    @IBOutlet weak var oneoff_returnView: UIView!           // height = 90
    @IBOutlet weak var regular_returnView: UIView!          // 150
    @IBOutlet weak var regularLiftView: UIView! // regular lift date & time view
    
    /**
     **  flexibility selection dropdown
     **/
    // one-off lift
    @IBOutlet weak var oneoff_flexibilityView: UIView!
    @IBOutlet weak var oneoff_fifteenMin: UIButton!
    @IBOutlet weak var oneoff_thirtyMin: UIButton!
    @IBOutlet weak var oneoff_flexibilityDropDown: UIButton!
    
    // regular lift
    @IBOutlet weak var regular_flexibilityView: UIView!
    @IBOutlet weak var regular_fifteenMin: UIButton!
    @IBOutlet weak var regular_thirtyMin: UIButton!
    @IBOutlet weak var regular_flexibilityDropDown: UIButton!
    
    // segment control (One-off lift or Regular lift)
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
    // route view outlet
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var tblStopover: UITableView!
    
    // date & time view outlet
    // one-off lift
    @IBOutlet weak var oneoff_departureDate: UITextField!
    @IBOutlet weak var oneoff_departureTime: UITextField!
    @IBOutlet weak var oneoff_returnDate: UITextField!
    @IBOutlet weak var oneoff_returnTime: UITextField!
    
    // regular lift
    @IBOutlet weak var regular_departureTime: UITextField!
    @IBOutlet weak var regular_From: UITextField!
    @IBOutlet weak var regular_To: UITextField!
    @IBOutlet weak var regular_returnTime: UITextField!
    
    @IBOutlet weak var regular_depSun: UIButton!
    @IBOutlet weak var regular_depMon: UIButton!
    @IBOutlet weak var regular_depTue: UIButton!
    @IBOutlet weak var regular_depWed: UIButton!
    @IBOutlet weak var regular_depThu: UIButton!
    @IBOutlet weak var regular_depFri: UIButton!
    @IBOutlet weak var regular_depSat: UIButton!
    
    var arrDepartures = [UIButton]()
    
    @IBOutlet weak var regular_resSun: UIButton!
    @IBOutlet weak var regular_resMon: UIButton!
    @IBOutlet weak var regular_resTue: UIButton!
    @IBOutlet weak var regular_resWed: UIButton!
    @IBOutlet weak var regular_resThu: UIButton!
    @IBOutlet weak var regular_resFri: UIButton!
    @IBOutlet weak var regular_resSat: UIButton!
    var arrReturns = [UIButton]()
    
    
    // activityindicator
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    // left menu open blur view
    var blurView = UIView()
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    private var responseData: NSMutableData?
    private var connection: NSURLConnection?
    private let googleMapsKey = "AIzaSyDg2tlPcoqxx2Q2rfjhsAKS-9j0n3JA_a4"
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    var active = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // initialize side navigation menu
        self.sideMenuController()?.sideMenu?.delegate = self
        
        let btnMenu = UIButton()
        btnMenu.setImage(UIImage(named: "menu.png"), forState: .Normal)
        btnMenu.frame = CGRectMake(0, 0, 30, 30)
        btnMenu.addTarget(self, action: Selector("toggleMenu:"), forControlEvents: .TouchUpInside)
        
        let menuButton = UIBarButtonItem()
        menuButton.customView = btnMenu
        self.navigationItem.leftBarButtonItem = menuButton
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        // initialize right bar button item
        let rightButton = UIButton()
        rightButton.setImage(UIImage(named: "bell.png"), forState: .Normal)
        rightButton.frame = CGRectMake(0, 0, 30, 30)
        rightButton.addTarget(self, action: Selector("rightMenuClicked:"), forControlEvents: .TouchUpInside)
        
        let rightMenu = UIBarButtonItem()
        rightMenu.customView = rightButton
        self.navigationItem.rightBarButtonItem = rightMenu
        
        
        // set navigation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.row2 : R.en.row2
        
        arrTabs.append(tabRoute)
        arrTabs.append(tabDeparture)
        arrTabs.append(tabDestination)
        
        for btnTab in arrTabs {
            btnTab.addTarget(self, action: "onTabClicked:", forControlEvents: .TouchUpInside)
        }
        
//        selectTab(0)
        
        arrDepartures.append(regular_depSun)
        arrDepartures.append(regular_depMon)
        arrDepartures.append(regular_depTue)
        arrDepartures.append(regular_depWed)
        arrDepartures.append(regular_depThu)
        arrDepartures.append(regular_depFri)
        arrDepartures.append(regular_depSat)
        
        arrReturns.append(regular_resSun)
        arrReturns.append(regular_resMon)
        arrReturns.append(regular_resTue)
        arrReturns.append(regular_resWed)
        arrReturns.append(regular_resThu)
        arrReturns.append(regular_resFri)
        arrReturns.append(regular_resSat)
        
        for btnDeparture in arrDepartures {
            btnDeparture.addTarget(self, action: "onDepartureDateSelected:", forControlEvents: .TouchUpInside)
        }
        
        for btnReturn in arrReturns {
            btnReturn.addTarget(self, action: "onReturnDateSelected:", forControlEvents: .TouchUpInside)
        }
    }
    
    override func viewWillAppear(animated: Bool) {

        super.viewWillAppear(animated)
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initView() {
    }
    
    // remove activity indicator view
    func removeActivityView() {
        self.activityView.stopAnimating()
        self.view.viewWithTag(111)?.removeFromSuperview()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //   Mark - NSURLConnectionDataDelegate
    ////////////////////////////////////////////////////////////////////////////
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Action
    ////////////////////////////////////////////////////////////////////////////
    @IBAction func viewDidTapped(sender: AnyObject) {
        
        self.view .endEditing(true)
    }
    
    @IBAction func addStopover(sender: AnyObject) {
        
        stopOverCnt++
        
        layout_height_routeView.constant += 50
        layout_height_stopoverHeight.constant += 50
        layout_height_superView.constant += 50
        UIView.animateWithDuration(0.5) { () -> Void in
            self.view .layoutIfNeeded()
            
            self.tblStopover.reloadData()
        }
    }
    
    func selectTab(newTabIndex: Int) {
        
        // present new tab selected state
        self.layout_leading_tabIndicator.constant = self.tabIndicator.frame.size.width * CGFloat(newTabIndex)
        UIView.animateWithDuration(0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
        
        // do process on mapview
        selectedTabIdx = newTabIndex
        
    }
    
    // tab button click action
    func onTabClicked(sender: UIButton!) {
        
        for btnTab in arrTabs {
            btnTab.setTitleColor(UIColor(colorLiteralRed: 110/255.0, green: 110/255.0, blue: 110/255.0, alpha: 1.0), forState: .Normal)
        }
        
        if(sender == tabRoute) {
            selectTab(0)
            // change tab button text color
            tabRoute.setTitleColor(UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0), forState: .Normal)
        } else if(sender == tabDeparture) {
            // change tab button text color
            tabDeparture.setTitleColor(UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0), forState: .Normal)
            selectTab(1)
        } else {
            selectTab(2)
            // change tab button text color
            tabDestination.setTitleColor(UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0), forState: .Normal)
        }
    }
    
    // depature date select action -  onDepartureDateSelected:
    func onDepartureDateSelected(sender: UIButton!) {
        
        sender.selected = !sender.selected
        
        if(sender.selected) {
            sender.backgroundColor = UIColor(colorLiteralRed: 178/255.0, green: 209/255.0, blue: 49/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = UIColor.whiteColor()
        }
    }
    
    // return date select action - onReturnDateSelected:
    func onReturnDateSelected(sender: UIButton!) {
        
        sender.selected = !sender.selected
        
        if(sender.selected) {
            sender.backgroundColor = UIColor(colorLiteralRed: 178/255.0, green: 209/255.0, blue: 49/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = UIColor.whiteColor()
        }
    }
    
    // lift kind select action - (One-off lift, regular lift)
    @IBAction func segmentControlAction(sender: AnyObject) {
        
        var deltaHeight: CGFloat = 150.0
        
        if(segmentControl.selectedSegmentIndex == 0 && liftKind != 0) {
            
            print("One-off lift")
            
            if(!regular_returnToggle.on) {
                
                if(oneoff_returnToogle.on) {
                    deltaHeight = 0.0
                } else {
                    deltaHeight = 95.0
                }
                
            } else {
                
                if(!oneoff_returnToogle.on) {
                    deltaHeight = 245.0
                }
            }

        
            // hide regular lift view
            liftKind = 0
            self.regularLiftView.hidden = true
            
            // change the date & time super container view , rootview( decrease 150)
            layout_height_dateTimeSuperView.constant -= deltaHeight
            layout_height_superView.constant -= deltaHeight
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })

        } else if(segmentControl.selectedSegmentIndex == 1 && liftKind != 1) {
            
            print("Regular Lift")
            
            if(!oneoff_returnToogle.on) {
                
                if(regular_returnToggle.on) {
                    deltaHeight = 245.0
                } else {
                    deltaHeight = 95.0
                }
                
            } else {
                
                if(!regular_returnToggle.on) {
                    deltaHeight = 0
                }
            }
            
            // show regular lift view
            liftKind = 1
            self.regularLiftView.hidden = false
            
            // change the date & time super container view , rootview ( increase 150)
            layout_height_dateTimeSuperView.constant += deltaHeight
            layout_height_superView.constant += deltaHeight
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func hideOrShowGoogleMap(sender: AnyObject) {
        
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(13.0),
            NSForegroundColorAttributeName : UIColor(colorLiteralRed: 32/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        var attributedString: NSMutableAttributedString!
        
        if(isVisibleMapView) {
            
            isVisibleMapView = false
            self.mapContainer.hidden = true
            layout_height_mapView.constant = 0
            layout_height_routeView.constant -= 210
            layout_height_superView.constant -= 210
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                
                self.view.layoutIfNeeded()
            })
            
            attributedString = NSMutableAttributedString(string:"View Route in Google Map", attributes:attrs)
            
        } else {
            
            isVisibleMapView = true
            layout_height_mapView.constant = 210
            layout_height_routeView.constant += 210
            layout_height_superView.constant += 210
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
                
            })
            
            self.mapContainer.hidden = false
            attributedString = NSMutableAttributedString(string:"Hide Google Map", attributes:attrs)
        }
        
        sender.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    // hide and show return date & time view
    @IBAction func returnSwitchToggle(sender: AnyObject) {
        
        // One-off lift
        if(liftKind == 0) {
        
            if(self.oneoff_returnToogle.on) {
            
                // show return date & time view
                oneoff_returnView.hidden = false
                layout_height_oneoff_returnView.constant = 95;
                layout_height_dateTimeSuperView.constant += 95
                layout_height_superView.constant += 95
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            } else {
                
                // hide return date & time view
                oneoff_returnView.hidden = true
                layout_height_oneoff_returnView.constant = 0
                layout_height_dateTimeSuperView.constant -= 95
                layout_height_superView.constant -= 95
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
        // regular lift
        else {
            if(self.regular_returnToggle.on) {
                
                // show return date & time view
                regular_returnView.hidden = false
                layout_height_regular_returnView.constant = 150;
                layout_height_regularLiftView.constant += 150
                layout_height_dateTimeSuperView.constant += 150
                layout_height_superView.constant += 150
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            } else {
                
                // hide return date & time view
                regular_returnView.hidden = true
                layout_height_regular_returnView.constant = 0
                layout_height_regularLiftView.constant -= 150
                layout_height_dateTimeSuperView.constant -= 150
                layout_height_superView.constant -= 150
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    // flexibility change action
    @IBAction func flexibilityChangeAction(sender: UIButton!) {
        
        // one_off lift
        if(liftKind == 0) {
    
            if(sender == oneoff_flexibilityDropDown) {
                
                if(self.oneoff_flexibilityView.hidden) {
                    
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.oneoff_flexibilityView.hidden = false
                    })
                } else {
                    
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.oneoff_flexibilityView.hidden = true
                    })
                }
                
            } else if(sender == oneoff_fifteenMin) {
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.oneoff_flexibilityView.hidden = true
                })
                
                self.oneoff_flexibilityDropDown.setTitle("+/-15 minutes", forState: .Normal)
            } else if(sender == oneoff_thirtyMin) {
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.oneoff_flexibilityView.hidden = true
                })
                
                self.oneoff_flexibilityDropDown.setTitle("+/-30 minutes", forState: .Normal)
            }
        }
        // regular lift
        else {
            if(sender == regular_flexibilityDropDown) {
                
                if(self.regular_flexibilityView.hidden) {
                    
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.regular_flexibilityView.hidden = false
                    })
                } else {
                    
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.regular_flexibilityView.hidden = true
                    })
                }
                
            } else if(sender == regular_fifteenMin) {
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.regular_flexibilityView.hidden = true
                })
                
                self.regular_flexibilityDropDown.setTitle("+/-15 minutes", forState: .Normal)
                
            } else if(sender == regular_thirtyMin) {
                
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.regular_flexibilityView.hidden = true
                })
                
                self.regular_flexibilityDropDown.setTitle("+/-30 minutes", forState: .Normal)
            }
        }
    }
    
    @IBAction func saveAction(sender: AnyObject) {
        
        self.performSegueWithIdentifier("SeguePriceVehicle", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //   Mark - UITableView Datasource
    ////////////////////////////////////////////////////////////////////////////
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stopOverCnt
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StopOverCell", forIndexPath:  indexPath) as! StopOverCell
        
        cell.txtStopLocation.delegate = self
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
//    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
//        
//        let deleteAction = UITableViewRowAction(style: .Default, title: nil, handler: { (action, indexPath) in
//            tableView.dataSource?.tableView?(
//                tableView,
//                commitEditingStyle: .Delete,
//                forRowAtIndexPath: indexPath
//            )
//            
//            return
//        })
//        
////        let deleteAction = UITableViewRowAction(style: .Default, title: nil) {
////            (rowAction:UITableViewRowAction, indexPath:NSIndexPath) -> Void in
////            //TODO: Delete the row at indexPath here
////        }
//        deleteAction.backgroundColor = UIColor(patternImage: UIImage(named: "delete.png")!)
//        return [deleteAction]
//    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {

        if(editingStyle == UITableViewCellEditingStyle.Delete) {
            
            self.stopOverCnt--
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            
            layout_height_routeView.constant -= 50
            layout_height_stopoverHeight.constant -= 50
            layout_height_superView.constant -= 50
            UIView.animateWithDuration(0.5) { () -> Void in
                self.view .layoutIfNeeded()
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // menu UITextFieldDelegate
    ////////////////////////////////////////////////////////////////////////////
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if(textField == oneoff_departureDate || textField == oneoff_returnDate || textField == regular_From || textField == regular_To) {
            
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Date
            textField.inputView = datePickerView
            
            datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
            
        } else if(textField == oneoff_departureTime || textField == oneoff_returnTime || textField == regular_departureTime || textField == regular_returnTime) {
            
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Time
            textField.inputView = datePickerView
            
            datePickerView.addTarget(self, action: Selector("timePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        }
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        if(oneoff_departureDate.isFirstResponder()) {
            oneoff_departureDate.text = dateFormatter.stringFromDate(sender.date)
        } else if(oneoff_returnDate.isFirstResponder()) {
            oneoff_returnDate.text = dateFormatter.stringFromDate(sender.date)
        } else if (regular_From.isFirstResponder()) {
            regular_From.text = dateFormatter.stringFromDate(sender.date)
        } else if(regular_To.isFirstResponder()) {
            regular_To.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    func timePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        
        if(oneoff_departureTime.isFirstResponder()) {
            oneoff_departureTime.text = dateFormatter.stringFromDate(sender.date)
        } else if(oneoff_returnTime.isFirstResponder()) {
            oneoff_returnTime.text = dateFormatter.stringFromDate(sender.date)
        } else if (regular_departureTime.isFirstResponder()) {
            regular_departureTime.text = dateFormatter.stringFromDate(sender.date)
        } else if(regular_returnTime.isFirstResponder()) {
            regular_returnTime.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // menu button click action
    ////////////////////////////////////////////////////////////////////////////
    func toggleMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    func rightMenuClicked(sender: AnyObject) {
        
        print("right menu bell clicked")
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //   Mark - ENSideMenu Delegate
    ////////////////////////////////////////////////////////////////////////////
    func sideMenuWillOpen() {
        
        blurView.frame = self.view.bounds
        blurView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        blurView.tag = 125
        self.view.addSubview(blurView)
        
        // add a tap recognizer
        let tapRecognizer = UITapGestureRecognizer(target: self, action: "respondToTapGesture:")
        self.blurView.addGestureRecognizer(tapRecognizer)
        tapRecognizer.delegate = self
    }
    
    func sideMenuWillClose() {
        if let viewWithTag = self.view.viewWithTag(125) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func respondToTapGesture(gesture: UIGestureRecognizer) {
        toggleSideMenuView()
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
