//
//  Price_VehicleVC.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/21/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class Price_VehicleVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var stopOverCnt = 3 // for test
    var isVisiblePricePerRoute = false
    
    var defLang = "fr"
    var prefs = NSUserDefaults.standardUserDefaults()
    
    // ui configuration
    @IBOutlet weak var lblPriceandVehicle: UILabel!
    @IBOutlet weak var lblPricePerSeat: UILabel!
    @IBOutlet weak var lblFromTo: UILabel!
    @IBOutlet weak var lblSuggestionPrice: UILabel!
    
    @IBOutlet weak var tblStopovers: UITableView!
    
    // constraint outlet
    // stopovers tableview height
    @IBOutlet weak var layout_height_stopovers: NSLayoutConstraint! // original height = 0, each cell height = 50
    @IBOutlet weak var layout_height_view_stopovers: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        // set navigation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.title = defLang == "fr" ? R.fr.row2 : R.en.row2
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func initView() {
        
//        if(defLang == "fr") {
//            
//            
//        } else {
        let strSuggestion = NSMutableAttributedString(string: "Our price suggestions (Automatically calculated)")
        strSuggestion.addAttribute(NSForegroundColorAttributeName, value: UIColor(colorLiteralRed: 223/255.0, green: 25/255.0, blue: 108/255.0, alpha: 1.0), range: NSMakeRange(0, 22))
        strSuggestion.addAttribute(NSForegroundColorAttributeName, value: UIColor(colorLiteralRed: 48/255.0, green: 48/255.0, blue: 48/255.0, alpha: 1.0), range: NSMakeRange(22, 26))
        lblSuggestionPrice.attributedText = strSuggestion
        

//        }
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //   Mark - UITableView Datasource
    ////////////////////////////////////////////////////////////////////////////
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stopOverCnt + 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StopOverPriceCell", forIndexPath:  indexPath) as! StopOverPriceCell
        
//        cell.txtStopLocation.delegate = self
        
        if(indexPath.row == 0) {
            cell.setPointMarker("blue_point.png")
            cell.hideControl(true)
        } else if(indexPath.row == stopOverCnt + 1) {
            cell.setPointMarker("green_point.png")
            cell.hideControl(true)
        } else {
            cell.setPointMarker("blue_dot.png")
        }
        
        return cell
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Action
    ////////////////////////////////////////////////////////////////////////////

    @IBAction func showPricePerRoute(sender: AnyObject?) {
        
        isVisiblePricePerRoute  = !isVisiblePricePerRoute
        
        if(isVisiblePricePerRoute) {
            
            layout_height_stopovers.constant = (CGFloat)(stopOverCnt + 2) * 50 + 20.0
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
        } else {
            
            layout_height_stopovers.constant = 0
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
        }
        
    }
    
}
