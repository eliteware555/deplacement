//
//  FindLiftVC.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

class FindLiftVC: UIViewController, ENSideMenuDelegate, UIGestureRecognizerDelegate, NSURLConnectionDataDelegate {
    
    @IBOutlet var fromField: AutoCompleteTextField!
    @IBOutlet var toField: AutoCompleteTextField!
    @IBOutlet var rangeSlider: RangeSlider!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var byTimeLabel: UILabel!
    @IBOutlet var sortByLabel: UILabel!
    @IBOutlet var proximitySort: UIButton!
    @IBOutlet var priceSort: UIButton!
    @IBOutlet var timeSort: UIButton!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var priceFrom: UITextField!
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet var priceTo: UITextField!
    @IBOutlet var frequencyLabel: UILabel!
    @IBOutlet var oneOffLabel: UILabel!
    @IBOutlet var regularLabel: UILabel!
    @IBOutlet var allLabel: UILabel!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var oneOffRadioButton:DownStateButton?
    @IBOutlet var regularRadioButton:DownStateButton?
    @IBOutlet var allRadioButton:DownStateButton?
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var dateField: UITextField!
    @IBOutlet var fromDistView: UIView!
    @IBOutlet var toDistView: UIView!
    @IBOutlet var fromDistButton: UIButton!
    @IBOutlet var toDistButton: UIButton!
    
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    var blurView = UIView() //side menu open black blur
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    var pickerContainer = UIView()
    
    var sortType = "proximity"
    var minTimeValue = "0:05"
    var maxTimeValue = "24:00"
    var freqType = 0
    var fromLat = ""
    var fromLon = ""
    var toLat = ""
    var toLon = ""
    var fromDist = "25"
    var toDist = "25"
    
    private var responseData:NSMutableData?
    private var connection:NSURLConnection?
    private let googleMapsKey = "AIzaSyDg2tlPcoqxx2Q2rfjhsAKS-9j0n3JA_a4"
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    var active = 0
    var rides: [Ride] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        defLang = prefs.stringForKey("LANGUAGE")!
        
        //initialize side navigation menu
        self.sideMenuController()?.sideMenu?.delegate = self
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        //set navigation back bar button item text as nil
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style: .Plain, target: nil, action: nil)
        navigationItem.title =  defLang == "fr" ? R.fr.row1 : R.en.row1
        
        rangeSlider.addTarget(self, action: "rangeSliderValueChanged:", forControlEvents: .ValueChanged)
        
        configureTextField()
        handleTextFieldInterfaces()
        configureDateField()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //customise navigation bar and status bar color
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.Black
        nav?.barTintColor = UIColor(red: CGFloat(5/255.0), green: CGFloat(141/255.0), blue: CGFloat(100/255.0), alpha: 1)
        nav?.tintColor = UIColor.whiteColor()
        nav?.translucent = false
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        initViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func configureDateField() {
        pickerContainer.frame = CGRectMake(0, 0, 320.0, 200.0)
        pickerContainer.backgroundColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1)
        
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.frame    = CGRectMake(0.0, 0.0, 320.0, 200.0)
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
        
        pickerContainer.addSubview(datePickerView)
        dateField.inputView = pickerContainer
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.stringFromDate(sender.date)
        dateField.text = strDate
    }
    
    private func configureTextField(){
        fromField.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        fromField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        fromField.autoCompleteCellHeight = 35.0
        fromField.maximumAutoCompleteCount = 20
        fromField.hidesWhenSelected = true
        fromField.hidesWhenEmpty = true
        fromField.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        fromField.autoCompleteAttributes = attributes
        
        toField.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        toField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        toField.autoCompleteCellHeight = 35.0
        toField.maximumAutoCompleteCount = 20
        toField.hidesWhenSelected = true
        toField.hidesWhenEmpty = true
        toField.enableAttributedText = true
        toField.autoCompleteAttributes = attributes
    }
    
    private func handleTextFieldInterfaces(){
        fromField.onTextChange = {text in
            if !text.isEmpty{
                if self.connection != nil{
                    self.connection!.cancel()
                    self.connection = nil
                }
                let urlString = "\(self.baseURLString)?key=\(self.googleMapsKey)&input=\(text)"
                let url = NSURL(string: (urlString as NSString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
                if url != nil{
                    let urlRequest = NSURLRequest(URL: url!)
                    self.connection = NSURLConnection(request: urlRequest, delegate: self)
                    self.active = 0
                }
            }
        }
        
        fromField.onSelect = {text, indexpath in
            Location.geocodeAddressString(text, completion: { (placemark, error) -> Void in
                if let coordinate = placemark?.location?.coordinate{
                    self.fromLat = String(format:"%f", coordinate.latitude)
                    self.fromLon = String(format:"%f", coordinate.longitude)
                }
            })
        }
        
        toField.onTextChange = {text in
            if !text.isEmpty{
                if self.connection != nil{
                    self.connection!.cancel()
                    self.connection = nil
                }
                let urlString = "\(self.baseURLString)?key=\(self.googleMapsKey)&input=\(text)"
                let url = NSURL(string: (urlString as NSString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
                if url != nil{
                    let urlRequest = NSURLRequest(URL: url!)
                    self.connection = NSURLConnection(request: urlRequest, delegate: self)
                    self.active = 1
                }
            }
        }
        
        toField.onSelect = {text, indexpath in
            Location.geocodeAddressString(text, completion: { (placemark, error) -> Void in
                if let coordinate = placemark?.location?.coordinate{
                    self.toLat = String(format:"%f", coordinate.latitude)
                    self.toLon = String(format:"%f", coordinate.longitude)
                }
            })
        }
    }
    
    
    //MARK: NSURLConnectionDelegate
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        responseData = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        responseData?.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        if let data = responseData {
            do {
                let result = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                
                if let status = result["status"] as? String{
                    if status == "OK"{
                        if let predictions = result["predictions"] as? NSArray{
                            var locations = [String]()
                            for dict in predictions as! [NSDictionary]{
                                locations.append(dict["description"] as! String)
                            }
                            
                            switch active {
                            case 0:
                                self.fromField.autoCompleteStrings = locations
                                break
                            case 1:
                                self.toField.autoCompleteStrings = locations
                                break
                            default:
                                break
                            }
                            
                            return
                        }
                    }
                }
                
                switch active {
                case 0:
                    self.fromField.autoCompleteStrings = nil
                    break
                case 1:
                    self.toField.autoCompleteStrings = nil
                    break
                default:
                    break
                }
            }
            catch let error as NSError {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        print("Error: \(error.localizedDescription)")
    }
    
    //adjust range slider margin (N.B: needed to fix bug with auto layout constraints)
    override func viewDidLayoutSubviews() {
        let margin: CGFloat = 20.0
        let width = view.bounds.width - 2.0 * margin
        rangeSlider.frame = CGRect(x: margin, y: timeLabel.frame.origin.y + 29,
            width: width, height: 31.0)
    }
    
    func initViews() {
        rangeSlider.trackHighlightTintColor = ColorUtils.uicolorFromHex(0xCCCCCC)
        
        //set text to views
        sortByLabel.text = defLang == "fr" ? R.fr.sort_by : R.en.sort_by
        byTimeLabel.text = defLang == "fr" ? R.fr.by_time : R.en.by_time
        timeLabel.text = "[0:00 - 24:00]"
        priceLabel.text = defLang == "fr" ? R.fr.price : R.en.price
        priceFrom.placeholder = defLang == "fr" ? R.fr.from : R.en.from
        priceTo.placeholder = defLang == "fr" ? R.fr.to : R.en.to
        frequencyLabel.text = defLang == "fr" ? R.fr.freq : R.en.freq
        oneOffLabel.text = defLang == "fr" ? R.fr.one_off : R.en.one_off
        regularLabel.text = defLang == "fr" ? R.fr.regular : R.en.regular
        allLabel.text = defLang == "fr" ? R.fr.all : R.en.all
        submitButton.setTitle(defLang == "fr" ? R.fr.submit : R.en.submit, forState: .Normal)
        fromField.placeholder = defLang == "fr" ? R.fr.from_loc : R.en.from_loc
        toField.placeholder = defLang == "fr" ? R.fr.to_loc : R.en.to_loc
        
        //frequency radio button initializations
        oneOffRadioButton?.downStateImage = "radio_button_on"
        oneOffRadioButton?.myAlternateButton = [regularRadioButton!, allRadioButton!]
        
        regularRadioButton?.downStateImage = "radio_button_on"
        regularRadioButton?.myAlternateButton = [oneOffRadioButton!, allRadioButton!]
        
        allRadioButton?.downStateImage = "radio_button_on"
        allRadioButton?.myAlternateButton = [oneOffRadioButton!, regularRadioButton!]
        allRadioButton?.toggleButton()
        
        
        fromDistView!.layer.masksToBounds = true
        fromDistView!.layer.borderColor = ColorUtils.uicolorFromHex(0xF9F9F9).CGColor
        fromDistView!.layer.borderWidth = 2.0
        
        toDistView!.layer.masksToBounds = true
        toDistView!.layer.borderColor = ColorUtils.uicolorFromHex(0xF9F9F9).CGColor
        toDistView!.layer.borderWidth = 2.0
        
        //activity indicator view
        activityView.center = self.view.center
        activityView.color = UIColor.redColor()
        activityView.tag = 111
    }
    
    //time range slide on value changed listener
    func rangeSliderValueChanged(rangeSlider: RangeSlider) {
        minTimeValue =  getTimeStringFromSeconds(rangeSlider.lowerValue)
        maxTimeValue = getTimeStringFromSeconds(rangeSlider.upperValue)
        timeLabel.text = "[\(minTimeValue) - \(maxTimeValue)]"
    }
    
    //convert seconds from time range slider to 24hour time format
    func getTimeStringFromSeconds(seconds: Double) -> String {
        let dcFormatter = NSDateComponentsFormatter()
        dcFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehavior.Pad
        dcFormatter.allowedUnits = [NSCalendarUnit.Hour, NSCalendarUnit.Minute]
        dcFormatter.unitsStyle = NSDateComponentsFormatterUnitsStyle.Positional
        return dcFormatter.stringFromTimeInterval(seconds)!
    }
    
    //proximity sort button click event
    @IBAction func proximitySortClicked(sender: UIButton) {
        proximitySort.setBackgroundImage(UIImage(named: "Geo-fence"), forState: .Normal)
        timeSort.setBackgroundImage(UIImage(named: "Time-Grey"), forState: .Normal)
        priceSort.setBackgroundImage(UIImage(named: "Dollar-Grey"), forState: .Normal)
        
        sortType = "proximity"
    }
    
    //time sort button click event
    @IBAction func timeSortClicked(sender: UIButton) {
        proximitySort.setBackgroundImage(UIImage(named: "Geo-fence-Grey"), forState: .Normal)
        timeSort.setBackgroundImage(UIImage(named: "Time"), forState: .Normal)
        priceSort.setBackgroundImage(UIImage(named: "Dollar-Grey"), forState: .Normal)
        
        sortType = "time"
    }
    
    //price sort button click event
    @IBAction func priceSortClicked(sender: UIButton) {
        proximitySort.setBackgroundImage(UIImage(named: "Geo-fence-Grey"), forState: .Normal)
        timeSort.setBackgroundImage(UIImage(named: "Time-Grey"), forState: .Normal)
        priceSort.setBackgroundImage(UIImage(named: "Dollar"), forState: .Normal)
        
        sortType = "price"
    }
    
    @IBAction func viewTapped(sender: AnyObject) {
        fromField.resignFirstResponder()
        toField.resignFirstResponder()
        priceFrom.resignFirstResponder()
        priceTo.resignFirstResponder()
        dateField.resignFirstResponder()
    }
    
    //frequency type radio button click event
    @IBAction func optionOneOffClicked(sender: AnyObject) {
        freqType = 1
    }
    @IBAction func optionRegularClicked(sender: AnyObject) {
        freqType = 2
    }
    @IBAction func optionAllClicked(sender: AnyObject) {
        freqType = 0
    }
    
    @IBAction func swapClicked(sender: UIButton) {
        let temp1 = fromField.text
        let temp2 = toField.text
        fromField.text = temp2
        toField.text = temp1
        
        let tLat1 = fromLat
        let tLat2 = toLat
        let tLon1 = fromLon
        let tLon2 = toLon
        fromLat = tLat2
        toLat = tLat1
        fromLon = tLon2
        toLon = tLon1
    }
    
    @IBAction func fromDistanceClicked(sender: UIButton) {
        if(toDistView.hidden == false) {
            toDistView.hidden = true
        }
        fromDistHideUnhide()
    }
    
    @IBAction func toDistanceClicked(sender: UIButton) {
        if(fromDistView.hidden == false) {
            fromDistView.hidden = true
        }
        toDistHideUnhide()
    }
    
    @IBAction func distFromClicked(sender: UIButton) {
        fromDistButton.setTitle("+\(sender.tag) KM", forState: .Normal)
        fromDist = "\(sender.tag)"
        fromDistHideUnhide()
    }
    
    @IBAction func distToClicked(sender: UIButton) {
        toDistButton.setTitle("+\(sender.tag) KM", forState: .Normal)
        toDist = "\(sender.tag)"
        toDistHideUnhide()
    }
    
    func toDistHideUnhide() {
        if(toDistView.hidden == false) {
            toDistView.hidden = true
        } else {
            toDistView.hidden = false
        }
    }
    
    func fromDistHideUnhide() {
        if(fromDistView.hidden == false) {
            fromDistView.hidden = true
        } else {
            fromDistView.hidden = false
        }
    }
    
    @IBAction func submitClicked(sender: UIButton) {
        activityView.startAnimating()
        self.view.addSubview(activityView)
        
        if(fromField.text != nil || toField.text != nil) {
            searchLifts("\(0)")
        } else {
            JLToast.makeText(self.defLang == "fr" ? R.fr.invalid_search : R.en.invalid_search, duration: 3).show()
        }
    }
    
    func searchLifts(start: String) {
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "search_from": (!fromLat.isEmpty && !fromLon.isEmpty) ? fromField.text! : "",
            "search_to": (!toLat.isEmpty && !toLon.isEmpty) ? toField.text! : "",
            "search_date": dateField.text!,
            "start_time": minTimeValue + ":00",
            "end_time": maxTimeValue + ":00",
            "from_distance": fromDist,
            "to_distance": toDist,
            "from_lat": fromLat,
            "from_lng": fromLon,
            "to_lat": toLat,
            "to_lng": toLon,
            "frequency": "\(freqType)",
            "price_from": priceFrom.text!,
            "price_to": priceTo.text!,
            "sort_by": sortType,
            "start": start,
            "site_lang": defLang
        ]
        print(params)
        
        Alamofire.request(.GET, kBaseURL + "/search/list", parameters: params)
            .validate()
            .responseJSON { response in
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: 3).show()
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /search/list")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: 3).show()
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("The JSON is: \(jsonResponse)")
                
                
                if let total = jsonResponse["total"].int {
                    if(total > 0) {
                        self.rides.removeAll()
                        
                        for var index = 0; index < total; ++index {
                            let ride = Ride(
                                hash_key: jsonResponse["rides"][index]["ride_hash"].string!,
                                start_point_en: jsonResponse["rides"][index]["start_point_en"].string!,
                                start_point_fr: jsonResponse["rides"][index]["start_point_fr"].string!,
                                start_lat: jsonResponse["rides"][index]["start_lat"].string!,
                                start_lon: jsonResponse["rides"][index]["start_lon"].string!,
                                end_point_en: jsonResponse["rides"][index]["end_point_en"].string!,
                                end_point_fr: jsonResponse["rides"][index]["end_point_fr"].string!,
                                end_lat: jsonResponse["rides"][index]["end_lat"].string!,
                                end_lon: jsonResponse["rides"][index]["end_lon"].string!,
                                price_per_seat: jsonResponse["rides"][index]["price_per_seat"].string!,
                                driver_first_name: jsonResponse["rides"][index]["diver_first_name"].string!,
                                driver_last_name: jsonResponse["rides"][index]["diver_last_name"].string!,
                                auto_accept: jsonResponse["rides"][index]["auto_accept"].string!,
                                unix_depature_time: jsonResponse["rides"][index]["unix_depature_time"].string!,
                                seats_available: jsonResponse["rides"][index]["seats_available"].string!
                            )
                            
                            self.rides.append(ride)
                        }
                        self.removeActivityView()
                        self.performSegueWithIdentifier("RidesListSegue", sender: self)
                        
                    } else {
                        JLToast.makeText(self.defLang == "fr" ? R.fr.no_rides : R.en.no_rides, duration: 3).show()
                        self.removeActivityView()
                    }
                    
                } else {
                    print("Error parsing /search/list")
                    return
                }
                
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "RidesListSegue" {
            let destinationViewController: RidesListVC = segue.destinationViewController as! RidesListVC
            destinationViewController.rides = []
            destinationViewController.rides = self.rides
        }
    }
    
    func removeActivityView() {
        self.activityView.stopAnimating()
        self.view.viewWithTag(111)?.removeFromSuperview()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    //menu button click action
    @IBAction func toggleMenu(sender: UIBarButtonItem) {
        toggleSideMenuView()
    }
    
    //ENSideMenu Delegate
    func sideMenuWillOpen() {
        blurView.frame = self.view.bounds
        blurView.backgroundColor =  UIColor.blackColor().colorWithAlphaComponent(0.5)
        blurView.tag = 125
        self.view.addSubview(blurView)
        
        //add a tap recognizer
        let recognizer1 = UITapGestureRecognizer(target: self, action: "respondToTapGesture:")
        self.blurView.addGestureRecognizer(recognizer1)
        recognizer1.delegate = self
    }
    
    func respondToTapGesture(gesture: UIGestureRecognizer) {
        toggleSideMenuView()
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(_: UIGestureRecognizer,
        shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
            return true
    }
    
    func sideMenuWillClose() {
        if let viewWithTag = self.view.viewWithTag(125) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
}
