//
//  RideDetailTVC.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair on 11/01/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit
import Alamofire
import JLToast
import SwiftyJSON

enum TravelModes: Int {
    case driving
    case walking
    case bicycling
}

class RideDetailTVC: UITableViewController {
    
    @IBOutlet weak var viewMap: GMSMapView!
    
    @IBOutlet var fromLabel: UILabel!
    @IBOutlet var toLabel: UILabel!
    @IBOutlet var fromField: UILabel!
    @IBOutlet var toField: UILabel!
    @IBOutlet var departureLabel: UILabel!
    @IBOutlet var departureField: UILabel!
    @IBOutlet var etaLabel: UILabel!
    @IBOutlet var etaField: UILabel!
    @IBOutlet var seatsLabel: UILabel!
    @IBOutlet var seatsField: UILabel!
    @IBOutlet var luggageLabel: UILabel!
    @IBOutlet var luggageField: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var timeField: UILabel!
    @IBOutlet var detourLabel: UILabel!
    @IBOutlet var detourField: UILabel!
    @IBOutlet var stopoverLabel: UILabel!
    @IBOutlet var stopoverField: UILabel!
    @IBOutlet var smokeImage: UIButton!
    @IBOutlet var eatImage: UIButton!
    @IBOutlet var musicImage: UIButton!
    @IBOutlet var petsImage: UIButton!
    @IBOutlet var chatImage: UIButton!
    @IBOutlet var handicappedImage: UIButton!
    @IBOutlet var childrenImage: UIButton!
    
    @IBOutlet var driverImage: UIImageView!
    @IBOutlet var driverName: UILabel!
    @IBOutlet var driverAge: UILabel!
    @IBOutlet var driverRating: FloatRatingView!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var offlineLabel: UILabel!
    @IBOutlet var emailField: UILabel!
    @IBOutlet var phoneField: UILabel!
    @IBOutlet var offlineField: UILabel!
    
    @IBOutlet var vehicleName: UILabel!
    @IBOutlet var vehicleYear: UILabel!
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var acLabel: UILabel!
    @IBOutlet var acField: UILabel!
    @IBOutlet var regNumberLabel: UILabel!
    @IBOutlet var regNumberField: UILabel!
    
    @IBOutlet var liftsLabel: UILabel!
    @IBOutlet var liftsField: UILabel!
    @IBOutlet var reviewsLabel: UILabel!
    @IBOutlet var reviewsField: UILabel!
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    var mapTasks = MapTasks()
    var locationMarker: GMSMarker!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    var routePolyline: GMSPolyline!
    var markersArray: Array<GMSMarker> = []
    var waypointsArray: Array<String> = []
    var travelMode = TravelModes.driving
    
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
    let prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    var ride_hash = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        //hide empty uitableview rows
        let backgroundView = UIView(frame: CGRectZero)
        self.tableView.tableFooterView = backgroundView
        self.tableView.alwaysBounceVertical = false
        
        //dummy header to disable header sticking to uitableview
        let dummyViewHeight: CGFloat = 40
        let dummyView = UIView(frame: CGRectMake(0, 0, self.tableView.bounds.size.width, dummyViewHeight))
        self.tableView.tableHeaderView = dummyView
        self.tableView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        
        initViews()
        
        viewMap.settings.zoomGestures = true
        
        if let _ = self.routePolyline {
            self.clearRoute()
            self.waypointsArray.removeAll(keepCapacity: false)
        }
        
        let coordinate = CLLocationCoordinate2D(latitude: 9.843307, longitude: 76.448603)
        let positionString = String(format: "%f", coordinate.latitude) + "," + String(format: "%f", coordinate.longitude)
        
        let coordinate1 = CLLocationCoordinate2D(latitude: 9.848751, longitude: 76.680672)
        let positionString1 = String(format: "%f", coordinate1.latitude) + "," + String(format: "%f", coordinate1.longitude)
        waypointsArray.append(positionString)
        waypointsArray.append(positionString1)

        let coordinateOrigin = CLLocationCoordinate2D(latitude: 9.9312454, longitude: 76.26732830000003)
        let positionStringOrigin = String(format: "%f", coordinateOrigin.latitude) + "," + String(format: "%f", coordinateOrigin.longitude)
        
        let coordinateDestination = CLLocationCoordinate2D(latitude: 8.524258399999999, longitude: 76.93667909999999)
        let positionStringDestination = String(format: "%f", coordinateDestination.latitude) + "," + String(format: "%f", coordinateDestination.longitude)
        
        let origin = positionStringOrigin
        let destination = positionStringDestination
        
        self.mapTasks.getDirections(origin, destination: destination, waypoints: waypointsArray, travelMode: self.travelMode, completionHandler: { (status, success) -> Void in
            if success {
                self.configureMapAndMarkersForRoute()
                self.drawRoute()
            }
            else {
                print(status)
            }
        })
    }
    
    func initViews() {
        
        //set text to views
        fromLabel.text = defLang == "fr" ? R.fr.from : R.en.from
        toLabel.text = defLang == "fr" ? R.fr.to : R.en.to
        emailLabel.text = defLang == "fr" ? R.fr.email : R.en.email
        departureLabel.text = defLang == "fr" ? R.fr.depature : R.en.depature
        etaLabel.text = defLang == "fr" ? R.fr.eta : R.en.eta
        seatsLabel.text = defLang == "fr" ? R.fr.seats : R.en.seats
        luggageLabel.text = defLang == "fr" ? R.fr.luggage_size : R.en.luggage_size
        timeLabel.text = defLang == "fr" ? R.fr.time_flexibility : R.en.time_flexibility
        detourLabel.text = defLang == "fr" ? R.fr.detour : R.en.detour
        stopoverLabel.text = defLang == "fr" ? R.fr.stopovers : R.en.stopovers
        phoneNumberLabel.text = defLang == "fr" ? R.fr.phone_number : R.en.phone_number
        offlineLabel.text = defLang == "fr" ? R.fr.offline_id : R.en.offline_id
        acLabel.text = defLang == "fr" ? R.fr.ac : R.en.ac
        regNumberLabel.text = defLang == "fr" ? R.fr.reg_number : R.en.reg_number
        liftsLabel.text = defLang == "fr" ? R.fr.lifts_offered : R.en.lifts_offered
        reviewsLabel.text = defLang == "fr" ? R.fr.total_reviews : R.en.total_reviews
        
        //activity indicator view
        activityView.center = self.view.center
        activityView.color = UIColor.redColor()
        activityView.tag = 111
        
        driverRating.editable = true
        
        //rounded driver image
        driverImage.layer.borderWidth=0.0
        driverImage.layer.masksToBounds = false
        driverImage.layer.borderColor = UIColor.whiteColor().CGColor
        driverImage.layer.cornerRadius = 13
        driverImage.layer.cornerRadius = driverImage.frame.size.height/2
        driverImage.clipsToBounds = true
        
        //rounded vehicle image
        vehicleImage.layer.borderWidth=0.0
        vehicleImage.layer.masksToBounds = false
        vehicleImage.layer.borderColor = UIColor.whiteColor().CGColor
        vehicleImage.layer.cornerRadius = 13
        vehicleImage.layer.cornerRadius = vehicleImage.frame.size.height/2
        vehicleImage.clipsToBounds = true
        
        fetchRide()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 10
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 1
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        //stopover field
        if(indexPath.section == 0 && indexPath.row == 7) {
            return 100
        }
        return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1)
        header.textLabel!.textColor = UIColor(red:0.57, green:0.57, blue:0.57, alpha:1)
        header.textLabel!.font = UIFont.boldSystemFontOfSize(14)
        header.alpha = 1
        
        switch section {
        case 0:
            header.textLabel!.text = defLang == "fr" ? R.fr.trip : R.en.trip
        case 1:
            header.textLabel!.text = defLang == "fr" ? R.fr.vehicle_info : R.en.vehicle_info
        case 2:
            header.textLabel!.text = defLang == "fr" ? R.fr.activity : R.en.activity
        default:
            header.textLabel!.text = defLang == "fr" ? "" : ""
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func fetchRide() {
        activityView.startAnimating()
        self.view.addSubview(activityView)
        
        let params = [
            "login_hash": prefs.stringForKey("USER_HASH")!,
            "ride_hash": ride_hash,
            "site_lang": defLang
        ]
        print(params)
        
        Alamofire.request(.GET, kBaseURL + "/ride/ride_detail", parameters: params)
            .validate()
            .responseJSON { response in
                
                guard let value = response.result.value else {
                    print("Error: did not receive data")
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: 3).show()
                    self.removeActivityView()
                    return
                }
                
                guard response.result.error == nil else {
                    print("Error calling GET on /ride/ride_detail")
                    print(response.result.error)
                    JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: 3).show()
                    self.removeActivityView()
                    return
                }
                
                let jsonResponse = JSON(value)
                print("The JSON is: \(jsonResponse)")
                
                
                if let status = jsonResponse["status"].bool  {
                    if(status) {
                        
                        self.fromField.text = jsonResponse["ride_data"]["ride"]["map_data"]["start"]["address"].string!
                        self.toField.text = jsonResponse["ride_data"]["ride"]["map_data"]["end"]["address"].string!
                        self.emailField.text = jsonResponse["ride_data"]["driver_details"]["email"].string!
                        self.departureField.text = jsonResponse["ride_data"]["ride"]["start_date"].string!
                        self.etaField.text = jsonResponse["ride_data"]["ride"]["end_date"].string!
                        self.seatsField.text = jsonResponse["ride_data"]["ride"]["seats_available"].string!
                        self.luggageField.text = jsonResponse["ride_data"]["ride"]["luggage_per_seat"].string!
                        self.timeField.text = jsonResponse["ride_data"]["ride"]["flexibility"].string! == "00:15:00" ? "Around 15 minutes" : "Around 30 minutes"
                        
                        let detour = jsonResponse["ride_data"]["ride"]["detour"].string!
                        switch detour {
                        case "0":
                            self.detourField.text = "None"
                        case "1":
                            self.detourField.text = "Maximum 15 minutes"
                        case "2":
                            self.detourField.text = "Maximum 30 minutes"
                        case "3":
                            self.detourField.text = "Any detour"
                        default:
                            self.detourField.text = "None"
                        }
                        
                        
                        self.stopoverField.text = ""
                        
                        self.driverName.text = jsonResponse["ride_data"]["driver_details"]["first_name"].string! + " " + jsonResponse["ride_data"]["driver_details"]["last_name"].string!
                        self.driverAge.text = jsonResponse["ride_data"]["driver_details"]["age"].string! + " years old"
                        self.driverRating.rating = Float(jsonResponse["ride_data"]["driver_details"]["user_rating"].int!)
                        self.phoneField.text = jsonResponse["ride_data"]["driver_details"]["phone"].string!
                        self.offlineField.text = !jsonResponse["ride_data"]["driver_details"]["id_proof_expiry_date"].string!.isEmpty ? self.defLang == "fr" ? "Vérifié" : "Verified" : self.defLang == "fr" ? "Non vérifié" : "Not Verified"
                        self.acField.text = jsonResponse["ride_data"]["vehicle"]["ac"].string! != "0" ? "Yes" : "No"
                        self.regNumberField.text = jsonResponse["ride_data"]["vehicle"]["number_plate"].string!
                        self.vehicleName.text = jsonResponse["ride_data"]["vehicle"]["make"].string! + " " + jsonResponse["ride_data"]["vehicle"]["model"].string!
                        self.vehicleYear.text = jsonResponse["ride_data"]["vehicle"]["year"].string!
                        self.liftsField.text = String(jsonResponse["ride_data"]["count_active_rides"].int!)
                        self.reviewsField.text = String(jsonResponse["ride_data"]["rating_datas"].count)
                        
                        if let profile_pic = jsonResponse["ride_data"]["driver_details"]["profile_pic"].string {
                            if(!profile_pic.isEmpty) {
                                self.load_driverimage(profile_pic)
                            }
                            
                        } else {
                            print("Error parsing profile_pic")
                        }
                        
                        if let vehicle_pic = jsonResponse["ride_data"]["vehicle"]["vehicle_image"].string {
                            if(!vehicle_pic.isEmpty) {
                                self.load_vehiclerimage(vehicle_pic)
                            }
                            
                        } else {
                            print("Error parsing vehicle_image")
                        }
                        
                        self.smokeImage.setBackgroundImage(UIImage(named:  "smoke" +  jsonResponse["ride_data"]["ride"]["smoking"].string!), forState: .Normal)
                        
                        self.eatImage.setBackgroundImage(UIImage(named:  "eat" +  jsonResponse["ride_data"]["ride"]["food"].string!), forState: .Normal)
                        
                        self.musicImage.setBackgroundImage(UIImage(named:  "music" +  jsonResponse["ride_data"]["ride"]["music"].string!), forState: .Normal)
                        
                        self.petsImage.setBackgroundImage(UIImage(named:  "pets" +  jsonResponse["ride_data"]["ride"]["pets"].string!), forState: .Normal)
                        
                        self.chatImage.setBackgroundImage(UIImage(named:  "chat" +  jsonResponse["ride_data"]["ride"]["chatting"].string!), forState: .Normal)
                        
                        self.childrenImage.setBackgroundImage(UIImage(named:  "children" +  jsonResponse["ride_data"]["ride"]["children"].string!), forState: .Normal)
                        
                        self.handicappedImage.setBackgroundImage(UIImage(named:  "handicapped" +  jsonResponse["ride_data"]["ride"]["handicapped"].string!), forState: .Normal)
                        
                        //self.loadMap(jsonResponse)
                        
                        self.removeActivityView()
                        
                    } else {
                        JLToast.makeText(self.defLang == "fr" ? R.fr.error_connecting : R.en.error_connecting, duration: 3).show()
                        self.removeActivityView()
                    }
                    
                } else {
                    print("Error parsing /ride/ride_detail")
                    return
                }
        }
    }
    
//    func loadMap(jsonResponse: JSON){
//        
//        for stopover in jsonResponse["ride_data"]["ride"]["map_data"]["stopovers"] {
//            let coordinate = CLLocationCoordinate2D(latitude: 9.843307, longitude: 76.448603)
//            let positionString = String(format: "%f", coordinate.latitude) + "," + String(format: "%f", coordinate.longitude)
//        }
//        let coordinate = CLLocationCoordinate2D(latitude: 9.843307, longitude: 76.448603)
//        let positionString = String(format: "%f", coordinate.latitude) + "," + String(format: "%f", coordinate.longitude)
//        
//        let coordinate1 = CLLocationCoordinate2D(latitude: 9.848751, longitude: 76.680672)
//        let positionString1 = String(format: "%f", coordinate1.latitude) + "," + String(format: "%f", coordinate1.longitude)
//        waypointsArray.append(positionString)
//        waypointsArray.append(positionString1)
//        
//        let coordinateOrigin = CLLocationCoordinate2D(latitude: 9.9312454, longitude: 76.26732830000003)
//        let positionStringOrigin = String(format: "%f", coordinateOrigin.latitude) + "," + String(format: "%f", coordinateOrigin.longitude)
//        
//        let coordinateDestination = CLLocationCoordinate2D(latitude: 8.524258399999999, longitude: 76.93667909999999)
//        let positionStringDestination = String(format: "%f", coordinateDestination.latitude) + "," + String(format: "%f", coordinateDestination.longitude)
//        
//        let origin = positionStringOrigin
//        let destination = positionStringDestination
//        
//        self.mapTasks.getDirections(origin, destination: destination, waypoints: waypointsArray, travelMode: self.travelMode, completionHandler: { (status, success) -> Void in
//            if success {
//                self.configureMapAndMarkersForRoute()
//                self.drawRoute()
//            }
//            else {
//                print(status)
//            }
//        })
//    }
    
    func load_driverimage(urlString:String) {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    self.driverImage.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
        }
        
        task.resume()
    }
    
    func load_vehiclerimage(urlString:String) {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil) {
                func display_image() {
                    self.vehicleImage.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
        }
        
        task.resume()
    }
    
    func removeActivityView() {
        self.activityView.stopAnimating()
        self.view.viewWithTag(111)?.removeFromSuperview()
    }
    
    func clearRoute() {
        originMarker.map = nil
        destinationMarker.map = nil
        routePolyline.map = nil
        
        originMarker = nil
        destinationMarker = nil
        routePolyline = nil
        
        if markersArray.count > 0 {
            for marker in markersArray {
                marker.map = nil
            }
            
            markersArray.removeAll(keepCapacity: false)
        }
    }
    
    func createRoute() {
        if let _ = routePolyline {
            clearRoute()
            
            mapTasks.getDirections(mapTasks.originAddress, destination: mapTasks.destinationAddress, waypoints: waypointsArray, travelMode: travelMode, completionHandler: { (status, success) -> Void in
                
                if success {
                    self.configureMapAndMarkersForRoute()
                    self.drawRoute()
                }
                else {
                    print(status)
                }
            })
        }
    }
    
    func configureMapAndMarkersForRoute() {
        viewMap.camera = GMSCameraPosition.cameraWithTarget(mapTasks.originCoordinate, zoom: 8.0)
        
        originMarker = GMSMarker(position: self.mapTasks.originCoordinate)
        originMarker.map = self.viewMap
        originMarker.icon = GMSMarker.markerImageWithColor(UIColor.greenColor())
        originMarker.title = self.mapTasks.originAddress
        
        destinationMarker = GMSMarker(position: self.mapTasks.destinationCoordinate)
        destinationMarker.map = self.viewMap
        destinationMarker.icon = GMSMarker.markerImageWithColor(UIColor.redColor())
        destinationMarker.title = self.mapTasks.destinationAddress
        
        if waypointsArray.count > 0 {
            for waypoint in waypointsArray {
                let lat: Double = (waypoint.componentsSeparatedByString(",")[0] as NSString).doubleValue
                let lng: Double = (waypoint.componentsSeparatedByString(",")[1] as NSString).doubleValue
                
                let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                marker.map = viewMap
                marker.icon = GMSMarker.markerImageWithColor(UIColor.purpleColor())
                
                markersArray.append(marker)
            }
            
            
        }
    }
    
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)
        routePolyline = GMSPolyline(path: path)
        routePolyline.map = viewMap
    }
}
