//
//  Ride.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//


import Foundation
import UIKit

class Ride {
    
    var hash_key: String
    var start_point_en: String
    var start_point_fr: String
    var start_lat: String
    var start_lon: String
    var end_point_en: String
    var end_point_fr: String
    var end_lat: String
    var end_lon: String
    var price_per_seat: String
    var driver_first_name: String
    var driver_last_name: String
    var auto_accept: String
    var unix_depature_time: String
    var seats_available: String
    var suggested_price: Int = 0
    
//    var stopovers: [StopOver] = []
    
    init(hash_key: String, start_point_en: String, start_point_fr: String, start_lat: String, start_lon: String, end_point_en: String, end_point_fr: String, end_lat: String, end_lon: String, price_per_seat: String, driver_first_name: String, driver_last_name: String, auto_accept: String, unix_depature_time: String, seats_available: String) {
        self.hash_key = hash_key
        self.start_point_en = start_point_en
        self.start_point_fr = start_point_fr
        self.start_lat = start_lat
        self.start_lon = start_lon
        self.end_point_en = end_point_en
        self.end_point_fr = end_point_fr
        self.end_lat = end_lat
        self.end_lon = end_lon
        self.price_per_seat = price_per_seat
        self.driver_first_name = driver_first_name
        self.driver_last_name = driver_last_name
        self.auto_accept = auto_accept
        self.unix_depature_time = unix_depature_time
        self.seats_available = seats_available
    }
}
