//
//  PendingRating.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/7/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation
import UIKit

class PendingRating {
    
    var start_point: String = ""
    var end_point: String = ""
    var date_time: String = ""
    var ride_hash: String = ""
    var ride_trip: Int = 1
    var passengers: [Passenger] = []
    
    init() { }
}

