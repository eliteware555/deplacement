//
//  Vehicle.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/6/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation
import UIKit

class Vehicle {
    
    var veh_id: Int = 0
    var hash_key: String = ""
    
    var year: String = ""
    var make_name: String = ""
    var model_name: String = ""
    
    var simple_name: String = ""
    var detail_name: String = ""
    var license_plate: String = ""
    var body_type: String = ""
    var color: String = ""
    
    var license_number: String = ""
    
    var airCondition: String = "No"
    
    var luggageSize: Int = 0
    
    var comfortLevel: Float = 0.0
    var bikeRack: String = "No"
    var skiRack: String = "No"
    
    var veh_pic: String = "" // image url
    var co2_output: String = ""
    var fuel_efficiency: String = ""
    var distance_per_year: String = ""
    
    var transmission: Int = 1
    var mileage: Int = 1
    
    var seats: Int = 0
    var type_tires: String = ""
    
    var ratedValue = 0
    
    init() { }
    
    // account/vehicleinfo (year make model)
    func makeSimpleName() {
        simple_name = year + " " + make_name + " " + model_name
    }
    
    // settings/vehicle setting(year make model, body_type, color)
    func makeDetailName() {
        detail_name = year + " " + make_name + ",\n" + model_name + ", " + body_type + ", " + color
    }
}
