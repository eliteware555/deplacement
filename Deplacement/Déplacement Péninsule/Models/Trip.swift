//
//  Trip.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/5/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation
import UIKit

class Trip {

    var start_point_en: String = ""
    var end_point_en:String = ""
    var start_point_fr: String = ""
    var end_point_fr:String = ""
    var start_lat = ""
    var start_lng = ""
    var end_lat = ""
    var end_lng = ""
    var departureDate: String = ""
    var returnDate: String = ""
    var ride_hash: String = ""
    
    init() { }
}
