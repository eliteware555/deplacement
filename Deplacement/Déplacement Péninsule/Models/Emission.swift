//
//  Emission.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/14/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation

class Emission {
    
    var veh_id: Int = 0
    var transmission: Int = 1
    var city_mileage: Float = 0.0
    var highway_mileage: Float = 0.0
    var mileage_mpg: Int = 0
    var co2_emission: Int = 0
    var mileage: Float = 0.0
    var co2_output: Float = 0.0
    
    init() {}
}
