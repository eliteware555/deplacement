
//
//  WyaPoint.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/11/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation


class WayPoint {
    
    var ride_key: String  = ""
    
    var id: Int = -1
    var ride_id: Int = -1
    
    var start_point_en: String = ""
    var start_point_fr: String = ""
    var start_loc_en: String = ""
    var start_loc_fr: String = ""
    var start_lat: String = ""
    var start_lon: String = ""
    
    var end_point_en: String = ""
    var end_point_fr: String = ""
    var end_loc_en: String = ""
    var end_loc_fr: String = ""
    var end_lat: String = ""
    var end_lon: String = ""
    
    var waypoints: String = ""
//    var suggested_price: String = ""
//    var price_per_seat: String = ""
    var route_distance: String = ""
    var position: Int = -1
    
    var suggested_price: Int = 0
    var price_per_seat: Int = 0
    
    init() { }
}