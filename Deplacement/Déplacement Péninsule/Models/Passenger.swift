//
//  File.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/7/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation

class Passenger {
    
    var passenger_id: Int = -1
    var first_name: String = ""
    var last_name: String = ""
    var gender: Int = 1
    var profile_pic: String = ""
    var pas_hash_key: String = ""
    
    var ratedValue: Float = 0.0
    
    init() { }
}