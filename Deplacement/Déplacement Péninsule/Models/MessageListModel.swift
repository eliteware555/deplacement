//
//  MessageListModel.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation

class MessageListModel {
    
    var id: Int = 0
    var pic_url: String = ""
    var name: String = ""
    var role: String = ""
    var from_to: String = ""
    var time: String = ""
    var unread_cnt: Int = 0
    var message: String = ""
    
    var start_point:String = ""
    var end_point: String = ""
    
    var hash_key: String = ""
    var thread_hash: String = ""
    
    var sender_id: Int = 0
    var thread_id: Int = 0
    
    var created_date: String = ""
    
    var last_message_date: String = ""
    
    var gender = 1
    
    
    init() { }
}
