//
//  MessageModel.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation

class MessageModel {
    
    var message: String = ""
    var time: String = ""
    
    var fromMe: Bool = false
    
    init() { }
    
    func setFromMe() {
        fromMe = true
    }
}
