//
//  UserModel.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/25/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit


public class UserInfo {
    
    var user_id: Int = -1
    
    // user personal info (profile)
    var firstName: String! = nil
    var lastName: String! = nil
    var email: String! = nil
    var user_pic: String! = nil
    var hash_key: String! = nil
    var name: String = ""
    
    var city: String = ""
    var province: String = ""
    var country: String = ""
    
    var rating: Float = 0.0
    var offlineID: String = "" // license verification (/users/lic_verify)
    
    var phoneNumber: String = ""
    
    var occupation: String = ""
    var language: [String] = ["", ""]
    
    var smoker: Int = 0 // 0 = no smoker
    var facebook_url = ""
    var twitter_url = ""
    
    var liftOffered: Int = 0
    var totalReviews: Int = 0
    
    var recentTrips: [Trip] = []
    var vehicle: [Vehicle] = []    
    
    var extra_email: [String] = []
    var extra_phone: [String] = []
    var addressLine: [String] = ["", ""]
    var shortBio = ""
    var birthday = ""
    var marital_status: Int = 0 // single
    var gender: Int = 0         // 1 - male, 2 - female
    var shareMobile : Int = 1   // share phone number
    var shareEmail: Int = 1     // share email
    
    var paymentMethod: Int = 1  // stripe
    var memberSince: String = ""
    var expireDate: String = ""
    var expireRemaining: String = ""
    
    // authentication
    var id_expiry_date: String = ""
    var id_proof_method: Int = 0
    
    var prefs_chat: Int = 0
    var prefs_music: Int = 0
    var prefs_pets: Int = 0
    var prefs_smoking: Int = 0
    var prefs_food: Int = 0
    
    var prefs_ladies: Int = 0
    var prefs_kid: Int = 0
    var prefs_handicapped: Int = 0
    
    var email_verified:Bool = false
    var phone_verified:Bool = false
    var admin_approved:Bool = false
    
    var membership_verified: Bool = false
    
    init() { }    
    
    class func load() -> UserInfo! {
    
        let userInfo: UserInfo! = UserInfo()
        let prefs = NSUserDefaults.standardUserDefaults()
        
        userInfo.firstName = prefs.objectForKey("USER_FIRST_NAME") as! String!
        userInfo.lastName = prefs.objectForKey("USER_LAST_NAME") as! String!
        userInfo.email = prefs.objectForKey("USER_EMAIL") as! String!
        userInfo.user_pic = prefs.objectForKey("USER_PIC") as! String!
        userInfo.hash_key = prefs.objectForKey("USER_HASH") as! String!
        userInfo.gender = prefs.objectForKey("USER_GENDER") as! Int!
        userInfo.email_verified = prefs.objectForKey("EMAIL_VERIFIED") as! Bool!
        userInfo.phone_verified = prefs.objectForKey("PHONE_VERIFIED") as! Bool!
        userInfo.admin_approved = prefs.objectForKey("ADMIN_APPROVED") as! Bool!
        
        if(userInfo.firstName == nil && userInfo.lastName == nil) {
            userInfo.name = ""
        } else if (userInfo.firstName == nil) {
            
            userInfo.name = userInfo.lastName
        } else if (userInfo.lastName == nil) {
            userInfo.name = userInfo.firstName
        } else {
            userInfo.name = userInfo.firstName + " " + userInfo.lastName
        }
        
        return userInfo
    }
    
    func save() {
        
        let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        prefs.setObject(firstName, forKey: "USER_FIRST_NAME")
        prefs.setObject(lastName, forKey: "USER_LAST_NAME")
        prefs.setObject(email, forKey: "USER_EMAIL")
        
        
        prefs.synchronize()
    }
    
    func saveUserPic() {
        let prefs: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        prefs.setObject(user_pic, forKey: "USER_PIC")
        prefs.synchronize()
    }
    
    func savePreferences() {
        
        let prefs = NSUserDefaults.standardUserDefaults()
        
        prefs.setObject(prefs_chat, forKey: "PREFS_CHAT")
        prefs.setObject(prefs_music, forKey: "PREFS_MUSIC")
        prefs.setObject(prefs_pets, forKey: "PREFS_PETS")
        prefs.setObject(prefs_smoking, forKey: "PREFS_SMOKING")
        prefs.setObject(prefs_food, forKey: "PREFS_FOOD")
        
        prefs.setObject(prefs_ladies, forKey: "PREFS_LADIES")
        prefs.setObject(prefs_kid, forKey: "PREFS_KID")
        prefs.setObject(prefs_handicapped, forKey: "PREFS_HANDICAPPED")
        
        prefs.synchronize()
    }
    
    func loadPreferences() {
     
        let prefs = NSUserDefaults.standardUserDefaults()
        
        prefs_chat = prefs.integerForKey("PREFS_CHAT")
        prefs_music = prefs.integerForKey("PREFS_MUSIC")
        prefs_pets = prefs.integerForKey("PREFS_PETS")
        prefs_smoking = prefs.integerForKey("PREFS_SMOKING")
        prefs_food = prefs.integerForKey("PREFS_FOOD")
        
        prefs_ladies = prefs.integerForKey("PREFS_LADIES")
        prefs_kid = prefs.integerForKey("PREFS_KID")
        prefs_handicapped = prefs.integerForKey("PREFS_HANDICAPPED")
    }
    
    func saveDefaultPrefs() {
        
        let prefs = NSUserDefaults.standardUserDefaults()
        
        prefs.registerDefaults(["PREFS_CHAT" : 0])
        prefs.registerDefaults(["PREFS_MUSIC" : 0])
        prefs.registerDefaults(["PREFS_PETS" : 0])
        prefs.registerDefaults(["PREFS_SMOKING" : 0])
        prefs.registerDefaults(["PREFS_FOOD" : 0])
        
        prefs.registerDefaults(["PREFS_LADIES" : 0])
        prefs.registerDefaults(["PREFS_KID" : 0])
        prefs.registerDefaults(["PREFS_HANDICAPPED" : 0])
    }
}


