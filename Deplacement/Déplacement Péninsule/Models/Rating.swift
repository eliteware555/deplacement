//
//  Rating.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/15/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation

class Rating {
    
    var name = ""
    var vote: Float = 0.0
    var voted_date = ""
    var comment = ""
    var user_pic = ""
    var gender = 1
    
    init() { }
}
