//
//  DownStateButton.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair on 05/01/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class DownStateButton : UIButton {
    
    var myAlternateButton:Array<DownStateButton>?
    
    var downStateImage:String? = "radio_button_on.png"{
        didSet{
            if downStateImage != nil {
                self.setBackgroundImage(UIImage(named: downStateImage!), forState: UIControlState.Selected)
            }
        }
    }
    
    func unselectAlternateButtons(){
        if myAlternateButton != nil {
            self.selected = true
            for aButton:DownStateButton in myAlternateButton! {
                aButton.selected = false
            }
        }else{
            toggleButton()
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, withEvent: event)
    }
    
    func toggleButton(){
        if self.selected==false{
            self.selected = true
        }else {
            self.selected = false
        }
    }
}
