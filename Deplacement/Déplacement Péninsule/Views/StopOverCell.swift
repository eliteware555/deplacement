//
//  StopOverCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/20/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class StopOverCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var txtStopLocation: AutoCompleteTextField!
//    @IBOutlet weak var btnDelete: UIButton!
    
    private var responseData: NSMutableData?
    private var connection: NSURLConnection?
    private let googleMapsKey = "AIzaSyDg2tlPcoqxx2Q2rfjhsAKS-9j0n3JA_a4"
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    
    var bAutoCompleted = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configureTextField()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // initialize AutoCompleteTextField
    private func configureTextField() {
        
        txtStopLocation.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        txtStopLocation.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        txtStopLocation.autoCompleteCellHeight = 35.0
        txtStopLocation.maximumAutoCompleteCount = 20
        txtStopLocation.hidesWhenSelected = true
        txtStopLocation.hidesWhenEmpty = true
        txtStopLocation.clearButtonMode = .Never
        txtStopLocation.enableAttributedText = true
        txtStopLocation.text = ""
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        txtStopLocation.autoCompleteAttributes = attributes
        
                
        txtStopLocation.placeholder = "Enter a Location"
        
        txtStopLocation.onTextChange = {text in
            
            self.bAutoCompleted = false
            
            if !text.isEmpty {
                
                if self.connection != nil {
                    self.connection!.cancel()
                    self.connection = nil
                }
                let urlString = "\(self.baseURLString)?key=\(self.googleMapsKey)&input=\(text)"
                let url = NSURL(string: (urlString as NSString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
                if url != nil{
                    let urlRequest = NSURLRequest(URL: url!)
                    self.connection = NSURLConnection(request: urlRequest, delegate: self)                   
                }
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if(!bAutoCompleted) {
            textField.text = ""
        }
    }
    
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        responseData = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        responseData?.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        
        if let data = responseData {
            do {
                let result = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
                
                if let status = result["status"] as? String{
                    if status == "OK"{
                        if let predictions = result["predictions"] as? NSArray{
                            var locations = [String]()
                            for dict in predictions as! [NSDictionary]{
                                locations.append(dict["description"] as! String)
                            }
                            
                            self.txtStopLocation.autoCompleteStrings = locations
                            
                            return
                        }
                    }
                }
                
                self.txtStopLocation.autoCompleteStrings = nil
                
            }
            catch let error as NSError {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        print("Error: \(error.localizedDescription)")
    }

}
