//
//  PendingRatingCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/7/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class PendingRatingCell: UITableViewCell, FloatRatingViewDelegate {
    
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var txvRating: UITextView!
//    @IBOutlet weak var btnSend: UIButton!
//    @IBOutlet weak var btnCancel: UIButton!
    
    var _passenger = Passenger()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        txvRating.placeholder = "Type your comments here."
        
        imvProfile.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        self.viewRating.emptyImage = UIImage(named: "star2.png")
        self.viewRating.fullImage = UIImage(named: "star1.png")
        // Optional params
        self.viewRating.delegate = self
        self.viewRating.contentMode = UIViewContentMode.ScaleAspectFit
        self.viewRating.maxRating = 5
        self.viewRating.minRating = 1
        self.viewRating.rating = 0
        self.viewRating.editable = true
        self.viewRating.halfRatings = false
        self.viewRating.floatRatings = false
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // -------------------------------------
    // MARK: FloatRatingViewDelegate
    // -------------------------------------
    
    func floatRatingView(ratingView: FloatRatingView, isUpdating rating:Float) {
        //        self.liveLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
    }
    
    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {
        
        print("current rating value : \(rating)")
        //        self.updatedLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
        self._passenger.ratedValue = rating
    }
    
    func setPassenger(passenger: Passenger) {
        
        self._passenger = passenger
        
        setProfileImage(passenger.profile_pic)
        setUserName(passenger.first_name + " " + passenger.last_name)
    }
    
    func setProfileImage(user_pic: String) {
        
        imvProfile.setImageWithUrl(NSURL(string: user_pic)!, placeHolderImage: UIImage(named: _passenger.gender == 1 ? "male_profile" : "female_profile"))
    }
    
    func setUserName(name: String) {
        
        lblName.text = name
    }
}
