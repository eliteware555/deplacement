//
//  ReceiveCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class ReceiveCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblRevTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setMessage(message: MessageModel) {
        
        lblMessage.text = message.message.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
//        let dateFormatter = NSDateFormatter()
        
//        let created_date = NSDate(timeIntervalSince1970: NSTimeInterval(Double(Int(message.time)!)))
        
//        dateFormatter.dateFormat = "hh:mm a"
//        lblRevTime.text = dateFormatter.stringFromDate(message.time)
        
        lblRevTime.text = message.time
    }
}
