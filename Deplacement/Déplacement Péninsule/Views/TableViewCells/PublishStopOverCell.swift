//
//  PublishStopOverCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/23/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class PublishStopOverCell: UITableViewCell {
    
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
