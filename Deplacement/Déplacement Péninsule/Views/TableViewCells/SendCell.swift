//
//  SendCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class SendCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblSndTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setMessage(message: MessageModel) {
    
        lblMessage.text = message.message
        lblSndTime.text = message.time
    }

}
