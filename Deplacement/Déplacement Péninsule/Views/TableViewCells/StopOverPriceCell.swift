//
//  PriceStopOverCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/22/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class StopOverPriceCell: UITableViewCell {
    
    @IBOutlet weak var imvPointMarker: UIImageView!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var ctrlView: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var ctrlMinus: UIButton!
    @IBOutlet weak var ctrlPlus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setPointMarker(imageName: String!) {
        
        imvPointMarker.image = UIImage(named:imageName)
    }
    
    func hideControl(isVisible: Bool) {
        ctrlView.hidden = isVisible
    }    

}
