//
//  MessageListCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class MessageListCell: UITableViewCell {
    
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblFromTo: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblUnreadCnt: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var viewUnreadCnt: UIView!
    
    var prefs = NSUserDefaults.standardUserDefaults()
    var defLang = "fr"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .None
        
        imvProfile.layer.borderColor = UIColor(red: 226/255.0, green: 226/255, blue: 226/255.0, alpha: 1.0).CGColor
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setMessage(model: MessageListModel) {
        
        defLang = prefs.stringForKey("LANGUAGE")!
        
        imvProfile.setImageWithUrl(NSURL(string: model.pic_url)!, placeHolderImage: UIImage(named: model.gender == 1 ? "male_profile" : "female_profile"))
        
        lblName.text = model.name
        lblSubject.text = model.role
        lblFromTo.text = model.from_to
        lblTime.text = model.last_message_date
        lblUnreadCnt.text = String(model.unread_cnt)
        lblCreateDate.text = model.created_date
        lblContent.text = model.message
        
        setUnreadMessage(model.unread_cnt)
    }
    
    func setUnreadMessage(unread_cnt: Int) {
        
        if(unread_cnt == 0) {
            setUnreadMessageVisible(false)
        } else {
            setUnreadMessageVisible(true)
        }
    }
    
    func setUnreadMessageVisible(isVisible: Bool) {
        
        viewUnreadCnt.hidden = !isVisible
        lblUnreadCnt.hidden = !isVisible
    }
}
