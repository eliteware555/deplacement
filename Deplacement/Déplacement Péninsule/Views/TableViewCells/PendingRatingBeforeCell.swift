//
//  PendingRatingBeforeCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/7/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class PendingRatingBeforeCell: UITableViewCell {
    
    @IBOutlet weak var lblFromTo: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var viewProfilePic: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPendingRating(ratings: Int) {
        
        let hLayout = HorizontalLayout(height: self.viewProfilePic.frame.height)
        viewProfilePic.addSubview(hLayout)
     
        for var index = 0; index < ratings; index++ {
            
            let imageView = UIImageView(frame: CGRectMake(5, 5, 40, 40))
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = 20.0
            imageView.layer.borderWidth = 2.0
            imageView.layer.borderColor = UIColor.lightGrayColor().CGColor
            
            imageView.image = UIImage(named: "none_profile.png")
            
            hLayout.addSubview(imageView)
        }
    }
    
    func setPendingRating(rating: PendingRating) {
        
        let hLayout = HorizontalLayout(height: self.viewProfilePic.frame.height)
        viewProfilePic.addSubview(hLayout)
        
        for var index = 0; index < rating.passengers.count; index++ {
            
            let imageView = UIImageView(frame: CGRectMake(5, 5, 40, 40))
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = 20.0
            imageView.layer.borderWidth = 2.0
            imageView.layer.borderColor = UIColor.lightGrayColor().CGColor
            
            let _gender = rating.passengers[index].gender
            imageView.setImageWithUrl(NSURL(string: rating.passengers[index].profile_pic)!, placeHolderImage: UIImage(named: _gender == 1 ? "male_profile" : "female_profile"))
            
            hLayout.addSubview(imageView)
        }
        
        if(rating.start_point != "" && rating.end_point != "") {
            lblFromTo.text = rating.start_point + " -> " + rating.end_point
        }
        
        if(rating.date_time != "") {
            
            let blackBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 85/255.0, green: 85/255.0,
                blue: 85/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 12.0)!]
            
            let attributedDate = NSMutableAttributedString(string: rating.date_time)
            
            attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(0, 2))
            attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(12, 8))
            
            lblDateTime.attributedText = attributedDate
        }
    }
}
