//
//  GivenRatingCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 3/6/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class GivenRatingCell: UITableViewCell {
    
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRevDate: UILabel!
    @IBOutlet weak var lblReviews: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imvProfile.layer.borderColor = UIColor(red: 226/255.0, green: 226/255, blue: 226/255.0, alpha: 1.0).CGColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setRating(value: Float) {
        
        self.ratingView.rating = value
    }
    
    func setName(value: String) {
        
        self.lblName.text = value
    }
    
    func setVotedDate(value: String) {
        
        let blackBoldAttributes = [NSForegroundColorAttributeName: UIColor(red: 85/255.0, green: 85/255.0,
            blue: 85/255.0, alpha: 1.0), NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 12.0)!]
        
        let attributedDate = NSMutableAttributedString(string: value)
        
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(0, 2))
        attributedDate.addAttributes(blackBoldAttributes, range: NSMakeRange(12, 8))
      
        self.lblRevDate.attributedText = attributedDate
    }
    
    func setReviews(value: String) {
        self.lblReviews.text = value
    }
    
    func setUserProfilePic(value: String) {

        imvProfile.setImageWithUrl(NSURL(string: value)!, placeHolderImage: UIImage(named: "none_profile.png"))
    }
    
    func setRatingModel(rating: Rating) {

        imvProfile.setImageWithUrl(NSURL(string: rating.user_pic)!, placeHolderImage: UIImage(named: rating.gender == 0 ? "male_profile" : "female_profile"))
        
        setName(rating.name)
        setRating(rating.vote)
        setVotedDate(rating.voted_date)
        setReviews(rating.comment)
    }
}
