//
//  AddPhoneEmailCell.swift
//  Déplacement Péninsule
//
//  Created by victory on 2/28/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import UIKit

class AddPhoneEmailCell: UITableViewCell {
    
    @IBOutlet weak var txtPhoneorEmail: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imvAdd: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
