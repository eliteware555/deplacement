//
//  AppDelegate.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair on 13/10/15.
//  Copyright © 2015 MaaS Pros. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import CoreLocation

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var comefromSetting: Bool = false
    
    var bLoadFromData: Bool = true
    
    var bOfferLift = false
    
    ////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Global data
    ////////////////////////////////////////////////////////////////////////////////////
    
    var me: UserInfo!
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyClVU98VFlEenrt7pdFGmeqBW4w9dd5vH0")
        //Stripe.setDefaultPublishableKey("pk_test_EELCGN5g7Pb9tzxCF2dQ57rz")
//        Stripe.setDefaultPublishableKey("pk_live_krZIbaG2MqYHKvhM5pHntWbc")
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationWillResignActive(application: UIApplication) {
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
    }
    
    func applicationWillTerminate(application: UIApplication) {
        self.saveContext()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let snslogin = defaults.stringForKey("SocialLogin") {
            switch snslogin{
            case "facebook":
                return FBSDKApplicationDelegate.sharedInstance().application(application,openURL: url, sourceApplication: sourceApplication, annotation: annotation)
            case "google":
                return GPPURLHandler.handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
            default:
                return true
            }
        }
        
        return true
    }
    func application(application: UIApplication,
                                handleActionWithIdentifier identifier: String?,
                                                           for notification: UILocalNotification,
                                                               completionHandler: () -> Void)
    {
        if identifier == "declineAction"
        {
            
        }
        else if identifier == "answerAction"
        {
            
        }
    }
    
    // MARK: - Core Data stack    
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = NSBundle.mainBundle().URLForResource("DeplacementPeninsule", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("DeplacementPeninsule.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            if let underlyingError = error as? NSError {
                dict[NSUnderlyingErrorKey] = underlyingError
            }
//            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}




