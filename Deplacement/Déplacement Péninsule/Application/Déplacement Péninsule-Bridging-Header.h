//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import "BFPaperCheckbox.h"
#ifndef swiftsignin_Bridging_h
#define swiftsignin_Bridging_h

#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "SWRevealViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import "UITextView+Placeholder.h"

#import "DAKeyboardControl.h"
#import "Inputbar.h"
#import "M13BadgeView.h"
#import "UIImage+ResizeMagick.h"
//#import <Stripe/Stripe.h>

#endif