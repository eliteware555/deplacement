//
//  DateUtils.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair on 08/01/16.
//  Copyright © 2016 MaaS Pros. All rights reserved.
//

import Foundation
struct DateUtils {
    static func convertUnix(time: Double) -> String {
        let date = NSDate(timeIntervalSince1970: time)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy h:mm a"
        return dateFormatter.stringFromDate(date)
    }
}