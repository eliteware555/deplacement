//
//  DeviceUtils.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair.
//  Copyright © 2015 MaaS Pros. All rights reserved.
//

import UIKit

struct DeviceSize {
    static func bounds()->CGRect {
        return UIScreen.mainScreen().bounds
    }
    
    static func screenWidth()->Int {
        return Int(UIScreen.mainScreen().bounds.size.width)
    }
    
    static func screenHeight()->Int {
        return Int(UIScreen.mainScreen().bounds.size.height)
    }
    
    static func isIphone4OrLess()->Bool {
        let IS_IPHONE = UIDevice.currentDevice().userInterfaceIdiom == .Phone
        let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
        let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
        let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)
        let IS_IPHONE_4_OR_LESS = (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
        if(IS_IPHONE_4_OR_LESS) {
            return true
        } else {
            return false
        }
    }
}