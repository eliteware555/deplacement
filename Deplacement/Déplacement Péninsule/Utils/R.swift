//
//  Strings.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair.
//  Copyright © 2015 MaaS Pros. All rights reserved.
//

import Foundation

let kBaseURL = "http://api-covoiturage.deplacementpeninsule.ca/api/"
//let kBaseURL = "http://rsa-api.myprodesigns.net/api/"

struct R {
    
    static let tDuration = 4.0
    
    struct en {
        //intro screen
        static let intro1 = "Share the road with verified and trustworthy community members."
        static let intro2 = "Stop traveling alone! Save money by taking your passengers during long car trips."
        static let intro3 = "Profiles with audited notes create a community worthy of trust."
        static let intro4 = "Book your seat and travel easily from one city to another for less, even at the last minute!"
        static let start = "Let's Start"
        
        //login screen
        static let email_address = "Email Address"
        static let password = "Password"
        static let login = "Log In"
        static let signup = "Sign Up"
        static let account = "Don't have an account?"
        static let remember_me = "Remember me?"
        static let or = "Or"
        static let forgot_password = "Forgot password?"
        static let email_required = "Email Address is required."
        static let pass_required = "Password is required."
        
        //register screen
        static let first_name = "First Name"
        static let last_name = "Last Name"
        static let email = "Email"
        static let join = "Join"
        static let member = "Already a member?"
        static let gender = "Gender"
        static let first_required = "First Name is required."
        static let last_required = "Last Name is required."
        static let mail_required = "Email is required."
        static let gender_required = "Gender field is required."
        static let valid_email = "Email must contain a valid email address."
        static let pass_characters = "Password must be at least 8 characters in length."
        
        //forgot password screen
        static let forgot_password_title = "Forgot Password"
        static let reset_password = "Reset Password"
        
        //common
        static let submit = "Submit"
        static let error_connecting = "Error connecting to server. Please try again later."
        static let permission_grant = "Please grant the necessary permission and try again."
        static let g_failed = "Google+ login failed. Please try again."
        static let f_failed = "Facebook login failed. Please try again."
        static let g_failed_person = "Google+ login failed. Person information not available. Please try again."
        
        //side menu
        static let row1 = "Find a Lift"
        static let row2 = "Offer a Lift"
        static let row3 = "My Account"
        static let row4 = "My Lift"
        static let row5 = "Ratings"
        static let row6 = "Settings"
        static let row7 = "Log Out"
        
        //find lift
        static let by_time = "By Time"
        static let sort_by = "Sort By"
        static let from_loc = "From (postal code, street or town)"
        static let to_loc = "To (postal code, street or town)"
        static let from = "From"
        static let to = "To"
        static let proximity = "Proximity"
        static let time = "Time"
        static let price = "Price"
        static let freq = "Frequency"
        static let one_off = "One-Off "
        static let regular = "Regular"
        static let all = "All"
        static let invalid_search = "Invalid search parameters. Please try again."
        
        //ride list
        static let available_seats = "Available Seats:"
        static let no_rides = "Your search did not return any results"
        static let carpooling = "Carpools"
        
        //ride detail
        static let trip = "Trip"
        static let depature = "Departure:"
        static let eta = "E.T.A:"
        static let seats = "Seats Available:"
        static let luggage_size = "Luggage Size:"
        static let time_flexibility = "Time Flexibility:"
        static let detour = "Detour :"
        static let stopovers = "Stopover(s):"
        static let phone_number = "Phone Number:"
        static let offline_id = "Offline ID : "
        static let vehicle_info = "Vehicle Info"
        static let ac = "Air Conditioning:"
        static let reg_number = "Number Plate:"
        static let activity = "Activity:"
        static let lifts_offered = "Lifts Offered : "
        static let total_reviews = "Total Reviews : "
        static let suspend_account = "Suspend Account"
        
        // offer a lift
        static let select_your_route = "Select Your Route"
        static let add_stopover = "+ Add Stopover"
        static let add_stopover_description = "Additional places where you are willing to pick people up or drop then off.(Optional but will likely get you more passengers)"
        static let frequency = "Frequency"
        static let date_time = "Date & Time"
        static let despature_date = "Departure Date"
        static let despature_time = "Departure Time"
        static let flexibility = "Flexibility"
        static let route = "Route"
        static let Depature = "Departure"
        static let destination = "Destination"
        static let return_toggle = "Return"
        static let return_date = "Return Date"
        static let return_time = "Return Time"
        static let save_continue = "SAVE & CONTINUE"
        static let regular_departure_days = "Departure Days"
        static let duration = "Duration: "
        static let hour = "hours"
        static let minutes = "minutes"
        static let Distance = "Distance"
        static let kilometre = "kilometre"
        static let days: [String] = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        static let regular_beginning_from = "Begining From"
        static let regular_end_to = "Until"
        // pricing
        static let price_vehicle_info = "Price & Vehicle Information"
        static let price_per_seat = "Price per seat"
        static let price_description = "Price is based on one seat, one way only"
        static let price_per_route = "Price per route"
        static let suggested_price = "Suggested Price"
        static let our_price_suggestions = "Our price suggestions \n(Automatically calculated)"
//        static let automatically_calculated = "(Automatically calculated)"
        static let our_price_description = "Each price suggestion is the average price for that route. You can adjust the prices for individual route sections"
        static let stopoevers = "Stopover(s)"
        static let btn_save = "Save"
        static let btn_cancel = "Cancel"
        static let tile_lift_detail = "Further Lift Details"
        static let your_car = "Your car"
        static let select_vehicle = "Select a vehicle"
        static let seat_available = "Seat Available"
        static let luggage_space = "Luggage Space"
        static let luggage_small = "Small"
        static let luggage_medium = "Medium"
        static let luggage_large = "Large"
        static let booking_method = "How will you confirm booking from passengers?"
        static let book_manually = "Manually [in less than 12 hours]"
        static let book_automatically = "Automatically"
        static let preferences = "Preferences"
        static let title_detour = "I can make detour"
        static let detour_none = "None"
        static let detour_15mins = "Maximum 15 minutes"
        static let detour_30mins = "Maximum 30 minutes"
        static let detour_any = "Any detour"
        static let title_additional_info = "Additional Information"
        static let additional_info_holder = "Provide additional information about your trip. More details about the meeting points, what type of music you like, who you are travelling or any other information that your passenggers should know."
        
        // add a car
        static let add_new_vehicle = "ADD NEW VEHICLE"
        static let year = "Year"
        static let make = "Make"
        static let model = "Model"
        static let colour = "Colour"
        static let distance_per_year = "Distance Driven per year in km"
        static let transmission = "Transmission"
        static let mileage = "Mileage"
        static let automatic = "Automatic"
        static let manual = "Manual"
        static let city = "City"
        static let highway = "Highway"
        static let fuel_efficiency = "Fuel Efficiency in L/100Km"
        static let co2_output = "CO2 Output in Kg"
        static let type_vehicle = "Type of Vehicle"
//        static let luggage_size = "Luggage Size"
        static let type_tires = "Type of Tires"
        static let passenger_seats = "Passenger Seats"
        static let license_plate = "License Plate"
        static let gnbb = "GNBB"
        static let additional_features = "Additional Features"
        static let air_conditioning = "Air Conditioning"
        static let bike_rack = "Bike Rack"
        static let ski_rack = "Ski Rack"
        static let comfort_level = "Comfort Level"
        static let comfort_basic = "Basic"
        static let comfort_normal = "Normal"
        static let comfort_comfortable = "Comfortable"
        static let comfort_luxury = "Luxury"
        
        static let vehicle_delete_title = "Vehicle Delete"
        static let vehicle_delete_alert = "Are you sure you want to delete this vehicle?"
        static let alert_ok = "OK"
        static let alert_cancel = "Cancel"
        
        // offer a ride - step3
        static let confirm_trip = "Confirm Your Trip"
        static let details_correct = "Are these details correct?"
        static let something_wrong = "If something wrong, use the navigation bar on top to go back."
        
        static let person = "person"
        
        static let stopover = "Stopover :"
        static let vehicle = "Vehicle :"
        static let confirmation = "Confirmation :"
        
        static let publish_lift = "PUBLISH LIFT"
        
        // my account right menu
        static let profile = "Profile"
        static let profile_vehicle_info = "Vehicle Info"
        static let profile_recent_lift = "Recent Lifts"
        static let profile_reviews_ratings = "Reviews and Ratings"
        static let profile_message = "Message"
        
        // setting right menu
        static let view_public_profile = "View My Pulbic Profile"
        static let personal_info = "Personal Info"
        static let change_password = "Change Password"
        static let membership = "Membership"
        static let authentifications = "Authentifications"
        static let vehicle_settings  = "Vehicle Settings"
        //        static let preferences = "Préférences"
        
        // rating
        static let rating_pending = "Ratings Pending"
        static let rating_received = "Received Ratings"
        static let rating_given = "Ratings Given"
        
        static let languages = "Languages"
        static let smoker = "Smoker"
        static let share = "Share"
//        static let activity = "Activity"
        static let lift_offered = "Lift Offered : "
        static let total_review = "Total Reviews : "
        static let suspend_account_description = "If you would like to delete your Déplacement Péninsule Ride Sharing profile, please contact info@deplacementpeninsule.ca with your Full Name and Email Address and indicate that you would like us to delete your account."
        
        static let yes = "Yes"
        static let no = "No"
        
        
        // setting page
        // - 1 Personal Information
        static let setting_phone_number = "Phone Number"
        static let addrees_line = "Address Line"
        static let province = "Province"
        static let country = "Country"
        static let short_bio = "Short Bio"
        
        static let additional_information = "Additional Information"
        static let phone_description = "This number will treated as your primary phone number of contact for Phone Verification"
        static let email_description = "This ID will treated as your primary email for Email Verification"
        static let dob = "DOB"
        static let occupation = "Occupation"
        static let english = "English"
        static let french = "French"
        static let language = "Language"
        static let marital_status = "Marital Status"
        static let single = "Single"
        static let married = "Married"
        static let male = "Male"
        static let female = "Female"
        static let share_information = "Share the following information with users that booked Lift with me:"
        static let share_mobile_phone = "Mobile Phone Number"
        static let share_email_address = "Email Address"
        
        // change password
        static let current_password = "Current Password"
        static let new_password = "New Password"
        static let confirm_password = "Confirm Password"
    
        // membership page
        static let lift_with_us = "Lift with Us!"
        static let book_your_seat = "Book your seats and publish your lift easy!"
        static let current_membership_status = "Current Membership Status"
        static let member_since = "Member since "
        static let payment_through = "Payment through "
        static let expires_on = "Expires on "
        static let days_remaining = " days remaining"
        
        static let btn_renew = "Renew"
        
        // authentification page
        static let authentification_info = "We verify all users to try and safeguard all passengers and drivers. Help us in our goal to try and make it more secure for everyone."
        static let email_verification = "Email verification"
        static let mobile_verification = "Mobile Verification"
        static let change_email = "Change Email"
        static let change_number = "Change Number"
        static let id_details = "ID Details"
        static let form = "From: "
        static let expiry_date = "Expiry Date: "
        
        // Vehicle Setting Page
        static let vehicle_details = "Your Vehicle Details"
        static let air_conditions = "Air Conditions"
        static let setting_co2_output = "CO2 Output"
        static let setting_fuel_efficiency = "Fuel Efficiency"
        static let setting_distance_driven = "Distance Driven per Year"
        static let setting_seats_passenger = "No. of Passenger Seats"
        static let setting_picture = "Picture"
        
        // Preferences
        static let set_your_preferences = "Set Your Preferences"
        static let prefs_chat = "Chat"
        static let prefs_music = "Music"
        static let prefs_pets = "Pets"
        static let prefs_smoking = "Smoking"
        static let prefs_food = "Food"
        static let despends_on_conditions = "Depends on conditions"
        static let other_preferences = "Other Preferences"
        static let prefer_ladies_only = "Prefer Ladies Only Lift"
        static let allow_lift_with_kid = "Allow Lift with Kid"
        static let ready_handicapped_person = "Ready to Offer Lift to a Physically Handicapped Person"
        
        // book a ride
        static let book_my_seat = "Book My Seat"
        static let contact_driver = "Contact the Drivier"
        static let contact_passenger = "Contact Passengers"
        
        static let btn_book_now = "Book Now"
        static let book_seats_available = "Seats Available"
        static let book_price_per_person = "Price per person"
        static let book_total = "Total : $"
        
        static let send_message_to = "SEND MESSAGE_TO "
        static let btn_send = "Send"
        
        
        static let country_name = "Canada"
        static let province_name = "New Brunswick"
        
        static let id_verified = "Verfied"
        static let id_none_verified = "No Verified"
        
        
        // --------------------------------------------
        //  MARK: Alert String
        // --------------------------------------------
        
        static let invalid_departure  =  "Please select a valid departure"
        static let invalid_destination = "Please select a valide destination"
        // oneoff lift
        static let require_departure_date = "Departure date is required."
        static let require_return_date = "Return date is required."
        // regular lift
        static let require_departure_days = "Departure days is required."
        static let require_return_days = "Return days is required."
        static let require_initial_date = "Initial date is required."
        static let require_final_date = "Final date is required."
        static let require_name = "Name is required."
        static let require_phone_number = "PhoneNumber is required."
        static let require_email = "Email is required."
        static let require_city = "City name is required."
        static let require_birthday = "Birthday is required."
        static let require_language = "Please select a language."
        
        static let require_year = "Year is required."
        static let require_make = "Make is required."
        static let require_model = "Model is required."
        static let require_distance_driven = "Distance Driven per year in km is required."
        
        static let require_type_vehicle = "Type of Vehicle is required."
        static let require_seats = "Passenger Seats is required."
        static let require_luggage = "Luggage size is required."
        static let require_plat_number = "License Plate Number is required."
        
        static let require_rating_stars = "Please provide star rating for "
        static let require_rating_comments = "Please provide comment for "
        
        static let verify_your_email = "In order to access your Ride Sharing account and get started, please verify your email address."
        
        static let no_pending_ratings = "No Ratings Pending"
        static let no_given_ratings = "No Ratings Given"
        static let no_received_ratings = "No Ratings Received"
        
        static let no_message_data = "No Messages"
        
        static let no_lift = "No Lift"
        
        static let title_not_membership = "You are not a member yet."
        static let not_member_prefix = "To book your seat, "
        static let not_member_middle = "You must complete the registration process"
        static let not_member_subfix = " first."
        
        static let title_upload_license = "Upload your licence"
        static let upload_license_prefix = "In order to post this lift "
        static let upload_license_middle = "You must upload your Driving Licence"
        static let upload_license_subfix = " first and wait for admin approval."
        
        static let title_not_membership_paid = "You are not a member yet."
        static let not_member_paid_prefix = "In order to publish this lift "
        static let not_member_paid_middle = "You must complete the registration process"
        static let not_member_paid_subfix = " first. The lift is saved into your account under My Trips. You can publish it later after the verification process."
        
        static let input_current_password = "Current Password is required."
        static let input_new_password = "new Password is required."
        static let input_confirm_password = "nonfirm Password is required."
        
        // i added
        static let Error_Strip = "Please Try Again"
        static let BtnOnlineTitle = "Online"
        static let BtnOfficeTitle = "Déplacement Péninsule Office"
        static let GetMembership = "Get 1 Year Membership and Lift With Us for Just $10!"
        static let MemberStatus = "You are not a member yet."
         static let MemberNotDetail = "Please use any of the following option for both Home Transport&Carpooling and become a memeber soon!"
         static let adressDetail = "you can send your payment by mail or in person at following address: "
         static let officeAddress1 = "22 boul. Is St-Pierre"
    }
    
    struct fr {
        
        //intro screen
        static let intro1 = "Partagez la route avec une communauté de confiance et des membres vérifiés."
        static let intro2 = "Arrêtez de voyager seul! Économisez de l'argent en prenant des passagers pendant vos longs trajets en voiture."
        static let intro3 = "Des profils vérifiés et évalués créent une communauté de confiance."
        static let intro4 = "Réserver facilement votre siège et voyager d'une ville à l'autre pour beaucoup moins, même à la dernière minute!"
        static let start = "Débuter"
        
        //login screen
        static let email_address = "Adresse courriel / Nom d'utilisateur"
        static let password = "Mot de passe"
        static let login = "Connectez-vous"
        static let signup = "Inscrivez-vous"
        static let account = "Vous n'avez pas de compte?"
        static let remember_me = "Souviens-toi de moi?"
        static let or = "Ou"
        static let forgot_password = "Mot de passe oublié?"
        static let email_required = "Le champ Adresse courriel / Nom d'utilisateur est requis."
        static let pass_required = "Le champ Mot de passe est requis."
        
        //register screen
        static let first_name = "Prénom"
        static let last_name = "Nom"
        static let email = "Adresse courriel"
        static let join = "Rejoindre"
        static let member = "Déjà membre?"
        static let gender = "Sexe"
        static let first_required = "Le champ Prénom est requis."
        static let last_required = "Le champ Nom est requis."
        static let mail_required = "Le champ Adresse courriel est requis."
        static let gender_required = "Champ Sexe est nécessaire."
        static let valid_email = "Le champ Adresse courriel doit contenir une adresse email valide."
        static let pass_characters = "Le champ Mot de passe doit contenir au moins 8 caractères."
        
        //forgot password screen
        static let forgot_password_title = "Mot de passe oublié"
        static let reset_password = "Réinitialiser le mot de passe"
        
        //common
        static let submit = "Soumettre"
        static let error_connecting = "Erreur de connexion au serveur. Veuillez réessayer plus tard."
        static let permission_grant = "Veuillez octroyer les autorisations nécessaires et réessayez."
        static let g_failed = "Google+ connexion a échoué. S'il vous plaît essayer à nouveau."
        static let f_failed = "Facebook connexion a échoué. S'il vous plaît essayer à nouveau."
        static let g_failed_person = "Google+ connexion a échoué. Personne informations non disponible. S'il vous plaît essayer à nouveau."
        
        //side menu
        static let row1 = "Trouver un trajet"
        static let row2 = "Offrir un trajet"
        static let row3 = "Mon Compte"
        static let row4 = "Mes trajets"
        static let row5 = "Évaluations"
        static let row6 = "Paramètres"
        static let row7 = "Se déconnecter"
        
        //find lift
        static let by_time = "Par temps"
        static let sort_by = "Trier par"
        static let from_loc = "De (code postal, rue ou ville)"
        static let to_loc = "À (code postal, rue ou ville)"
        static let from = "De"
        static let to = "À"
        static let proximity = "Proximité"
        static let time = "Temps"
        static let price = "Prix"
        static let freq = "Fréquence"
        static let one_off = "Ponctuel"
        static let regular = "Régulier"
        static let all = "Tous"
        static let invalid_search = "Non valide recherche paramètres. S'il vous plaît essayer à nouveau."
        
        //ride list
        static let available_seats = "Places libres:"
        static let no_rides = "Votre recherche n'a obtenu aucun résultat"
        static let carpooling = "Covoiturage"
        
        //ride detail
        static let trip = "Trajet"
        static let depature = "Départ:"
        static let eta = "Heure d’arrivée prévue:"
        static let seats = "Sièges disponible:"
        static let luggage_size = "Taille des bagages:"
        static let time_flexibility = "Flexibilité:"
        static let detour = "Détour: "
        static let stopovers = "Escales(s):"
        static let phone_number = "Numéro de téléphone:"
        static let offline_id = "ID Hors-ligne : "
        static let vehicle_info = "Information du véhicule"
        static let ac = "Air conditionné:"
        static let reg_number = "Numéro d'immatriculation:"
        static let activity = "Activité"
        static let lifts_offered = "Trajets offerts : "
        static let total_reviews = "Nombre de commentaires : "
        static let suspend_account = "Suspendre le compte"
        
        // offer a lift
        static let select_your_route = "Sélectionner votre trajet"
        static let add_stopover = "+ Ajouter un arrêt"
        static let add_stopover_description = "Endroit supplémentaires où vous êtes prêt à prendre les gens ou les déposer. (facultatif mais pourrait probablement vous obtenir plus de passagers)"
        static let frequency = "Fréquence"
        static let date_time = "Date et heure"
        static let despature_date = "Date de départ"
        static let despature_time = "Heure de départ"
        static let flexibility = "Flexibilité"
        static let route = "Trajet"
        static let Depature = "Départ"
        static let destination = "Destination"
        static let regular_departure_days = "Date de départ"
        static let return_toggle = "Retour"
        static let return_date = "Date de retour"
        static let return_time = "Heure de retour"
        static let save_continue = "Sauvegarder et continuer"
        static let duration = "Durée: "
        static let hour = "heures"
        static let minutes = "minutes"
        static let Distance = "Distance"
        static let kilometre = "kilomètre"
        static let days: [String] = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]
        static let regular_beginning_from = "Date de début"
        static let regular_end_to = "Jusqu'à"
        // pricing
        static let price_vehicle_info = "Prix et information du véhicule"
        static let price_per_seat = "Prix par siège"
        static let price_description = "Le prix est basé sur un siège et un aller seulement"
        static let pricer_per_route = "Prix par trajet"
        static let suggested_price = "Prix ​​suggéré"
        static let our_price_suggestions = "Notre prix suggéré \n(Calculé automatiquement)"
//        static let automatically_calculated = "(Automatically calculated)"
        static let our_price_description = "Chaque suggestion de prix est le prix moyen pour ce trajet. Vous pouvez ajuster les prix pour des trajets individuelles."
        static let stopoevers = "Escales(s)"
        static let btn_save = "Sauvegarder"
        static let btn_cancel = "Annuler"
        static let tile_lift_detail = "Plus de détails sur le trajet"
        static let your_car = "Votre voiture"
        static let select_vehicle = "Sélectionner un véhicule"
        static let seat_available = "Siège disponible"
        static let luggage_space = "Espace pour les bagages"
        static let luggage_small = "Petit"
        static let luggage_medium = "Moyen"
        static let luggage_large = "Grand"
        static let booking_method = "Comment allez-vous confirmer les réservations des passagers?"
        static let book_manually = "Manuellement [en moins de 12 heures]"
        static let book_automatically = "Automatiquement"
        static let preferences = "Préférences"
        static let title_detour = "Je peux faire un détour"
        static let detour_none = "Aucun"
        static let detour_15mins = "Maximum de 15 minutes"
        static let detour_30mins = "Maximum de 30 minutes"
        static let detour_any = "pas de détour"
        static let title_additional_info = "Information supplémentaire"
        static let additional_info_holder = "Fournir des informations supplémentaires à propos de votre voyage. Plus de détails sur les points de rencontre, quels types de musique que vous aimez, pourquoi vous voyagez ou toute autre information que vos passagers doivent savoir."
        
        // add a car
        static let add_new_vehicle = "Ajoutez votre véhicule"
        static let year = "Année"
        static let make = "Marque"
        static let model = "Modèle"
        static let colour = "Couleur"
        static let distance_per_year = "Distance moyenne parcourue par année en km"
        static let transmission = "Transmission"
        static let mileage = "Kilométrage"
        static let automatic = "Automatique"
        static let manual = " Manuel"
        static let city = "Ville"
        static let highway = "Autoroute"
        static let fuel_efficiency = "Efficacité de carburant en L / 100 km"
        static let co2_output = "Sortie de CO2 en kg"
        static let type_vehicle = "Type de véhicule"
        //        static let luggage_size = "Luggage Size"
        static let type_tires = "Type de pneus"
        static let passenger_seats = "Sièges passagers"
        static let license_plate = "Plaque d'immatriculation"
        static let gnbb = "GNBB"
        static let additional_features = "Caractéristiques supplémentaires"
        static let air_conditioning = "Air conditionné"
        static let bike_rack = "Porte-vélos"
        static let ski_rack = "Porte-Skis"
        static let comfort_level = "Niveau de confort"
        static let comfort_basic = "De base"
        static let comfort_normal = "Normal"
        static let comfort_comfortable = "Confortable"
        static let comfort_luxury = "Luxe"
        
        static let vehicle_delete_title = "Véhicule Effacer"
        static let vehicle_delete_alert = "Êtes-vous sûr de vouloir supprimer ce véhicule?"
        static let alert_ok = "OK"
        static let alert_cancel = "Annuler"
        
        // offer a ride - step3
        static let confirm_trip = "Confirmer votre trajet"
        static let details_correct = "Ces détails sont corrects?"
        static let something_wrong = "Si quelque chose n'est pas correct, utilisez la barre de navigation en haut pour revenir en arrière."
        
        static let person = "personne"
        
        static let stopover = "Escales :"
        static let vehicle = "Véhicule :"
        static let confirmation = "Confirmation :"
        
        static let publish_lift = "Publier le trajet"
        
        // my account right menu
        
        static let profile = "Profil"
        static let profile_vehicle_info = "Info du véhicule"
        static let profile_recent_lift = "Trajets offerts"
        static let profile_reviews_ratings = "Avis et évaluations"
        static let profile_message = "Message"
        
        // setting right menu
        static let view_public_profile = "Voir mon profil public"
        static let personal_info = "Informations personnelles"
        static let change_password = "Modifier le mot de passe"
        static let membership = "Adhésion"
        static let authentifications = "Identification"
        static let vehicle_settings  = "Paramètres de véhicules"
//        static let preferences = "Préférences"
        
        // rating
        static let rating_pending = "Évaluations en attente"
        static let rating_received = "Évaluations reçues"
        static let rating_given = "Évaluations données"
        
        static let languages = "Langue parlé"
        static let smoker = "Fumeur"
        static let share = "Partager"
        //        static let activity = "Activity"
        static let lift_offered = "Trajet(s) offert(s) : "
        static let total_review = "Avis et évaluations : "
        
        static let suspend_account_description = "Si vous souhaitez suspendre votre compte utilisateur, SVP, écrivez-nous à info@deplacementpeninsule.ca avec votre nom et votre adresse courriel et indiquer que vous souhaitez suspendre votre compte."
        
        static let yes = "Oui"
        static let no = "Non"
        
        // setting page
        // - 1 Personal Information
        static let setting_phone_number = "Numéro de téléphone"
        static let phone_description = "Ce numéro sera traité comme votre numéro de contact principal et sera utilisé dans la vérification de votre téléphone."
        static let email_description = "L'adresse courriel sera traitée comme votre adresse courriel principale et pour la vérification de votre courriel."
        static let addrees_line = "Adresse ligne"
        static let province = "Province"
        static let country = "Pays"
        static let short_bio = "Courte biographie"
        
        static let additional_information = "Informations supplémentaires"
        static let dob = "Date de Naissance"
        static let occupation = "Occupation"
        static let english = "Anglais"
        static let french = "Français"
        static let language = "Langue"
        static let marital_status = "État civil"
        static let single = "Célibataire"
        static let married = "Marié"
        static let male = "Masculin"
        static let female = "Féminin"
        static let share_information = "Partagez les informations suivantes avec les utilisateurs qui ont réservé des trajets avec moi :"
        static let share_mobile_phone = "Téléphone mobile"
        static let share_email_address = "Courriel"
        
        // change password
        static let current_password = "Mot de passe actuel"
        static let new_password = "Nouveau mot de passe"
        static let confirm_password = "Confirmez le mot de passe"
        
        // membership page
        static let lift_with_us = "Trajet avec nous!"
        static let book_your_seat = "Réserver vos sièges et publier vos trajets facilement!"
        static let current_membership_status = "Statut de membre actuel"
        static let member_since = "Statut de membre actuel "
        static let payment_through = "Paiement au  "
        static let expires_on = "Expire le"
        static let days_remaining = " jours restant"
        
        static let btn_renew = "Renouveler"
        
        // authentification page
        static let authentification_info = "Nous vérifions tous les utilisateurs pour la sécurité des passagers et des conducteurs. Aidez-nous à rendre le covoiturage sécuritaire pour tous."
        static let email_verification = "Vérification du courriel"
        static let change_email = "Modifier courriel"
        static let mobile_verification = "Vérification mobile"
        static let change_number = "Modifier numéro"
        static let id_details = "Détails de l'identification"
        static let form = "De: "
        static let expiry_date = "Date d'expiration: "
        
        // Vehicle Setting Page
        static let vehicle_details = "Détails de votre véhicule"
        static let air_conditions = "Air conditionné"
        static let setting_co2_output = "Sortie de CO2"
        static let setting_fuel_efficiency = "La consommation de carburant"
        static let setting_distance_driven = "Distance parcourus par an"
        static let setting_seats_passenger = "Nombre de sièges passagers"
        static let setting_picture = "Photo"
        
        // Preferences
        static let set_your_preferences = "Définissez vos préférences"
        static let prefs_chat = "Bavarder"
        static let prefs_music = "Musique"
        static let prefs_pets = "Animaux"
        static let prefs_smoking = "Fumeur"
        static let prefs_food = "Nourriture"
        static let despends_on_conditions = "Dépend des conditions"
        static let other_preferences = "Autres Préférences"
        static let prefer_ladies_only = "Autres Préférences"
        static let allow_lift_with_kid = "Enfants autorisés"
        static let ready_handicapped_person = "Peut accommoder une personne ayant un handicap physique"
        
        // book a ride
        static let book_my_seat = "Réservez mon siège"
        static let contact_driver = "Contacter le conducteur"
        static let contact_passenger = "Contactez passagers"
        
        static let btn_book_now = "Réservez maintenant"
        static let book_seats_available = "Sièges disponibles"
        static let book_ctrl_seats_available = "Nombre de passagers"
        static let book_price_per_person = "Prix par psersonne"
        static let book_total = "Total : $"
        
        static let send_message_to = "SEND MESSAGE_TO "
        static let btn_send = "Envoyez"
        
        static let country_name = "Canada"
        static let province_name = "Nouveau-Brunswick"
        
        static let id_verified = "Vérifié"
        static let id_none_verified = "Non Vérifié"
        
        // --------------------------------------------
        //  MARK: Alert String
        // --------------------------------------------
        static let invalid_departure  =  "Please select a valid departure"
        static let invalid_destination = "SVP, sélectionner une destination valide."
        // oneoff lift
        static let require_departure_date = "La date de départ est nécessaire."
        static let require_return_date = "La date de retour est nécessaire."
        // regular lift
        static let require_departure_days = "Le jour de départ est nécessaire."
        static let require_return_days = "Le jour de retour est nécessaire."
        static let require_initial_date = "La date initiale est nécessaire."
        static let require_final_date = "La date limite est nécessaire."
        static let require_name = "Le nom complet est nécessaire."
        static let require_phone_number = "Le numéro de téléphone est nécessaire."
        static let require_email = "Le courriel est nécessaire."
        static let require_city = "Le nom de la ville est nécessaire."
        static let require_birthday = "La date de naissance est nécessaire."
        static let require_language = "SVP, sélectionner une langue."
        
        static let require_year = "L'année est nécessaire."
        static let require_make = "La marque est nécessaire."
        static let require_model = "Le modèle est nécessaire."
        static let require_distance_driven = "La distance parcourue en km par année est nécessaire."
        
        static let require_type_vehicle = "Le type de véhicule est nécessaire."
        static let require_seats = "Les sièges passagers sont nécessaire."
        static let require_luggage = "La taille des bagages est nécessaire."
        static let require_plat_number = "Le numéro d'immatriculation est nécessaire."
        
        static let require_rating_stars = "S'il vous plaît, fournir l'évaluation pour les "
        static let require_rating_comments = "S'il vous plaît, fournir des commentaires pour les "
        
        static let verify_your_email = "Afin d'accéder et utiliser votre compte de covoiturage, s'il vous plaît, vérifier votre adresse courriel pour valider votre compte."
        
        static let no_pending_ratings = "Aucune évaluation en attente"
        static let no_given_ratings = "Aucune évaluation donnée"
        static let no_received_ratings = " Aucune évaluation reçue"
        
        static let no_message_data = "Non Messages"
        
        static let no_lift = "Aucun trajet"
        
        static let title_not_membership = "Vous n'êtes pas encore membre."
        static let not_member_prefix = "Vous n'êtes pas encore membre, "
        static let not_member_middle = "Vous devez en premier lieu compléter le processus d'inscription."
        static let not_member_subfix = ""
        
        static let title_upload_license = "Téléchargez votre permis de conduire"
        static let upload_license_prefix = "Pour publier ce trajet, "
        static let upload_license_middle = "vous devez d'abord télécharger "
        static let upload_license_subfix = " votre permis de conduire et attendre l'approbation de l'administrateur."
        
        static let title_not_membership_paid = "Vous n'êtes pas encore membre."
        static let not_member_paid_prefix = "Afin de publier ce trajet, "
        static let not_member_paid_middle = "Vous devez en premier lieu compléter le processus d'inscription. "
        static let not_member_paid_subfix = " Le trajet est enregistré dans votre compte sous Mes trajets. Vous pouvez le publier plus tard, après le processus de vérification."        
        
        static let input_current_password = "Le champ Mot de passe actuel est requis."
        static let input_new_password = "Le champ nouveau mot de passe est requis."
        static let input_confirm_password = "Le champ confirmez le mot de passe est requis."
        
        
        // i added 
        static let Error_Strip = "Veuillez réessayer"
        static let BtnOnlineTitle = "en ligne"
        static let BtnOfficeTitle = "Déplacement Bureau Péninsule"
        static let GetMembership = "Obtenez 1 an Adhésion et ascenseur avec nous pour seulement 10 $ !"
        static let MemberStatus = "Vous n'êtes pas encore membre ."
        static let MemberNotDetail = "Please Utilisez l'une des options suivantes pour la maison Transport & Covoiturage et deviens un memeber bientôt!"
       static let adressDetail = "Vous pouvez envoyer votre paiement par la poste ou en personne à l'adresse suivante"
        static let officeAddress1 = "22 boul . Est- St- Pierre"
        static let officeAddress2 = "Caraquet NB E! W ! B6"
        
       }
}









