//
//  ValidationUtils.swift
//  Déplacement Péninsule
//
//  Created by Alok Nair.
//  Copyright © 2015 MaaS Pros. All rights reserved.
//

import Foundation

struct ValidationUtils {
    static func isValidEmail(email:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
    }
    
    static func isValidPassword(password:String) -> Bool {
        return password.characters.count >= 8 ? true : false
    }
    
    static func isEmpty(input: String) -> Bool {

        let components = input.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).filter({!isEmpty($0)})
        
        let fullString =  components.joinWithSeparator("")
        
        if(fullString.characters.count == 0) {
            return true
        }

        return false
    }
}